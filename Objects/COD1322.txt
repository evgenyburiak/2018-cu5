OBJECT Codeunit 1322 Correct PstdSalesInv (Yes/No)
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    TableNo=112;
    Permissions=TableData 112=rm,
                TableData 114=rm;
    OnRun=BEGIN
            CorrectInvoice(Rec);
          END;

  }
  CODE
  {
    VAR
      CorrectPostedInvoiceQst@1000 : TextConst 'ENU=The posted sales invoice will be canceled, and a new version of the sales invoice will be created so that you can make the correction.\ \Do you want to continue?;ENG=The posted sales invoice will be cancelled, and a new version of the sales invoice will be created so that you can make the correction.\ \Do you want to continue?';

    [Internal]
    PROCEDURE CorrectInvoice@1(VAR SalesInvoiceHeader@1002 : Record 112) : Boolean;
    VAR
      SalesHeader@1001 : Record 36;
      CorrectPostedSalesInvoice@1000 : Codeunit 1303;
    BEGIN
      CorrectPostedSalesInvoice.TestCorrectInvoiceIsAllowed(SalesInvoiceHeader,FALSE);
      IF CONFIRM(CorrectPostedInvoiceQst) THEN BEGIN
        CorrectPostedSalesInvoice.CancelPostedInvoiceStartNewInvoice(SalesInvoiceHeader,SalesHeader);
        PAGE.RUN(PAGE::"Sales Invoice",SalesHeader);
        EXIT(TRUE);
      END;

      EXIT(FALSE);
    END;

    BEGIN
    END.
  }
}

