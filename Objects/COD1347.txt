OBJECT Codeunit 1347 User Task Activities Mgt.
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    OnRun=BEGIN
            IF IsActivitiesVisible THEN
              DisableActivitiesForCurrentUser
            ELSE
              EnableActivitiesForCurrentUser;
          END;

  }
  CODE
  {
    VAR
      SETUPUSERTASKACTIVITIESTxt@1000 : TextConst '@@@={Locked};ENU=USERTASKACTIVITIES;ENG=USERTASKACTIVITIES';

    LOCAL PROCEDURE EnableActivitiesForCurrentUser@9();
    VAR
      UserPreference@1000 : Record 1306;
    BEGIN
      UserPreference.EnableInstruction(GetActivitiesCode);
    END;

    LOCAL PROCEDURE DisableActivitiesForCurrentUser@4();
    VAR
      UserPreference@1000 : Record 1306;
    BEGIN
      UserPreference.DisableInstruction(GetActivitiesCode);
    END;

    [External]
    PROCEDURE IsActivitiesVisible@3() : Boolean;
    VAR
      UserPreference@1000 : Record 1306;
    BEGIN
      EXIT(NOT UserPreference.GET(USERID,GetActivitiesCode));
    END;

    LOCAL PROCEDURE GetActivitiesCode@2() : Code[20];
    BEGIN
      EXIT(SETUPUSERTASKACTIVITIESTxt);
    END;

    BEGIN
    END.
  }
}

