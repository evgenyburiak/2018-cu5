OBJECT Codeunit 1353 Generate Master Data Telemetry
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    OnRun=BEGIN
            OnMasterDataTelemetry;
          END;

  }
  CODE
  {
    VAR
      AlCompanyMasterdataCategoryTxt@1003 : TextConst '@@@=Locked;ENU=AL Company Masterdata;ENG=AL Company Masterdata';
      MasterdataTelemetryMessageTxt@1006 : TextConst '@@@=Locked;ENU=Customers: %1, Vendors: %2, Items: %3, G/L Accounts: %4, Contacts: %5;ENG=Customers: %1, Vendors: %2, Items: %3, G/L Accounts: %4, Contacts: %5';

    [EventSubscriber(Codeunit,1353,OnMasterDataTelemetry,"",Skip,Skip)]
    LOCAL PROCEDURE SendTelemetryOnMasterDataTelemetry@5();
    VAR
      Customer@1005 : Record 18;
      Vendor@1004 : Record 23;
      Item@1003 : Record 27;
      GLAccount@1002 : Record 15;
      Contact@1001 : Record 5050;
      TelemetryMsg@1000 : Text;
    BEGIN
      TelemetryMsg := STRSUBSTNO(MasterdataTelemetryMessageTxt,
          Customer.COUNT,Vendor.COUNT,Item.COUNT,GLAccount.COUNT,Contact.COUNT);
      SENDTRACETAG('000018V',AlCompanyMasterdataCategoryTxt,VERBOSITY::Normal,TelemetryMsg);
    END;

    [Integration]
    LOCAL PROCEDURE OnMasterDataTelemetry@3();
    BEGIN
    END;

    BEGIN
    END.
  }
}

