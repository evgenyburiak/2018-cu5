OBJECT Codeunit 1802 Data Migration Notifier
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      ListEmptyMsg@1000 : TextConst 'ENU=Want to import entries?;ENG=Want to import entries?';
      OpenDataMigrationTxt@1001 : TextConst 'ENU=Open Data Migration;ENG=Open Data Migration';
      DataTypeManagement@1003 : Codeunit 701;

    [EventSubscriber(Page,22,OnOpenPageEvent)]
    LOCAL PROCEDURE OnCustomerListOpen@1(VAR Rec@1000 : Record 18);
    BEGIN
      ShowListEmptyNotification(Rec);
    END;

    [EventSubscriber(Page,27,OnOpenPageEvent)]
    LOCAL PROCEDURE OnVendorListOpen@6(VAR Rec@1000 : Record 23);
    BEGIN
      ShowListEmptyNotification(Rec);
    END;

    [EventSubscriber(Page,31,OnOpenPageEvent)]
    LOCAL PROCEDURE OnItemListOpen@7(VAR Rec@1000 : Record 27);
    BEGIN
      ShowListEmptyNotification(Rec);
    END;

    LOCAL PROCEDURE ShowListEmptyNotification@3(RecordVariant@1001 : Variant);
    VAR
      ListEmptyNotification@1000 : Notification;
      RecRef@1002 : RecordRef;
    BEGIN
      DataTypeManagement.GetRecordRef(RecordVariant,RecRef);

      IF NOT RecRef.ISEMPTY THEN
        EXIT;

      ListEmptyNotification.MESSAGE := ListEmptyMsg;
      ListEmptyNotification.SCOPE := NOTIFICATIONSCOPE::LocalScope;
      ListEmptyNotification.ADDACTION(OpenDataMigrationTxt,CODEUNIT::"Data Migration Notifier",'OpenDataMigrationWizard');
      ListEmptyNotification.SEND;
    END;

    [External]
    PROCEDURE OpenDataMigrationWizard@5(ListEmptyNotification@1000 : Notification);
    BEGIN
      PAGE.RUN(PAGE::"Data Migration Wizard");
    END;

    [EventSubscriber(Table,1800,OnGetInstructions,"",Skip,Skip)]
    LOCAL PROCEDURE OnGetInstructionsSubscriber@2(VAR Sender@1000 : Record 1800;VAR Instructions@1001 : Text;VAR Handled@1002 : Boolean);
    BEGIN
      SENDTRACETAG('00001DB','AL ' + Sender.TABLENAME,VERBOSITY::Normal,Sender.Description);
    END;

    BEGIN
    END.
  }
}

