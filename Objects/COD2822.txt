OBJECT Codeunit 2822 Native - Reports
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {

    PROCEDURE PostedSalesInvoiceReportId@2() : Integer;
    VAR
      TempReportSelections@1000 : TEMPORARY Record 77;
    BEGIN
      EXIT(TempReportSelections.Usage::"S.Invoice");
    END;

    PROCEDURE DraftSalesInvoiceReportId@3() : Integer;
    VAR
      TempReportSelections@1000 : TEMPORARY Record 77;
    BEGIN
      EXIT(TempReportSelections.Usage::"S.Invoice Draft");
    END;

    PROCEDURE SalesQuoteReportId@4() : Integer;
    VAR
      TempReportSelections@1000 : TEMPORARY Record 77;
    BEGIN
      EXIT(TempReportSelections.Usage::"S.Quote");
    END;

    BEGIN
    END.
  }
}

