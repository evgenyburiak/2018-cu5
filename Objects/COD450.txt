OBJECT Codeunit 450 Job Queue Error Handler
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    TableNo=472;
    OnRun=BEGIN
            IF NOT DoesExistLocked THEN
              EXIT;
            SetError(GETLASTERRORTEXT);
            LogError(Rec);
          END;

  }
  CODE
  {

    LOCAL PROCEDURE LogError@1(JobQueueEntry@1000 : Record 472);
    VAR
      JobQueueLogEntry@1002 : Record 474;
    BEGIN
      WITH JobQueueLogEntry DO BEGIN
        SETRANGE(ID,JobQueueEntry.ID);
        SETRANGE(Status,Status::"In Process");
        IF FINDFIRST THEN BEGIN
          SetErrorMessage(JobQueueEntry.GetErrorMessage);
          Status := Status::Error;
          MODIFY;
        END ELSE BEGIN
          JobQueueEntry.InsertLogEntry(JobQueueLogEntry);
          JobQueueEntry.FinalizeLogEntry(JobQueueLogEntry);
        END;
      END;
    END;

    BEGIN
    END.
  }
}

