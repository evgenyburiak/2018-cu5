OBJECT Codeunit 5332 Lookup CRM Tables
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {

    [External]
    PROCEDURE Lookup@1(CRMTableID@1001 : Integer;NAVTableId@1004 : Integer;SavedCRMId@1000 : GUID;VAR CRMId@1002 : GUID) : Boolean;
    VAR
      IntTableFilter@1003 : Text;
    BEGIN
      IntTableFilter := GetIntegrationTableFilter(CRMTableID,NAVTableId);

      CASE CRMTableID OF
        DATABASE::"CRM Account":
          EXIT(LookupCRMAccount(SavedCRMId,CRMId,IntTableFilter));
        DATABASE::"CRM Contact":
          EXIT(LookupCRMContact(SavedCRMId,CRMId,IntTableFilter));
        DATABASE::"CRM Systemuser":
          EXIT(LookupCRMSystemuser(SavedCRMId,CRMId,IntTableFilter));
        DATABASE::"CRM Transactioncurrency":
          EXIT(LookupCRMCurrency(SavedCRMId,CRMId,IntTableFilter));
        DATABASE::"CRM Pricelevel":
          EXIT(LookupCRMPriceList(SavedCRMId,CRMId,IntTableFilter));
        DATABASE::"CRM Product":
          EXIT(LookupCRMProduct(SavedCRMId,CRMId,IntTableFilter));
        DATABASE::"CRM Uomschedule":
          EXIT(LookupCRMUomschedule(SavedCRMId,CRMId,IntTableFilter));
        DATABASE::"CRM Opportunity":
          EXIT(LookupCRMOpportunity(SavedCRMId,CRMId,IntTableFilter));
      END;
      EXIT(FALSE);
    END;

    LOCAL PROCEDURE LookupCRMAccount@2(SavedCRMId@1004 : GUID;VAR CRMId@1000 : GUID;IntTableFilter@1005 : Text) : Boolean;
    VAR
      CRMAccount@1003 : Record 5341;
      OriginalCRMAccount@1002 : Record 5341;
      CRMAccountList@1001 : Page 5341;
    BEGIN
      IF NOT ISNULLGUID(CRMId) THEN BEGIN
        CRMAccount.GET(CRMId);
        CRMAccountList.SETRECORD(CRMAccount);
        IF NOT ISNULLGUID(SavedCRMId) THEN
          OriginalCRMAccount.GET(SavedCRMId);
        CRMAccountList.SetCurrentlyCoupledCRMAccount(OriginalCRMAccount);
      END;
      CRMAccount.SETVIEW(IntTableFilter);
      CRMAccountList.SETTABLEVIEW(CRMAccount);
      CRMAccountList.LOOKUPMODE(TRUE);
      IF CRMAccountList.RUNMODAL = ACTION::LookupOK THEN BEGIN
        CRMAccountList.GETRECORD(CRMAccount);
        CRMId := CRMAccount.AccountId;
        EXIT(TRUE);
      END;
      EXIT(FALSE);
    END;

    LOCAL PROCEDURE LookupCRMContact@3(SavedCRMId@1004 : GUID;VAR CRMId@1003 : GUID;IntTableFilter@1005 : Text) : Boolean;
    VAR
      CRMContact@1002 : Record 5342;
      OriginalCRMContact@1001 : Record 5342;
      CRMContactList@1000 : Page 5342;
    BEGIN
      IF NOT ISNULLGUID(CRMId) THEN BEGIN
        CRMContact.GET(CRMId);
        CRMContactList.SETRECORD(CRMContact);
        IF NOT ISNULLGUID(SavedCRMId) THEN
          OriginalCRMContact.GET(SavedCRMId);
        CRMContactList.SetCurrentlyCoupledCRMContact(OriginalCRMContact);
      END;
      CRMContact.SETVIEW(IntTableFilter);
      CRMContactList.SETTABLEVIEW(CRMContact);
      CRMContactList.LOOKUPMODE(TRUE);
      IF CRMContactList.RUNMODAL = ACTION::LookupOK THEN BEGIN
        CRMContactList.GETRECORD(CRMContact);
        CRMId := CRMContact.ContactId;
        EXIT(TRUE);
      END;
      EXIT(FALSE);
    END;

    LOCAL PROCEDURE LookupCRMSystemuser@4(SavedCRMId@1004 : GUID;VAR CRMId@1003 : GUID;IntTableFilter@1005 : Text) : Boolean;
    VAR
      CRMSystemuser@1002 : Record 5340;
      OriginalCRMSystemuser@1001 : Record 5340;
      CRMSystemuserList@1000 : Page 5340;
    BEGIN
      IF NOT ISNULLGUID(CRMId) THEN BEGIN
        CRMSystemuser.GET(CRMId);
        CRMSystemuserList.SETRECORD(CRMSystemuser);
        IF NOT ISNULLGUID(SavedCRMId) THEN
          OriginalCRMSystemuser.GET(SavedCRMId);
        CRMSystemuserList.SetCurrentlyCoupledCRMSystemuser(OriginalCRMSystemuser);
      END;
      CRMSystemuser.SETVIEW(IntTableFilter);
      CRMSystemuserList.SETTABLEVIEW(CRMSystemuser);
      CRMSystemuserList.LOOKUPMODE(TRUE);
      IF CRMSystemuserList.RUNMODAL = ACTION::LookupOK THEN BEGIN
        CRMSystemuserList.GETRECORD(CRMSystemuser);
        CRMId := CRMSystemuser.SystemUserId;
        EXIT(TRUE);
      END;
      EXIT(FALSE);
    END;

    LOCAL PROCEDURE LookupCRMCurrency@5(SavedCRMId@1004 : GUID;VAR CRMId@1003 : GUID;IntTableFilter@1005 : Text) : Boolean;
    VAR
      CRMTransactioncurrency@1002 : Record 5345;
      OriginalCRMTransactioncurrency@1001 : Record 5345;
      CRMTransactionCurrencyList@1000 : Page 5345;
    BEGIN
      IF NOT ISNULLGUID(CRMId) THEN BEGIN
        CRMTransactioncurrency.GET(CRMId);
        CRMTransactionCurrencyList.SETRECORD(CRMTransactioncurrency);
        IF NOT ISNULLGUID(SavedCRMId) THEN
          OriginalCRMTransactioncurrency.GET(SavedCRMId);
        CRMTransactionCurrencyList.SetCurrentlyCoupledCRMTransactioncurrency(OriginalCRMTransactioncurrency);
      END;
      CRMTransactioncurrency.SETVIEW(IntTableFilter);
      CRMTransactionCurrencyList.SETTABLEVIEW(CRMTransactioncurrency);
      CRMTransactionCurrencyList.LOOKUPMODE(TRUE);
      IF CRMTransactionCurrencyList.RUNMODAL = ACTION::LookupOK THEN BEGIN
        CRMTransactionCurrencyList.GETRECORD(CRMTransactioncurrency);
        CRMId := CRMTransactioncurrency.TransactionCurrencyId;
        EXIT(TRUE);
      END;
      EXIT(FALSE);
    END;

    LOCAL PROCEDURE LookupCRMPriceList@8(SavedCRMId@1004 : GUID;VAR CRMId@1003 : GUID;IntTableFilter@1005 : Text) : Boolean;
    VAR
      CRMPricelevel@1002 : Record 5346;
      OriginalCRMPricelevel@1001 : Record 5346;
      CRMPricelevelList@1000 : Page 5346;
    BEGIN
      IF NOT ISNULLGUID(CRMId) THEN BEGIN
        CRMPricelevel.GET(CRMId);
        CRMPricelevelList.SETRECORD(CRMPricelevel);
        IF NOT ISNULLGUID(SavedCRMId) THEN
          OriginalCRMPricelevel.GET(SavedCRMId);
        CRMPricelevelList.SetCurrentlyCoupledCRMPricelevel(OriginalCRMPricelevel);
      END;
      CRMPricelevel.SETVIEW(IntTableFilter);
      CRMPricelevelList.SETTABLEVIEW(CRMPricelevel);
      CRMPricelevelList.LOOKUPMODE(TRUE);
      IF CRMPricelevelList.RUNMODAL = ACTION::LookupOK THEN BEGIN
        CRMPricelevelList.GETRECORD(CRMPricelevel);
        CRMId := CRMPricelevel.PriceLevelId;
        EXIT(TRUE);
      END;
      EXIT(FALSE);
    END;

    LOCAL PROCEDURE LookupCRMProduct@6(SavedCRMId@1004 : GUID;VAR CRMId@1003 : GUID;IntTableFilter@1005 : Text) : Boolean;
    VAR
      CRMProduct@1002 : Record 5348;
      OriginalCRMProduct@1001 : Record 5348;
      CRMProductList@1000 : Page 5348;
    BEGIN
      IF NOT ISNULLGUID(CRMId) THEN BEGIN
        CRMProduct.GET(CRMId);
        CRMProductList.SETRECORD(CRMProduct);
        IF NOT ISNULLGUID(SavedCRMId) THEN
          OriginalCRMProduct.GET(SavedCRMId);
        CRMProductList.SetCurrentlyCoupledCRMProduct(OriginalCRMProduct);
      END;
      CRMProduct.SETVIEW(IntTableFilter);
      CRMProductList.SETTABLEVIEW(CRMProduct);
      CRMProductList.LOOKUPMODE(TRUE);
      IF CRMProductList.RUNMODAL = ACTION::LookupOK THEN BEGIN
        CRMProductList.GETRECORD(CRMProduct);
        CRMId := CRMProduct.ProductId;
        EXIT(TRUE);
      END;
      EXIT(FALSE);
    END;

    LOCAL PROCEDURE LookupCRMUomschedule@7(SavedCRMId@1004 : GUID;VAR CRMId@1003 : GUID;IntTableFilter@1005 : Text) : Boolean;
    VAR
      CRMUomschedule@1002 : Record 5362;
      OriginalCRMUomschedule@1001 : Record 5362;
      CRMUnitGroupList@1000 : Page 5362;
    BEGIN
      IF NOT ISNULLGUID(CRMId) THEN BEGIN
        CRMUomschedule.GET(CRMId);
        CRMUnitGroupList.SETRECORD(CRMUomschedule);
        IF NOT ISNULLGUID(SavedCRMId) THEN
          OriginalCRMUomschedule.GET(SavedCRMId);
        CRMUnitGroupList.SetCurrentlyCoupledCRMUomschedule(OriginalCRMUomschedule);
      END;
      CRMUomschedule.SETVIEW(IntTableFilter);
      CRMUnitGroupList.SETTABLEVIEW(CRMUomschedule);
      CRMUnitGroupList.LOOKUPMODE(TRUE);
      IF CRMUnitGroupList.RUNMODAL = ACTION::LookupOK THEN BEGIN
        CRMUnitGroupList.GETRECORD(CRMUomschedule);
        CRMId := CRMUomschedule.UoMScheduleId;
        EXIT(TRUE);
      END;
      EXIT(FALSE);
    END;

    LOCAL PROCEDURE LookupCRMOpportunity@10(SavedCRMId@1004 : GUID;VAR CRMId@1003 : GUID;IntTableFilter@1005 : Text) : Boolean;
    VAR
      CRMOpportunity@1002 : Record 5343;
      OriginalCRMOpportunity@1001 : Record 5343;
      CRMOpportunityList@1000 : Page 5343;
    BEGIN
      IF NOT ISNULLGUID(CRMId) THEN BEGIN
        CRMOpportunity.GET(CRMId);
        CRMOpportunityList.SETRECORD(CRMOpportunity);
        IF NOT ISNULLGUID(SavedCRMId) THEN
          OriginalCRMOpportunity.GET(SavedCRMId);
        CRMOpportunityList.SetCurrentlyCoupledCRMOpportunity(OriginalCRMOpportunity);
      END;
      CRMOpportunity.SETVIEW(IntTableFilter);
      CRMOpportunityList.SETTABLEVIEW(CRMOpportunity);
      CRMOpportunityList.LOOKUPMODE(TRUE);
      IF CRMOpportunityList.RUNMODAL = ACTION::LookupOK THEN BEGIN
        CRMOpportunityList.GETRECORD(CRMOpportunity);
        CRMId := CRMOpportunity.OpportunityId;
        EXIT(TRUE);
      END;
      EXIT(FALSE);
    END;

    PROCEDURE GetIntegrationTableFilter@9(CRMTableId@1001 : Integer;NAVTableId@1000 : Integer) : Text;
    VAR
      IntegrationTableMapping@1002 : Record 5335;
    BEGIN
      IntegrationTableMapping.SETRANGE("Synch. Codeunit ID",CODEUNIT::"CRM Integration Table Synch.");
      IntegrationTableMapping.SETRANGE("Table ID",NAVTableId);
      IntegrationTableMapping.SETRANGE("Integration Table ID",CRMTableId);
      IntegrationTableMapping.SETRANGE("Delete After Synchronization",FALSE);
      IF IntegrationTableMapping.FINDFIRST THEN
        EXIT(IntegrationTableMapping.GetIntegrationTableFilter);
      EXIT('');
    END;

    BEGIN
    END.
  }
}

