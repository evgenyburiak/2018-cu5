OBJECT Codeunit 581 Run Template Aged Acc. Pay.
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    OnRun=VAR
            ODataUtility@1001 : Codeunit 6710;
            ObjectTypeParam@1000 : ',,,,,,,,Page,Query';
            StatementType@1002 : 'BalanceSheet,SummaryTrialBalance,CashFlowStatement,StatementOfRetainedEarnings,AgedAccountsReceivable,AgedAccountsPayable,IncomeStatement';
          BEGIN
            IF NOT (ClientTypeManagement.GetCurrentClientType IN [CLIENTTYPE::Phone,CLIENTTYPE::Tablet]) THEN
              ODataUtility.GenerateExcelTemplateWorkBook(ObjectTypeParam::Page,'ExcelTemplateAgedAccountsPayable',TRUE,
                StatementType::AgedAccountsPayable)
            ELSE BEGIN
              MESSAGE(OfficeMobileMsg);
              EXIT;
            END;
          END;

  }
  CODE
  {
    VAR
      OfficeMobileMsg@1000 : TextConst 'ENU=Excel Reports cannot be opened in this environment because this version of Office does not support the file format.;ENG=Excel Reports cannot be opened in this environment because this version of Office does not support the file format.';
      ClientTypeManagement@1001 : Codeunit 4;

    BEGIN
    END.
  }
}

