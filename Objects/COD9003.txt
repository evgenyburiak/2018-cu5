OBJECT Codeunit 9003 Team Member Action Manager
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      TeamMemberErr@1000 : TextConst 'ENU=You are logged in as a Team Member role, so you can only create sales quotes.;ENG=You are logged in as a Team Member role, so you can only create sales quotes.';
      TeamMemberTxt@1001 : TextConst 'ENU=TEAM MEMBER;ENG=TEAM MEMBER';

    [EventSubscriber(Table,36,OnBeforeInsertEvent)]
    LOCAL PROCEDURE OnBeforeSalesHeaderInsert@2(VAR Rec@1000 : Record 36;RunTrigger@1001 : Boolean);
    BEGIN
      CheckTeamMemberPermissionOnSalesHeaderTable(Rec);
    END;

    LOCAL PROCEDURE CheckTeamMemberPermissionOnSalesHeaderTable@12(VAR SalesHeader@1001 : Record 36);
    VAR
      ConfPersonalizationMgt@1000 : Codeunit 9170;
    BEGIN
      IF ConfPersonalizationMgt.GetCurrentProfileIDNoError = TeamMemberTxt THEN
        IF SalesHeader."Document Type" <> SalesHeader."Document Type"::Quote THEN
          ERROR(TeamMemberErr);
    END;

    BEGIN
    END.
  }
}

