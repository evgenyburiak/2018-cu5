OBJECT Page 1010 Job WIP Methods
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Job WIP Methods;
               ENG=Job WIP Methods];
    SourceTable=Table1006;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the Job WIP Method. There are system-defined codes. In addition, you can create a Job WIP Method, and the code for it is in the list of Job WIP Methods.;
                           ENG=Specifies the code for the Job WIP Method. There are system-defined codes. In addition, you can create a Job WIP Method, and the code for it is in the list of Job WIP Methods.];
                ApplicationArea=#Jobs;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the description of the job WIP method. If the WIP method is system-defined, you cannot edit the description. You can enter a maximum of 50 characters, including spaces.;
                           ENG=Specifies the description of the job WIP method. If the WIP method is system-defined, you cannot edit the description. You can enter a maximum of 50 characters, including spaces.];
                ApplicationArea=#Jobs;
                SourceExpr=Description }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a Recognized Cost option to apply when creating a calculation method for WIP. You must select one of the five options:;
                           ENG=Specifies a Recognised Cost option to apply when creating a calculation method for WIP. You must select one of the five options:];
                ApplicationArea=#Jobs;
                SourceExpr="Recognized Costs" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a Recognized Sales option to apply when creating a calculation method for WIP. You must select one of the six options:;
                           ENG=Specifies a Recognised Sales option to apply when creating a calculation method for WIP. You must select one of the six options:];
                ApplicationArea=#Jobs;
                SourceExpr="Recognized Sales" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the calculation formula, depending on the parameters that you have specified when creating a calculation method for WIP. You can edit the check box, depending on the values set in the Recognized Costs and Recognized Sales fields.;
                           ENG=Specifies the calculation formula, depending on the parameters that you have specified when creating a calculation method for WIP. You can edit the check box, depending on the values set in the Recognised Costs and Recognised Sales fields.];
                ApplicationArea=#Jobs;
                SourceExpr="WIP Cost" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the parameters that apply when creating a calculation method for WIP. You can edit the check box, depending on the values set in the Recognized Costs and Recognized Sales fields.;
                           ENG=Specifies the parameters that apply when creating a calculation method for WIP. You can edit the check box, depending on the values set in the Recognised Costs and Recognised Sales fields.];
                ApplicationArea=#Jobs;
                SourceExpr="WIP Sales" }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether a WIP method can be associated with a job when you are creating or modifying a job. If you select this check box in the Job WIP Methods window, you can then set the method as a default WIP method in the Jobs Setup window.;
                           ENG=Specifies whether a WIP method can be associated with a job when you are creating or modifying a job. If you select this check box in the Job WIP Methods window, you can then set the method as a default WIP method in the Jobs Setup window.];
                ApplicationArea=#Jobs;
                SourceExpr=Valid }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether a Job WIP Method is system-defined.;
                           ENG=Specifies whether a Job WIP Method is system-defined.];
                ApplicationArea=#Jobs;
                SourceExpr="System Defined" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

