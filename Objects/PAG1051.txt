OBJECT Page 1051 Additional Fee Chart
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Additional Fee Visualization;
               ENG=Additional Fee Visualization];
    SourceTable=Table485;
    PageType=CardPart;
    OnAfterGetRecord=BEGIN
                       Update(CurrPage.BusinessChart);
                     END;

  }
  CONTROLS
  {
    { 1000;    ;Container ;
                ContainerType=ContentArea }

    { 1005;1   ;Group     ;
                Name=Options;
                CaptionML=[ENU=Options;
                           ENG=Options];
                GroupType=Group }

    { 1001;2   ;Field     ;
                Name=ChargePerLine;
                CaptionML=[ENU=Line Fee;
                           ENG=Line Fee];
                ToolTipML=[ENU=Specifies the additional fee for the line.;
                           ENG=Specifies the additional fee for the line.];
                ApplicationArea=#Advanced;
                SourceExpr=ChargePerLine;
                Visible=ShowOptions;
                OnValidate=BEGIN
                             UpdateData;
                           END;
                            }

    { 1003;2   ;Field     ;
                Name=Currency;
                CaptionML=[ENU=Currency Code;
                           ENG=Currency Code];
                ToolTipML=[ENU=Specifies the code for the currency that amounts are shown in.;
                           ENG=Specifies the code for the currency that amounts are shown in.];
                ApplicationArea=#Advanced;
                SourceExpr=Currency;
                TableRelation=Currency.Code;
                LookupPageID=Currencies;
                OnValidate=BEGIN
                             UpdateData;
                           END;
                            }

    { 1004;2   ;Field     ;
                Name=Max. Remaining Amount;
                CaptionML=[ENU=Max. Remaining Amount;
                           ENG=Max. Remaining Amount];
                ToolTipML=[ENU=Specifies the maximum amount that is displayed as remaining in the chart.;
                           ENG=Specifies the maximum amount that is displayed as remaining in the chart.];
                ApplicationArea=#Advanced;
                SourceExpr=MaxRemAmount;
                MinValue=0;
                OnValidate=BEGIN
                             UpdateData;
                           END;
                            }

    { 1006;1   ;Group     ;
                Name=Graph;
                CaptionML=[ENU=Graph;
                           ENG=Graph];
                GroupType=Group }

    { 1002;2   ;Field     ;
                Name=BusinessChart;
                ControlAddIn=[Microsoft.Dynamics.Nav.Client.BusinessChart;PublicKeyToken=31bf3856ad364e35] }

  }
  CODE
  {
    VAR
      ReminderLevel@1001 : Record 293;
      TempSortingTable@1012 : TEMPORARY Record 1051;
      ChargePerLine@1000 : Boolean;
      RemAmountTxt@1004 : TextConst 'ENU=Remaining Amount;ENG=Remaining Amount';
      Currency@1011 : Code[10];
      MaxRemAmount@1009 : Decimal;
      ShowOptions@1003 : Boolean;
      AddInIsReady@1010 : Boolean;

    [External]
    PROCEDURE SetViewMode@1000(SetReminderLevel@1000 : Record 293;SetChargePerLine@1002 : Boolean;SetShowOptions@1003 : Boolean);
    BEGIN
      ReminderLevel := SetReminderLevel;
      ChargePerLine := SetChargePerLine;
      ShowOptions := SetShowOptions;
    END;

    [Internal]
    PROCEDURE UpdateData@1003();
    BEGIN
      IF NOT AddInIsReady THEN
        EXIT;

      TempSortingTable.UpdateData(Rec,ReminderLevel,ChargePerLine,Currency,RemAmountTxt,MaxRemAmount);
      Update(CurrPage.BusinessChart);
    END;

    EVENT BusinessChart@-1002::AddInReady@3();
    BEGIN
      AddInIsReady := TRUE;
      UpdateData;
    END;

    EVENT BusinessChart@-1002::Refresh@4();
    BEGIN
      UpdateData;
    END;

    BEGIN
    END.
  }
}

