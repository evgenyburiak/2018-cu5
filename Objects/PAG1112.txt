OBJECT Page 1112 Cost Object Card
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Cost Object Card;
               ENG=Cost Object Card];
    SourceTable=Table1113;
    PageType=Card;
    RefreshOnActivate=Yes;
    ActionList=ACTIONS
    {
      { 1       ;0   ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 2       ;1   ;ActionGroup;
                      CaptionML=[ENU=&Cost Object;
                                 ENG=&Cost Object];
                      Image=Costs }
      { 3       ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=E&ntries;
                                 ENG=E&ntries];
                      ToolTipML=[ENU=View the entries for the cost object.;
                                 ENG=View the entries for the cost object.];
                      ApplicationArea=#CostAccounting;
                      RunObject=Page 1103;
                      RunPageView=SORTING(Cost Object Code,Cost Type No.,Allocated,Posting Date);
                      RunPageLink=Cost Object Code=FIELD(Code);
                      Image=Entries }
      { 4       ;2   ;Separator  }
      { 5       ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=&Balance;
                                 ENG=&Balance];
                      ToolTipML=[ENU=View a summary of the balance at date or the net change for different time periods for the cost object that you select. You can select different time intervals and set filters on the cost centers and cost objects that you want to see.;
                                 ENG=View a summary of the balance at date or the net change for different time periods for the cost object that you select. You can select different time intervals and set filters on the cost centres and cost objects that you want to see.];
                      ApplicationArea=#CostAccounting;
                      Image=Balance;
                      OnAction=VAR
                                 CostType@1000 : Record 1103;
                               BEGIN
                                 IF Totaling = '' THEN
                                   CostType.SETFILTER("Cost Object Filter",Code)
                                 ELSE
                                   CostType.SETFILTER("Cost Object Filter",Totaling);

                                 PAGE.RUN(PAGE::"Cost Type Balance",CostType);
                               END;
                                }
      { 6       ;0   ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 7       ;1   ;Action    ;
                      CaptionML=[ENU=Dimension Values;
                                 ENG=Dimension Values];
                      ToolTipML=[ENU=View or edit the dimension values for the current dimension.;
                                 ENG=View or edit the dimension values for the current dimension.];
                      ApplicationArea=#Dimensions;
                      RunObject=Page 537;
                      Promoted=Yes;
                      Image=Dimensions;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 8   ;0   ;Container ;
                ContainerType=ContentArea }

    { 9   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           ENG=General] }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the cost object.;
                           ENG=Specifies the code for the cost object.];
                ApplicationArea=#CostAccounting;
                SourceExpr=Code }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the cost object card.;
                           ENG=Specifies the name of the cost object card.];
                ApplicationArea=#CostAccounting;
                SourceExpr=Name;
                Importance=Promoted }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the purpose of the cost object, such as Cost Object, Heading, or Begin-Total. Newly created cost objects are automatically assigned the Cost Object type, but you can change this.;
                           ENG=Specifies the purpose of the cost object, such as Cost Object, Heading, or Begin-Total. Newly created cost objects are automatically assigned the Cost Object type, but you can change this.];
                ApplicationArea=#CostAccounting;
                SourceExpr="Line Type" }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an account interval or a list of account numbers. The entries of the account will be totaled to give a total balance. How entries are totaled depends on the value in the Account Type field.;
                           ENG=Specifies an account interval or a list of account numbers. The entries of the account will be totalled to give a total balance. How entries are totalled depends on the value in the Account Type field.];
                ApplicationArea=#CostAccounting;
                SourceExpr=Totaling }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a comment that applies to the cost object.;
                           ENG=Specifies a comment that applies to the cost object.];
                ApplicationArea=#CostAccounting;
                SourceExpr=Comment }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the net change in the account balance during the time period in the Date Filter field.;
                           ENG=Specifies the net change in the account balance during the time period in the Date Filter field.];
                ApplicationArea=#CostAccounting;
                SourceExpr="Net Change";
                Importance=Promoted }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the sorting order of the cost object.;
                           ENG=Specifies the sorting order of the cost object.];
                ApplicationArea=#CostAccounting;
                SourceExpr="Sorting Order" }

    { 17  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether you want a blank line to appear immediately after this cost center when you print the chart of cost centers. The New Page, Blank Line, and Indentation fields define the layout of the chart of cost centers.;
                           ENG=Specifies whether you want a blank line to appear immediately after this cost centre when you print the chart of cost centres. The New Page, Blank Line, and Indentation fields define the layout of the chart of cost centres.];
                ApplicationArea=#CostAccounting;
                SourceExpr="Blank Line" }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if you want a new page to start immediately after this cost center when you print the chart of cash flow accounts.;
                           ENG=Specifies if you want a new page to start immediately after this cost centre when you print the chart of cash flow accounts.];
                ApplicationArea=#CostAccounting;
                SourceExpr="New Page" }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that the related record is blocked from being posted in transactions, for example a customer that is declared insolvent or an item that is placed in quarantine.;
                           ENG=Specifies that the related record is blocked from being posted in transactions, for example a customer that is declared insolvent or an item that is placed in quarantine.];
                ApplicationArea=#CostAccounting;
                SourceExpr=Blocked }

  }
  CODE
  {

    BEGIN
    END.
  }
}

