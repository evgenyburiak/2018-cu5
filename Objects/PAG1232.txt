OBJECT Page 1232 Positive Pay Entry Details
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Positive Pay Entry Details;
               ENG=Positive Pay Entry Details];
    SourceTable=Table1232;
    PageType=List;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the bank account number. If you select Balance at Date, the balance as of the last day in the relevant time interval is displayed.;
                           ENG=Specifies the bank account number. If you select Balance at Date, the balance as of the last day in the relevant time interval is displayed.];
                ApplicationArea=#Suite;
                SourceExpr="Bank Account No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the involved entry or record, according to the specified number series.;
                           ENG=Specifies the number of the involved entry or record, according to the specified number series.];
                ApplicationArea=#Suite;
                SourceExpr="No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number on the check.;
                           ENG=Specifies the number on the cheque.];
                ApplicationArea=#Suite;
                SourceExpr="Check No." }

    { 7   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the currency code for the amount on the line.;
                           ENG=Specifies the currency code for the amount on the line.];
                ApplicationArea=#Suite;
                SourceExpr="Currency Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of the document on the line.;
                           ENG=Specifies the type of the document on the line.];
                ApplicationArea=#Suite;
                SourceExpr="Document Type" }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the related document was created.;
                           ENG=Specifies the date when the related document was created.];
                ApplicationArea=#Suite;
                SourceExpr="Document Date" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the payment amount.;
                           ENG=Specifies the payment amount.];
                ApplicationArea=#Suite;
                SourceExpr=Amount }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the recipient of the payment.;
                           ENG=Specifies the recipient of the payment.];
                ApplicationArea=#Suite;
                SourceExpr=Payee }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the user who posted the entry, to be used, for example, in the change log.;
                           ENG=Specifies the ID of the user who posted the entry, to be used, for example, in the change log.];
                ApplicationArea=#Suite;
                SourceExpr="User ID" }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies when the Positive Pay export was updated.;
                           ENG=Specifies when the Positive Pay export was updated.];
                ApplicationArea=#Suite;
                SourceExpr="Update Date" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

