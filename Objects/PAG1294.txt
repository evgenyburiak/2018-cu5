OBJECT Page 1294 Pmt. Reconciliation Journals
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Payment Reconciliation Journals;
               ENG=Payment Reconciliation Journals];
    InsertAllowed=No;
    SourceTable=Table273;
    SourceTableView=WHERE(Statement Type=CONST(Payment Application));
    PageType=List;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Bank;
                                ENG=New,Process,Report,Bank];
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 11      ;1   ;ActionGroup;
                      CaptionML=[ENU=Process;
                                 ENG=Process];
                      Image=Action }
      { 13      ;2   ;Action    ;
                      Name=ImportBankTransactionsToNew;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Import Bank Transactions;
                                 ENG=&Import Bank Transactions];
                      ToolTipML=[ENU=To start the process of reconciling new payments, import a bank feed or electronic file containing the related bank transactions.;
                                 ENG=To start the process of reconciling new payments, import a bank feed or electronic file containing the related bank transactions.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Import;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ImportAndProcessToNewStatement
                               END;
                                }
      { 2       ;2   ;Action    ;
                      Name=EditJournal;
                      ShortCutKey=Return;
                      CaptionML=[ENU=Edit Journal;
                                 ENG=Edit Journal];
                      ToolTipML=[ENU=Modify an existing payment reconciliation journal for a bank account.;
                                 ENG=Modify an existing payment reconciliation journal for a bank account.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=OpenWorksheet;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 BankAccReconciliation@1001 : Record 273;
                               BEGIN
                                 IF NOT BankAccReconciliation.GET("Statement Type","Bank Account No.","Statement No.") THEN
                                   EXIT;

                                 OpenWorksheet(Rec);
                               END;
                                }
      { 3       ;2   ;Action    ;
                      Name=NewJournal;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&New Journal;
                                 ENG=&New Journal];
                      ToolTipML=[ENU=Create a payment reconciliation journal for a bank account to set up payments that have been recorded as transactions in an electronic bank and need to be applied to related open entries.;
                                 ENG=Create a payment reconciliation journal for a bank account to set up payments that have been recorded as transactions in an electronic bank and need to be applied to related open entries.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=NewDocument;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 OpenNewWorksheet
                               END;
                                }
      { 9       ;1   ;ActionGroup;
                      CaptionML=[ENU=Bank;
                                 ENG=Bank] }
      { 10      ;2   ;Action    ;
                      CaptionML=[ENU=Bank Account Card;
                                 ENG=Bank Account Card];
                      ToolTipML=[ENU=View or edit information about the bank account that is related to the payment reconciliation journal.;
                                 ENG=View or edit information about the bank account that is related to the payment reconciliation journal.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 1283;
                      RunPageLink=No.=FIELD(Bank Account No.);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=BankAccount;
                      PromotedCategory=Category4 }
      { 6       ;2   ;Action    ;
                      CaptionML=[ENU=List of Bank Accounts;
                                 ENG=List of Bank Accounts];
                      ToolTipML=[ENU=View and edit information about the bank accounts that are associated with the payment reconciliation journals that you use to reconcile payment transactions.;
                                 ENG=View and edit information about the bank accounts that are associated with the payment reconciliation journals that you use to reconcile payment transactions.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 1282;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=List;
                      PromotedCategory=Category4 }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the bank account that you want to reconcile with the bank's statement.;
                           ENG=Specifies the number of the bank account that you want to reconcile with the bank's statement.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Account No.";
                Editable=FALSE }

    { 7   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the bank account statement.;
                           ENG=Specifies the number of the bank account statement.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Statement No.";
                Editable=FALSE }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the sum of values in the Statement Amount field on all the lines in the Bank Acc. Reconciliation and Payment Reconciliation Journal windows.;
                           ENG=Specifies the sum of values in the Statement Amount field on all the lines in the Bank Acc. Reconciliation and Payment Reconciliation Journal windows.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Total Transaction Amount";
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=Remaining Amount to Apply;
                           ENG=Remaining Amount to Apply];
                ToolTipML=[ENU=Specifies the total amount that exists on the bank account per the last time it was reconciled.;
                           ENG=Specifies the total amount that exists on the bank account per the last time it was reconciled.];
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr="Total Difference";
                Editable=FALSE;
                Style=Unfavorable;
                StyleExpr=TRUE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the program to calculate VAT for accounts and balancing accounts on the journal line of the selected bank account reconciliation.;
                           ENG=Specifies whether the program to calculate VAT for accounts and balancing accounts on the journal line of the selected bank account reconciliation.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Copy VAT Setup to Jnl. Line" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

