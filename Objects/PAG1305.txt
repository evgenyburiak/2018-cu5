OBJECT Page 1305 O365 Developer Welcome
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Welcome;
               ENG=Welcome];
    SourceTable=Table1309;
    PageType=NavigatePage;
    OnInit=BEGIN
             SETRANGE("User ID",USERID);
           END;

    OnOpenPage=BEGIN
                 FirstPageVisible := TRUE;
                 O365GettingStartedPageData.GetPageImage(O365GettingStartedPageData,1,PAGE::"O365 Getting Started");
                 IF PageDataMediaResources.GET(O365GettingStartedPageData."Media Resources Ref") THEN;
               END;

    OnQueryClosePage=BEGIN
                       MarkAsCompleted;
                     END;

    ActionList=ACTIONS
    {
      { 6       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 7       ;1   ;Action    ;
                      Name=WelcomeTour;
                      CaptionML=[ENU=Learn More;
                                 ENG=Learn More];
                      ApplicationArea=#Basic,#Suite;
                      InFooterBar=Yes;
                      Image=Start;
                      OnAction=BEGIN
                                 HYPERLINK(LearnMoreLbl);
                                 CurrPage.CLOSE;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 5   ;0   ;Container ;
                ContainerType=ContentArea }

    { 4   ;1   ;Group     ;
                Visible=FirstPageVisible;
                GroupType=Group }

    { 3   ;2   ;Field     ;
                Name=Image1;
                CaptionML=[ENU=Image;
                           ENG=Image];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=PageDataMediaResources."Media Reference";
                Editable=FALSE;
                ShowCaption=No }

    { 2   ;2   ;Group     ;
                Name=Page1Group;
                CaptionML=[ENU=This is your sandbox environment (preview) for Dynamics 365 for Finance and Operations;
                           ENG=This is your sandbox environment (preview) for Dynamics 365 for Finance and Operations];
                GroupType=Group }

    { 1   ;3   ;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=MainTextLbl;
                Editable=FALSE;
                MultiLine=Yes;
                ShowCaption=No }

    { 8   ;3   ;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=SandboxTextLbl;
                Editable=FALSE;
                MultiLine=Yes;
                ShowCaption=No }

  }
  CODE
  {
    VAR
      O365GettingStartedPageData@1001 : Record 1308;
      MainTextLbl@1008 : TextConst 'ENU="This environment is for you to try out different product features without affecting data or settings in your production environment. You can use it for different non-production activities such as test, demonstration, or development. ";ENG="This environment is for you to try out different product features without affecting data or settings in your production environment. You can use it for different non-production activities such as test, demonstration, or development. "';
      LearnMoreLbl@1009 : TextConst '@@@={Locked};ENU=https://aka.ms/d365fobesandbox;ENG=https://aka.ms/d365fobesandbox';
      PageDataMediaResources@1003 : Record 2000000182;
      ClientTypeManagement@1004 : Codeunit 4;
      FirstPageVisible@1000 : Boolean;
      SandboxTextLbl@1002 : TextConst 'ENU=This Sandbox environment feature is provided as a free preview solely for testing, development and evaluation. You will not use the Sandbox in a live operating environment. Microsoft may, in its sole discretion, change the Sandbox environment or subject it to a fee for a final, commercial version, if any, or may elect not to release one.;ENG=This Sandbox environment feature is provided as a free preview solely for testing, development and evaluation. You will not use the Sandbox in a live operating environment. Microsoft may, in its sole discretion, change the Sandbox environment or subject it to a fee for a final, commercial version, if any, or may elect not to release one.';

    LOCAL PROCEDURE MarkAsCompleted@6();
    BEGIN
      "User ID" := USERID;
      "Display Target" := FORMAT(ClientTypeManagement.GetCurrentClientType);
      "Tour in Progress" := FALSE;
      "Tour Completed" := TRUE;
      INSERT;
    END;

    BEGIN
    END.
  }
}

