OBJECT Page 1309 O365 Getting Started
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Getting Started;
               ENG=Getting Started];
    SourceTable=Table1309;
    PageType=NavigatePage;
    OnInit=BEGIN
             SETRANGE("User ID",USERID);
           END;

    OnOpenPage=BEGIN
                 IF NOT AlreadyShown THEN BEGIN
                   FirstRun := TRUE;
                   MarkAsShown;
                 END;

                 IF UserTours.IsAvailable AND O365GettingStartedMgt.AreUserToursEnabled THEN BEGIN
                   UserTours := UserTours.Create;
                   UserTours.StopUserTour;
                 END;

                 MaxNumberOfSteps := 5;
                 LastPageIndex := 9;
                 LoadImages;
               END;

    OnQueryClosePage=BEGIN
                       IF DisableConfirmationOnPageClose THEN
                         EXIT(TRUE);

                       "Tour in Progress" := FALSE;
                       MODIFY;

                       IF UserTours.IsAvailable AND O365GettingStartedMgt.AreUserToursEnabled THEN
                         UserTours.StartUserTour(O365GettingStartedMgt.GetReturnToGettingStartedTourID);

                       EXIT(TRUE);
                     END;

    OnAfterGetCurrRecord=BEGIN
                           IF CurrentPage = 0 THEN
                             CurrentPage := "Current Page";

                           IF CurrentPage = 1 THEN
                             NextPageVisible := NOT FirstRun
                           ELSE
                             NextPageVisible := CurrentPage < LastPageIndex;

                           UpdatePageControls;
                           SetPageCaption;
                         END;

    ActionList=ACTIONS
    {
      { 19      ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 45      ;1   ;Action    ;
                      Name=SkipWizard;
                      CaptionML=[ENU=Not now;
                                 ENG=Not now];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=CurrentPage = 1;
                      InFooterBar=Yes;
                      OnAction=BEGIN
                                 MarkWizardAsDone;
                                 DisableConfirmationOnPageClose := FALSE;
                                 CurrPage.CLOSE;
                               END;
                                }
      { 14      ;1   ;Action    ;
                      Name=GetStarted;
                      CaptionML=[ENU=Get started;
                                 ENG=Get started];
                      ApplicationArea=#Basic,#Suite;
                      Visible=(CurrentPage = 1) AND FirstRun;
                      InFooterBar=Yes;
                      Image=Start;
                      OnAction=VAR
                                 ConfPersonalizationMgt@1000 : Codeunit 9170;
                                 AccountantProfile@1001 : Code[30];
                               BEGIN
                                 AccountantProfile := AccountantTxt;
                                 IF ConfPersonalizationMgt.GetCurrentProfileIDNoError = AccountantProfile THEN
                                   InvokeWalkMeButton(O365GettingStartedMgt.GetAccountantTourID)
                                 ELSE
                                   InvokeWalkMeButton(O365GettingStartedMgt.GetGettingStartedTourID);
                               END;
                                }
      { 54      ;1   ;Action    ;
                      Name=GetStartedSecondView;
                      CaptionML=[ENU=Show me welcome tour;
                                 ENG=Show me welcome tour];
                      ApplicationArea=#Basic,#Suite;
                      Visible=(CurrentPage = 1) AND (NOT FirstRun);
                      InFooterBar=Yes;
                      Image=NextRecord;
                      OnAction=VAR
                                 ConfPersonalizationMgt@1000 : Codeunit 9170;
                                 AccountantProfile@1001 : Code[30];
                               BEGIN
                                 AccountantProfile := AccountantTxt;
                                 IF ConfPersonalizationMgt.GetCurrentProfileIDNoError = AccountantProfile THEN
                                   InvokeWalkMeButton(O365GettingStartedMgt.GetAccountantTourID)
                                 ELSE
                                   InvokeWalkMeButton(O365GettingStartedMgt.GetGettingStartedTourID);
                               END;
                                }
      { 4       ;1   ;Action    ;
                      Name=Back;
                      CaptionML=[ENU=Go Back;
                                 ENG=Go Back];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=(CurrentPage > 1) AND (CurrentPage < LastPageIndex);
                      InFooterBar=Yes;
                      Image=PreviousRecord;
                      OnAction=BEGIN
                                 CurrentPage := GetNextPageID(-1,CurrentPage);
                                 CurrPage.UPDATE;
                               END;
                                }
      { 53      ;1   ;Action    ;
                      Name=BackLastPage;
                      CaptionML=[ENU=Go Back;
                                 ENG=Go Back];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=CurrentPage = LastPageIndex;
                      InFooterBar=Yes;
                      OnAction=BEGIN
                                 CurrentPage := GetNextPageID(-1,CurrentPage);
                                 CurrPage.UPDATE;
                               END;
                                }
      { 13      ;1   ;Action    ;
                      Name=ShowMeInvoicing;
                      CaptionML=[ENU=Show me Invoicing;
                                 ENG=Show me Invoicing];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=CurrentPage = 2;
                      InFooterBar=Yes;
                      Image=NextRecord;
                      OnAction=BEGIN
                                 InvokeWalkMeButton(O365GettingStartedMgt.GetInvoicingTourID);
                               END;
                                }
      { 7       ;1   ;Action    ;
                      Name=ShowMePurchasing;
                      CaptionML=[ENU=Show me Purchasing;
                                 ENG=Show me Purchasing];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=CurrentPage = 3;
                      InFooterBar=Yes;
                      Image=NextRecord;
                      OnAction=BEGIN
                                 InvokeWalkMeButton(-1);
                               END;
                                }
      { 46      ;1   ;Action    ;
                      Name=ShowMeInventory;
                      CaptionML=[ENU=Show me Inventory;
                                 ENG=Show me Inventory];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=CurrentPage = 4;
                      InFooterBar=Yes;
                      Image=NextRecord;
                      OnAction=BEGIN
                                 InvokeWalkMeButton(-1);
                               END;
                                }
      { 8       ;1   ;Action    ;
                      Name=ShowMeReporting;
                      CaptionML=[ENU=Show me Reporting;
                                 ENG=Show me Reporting];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=CurrentPage = 5;
                      InFooterBar=Yes;
                      Image=NextRecord;
                      OnAction=BEGIN
                                 InvokeWalkMeButton(O365GettingStartedMgt.GetReportingTourID);
                               END;
                                }
      { 51      ;1   ;Action    ;
                      Name=SetUpInOutlook;
                      CaptionML=[ENU=Set up in Outlook;
                                 ENG=Set up in Outlook];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=CurrentPage = 6;
                      InFooterBar=Yes;
                      Image=NextRecord;
                      OnAction=BEGIN
                                 AddToOutlook;
                               END;
                                }
      { 52      ;1   ;Action    ;
                      Name=SetupDevice;
                      CaptionML=[ENU=Show me how;
                                 ENG=Show me how];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=CurrentPage = 7;
                      InFooterBar=Yes;
                      Image=NextRecord;
                      OnAction=BEGIN
                                 PAGE.RUNMODAL(PAGE::"O365 Device Setup");
                               END;
                                }
      { 10      ;1   ;Action    ;
                      Name=ShowMeImport;
                      CaptionML=[ENU=Show me Import;
                                 ENG=Show me Import];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=CurrentPage = 8;
                      InFooterBar=Yes;
                      Image=NextRecord;
                      OnAction=BEGIN
                                 InvokeWalkMeButton(-1);
                               END;
                                }
      { 3       ;1   ;Action    ;
                      Name=Next;
                      CaptionML=[ENU=Next;
                                 ENG=Next];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=NextPageVisible;
                      InFooterBar=Yes;
                      Image=Approve;
                      OnAction=BEGIN
                                 CurrentPage := GetNextPageID(1,CurrentPage);
                                 CurrPage.UPDATE;
                               END;
                                }
      { 34      ;1   ;Action    ;
                      Name=Done;
                      CaptionML=[ENU=Done;
                                 ENG=Done];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=CurrentPage = LastPageIndex;
                      InFooterBar=Yes;
                      Image=NextRecord;
                      OnAction=BEGIN
                                 MarkWizardAsDone;
                                 CurrPage.CLOSE;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Visible=CurrentPage = 1;
                Editable=FALSE;
                GroupType=Group }

    { 35  ;2   ;Field     ;
                Name=Image1;
                CaptionML=[ENU=Image1;
                           ENG=Image1];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=MediaResourcesPageData1."Media Reference";
                Editable=FALSE;
                ShowCaption=No }

    { 5   ;2   ;Group     ;
                Name=Page1Group;
                CaptionML=[ENU=Welcome to Dynamics 365 for Finance and Operations. Try it out!;
                           ENG=Welcome to Dynamics 365 for Finance and Operations. Try it out!];
                GroupType=Group }

    { 16  ;3   ;Field     ;
                Name=BodyText1;
                CaptionML=[ENU=BodyText1;
                           ENG=BodyText1];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BodyText;
                Editable=FALSE;
                MultiLine=Yes;
                ShowCaption=No }

    { 17  ;1   ;Group     ;
                Visible=CurrentPage = 2;
                Editable=FALSE;
                GroupType=Group }

    { 36  ;2   ;Field     ;
                Name=Image2;
                CaptionML=[ENU=Image2;
                           ENG=Image2];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=MediaResourcesPageData2."Media Reference";
                Editable=FALSE;
                ShowCaption=No }

    { 15  ;2   ;Group     ;
                Name=Page2Group;
                CaptionML=[ENU=Grow sales, manage your customers, and make billing easy;
                           ENG=Grow sales, manage your customers, and make billing easy];
                GroupType=Group }

    { 12  ;3   ;Field     ;
                Name=BodyText2;
                CaptionML=[ENU=BodyText2;
                           ENG=BodyText2];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BodyText;
                Editable=FALSE;
                MultiLine=Yes;
                ShowCaption=No }

    { 21  ;1   ;Group     ;
                Visible=CurrentPage = 3;
                Editable=FALSE;
                GroupType=Group }

    { 37  ;2   ;Field     ;
                Name=Image3;
                CaptionML=[ENU=Image3;
                           ENG=Image3];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=MediaResourcesPageData3."Media Reference";
                Editable=FALSE;
                ShowCaption=No }

    { 20  ;2   ;Group     ;
                Name=Page3Group;
                CaptionML=[ENU=Managing your vendors;
                           ENG=Managing your vendors];
                GroupType=Group }

    { 18  ;3   ;Field     ;
                Name=BodyText3;
                CaptionML=[ENU=BodyText3;
                           ENG=BodyText3];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BodyText;
                Editable=FALSE;
                MultiLine=Yes;
                ShowCaption=No }

    { 24  ;1   ;Group     ;
                Visible=CurrentPage = 4;
                Editable=FALSE;
                GroupType=Group }

    { 38  ;2   ;Field     ;
                Name=Image4;
                CaptionML=[ENU=Image4;
                           ENG=Image4];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=MediaResourcesPageData4."Media Reference";
                Editable=FALSE;
                ShowCaption=No }

    { 23  ;2   ;Group     ;
                Name=Page4Group;
                CaptionML=[ENU=Do you keep stock and need to track your inventory?;
                           ENG=Do you keep stock and need to track your inventory?];
                GroupType=Group }

    { 22  ;3   ;Field     ;
                Name=BodyText4;
                CaptionML=[ENU=BodyText4;
                           ENG=BodyText4];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BodyText;
                Editable=FALSE;
                MultiLine=Yes;
                ShowCaption=No }

    { 27  ;1   ;Group     ;
                Visible=CurrentPage = 5;
                Editable=FALSE;
                GroupType=Group }

    { 39  ;2   ;Field     ;
                Name=Image5;
                CaptionML=[ENU=Image5;
                           ENG=Image5];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=MediaResourcesPageData5."Media Reference";
                Editable=FALSE;
                ShowCaption=No }

    { 26  ;2   ;Group     ;
                Name=Page5Group;
                CaptionML=[ENU=Simplify reporting;
                           ENG=Simplify reporting];
                GroupType=Group }

    { 25  ;3   ;Field     ;
                Name=BodyText5;
                CaptionML=[ENU=BodyText5;
                           ENG=BodyText5];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BodyText;
                Editable=FALSE;
                MultiLine=Yes;
                ShowCaption=No }

    { 49  ;1   ;Group     ;
                Visible=CurrentPage = 6;
                Editable=FALSE;
                GroupType=Group }

    { 48  ;2   ;Field     ;
                Name=Image6;
                CaptionML=[ENU=Image6;
                           ENG=Image6];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=MediaResourcesPageData6."Media Reference";
                Editable=FALSE;
                ShowCaption=No }

    { 47  ;2   ;Group     ;
                Name=Page6Group;
                CaptionML=[ENU=Run your business within Office 365;
                           ENG=Run your business within Office 365];
                GroupType=Group }

    { 9   ;3   ;Field     ;
                Name=BodyText6;
                CaptionML=[ENU=BodyText6;
                           ENG=BodyText6];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BodyText;
                Editable=FALSE;
                MultiLine=Yes;
                ShowCaption=No }

    { 30  ;1   ;Group     ;
                Visible=CurrentPage = 7;
                Editable=FALSE;
                GroupType=Group }

    { 40  ;2   ;Field     ;
                Name=Image7;
                CaptionML=[ENU=Image7;
                           ENG=Image7];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=MediaResourcesPageData7."Media Reference";
                Editable=FALSE;
                ShowCaption=No }

    { 29  ;2   ;Group     ;
                Name=Page7Group;
                CaptionML=[ENU=Do business anywhere;
                           ENG=Do business anywhere];
                GroupType=Group }

    { 28  ;3   ;Field     ;
                Name=BodyText7;
                CaptionML=[ENU=BodyText7;
                           ENG=BodyText7];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BodyText;
                Editable=FALSE;
                MultiLine=Yes;
                ShowCaption=No }

    { 33  ;1   ;Group     ;
                Visible=CurrentPage = 8;
                Editable=FALSE;
                GroupType=Group }

    { 41  ;2   ;Field     ;
                Name=Image8;
                CaptionML=[ENU=Image8;
                           ENG=Image8];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=MediaResourcesPageData8."Media Reference";
                Editable=FALSE;
                ShowCaption=No }

    { 32  ;2   ;Group     ;
                Name=Page8Group;
                CaptionML=[ENU=Easily import data from your current accounting system;
                           ENG=Easily import data from your current accounting system];
                GroupType=Group }

    { 31  ;3   ;Field     ;
                Name=BodyText8;
                CaptionML=[ENU=BodyText8;
                           ENG=BodyText8];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BodyText;
                Editable=FALSE;
                MultiLine=Yes;
                ShowCaption=No }

    { 44  ;1   ;Group     ;
                Visible=CurrentPage = 9;
                Editable=FALSE;
                GroupType=Group }

    { 43  ;2   ;Field     ;
                Name=Image9;
                CaptionML=[ENU=Image9;
                           ENG=Image9];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=MediaResourcesPageData9."Media Reference";
                Editable=FALSE;
                ShowCaption=No }

    { 42  ;2   ;Group     ;
                Name=Page9Group;
                CaptionML=[ENU=Try it out;
                           ENG=Try it out];
                GroupType=Group }

    { 6   ;3   ;Field     ;
                Name=BodyText9;
                CaptionML=[ENU=BodyText9;
                           ENG=BodyText9];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BodyText;
                Editable=FALSE;
                MultiLine=Yes;
                ShowCaption=No }

    { 11  ;2   ;Field     ;
                Name=DocumentationLink;
                CaptionML=[ENU=DocumentationLink;
                           ENG=DocumentationLink];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocumentationLinkCaptionLbl;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              HYPERLINK(DocumentationLinkCaptionLbl);
                            END;

                ShowCaption=No }

  }
  CODE
  {
    VAR
      O365GettingStartedPageData1@1000 : Record 1308;
      O365GettingStartedPageData2@1007 : Record 1308;
      O365GettingStartedPageData3@1009 : Record 1308;
      O365GettingStartedPageData4@1010 : Record 1308;
      O365GettingStartedPageData5@1014 : Record 1308;
      O365GettingStartedPageData6@1013 : Record 1308;
      O365GettingStartedPageData7@1012 : Record 1308;
      O365GettingStartedPageData8@1011 : Record 1308;
      O365GettingStartedPageData9@1015 : Record 1308;
      MediaResourcesPageData1@1023 : Record 2000000182;
      MediaResourcesPageData2@1024 : Record 2000000182;
      MediaResourcesPageData3@1025 : Record 2000000182;
      MediaResourcesPageData4@1027 : Record 2000000182;
      MediaResourcesPageData5@1031 : Record 2000000182;
      MediaResourcesPageData6@1030 : Record 2000000182;
      MediaResourcesPageData7@1029 : Record 2000000182;
      MediaResourcesPageData8@1028 : Record 2000000182;
      MediaResourcesPageData9@1032 : Record 2000000182;
      O365GettingStartedMgt@1002 : Codeunit 1309;
      UserTours@1019 : DotNet "'Microsoft.Dynamics.Nav.ClientExtensions, Version=11.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35'.Microsoft.Dynamics.Nav.Client.Capabilities.UserTours" WITHEVENTS RUNONCLIENT;
      CurrentPage@1003 : Integer INDATASET;
      NextPageVisible@1004 : Boolean;
      FirstRun@1001 : Boolean;
      BodyText@1005 : BigText;
      LastPageIndex@1006 : Integer;
      MaxNumberOfSteps@1021 : Integer;
      DisableConfirmationOnPageClose@1026 : Boolean;
      UserToursAreNotAvailableMsg@1016 : TextConst 'ENU=Tours are not available for this installation.;ENG=Tours are not available for this installation.';
      GettingStartedPageTxt@1017 : TextConst 'ENU=GETTING STARTED;ENG=GETTING STARTED';
      StepCaptionTxt@1020 : TextConst '@@@=%1 Current page number, %2 total number of pages;ENU=(%1 OF %2);ENG=(%1 OF %2)';
      OutlookSetupCompleteMsg@1008 : TextConst '@@@=%1 Product name;ENU=Your Business Inbox is all set up. When you''re ready, you can start using %1 in Outlook.;ENG=Your Business Inbox is all set up. When you''re ready, you can start using %1 in Outlook.';
      DocumentationLinkCaptionLbl@1022 : TextConst '@@@={Locked};ENU=https://aka.ms/learnbusinessedition;ENG=https://aka.ms/learnbusinessedition';
      AccountantTxt@1018 : TextConst 'ENU=Accountant;ENG=Accountant';

    LOCAL PROCEDURE InvokeWalkMeButton@1006(TourID@1000 : Integer);
    BEGIN
      IF (NOT UserTours.IsAvailable) OR (NOT O365GettingStartedMgt.AreUserToursEnabled) THEN BEGIN
        MESSAGE(UserToursAreNotAvailableMsg);
        CurrPage.CLOSE;
        EXIT;
      END;

      IF (TourID <> O365GettingStartedMgt.GetWizardDoneTourID) AND
         (TourID <> O365GettingStartedMgt.GetChangeCompanyTourID)
      THEN BEGIN
        "Current Page" := CurrentPage;
        "Tour in Progress" := TRUE;
        MODIFY;
      END;

      UserTours.StartUserTour(TourID);
      DisableConfirmationOnPageClose := TRUE;
      CurrPage.CLOSE;
    END;

    LOCAL PROCEDURE UpdatePageControls@1001();
    BEGIN
      GetBodyTextForCurrentPage;
    END;

    LOCAL PROCEDURE MarkWizardAsDone@1007();
    BEGIN
      "Tour in Progress" := FALSE;
      "Tour Completed" := TRUE;
      MODIFY;

      IF UserTours.IsAvailable AND O365GettingStartedMgt.AreUserToursEnabled THEN
        UserTours.StopNotifyShowTourWizard;
    END;

    LOCAL PROCEDURE GetBodyTextForCurrentPage@1002();
    VAR
      BodyTextO365GettingStartedPageData@1001 : Record 1308;
      MediaResourcesMgt@1000 : Codeunit 9755;
      TempBodyText@1003 : Text;
    BEGIN
      IF BodyTextO365GettingStartedPageData.GetPageBodyText(BodyTextO365GettingStartedPageData,CurrentPage,PAGE::"O365 Getting Started") THEN BEGIN
        TempBodyText := MediaResourcesMgt.ReadTextFromMediaResource(BodyTextO365GettingStartedPageData."Media Resources Ref");
        IF TempBodyText = '' THEN
          EXIT;
        CLEAR(BodyText);
        BodyText.ADDTEXT(STRSUBSTNO(TempBodyText,PRODUCTNAME.MARKETING));
      END;
    END;

    LOCAL PROCEDURE LoadImages@1004();
    VAR
      Index@1000 : Integer;
    BEGIN
      Index := 1;
      LoadImage(O365GettingStartedPageData1,Index);
      IF MediaResourcesPageData1.GET(O365GettingStartedPageData1."Media Resources Ref") THEN;
      LoadImage(O365GettingStartedPageData2,Index);
      IF MediaResourcesPageData2.GET(O365GettingStartedPageData2."Media Resources Ref") THEN;
      LoadImage(O365GettingStartedPageData3,Index);
      IF MediaResourcesPageData3.GET(O365GettingStartedPageData3."Media Resources Ref") THEN;
      LoadImage(O365GettingStartedPageData4,Index);
      IF MediaResourcesPageData4.GET(O365GettingStartedPageData4."Media Resources Ref") THEN;
      LoadImage(O365GettingStartedPageData5,Index);
      IF MediaResourcesPageData5.GET(O365GettingStartedPageData5."Media Resources Ref") THEN;
      LoadImage(O365GettingStartedPageData6,Index);
      IF MediaResourcesPageData6.GET(O365GettingStartedPageData6."Media Resources Ref") THEN;
      LoadImage(O365GettingStartedPageData7,Index);
      IF MediaResourcesPageData7.GET(O365GettingStartedPageData7."Media Resources Ref") THEN;
      LoadImage(O365GettingStartedPageData8,Index);
      IF MediaResourcesPageData8.GET(O365GettingStartedPageData8."Media Resources Ref") THEN;
      LoadImage(O365GettingStartedPageData9,Index);
      IF MediaResourcesPageData9.GET(O365GettingStartedPageData9."Media Resources Ref") THEN;
    END;

    LOCAL PROCEDURE LoadImage@1005(VAR O365GettingStartedPageData@1000 : Record 1308;VAR CurrentPageID@1001 : Integer);
    BEGIN
      O365GettingStartedPageData.GetPageImage(O365GettingStartedPageData,CurrentPageID,PAGE::"O365 Getting Started");
      CurrentPageID += 1;
    END;

    [External]
    PROCEDURE GetNextPageID@1014(Increment@1000 : Integer;CurrentPageID@1002 : Integer) NextPageID : Integer;
    BEGIN
      NextPageID := CurrentPageID + Increment;

      // Pages 3,4 and 8 are currently skipped in the wizard. They should be removed.
      IF NextPageID IN [3,4] THEN
        NextPageID += Increment * 2;

      IF NextPageID IN [8] THEN
        NextPageID += Increment;
    END;

    LOCAL PROCEDURE SetPageCaption@1003();
    VAR
      StepIndex@1000 : Integer;
    BEGIN
      CurrPage.CAPTION := GettingStartedPageTxt;
      IF CurrentPage IN [1,LastPageIndex] THEN
        EXIT;

      StepIndex := CurrentPage;

      IF CurrentPage IN [5,6,7] THEN
        StepIndex := CurrentPage - 2;

      CurrPage.CAPTION := STRSUBSTNO('%1 %2',GettingStartedPageTxt,STRSUBSTNO(StepCaptionTxt,StepIndex,MaxNumberOfSteps));
    END;

    LOCAL PROCEDURE AddToOutlook@1008();
    VAR
      AssistedSetup@1003 : Record 1803;
      OfficeAddin@1001 : Record 1610;
      ExchangeAddinSetup@1002 : Codeunit 5323;
    BEGIN
      IF ExchangeAddinSetup.PromptForCredentials THEN BEGIN
        ExchangeAddinSetup.DeployAddins(OfficeAddin);
        ExchangeAddinSetup.DeploySampleEmails;
        AssistedSetup.SetStatus(PAGE::"Exchange Setup Wizard",AssistedSetup.Status::Completed);
        MESSAGE(STRSUBSTNO(OutlookSetupCompleteMsg,PRODUCTNAME.FULL));
      END;
    END;

    EVENT UserTours@1019::ShowTourWizard@13(hasTourCompleted@1000 : Boolean);
    BEGIN
    END;

    EVENT UserTours@1019::IsTourInProgressResultReady@14(isInProgress@1000 : Boolean);
    BEGIN
    END;

    BEGIN
    END.
  }
}

