OBJECT Page 1561 Pick Report
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Pick Report;
               ENG=Pick Report];
    PageType=StandardDialog;
    OnOpenPage=BEGIN
                 ObjectOptions."Object Type" := ObjectOptions."Object Type"::Report;
                 ObjectOptions."Company Name" := COMPANYNAME;
                 ObjectOptions."User Name" := USERID;
                 ObjectOptions."Created By" := USERID;
               END;

  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                Name=ReportSettings;
                ContainerType=ContentArea }

    { 7   ;1   ;Group     ;
                Name=Settings;
                CaptionML=[ENU=Settings;
                           ENG=Settings];
                GroupType=Group }

    { 4   ;2   ;Field     ;
                Name=Name;
                CaptionML=[ENU=Name;
                           ENG=Name];
                ToolTipML=[ENU=Specifies the name of the new report settings entry.;
                           ENG=Specifies the name of the new report settings entry.];
                ApplicationArea=#Basic,#Suite;
                NotBlank=Yes;
                SourceExpr=ObjectOptions."Parameter Name" }

    { 3   ;2   ;Field     ;
                Name=Report ID;
                CaptionML=[ENU=Report ID;
                           ENG=Report ID];
                ToolTipML=[ENU=Specifies the ID of the report that uses the settings.;
                           ENG=Specifies the ID of the report that uses the settings.];
                ApplicationArea=#Basic,#Suite;
                NotBlank=Yes;
                SourceExpr=ObjectOptions."Object ID";
                TableRelation="Report Metadata".ID }

    { 5   ;2   ;Field     ;
                Name=Company Name;
                CaptionML=[ENU=Company Name;
                           ENG=Company Name];
                ToolTipML=[ENU=Specifies the company to which the report settings belong.;
                           ENG=Specifies the company to which the report settings belong.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=ObjectOptions."Company Name";
                TableRelation=Company.Name }

    { 6   ;2   ;Field     ;
                Name=Shared with All Users;
                CaptionML=[ENU=Shared with All Users;
                           ENG=Shared with All Users];
                ToolTipML=[ENU=Specifies whether the report settings are available to all users or only the user assigned to the settings.;
                           ENG=Specifies whether the report settings are available to all users or only the user assigned to the settings.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=ObjectOptions."Public Visible" }

  }
  CODE
  {
    VAR
      ObjectOptions@1000 : Record 2000000196;

    [Internal]
    PROCEDURE GetObjectOptions@11(VAR ObjectOptionsToReturn@1000 : Record 2000000196);
    BEGIN
      ObjectOptionsToReturn := ObjectOptions;
    END;

    BEGIN
    END.
  }
}

