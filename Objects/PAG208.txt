OBJECT Page 208 Work Types
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Work Types;
               ENG=Work Types];
    SourceTable=Table200;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code to identify the work type.;
                           ENG=Specifies a code to identify the work type.];
                ApplicationArea=#Jobs;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of this work type.;
                           ENG=Specifies a description of this work type.];
                ApplicationArea=#Jobs;
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how each unit of the item or resource is measured, such as in pieces or hours. By default, the value in the Base Unit of Measure field on the item or resource card is inserted.;
                           ENG=Specifies how each unit of the item or resource is measured, such as in pieces or hours. By default, the value in the Base Unit of Measure field on the item or resource card is inserted.];
                ApplicationArea=#Jobs;
                SourceExpr="Unit of Measure Code" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

