OBJECT Page 2134 O365 Import Export Settings
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Export and Synchronization;
               ENG=Export and Synchronisation];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table2132;
    PageType=List;
    SourceTableTemporary=Yes;
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 InsertMenuItems;
               END;

    OnAfterGetRecord=BEGIN
                       IF "Page ID" = PAGE::"O365 Sync with Microsoft Apps" THEN
                         Description := GetGraphStatus;
                     END;

    ActionList=ACTIONS
    {
      { 4       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 6       ;1   ;Action    ;
                      Name=Open;
                      ShortCutKey=Return;
                      CaptionML=[ENU=Open;
                                 ENG=Open];
                      ToolTipML=[ENU=Open the card for the selected record.;
                                 ENG=Open the card for the selected record.];
                      ApplicationArea=#Basic,#Suite,#Invoicing;
                      Image=DocumentEdit;
                      Scope=Repeater;
                      OnAction=BEGIN
                                 OpenPage;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=Title }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the import export setting.;
                           ENG=Specifies a description of the import export setting.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=Description }

  }
  CODE
  {
    VAR
      SyncMSAppsLbl@1004 : TextConst 'ENU=Sync with Microsoft Apps;ENG=Sync with Microsoft Apps';
      EnabledLbl@1005 : TextConst 'ENU=Enabled;ENG=Enabled';
      DisabledLbl@1006 : TextConst 'ENU=Disabled;ENG=Disabled';
      ExportTitleLbl@1002 : TextConst 'ENU=Export invoices;ENG=Export invoices';
      ExportDescriptionLbl@1003 : TextConst 'ENU=Export and send invoices.;ENG=Export and send invoices.';
      ImportCustomersTieleLbl@1000 : TextConst 'ENU=Import customers;ENG=Import customers';
      ImportCustomersDesriptionLbl@1001 : TextConst 'ENU=Import customers from Excel;ENG=Import customers from Excel';
      ImportItemsTieleLbl@1008 : TextConst 'ENU=Import prices;ENG=Import prices';
      ImportItemsDesriptionLbl@1007 : TextConst 'ENU=Import prices from Excel;ENG=Import prices from Excel';

    LOCAL PROCEDURE InsertMenuItems@1();
    VAR
      DummyCustomer@1002 : Record 18;
      DummyItem@1001 : Record 27;
      ClientTypeManagement@1000 : Codeunit 4;
    BEGIN
      InsertPageMenuItem(PAGE::"O365 Export Invoices",ExportTitleLbl,ExportDescriptionLbl);
      OnInsertMenuItems(Rec);
      InsertPageMenuItem(PAGE::"O365 Sync with Microsoft Apps",SyncMSAppsLbl,'');

      IF ClientTypeManagement.GetCurrentClientType <> CLIENTTYPE::Phone THEN BEGIN
        InsertPageWithParameterMenuItem(
          PAGE::"O365 Import from Excel Wizard",
          DummyCustomer.TABLENAME,
          ImportCustomersTieleLbl,
          ImportCustomersDesriptionLbl);
        InsertPageWithParameterMenuItem(
          PAGE::"O365 Import from Excel Wizard",
          DummyItem.TABLENAME,
          ImportItemsTieleLbl,
          ImportItemsDesriptionLbl);
      END;
    END;

    [Integration]
    [External]
    LOCAL PROCEDURE OnInsertMenuItems@2(VAR O365SettingsMenu@1000 : Record 2132);
    BEGIN
    END;

    LOCAL PROCEDURE GetGraphStatus@3() : Text[80];
    VAR
      MarketingSetup@1000 : Record 5079;
    BEGIN
      IF MarketingSetup.GET AND MarketingSetup."Sync with Microsoft Graph" THEN
        EXIT(EnabledLbl);
      EXIT(DisabledLbl);
    END;

    BEGIN
    END.
  }
}

