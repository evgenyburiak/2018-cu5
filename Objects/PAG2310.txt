OBJECT Page 2310 BC O365 Sales Invoice
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Draft Invoice;
               ENG=Draft Invoice];
    DeleteAllowed=No;
    SourceTable=Table36;
    DataCaptionExpr='';
    SourceTableView=WHERE(Document Type=CONST(Invoice));
    PageType=Document;
    RefreshOnActivate=Yes;
    OnOpenPage=VAR
                 O365SalesInitialSetup@6115 : Record 2110;
                 CalendarEventMangement@1000 : Codeunit 2160;
               BEGIN
                 IsUsingVAT := O365SalesInitialSetup.IsUsingVAT;

                 CalendarEventMangement.CreateCalendarEventForCodeunit(TODAY,SyncBusinessInfoTxt,CODEUNIT::"Graph Sync. - Business Profile");
               END;

    OnNewRecord=BEGIN
                  CustomerName := '';
                  CustomerEmail := '';
                  WorkDescription := '';
                  "Document Type" := "Document Type"::Invoice;

                  SetDefaultPaymentServices;
                END;

    OnDeleteRecord=VAR
                     CustInvoiceDisc@1000 : Record 19;
                   BEGIN
                     ForceExit := TRUE;

                     IF CustInvoiceDisc.GET("Invoice Disc. Code","Currency Code",0) THEN
                       CustInvoiceDisc.DELETE;
                   END;

    OnQueryClosePage=BEGIN
                       EXIT(O365SalesInvoiceMgmt.OnQueryCloseForSalesHeader(Rec,ForceExit,CustomerName));
                     END;

    OnAfterGetCurrRecord=BEGIN
                           O365SalesInvoiceMgmt.UpdateCustomerFields(Rec,CustomerName,CustomerEmail);
                           WorkDescription := GetWorkDescription;
                           CurrPageEditable := CurrPage.EDITABLE;
                           O365SalesInvoiceMgmt.OnAfterGetSalesHeaderRecord(Rec,CurrencyFormat,TaxAreaDescription);
                           O365SalesInvoiceMgmt.UpdateNoOfAttachmentsLabel(O365SalesAttachmentMgt.GetNoOfAttachments(Rec),NoOfAttachmentsValueTxt);
                           O365SalesInvoiceMgmt.UpdateAddress(Rec,FullAddress);
                           O365SalesInvoiceMgmt.CalcInvoiceDiscountAmount(Rec,SubTotalAmount,DiscountTxt,InvoiceDiscountAmount,InvDiscAmountVisible);
                           IsCompanyContact := O365SalesInvoiceMgmt.IsCustomerCompanyContact("Sell-to Customer No.");
                           O365SalesInvoiceMgmt.NotifyOrWarnAboutCoupons(Rec);
                           O365SalesInvoiceMgmt.GetCouponCodesAndCouponsExists(Rec,CouponCodes,CouponsExistsForCustomer);
                         END;

    ActionList=ACTIONS
    {
      { 22      ;    ;ActionContainer;
                      CaptionML=[ENU=Invoice;
                                 ENG=Invoice];
                      ActionContainerType=ActionItems }
      { 23      ;1   ;Action    ;
                      Name=Post;
                      ShortCutKey=Ctrl+Right;
                      CaptionML=[ENU=Send;
                                 ENG=Send];
                      ToolTipML=[ENU=Finalize and send the invoice.;
                                 ENG=Finalise and send the invoice.];
                      ApplicationArea=#Basic,#Suite,#Invoicing;
                      Promoted=Yes;
                      Enabled=CustomerName <> '';
                      PromotedIsBig=Yes;
                      Image=Invoicing-MDL-Send;
                      PromotedCategory=Process;
                      PromotedOnly=Yes;
                      OnAction=VAR
                                 O365SendResendInvoice@1001 : Codeunit 2104;
                               BEGIN
                                 IF O365SendResendInvoice.SendSalesInvoiceOrQuoteFromBC(Rec) THEN BEGIN
                                   ForceExit := TRUE;
                                   CurrPage.CLOSE;
                                 END;
                               END;
                                }
      { 3       ;1   ;Action    ;
                      Name=ViewPdf;
                      CaptionML=[ENU=Preview;
                                 ENG=Preview];
                      ToolTipML=[ENU=View the preview of the invoice before sending.;
                                 ENG=View the preview of the invoice before sending.];
                      ApplicationArea=#Basic,#Suite,#Invoicing;
                      Promoted=Yes;
                      Enabled=CustomerName <> '';
                      Image=Invoicing-MDL-PreviewDoc;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ReportSelections@1001 : Record 77;
                                 DocumentPath@1000 : Text[250];
                               BEGIN
                                 SETRECFILTER;
                                 LOCKTABLE;
                                 FIND;
                                 ReportSelections.GetPdfReport(DocumentPath,ReportSelections.Usage::"S.Invoice Draft",Rec,"Sell-to Customer No.");
                                 DOWNLOAD(DocumentPath,'','','',DocumentPath);
                                 FIND;
                               END;
                                }
      { 4       ;1   ;Action    ;
                      Name=DeleteAction;
                      CaptionML=[ENU=Discard;
                                 ENG=Discard];
                      ToolTipML=[ENU=Discards the draft invoice;
                                 ENG=Discards the draft invoice];
                      ApplicationArea=#Basic,#Suite,#Invoicing;
                      Promoted=Yes;
                      Image=Invoicing-MDL-Delete;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 CustInvoiceDisc@1000 : Record 19;
                               BEGIN
                                 IF NOT FIND THEN BEGIN
                                   CurrPage.CLOSE;
                                   EXIT;
                                 END;

                                 IF NOT CONFIRM(DeleteQst) THEN
                                   EXIT;

                                 ForceExit := TRUE;

                                 IF CustInvoiceDisc.GET("Invoice Disc. Code","Currency Code",0) THEN
                                   CustInvoiceDisc.DELETE;
                                 DELETE(TRUE);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 25  ;1   ;Group     ;
                CaptionML=[ENU=Sell to;
                           ENG=Sell to];
                GroupType=Group }

    { 9   ;2   ;Field     ;
                CaptionML=[ENU=Customer Name;
                           ENG=Customer Name];
                ToolTipML=[ENU=Specifies the customer's name.;
                           ENG=Specifies the customer's name.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                NotBlank=Yes;
                SourceExpr="Sell-to Customer Name";
                Importance=Promoted;
                LookupPageID=BC O365 Contact Lookup;
                OnValidate=BEGIN
                             O365SalesInvoiceMgmt.ValidateCustomerName(Rec,CustomerName,CustomerEmail);
                             O365SalesInvoiceMgmt.UpdateAddress(Rec,FullAddress);
                             CurrPage.UPDATE(TRUE);
                             O365SalesInvoiceMgmt.CustomerChanged;
                             O365SalesInvoiceMgmt.NotifyOrWarnAboutCoupons(Rec);
                             O365SalesInvoiceMgmt.GetCouponCodesAndCouponsExists(Rec,CouponCodes,CouponsExistsForCustomer);
                           END;

                ShowMandatory=TRUE;
                QuickEntry=FALSE }

    { 18  ;2   ;Field     ;
                ExtendedDatatype=E-Mail;
                CaptionML=[ENU=Email Address;
                           ENG=Email Address];
                ToolTipML=[ENU=Specifies the customer's email address.;
                           ENG=Specifies the customer's email address.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=CustomerEmail;
                Editable=CurrPageEditable AND (CustomerName <> '');
                OnValidate=BEGIN
                             O365SalesInvoiceMgmt.ValidateCustomerEmail(Rec,CustomerEmail);
                           END;
                            }

    { 24  ;2   ;Group     ;
                Visible=CustomerName <> '';
                GroupType=Group }

    { 16  ;3   ;Field     ;
                Name=ViewContactCard;
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=ViewContactDetailsLbl;
                Editable=FALSE;
                OnDrillDown=VAR
                              Customer@1000 : Record 18;
                            BEGIN
                              IF Customer.GET("Sell-to Customer No.") THEN
                                PAGE.RUNMODAL(PAGE::"BC O365 Sales Customer Card",Customer);
                            END;

                ShowCaption=No }

    { 11  ;2   ;Group     ;
                GroupType=Group }

    { 15  ;3   ;Field     ;
                CaptionML=[ENU=Address;
                           ENG=Address];
                ToolTipML=[ENU=Specifies the address where the customer is located.;
                           ENG=Specifies the address where the customer is located.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Sell-to Address";
                Editable=CustomerName <> '';
                OnValidate=BEGIN
                             O365SalesInvoiceMgmt.ValidateCustomerAddress("Sell-to Address","Sell-to Customer No.");
                             O365SalesInvoiceMgmt.UpdateAddress(Rec,FullAddress);
                           END;
                            }

    { 12  ;3   ;Field     ;
                CaptionML=[ENU=Address 2;
                           ENG=Address 2];
                ToolTipML=[ENU=Specifies additional address information.;
                           ENG=Specifies additional address information.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Sell-to Address 2";
                Editable=CustomerName <> '';
                OnValidate=BEGIN
                             O365SalesInvoiceMgmt.ValidateCustomerAddress2("Sell-to Address 2","Sell-to Customer No.");
                             O365SalesInvoiceMgmt.UpdateAddress(Rec,FullAddress);
                           END;
                            }

    { 5   ;3   ;Field     ;
                Lookup=No;
                CaptionML=[ENU=Post Code;
                           ENG=Postcode];
                ToolTipML=[ENU=Specifies the postal code.;
                           ENG=Specifies the postcode.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Sell-to Post Code";
                Editable=CustomerName <> '';
                OnValidate=BEGIN
                             O365SalesInvoiceMgmt.ValidateCustomerPostCode("Sell-to Post Code","Sell-to Customer No.");
                             O365SalesInvoiceMgmt.UpdateAddress(Rec,FullAddress);
                           END;
                            }

    { 2   ;3   ;Field     ;
                Lookup=No;
                CaptionML=[ENU=City;
                           ENG=City];
                ToolTipML=[ENU=Specifies the address city.;
                           ENG=Specifies the address city.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Sell-to City";
                Editable=CustomerName <> '';
                OnValidate=BEGIN
                             O365SalesInvoiceMgmt.ValidateCustomerCity("Sell-to City","Sell-to Customer No.");
                             O365SalesInvoiceMgmt.UpdateAddress(Rec,FullAddress);
                           END;
                            }

    { 8   ;3   ;Field     ;
                Lookup=No;
                CaptionML=[ENU=County;
                           ENG=County];
                ToolTipML=[ENU=Specifies the address county.;
                           ENG=Specifies the address county.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Sell-to County";
                Editable=CustomerName <> '';
                OnValidate=BEGIN
                             O365SalesInvoiceMgmt.ValidateCustomerCounty("Sell-to County","Sell-to Customer No.");
                             O365SalesInvoiceMgmt.UpdateAddress(Rec,FullAddress);
                           END;
                            }

    { 33  ;3   ;Field     ;
                CaptionML=[ENU=Country/Region Code;
                           ENG=Country/Region Code];
                ToolTipML=[ENU=Specifies the address country/region.;
                           ENG=Specifies the address country/region.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Sell-to Country/Region Code";
                Editable=CustomerName <> '';
                LookupPageID=BC O365 Country/Region List;
                OnValidate=BEGIN
                             O365SalesInvoiceMgmt.ValidateCustomerCountryRegion("Sell-to Country/Region Code","Sell-to Customer No.");
                             O365SalesInvoiceMgmt.UpdateAddress(Rec,FullAddress);
                           END;
                            }

    { 20  ;1   ;Group     ;
                CaptionML=[ENU=Invoice Details;
                           ENG=Invoice Details];
                GroupType=Group }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies when the sales invoice must be paid.;
                           ENG=Specifies when the sales invoice must be paid.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Due Date";
                Editable=CustomerName <> '';
                OnValidate=BEGIN
                             IF "Due Date" < "Document Date" THEN
                               VALIDATE("Due Date","Document Date");
                           END;
                            }

    { 6   ;2   ;Field     ;
                CaptionML=[ENU=Invoice Date;
                           ENG=Invoice Date];
                ToolTipML=[ENU=Specifies when the sales invoice was created.;
                           ENG=Specifies when the sales invoice was created.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Document Date";
                Editable=CustomerName <> '';
                OnValidate=VAR
                             NotificationLifecycleMgt@1001 : Codeunit 1511;
                             PastNotification@1000 : Notification;
                           BEGIN
                             VALIDATE("Posting Date","Document Date");

                             IF "Document Date" < WORKDATE THEN BEGIN
                               PastNotification.ID := CREATEGUID;
                               PastNotification.MESSAGE(DocumentDatePastMsg);
                               PastNotification.SCOPE(NOTIFICATIONSCOPE::LocalScope);
                               NotificationLifecycleMgt.SendNotification(PastNotification,RECORDID);
                             END;
                             O365SalesInvoiceMgmt.InvoiceDateChanged(Rec);
                           END;
                            }

    { 19  ;2   ;Field     ;
                CaptionML=[ENU=Customer is tax liable;
                           ENG=Customer is tax liable];
                ToolTipML=[ENU=Specifies if the sales invoice contains sales tax.;
                           ENG=Specifies if the sales invoice contains VAT.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Tax Liable";
                Visible=NOT IsUsingVAT;
                Editable=CustomerName <> '';
                OnValidate=BEGIN
                             CurrPage.UPDATE;
                           END;
                            }

    { 17  ;2   ;Field     ;
                Name=TaxAreaDescription;
                CaptionML=[ENU=Customer tax rate;
                           ENG=Customer tax rate];
                ToolTipML=[ENU=Specifies the customer's tax area.;
                           ENG=Specifies the customer's tax area.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                NotBlank=Yes;
                SourceExpr=TaxAreaDescription;
                Visible=NOT IsUsingVAT;
                Enabled=CustomerName <> '';
                Editable=FALSE;
                OnAssistEdit=VAR
                               TaxArea@1000 : Record 318;
                             BEGIN
                               IF PAGE.RUNMODAL(PAGE::"O365 Tax Area List",TaxArea) = ACTION::LookupOK THEN BEGIN
                                 VALIDATE("Tax Area Code",TaxArea.Code);
                                 TaxAreaDescription := TaxArea.GetDescriptionInCurrentLanguage;
                                 CurrPage.UPDATE;
                               END;
                             END;

                QuickEntry=FALSE }

    { 37  ;2   ;Field     ;
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="VAT Registration No.";
                Visible=IsUsingVAT;
                Editable=(IsUsingVAT AND IsCompanyContact) }

    { 14  ;1   ;Part      ;
                Name=Lines;
                CaptionML=[ENU=Line Items;
                           ENG=Line Items];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SubPageLink=Document Type=FIELD(Document Type),
                            Document No.=FIELD(No.);
                PagePartID=Page2311;
                Enabled=CustomerName <> '';
                Editable=CustomerName <> '';
                PartType=Page }

    { 40  ;1   ;Group     ;
                CaptionML=[ENU=Totals;
                           ENG=Totals];
                Visible=NOT InvDiscAmountVisible;
                GroupType=Group }

    { 39  ;2   ;Group     ;
                GroupType=Group }

    { 35  ;3   ;Field     ;
                Name=Amount;
                Lookup=No;
                DrillDown=No;
                CaptionML=[ENU=Net Total;
                           ENG=Net Total];
                ToolTipML=[ENU=Specifies the total amount on the sales invoice excluding VAT.;
                           ENG=Specifies the total amount on the sales invoice excluding VAT.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=Amount;
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormat }

    { 34  ;3   ;Field     ;
                Name=AmountInclVAT;
                Lookup=No;
                DrillDown=No;
                CaptionML=[ENU=Total Including VAT;
                           ENG=Total Including VAT];
                ToolTipML=[ENU=Specifies the total amount on the sales invoice including VAT.;
                           ENG=Specifies the total amount on the sales invoice including VAT.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Amount Including VAT";
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormat;
                Importance=Promoted;
                Style=Strong;
                StyleExpr=TRUE }

    { 38  ;3   ;Group     ;
                Visible=CouponsExistsForCustomer;
                GroupType=Group }

    { 42  ;4   ;Field     ;
                Name=CouponCodes;
                CaptionML=[ENU=Coupons;
                           ENG=Coupons];
                ToolTipML=[ENU=Specifies the coupons that are used on this invoice.;
                           ENG=Specifies the coupons that are used on this invoice.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=CouponCodes;
                Editable=FALSE;
                OnDrillDown=VAR
                              DisplayCoupons@1000 : Codeunit 2115;
                            BEGIN
                              DisplayCoupons.ShowCouponsForDocument("Document Type","No.");
                            END;
                             }

    { 36  ;3   ;Field     ;
                Name=DiscountLink;
                DrillDown=Yes;
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=DiscountTxt;
                Enabled=CustomerName <> '';
                Editable=FALSE;
                OnDrillDown=BEGIN
                              IF PAGE.RUNMODAL(PAGE::"O365 Sales Invoice Discount",Rec) = ACTION::LookupOK THEN
                                CurrPage.UPDATE;
                            END;

                ShowCaption=No }

    { 30  ;1   ;Group     ;
                CaptionML=[ENU=Totals;
                           ENG=Totals];
                Visible=InvDiscAmountVisible;
                GroupType=Group }

    { 32  ;2   ;Group     ;
                GroupType=Group }

    { 31  ;3   ;Field     ;
                CaptionML=[ENU=Subtotal;
                           ENG=Subtotal];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=SubTotalAmount;
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormat;
                Importance=Promoted;
                Editable=FALSE }

    { 29  ;3   ;Field     ;
                Name=InvoiceDiscount;
                CaptionML=[ENU=Invoice Discount;
                           ENG=Invoice Discount];
                ToolTipML=[ENU=Specifies the invoice discount amount. To edit the invoice discount, click on the amount.;
                           ENG=Specifies the invoice discount amount. To edit the invoice discount, click on the amount.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=-InvoiceDiscountAmount;
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormat;
                CaptionClass=GetInvDiscountCaption;
                Importance=Promoted;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              IF PAGE.RUNMODAL(PAGE::"O365 Sales Invoice Discount",Rec) = ACTION::LookupOK THEN
                                CurrPage.UPDATE;
                            END;
                             }

    { 28  ;3   ;Field     ;
                Name=Amount2;
                Lookup=No;
                DrillDown=No;
                CaptionML=[ENU=Net Total;
                           ENG=Net Total];
                ToolTipML=[ENU=Specifies the total amount on the sales invoice excluding VAT.;
                           ENG=Specifies the total amount on the sales invoice excluding VAT.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=Amount;
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormat }

    { 21  ;3   ;Field     ;
                Name=AmountInclVAT2;
                Lookup=No;
                DrillDown=No;
                CaptionML=[ENU=Total Including VAT;
                           ENG=Total Including VAT];
                ToolTipML=[ENU=Specifies the total amount on the sales invoice including VAT.;
                           ENG=Specifies the total amount on the sales invoice including VAT.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Amount Including VAT";
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormat;
                Importance=Promoted;
                Style=Strong;
                StyleExpr=TRUE }

    { 10  ;3   ;Group     ;
                Visible=CouponsExistsForCustomer;
                GroupType=Group }

    { 41  ;4   ;Field     ;
                Name=CouponCodes2;
                CaptionML=[ENU=Coupons;
                           ENG=Coupons];
                ToolTipML=[ENU=Specifies the coupons that are used on this invoice.;
                           ENG=Specifies the coupons that are used on this invoice.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=CouponCodes;
                Editable=FALSE;
                OnDrillDown=VAR
                              DisplayCoupons@1000 : Codeunit 2115;
                            BEGIN
                              DisplayCoupons.ShowCouponsForDocument("Document Type","No.");
                            END;
                             }

    { 7   ;1   ;Group     ;
                CaptionML=[ENU=Note for customer;
                           ENG=Note for customer];
                GroupType=Group }

    { 27  ;2   ;Field     ;
                CaptionML=[ENU=Note for customer;
                           ENG=Note for customer];
                ToolTipML=[ENU=Specifies the products or service being offered;
                           ENG=Specifies the products or service being offered];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=WorkDescription;
                Editable=CurrPageEditable AND (CustomerName <> '');
                MultiLine=Yes;
                OnValidate=BEGIN
                             SetWorkDescription(WorkDescription);
                           END;

                ShowCaption=No }

    { 13  ;1   ;Field     ;
                Name=NoOfAttachments;
                DrillDown=Yes;
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=NoOfAttachmentsValueTxt;
                Enabled=CustomerName <> '';
                Editable=FALSE;
                OnDrillDown=BEGIN
                              O365SalesInvoiceMgmt.UpdateNoOfAttachmentsLabel(O365SalesAttachmentMgt.EditAttachments(Rec),NoOfAttachmentsValueTxt);
                              CurrPage.UPDATE(FALSE);
                            END;

                ShowCaption=No }

  }
  CODE
  {
    VAR
      O365SalesAttachmentMgt@1006 : Codeunit 2112;
      O365SalesInvoiceMgmt@1023 : Codeunit 2310;
      CustomerName@1000 : Text[50];
      CustomerEmail@1004 : Text[80];
      WorkDescription@1002 : Text;
      FullAddress@1016 : Text;
      CurrPageEditable@1003 : Boolean;
      IsUsingVAT@6116 : Boolean;
      ForceExit@1012 : Boolean;
      NoOfAttachmentsValueTxt@1011 : Text;
      CurrencyFormat@1013 : Text;
      IsCompanyContact@1090 : Boolean;
      InvDiscAmountVisible@1024 : Boolean;
      InvoiceDiscountAmount@1001 : Decimal;
      CouponsExistsForCustomer@1014 : Boolean;
      SubTotalAmount@1017 : Decimal;
      DiscountTxt@1005 : Text;
      DeleteQst@1009 : TextConst 'ENU=Are you sure?;ENG=Are you sure?';
      TaxAreaDescription@1018 : Text[50];
      SyncBusinessInfoTxt@1010 : TextConst 'ENU=Delta sync Business Information;ENG=Delta sync Business Information';
      DocumentDatePastMsg@1008 : TextConst 'ENU=Invoice date is in the past.;ENG=Invoice date is in the past.';
      ViewContactDetailsLbl@1007 : TextConst 'ENU=Open contact details;ENG=Open contact details';
      CouponCodes@1015 : Text;

    PROCEDURE SuppressExitPrompt@5();
    BEGIN
      ForceExit := TRUE;
    END;

    LOCAL PROCEDURE GetInvDiscountCaption@8() : Text;
    BEGIN
      EXIT(O365SalesInvoiceMgmt.GetInvoiceDiscountCaption(ROUND("Invoice Discount Value",0.1)));
    END;

    BEGIN
    END.
  }
}

