OBJECT Page 2313 BCO365 Posted Sales Invoice
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Sent Invoice;
               ENG=Sent Invoice];
    SourceTable=Table112;
    PageType=Document;
    RefreshOnActivate=Yes;
    OnInit=VAR
             O365SalesInitialSetup@6115 : Record 2110;
           BEGIN
             IsUsingVAT := O365SalesInitialSetup.IsUsingVAT;
           END;

    OnAfterGetRecord=BEGIN
                       GetDocumentStatus;
                     END;

    OnAfterGetCurrRecord=VAR
                           DummyCustLedgerEntry@1001 : Record 21;
                           Customer@1002 : Record 18;
                           O365PostedCouponClaim@1008 : Record 2117;
                           TaxArea@1007 : Record 318;
                           TempStandardAddress@1006 : TEMPORARY Record 730;
                           O365SalesInvoicePayment@1000 : Codeunit 2105;
                           O365DocumentSendMgt@1003 : Codeunit 2158;
                         BEGIN
                           IsFullyPaid := O365SalesInvoicePayment.GetPaymentCustLedgerEntry(DummyCustLedgerEntry,"No.");
                           WorkDescription := GetWorkDescription;
                           UpdateNoOfAttachmentsLabel(O365SalesAttachmentMgt.GetNoOfAttachments(Rec));
                           InvoiceCancelled := O365SalesCancelInvoice.IsInvoiceCanceled(Rec);
                           IF Customer.GET("Sell-to Customer No.") THEN
                             CustomerEmail := Customer."E-Mail";
                           UpdateCurrencyFormat;
                           IF TaxArea.GET("Tax Area Code") THEN
                             TaxAreaDescription := TaxArea.GetDescriptionInCurrentLanguage;
                           TempStandardAddress.CopyFromSalesInvoiceHeaderSellTo(Rec);
                           CalcInvoiceDiscount;
                           DiscountVisible := InvoiceDiscountAmount <> 0;
                           CouponCodes := O365PostedCouponClaim.GetAppliedClaimsForSalesInvoice(Rec);

                           O365DocumentSendMgt.ShowInvoiceFailedNotificationOnOpenPage(Rec);
                         END;

    ActionList=ACTIONS
    {
      { 22      ;    ;ActionContainer;
                      CaptionML=[ENU=Invoice;
                                 ENG=Invoice];
                      ActionContainerType=ActionItems }
      { 5       ;1   ;Action    ;
                      Name=MarkAsPaid;
                      CaptionML=[ENU=Register payment;
                                 ENG=Register payment];
                      ToolTipML=[ENU=Pay the invoice as specified in the default Payment Registration Setup.;
                                 ENG=Pay the invoice as specified in the default Payment Registration Setup.];
                      ApplicationArea=#Basic,#Suite,#Invoicing;
                      Promoted=Yes;
                      Visible=NOT IsFullyPaid AND NOT InvoiceCancelled AND (Amount <> 0);
                      Image=Invoicing-MDL-RegisterPayment;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 IF O365SalesInvoicePayment.MarkAsPaid("No.") THEN
                                   CurrPage.CLOSE;
                               END;
                                }
      { 13      ;1   ;Action    ;
                      Name=ShowPayments;
                      CaptionML=[ENU=Payments;
                                 ENG=Payments];
                      ToolTipML=[ENU=Show a list of payments made for this invoice.;
                                 ENG=Show a list of payments made for this invoice.];
                      ApplicationArea=#Basic,#Suite,#Invoicing;
                      Promoted=Yes;
                      Visible=NOT InvoiceCancelled AND (Amount <> 0);
                      Image=Invoicing-MDL-Payments;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 O365SalesInvoicePayment.ShowHistory("No.");
                               END;
                                }
      { 3       ;1   ;Action    ;
                      Name=MarkAsUnpaid;
                      CaptionML=[ENU=Cancel payment;
                                 ENG=Cancel payment];
                      ToolTipML=[ENU=Cancel payment registrations for this invoice.;
                                 ENG=Cancel payment registrations for this invoice.];
                      ApplicationArea=#Basic,#Suite,#Invoicing;
                      Promoted=Yes;
                      Visible=IsFullyPaid AND NOT InvoiceCancelled AND (Amount <> 0);
                      Image=Invoicing-MDL-RegisterPayment;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 IF O365SalesInvoicePayment.CancelSalesInvoicePayment("No.") THEN
                                   CurrPage.CLOSE;
                               END;
                                }
      { 23      ;1   ;Action    ;
                      Name=Send;
                      CaptionML=[ENU=Resend;
                                 ENG=Resend];
                      ToolTipML=[ENU=Sends the invoice as pdf by email.;
                                 ENG=Sends the invoice as pdf by email.];
                      ApplicationArea=#Basic,#Suite,#Invoicing;
                      Promoted=Yes;
                      Visible=NOT InvoiceCancelled;
                      Image=Invoicing-MDL-Send;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 O365SendResendInvoice@1000 : Codeunit 2104;
                               BEGIN
                                 O365SendResendInvoice.ResendSalesInvoice(Rec);
                               END;
                                }
      { 20      ;1   ;Action    ;
                      Name=CancelInvoice;
                      CaptionML=[ENU=Cancel invoice;
                                 ENG=Cancel invoice];
                      ToolTipML=[ENU=Cancels the invoice.;
                                 ENG=Cancels the invoice.];
                      ApplicationArea=#Basic,#Suite,#Invoicing;
                      Promoted=Yes;
                      Visible=NOT InvoiceCancelled;
                      Image=Invoicing-MDL-CancelDoc;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 SalesInvoiceHeader@1001 : Record 112;
                                 O365SalesCancelInvoice@1000 : Codeunit 2103;
                                 O365DocumentSendMgt@1002 : Codeunit 2158;
                               BEGIN
                                 IF SalesInvoiceHeader.GET("No.") THEN BEGIN
                                   SalesInvoiceHeader.SETRECFILTER;
                                   O365SalesCancelInvoice.CancelInvoice(SalesInvoiceHeader);
                                 END;

                                 SalesInvoiceHeader.CALCFIELDS(Cancelled);
                                 IF SalesInvoiceHeader.Cancelled THEN
                                   O365DocumentSendMgt.RecallEmailFailedNotificationForInvoice;
                               END;
                                }
      { 24      ;1   ;Action    ;
                      Name=ViewPdf;
                      CaptionML=[ENU=View PDF;
                                 ENG=View PDF];
                      ToolTipML=[ENU=View the final invoice as pdf.;
                                 ENG=View the final invoice as pdf.];
                      ApplicationArea=#Basic,#Suite,#Invoicing;
                      Promoted=Yes;
                      Image=Invoicing-MDL-PreviewDoc;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ReportSelections@1001 : Record 77;
                                 DocumentPath@1002 : Text[250];
                               BEGIN
                                 SETRECFILTER;
                                 LOCKTABLE;
                                 FIND;
                                 ReportSelections.GetPdfReport(DocumentPath,ReportSelections.Usage::"S.Invoice",Rec,"Sell-to Customer No.");
                                 DOWNLOAD(DocumentPath,'','','',DocumentPath);
                                 FIND;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 25  ;1   ;Group     ;
                CaptionML=[ENU=Sell to;
                           ENG=Sell to];
                GroupType=Group }

    { 26  ;2   ;Field     ;
                CaptionML=[ENU=Customer Name;
                           ENG=Customer Name];
                ToolTipML=[ENU=Specifies the customer's name.;
                           ENG=Specifies the customer's name.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Sell-to Customer Name" }

    { 9   ;2   ;Field     ;
                ExtendedDatatype=E-Mail;
                CaptionML=[ENU=Email Address;
                           ENG=Email Address];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=CustomerEmail }

    { 31  ;2   ;Field     ;
                ExtendedDatatype=E-Mail;
                CaptionML=[ENU=Outstanding Amount;
                           ENG=Outstanding Amount];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Remaining Amount";
                AutoFormatExpr=CurrencyFormat;
                Importance=Promoted;
                Enabled=FALSE }

    { 32  ;2   ;Field     ;
                CaptionML=[ENU=Status;
                           ENG=Status];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=Status;
                AutoFormatExpr=CurrencyFormat;
                Importance=Promoted;
                Enabled=FALSE;
                StyleExpr=OutStandingStatusStyle;
                ShowCaption=No }

    { 35  ;2   ;Field     ;
                Name=ViewContactCard;
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=ViewContactDetailsLbl;
                Editable=FALSE;
                OnDrillDown=VAR
                              Customer@1000 : Record 18;
                            BEGIN
                              IF Customer.GET("Sell-to Customer No.") THEN
                                PAGE.RUNMODAL(PAGE::"BC O365 Sales Customer Card",Customer);
                            END;

                ShowCaption=No }

    { 16  ;2   ;Group     ;
                GroupType=Group }

    { 14  ;3   ;Field     ;
                CaptionML=[ENU=Address;
                           ENG=Address];
                ToolTipML=[ENU=Specifies the address where the customer is located.;
                           ENG=Specifies the address where the customer is located.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Sell-to Address" }

    { 11  ;3   ;Field     ;
                CaptionML=[ENU=Address 2;
                           ENG=Address 2];
                ToolTipML=[ENU=Specifies additional address information.;
                           ENG=Specifies additional address information.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Sell-to Address 2" }

    { 12  ;3   ;Field     ;
                CaptionML=[ENU=Post Code;
                           ENG=Postcode];
                ToolTipML=[ENU=Specifies the postal code.;
                           ENG=Specifies the postcode.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Sell-to Post Code" }

    { 2   ;3   ;Field     ;
                CaptionML=[ENU=City;
                           ENG=City];
                ToolTipML=[ENU=Specifies the address city.;
                           ENG=Specifies the address city.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Sell-to City" }

    { 33  ;3   ;Field     ;
                CaptionML=[ENU=County;
                           ENG=County];
                ToolTipML=[ENU=Specifies the address county.;
                           ENG=Specifies the address county.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Sell-to County" }

    { 34  ;3   ;Field     ;
                CaptionML=[ENU=Country/Region Code;
                           ENG=Country/Region Code];
                ToolTipML=[ENU=Specifies the address country/region.;
                           ENG=Specifies the address country/region.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Sell-to Country/Region Code" }

    { 29  ;1   ;Group     ;
                CaptionML=[ENU=Invoice Details;
                           ENG=Invoice Details];
                GroupType=Group }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=Invoice No.;
                           ENG=Invoice No.];
                ToolTipML=[ENU=Specifies the number of the record.;
                           ENG=Specifies the number of the record.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="No." }

    { 27  ;2   ;Field     ;
                CaptionML=[ENU=Due Date;
                           ENG=Due Date];
                ToolTipML=[ENU=Specifies when the posted sales invoice must be paid.;
                           ENG=Specifies when the posted sales invoice must be paid.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Due Date";
                Importance=Promoted }

    { 6   ;2   ;Field     ;
                CaptionML=[ENU=Invoice Date;
                           ENG=Invoice Date];
                ToolTipML=[ENU=Specifies when the posted sales invoice was created.;
                           ENG=Specifies when the posted sales invoice was created.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Document Date" }

    { 17  ;2   ;Field     ;
                CaptionML=[ENU=Customer is tax liable;
                           ENG=Customer is tax liable];
                ToolTipML=[ENU=Specifies if the sales invoice contains sales tax.;
                           ENG=Specifies if the sales invoice contains VAT.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Tax Liable";
                Importance=Additional;
                Visible=NOT IsUsingVAT }

    { 19  ;2   ;Field     ;
                CaptionML=[ENU=Customer tax rate;
                           ENG=Customer tax rate];
                ToolTipML=[ENU=Specifies the tax area code that is used to calculate and post sales tax.;
                           ENG=Specifies the tax area code that is used to calculate and post VAT.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=TaxAreaDescription;
                Importance=Additional;
                Visible=NOT IsUsingVAT }

    { 37  ;2   ;Field     ;
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="VAT Registration No.";
                Visible=IsUsingVAT }

    { 18  ;2   ;Field     ;
                Name=CouponCodes;
                CaptionML=[ENU=Coupons;
                           ENG=Coupons];
                ToolTipML=[ENU=Specifies the coupon codes used on this invoice.;
                           ENG=Specifies the coupon codes used on this invoice.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=CouponCodes;
                QuickEntry=FALSE }

    { 21  ;1   ;Part      ;
                Name=Lines;
                CaptionML=[ENU=Line Items;
                           ENG=Line Items];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SubPageLink=Document No.=FIELD(No.);
                PagePartID=Page2304;
                Editable=FALSE;
                PartType=Page }

    { 50  ;1   ;Group     ;
                CaptionML=[ENU=Totals;
                           ENG=Totals];
                Visible=NOT DiscountVisible;
                GroupType=Group }

    { 49  ;2   ;Group     ;
                GroupType=Group }

    { 48  ;3   ;Field     ;
                Name=Amount;
                Lookup=No;
                DrillDown=No;
                CaptionML=[ENU=Net Total;
                           ENG=Net Total];
                ToolTipML=[ENU=Specifies the total amount on the sales invoice excluding VAT.;
                           ENG=Specifies the total amount on the sales invoice excluding VAT.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=Amount;
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormat }

    { 47  ;3   ;Field     ;
                Name=AmountInclVAT;
                Lookup=No;
                DrillDown=No;
                CaptionML=[ENU=Total Including VAT;
                           ENG=Total Including VAT];
                ToolTipML=[ENU=Specifies the total amount on the sales invoice including VAT.;
                           ENG=Specifies the total amount on the sales invoice including VAT.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Amount Including VAT";
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormat;
                Importance=Promoted;
                Style=Strong;
                StyleExpr=TRUE }

    { 30  ;1   ;Group     ;
                CaptionML=[ENU=Totals;
                           ENG=Totals];
                Visible=DiscountVisible;
                GroupType=Group }

    { 36  ;2   ;Group     ;
                GroupType=Group }

    { 15  ;3   ;Field     ;
                CaptionML=[ENU=Subtotal;
                           ENG=Subtotal];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=SubTotalAmount;
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormat }

    { 10  ;3   ;Field     ;
                Name=InvoiceDiscountAmount;
                CaptionML=[ENU=Invoice Discount;
                           ENG=Invoice Discount];
                ToolTipML=[ENU=Specifies a discount amount that is deducted from the value in the Total Incl. VAT field. You can enter or change the amount manually.;
                           ENG=Specifies a discount amount that is deducted from the value in the Total Incl. VAT field. You can enter or change the amount manually.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=-InvoiceDiscountAmount;
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormat;
                CaptionClass=GetInvDiscountCaption;
                Importance=Promoted;
                Enabled=FALSE }

    { 7   ;3   ;Field     ;
                Name=Amount2;
                CaptionML=[ENU=Net Total;
                           ENG=Net Total];
                ToolTipML=[ENU=Specifies the total amount on the sales invoice excluding VAT.;
                           ENG=Specifies the total amount on the sales invoice excluding VAT.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=Amount;
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormat;
                Importance=Promoted;
                Enabled=FALSE }

    { 8   ;3   ;Field     ;
                Name=Amount Including VAT2;
                CaptionML=[ENU=Total Including VAT;
                           ENG=Total Including VAT];
                ToolTipML=[ENU=Specifies the total amount on the sales invoice including VAT.;
                           ENG=Specifies the total amount on the sales invoice including VAT.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Amount Including VAT";
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormat;
                Importance=Promoted;
                Enabled=FALSE;
                Style=Strong;
                StyleExpr=TRUE }

    { 39  ;1   ;Group     ;
                CaptionML=[ENU=Note for customer;
                           ENG=Note for customer];
                GroupType=Group }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the products or service being offered;
                           ENG=Specifies the products or service being offered];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=WorkDescription;
                Editable=FALSE;
                MultiLine=Yes }

    { 56  ;1   ;Field     ;
                Name=NoOfAttachments;
                DrillDown=Yes;
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=NoOfAttachmentsValueTxt;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              UpdateNoOfAttachmentsLabel(O365SalesAttachmentMgt.EditAttachments(Rec));
                            END;

                ShowCaption=No }

  }
  CODE
  {
    VAR
      O365SalesInvoicePayment@1002 : Codeunit 2105;
      O365SalesCancelInvoice@1007 : Codeunit 2103;
      NoOfAttachmentsTxt@1003 : TextConst '@@@="%1=an integer number, starting at 0";ENU=Attachments (%1);ENG=Attachments (%1)';
      O365SalesAttachmentMgt@1011 : Codeunit 2112;
      WorkDescription@1018 : Text;
      NoOfAttachmentsValueTxt@1017 : Text;
      CustomerEmail@1016 : Text;
      InvoiceCancelled@1015 : Boolean;
      IsFullyPaid@1014 : Boolean;
      IsUsingVAT@1013 : Boolean;
      CurrencyFormat@1012 : Text;
      InvoiceDiscountAmount@1001 : Decimal;
      AddAttachmentTxt@1004 : TextConst 'ENU=Add attachment;ENG=Add attachment';
      SubTotalAmount@1005 : Decimal;
      DiscountVisible@1006 : Boolean;
      TaxAreaDescription@1008 : Text[50];
      CouponCodes@1009 : Text;
      Status@1000 : Text;
      OutStandingStatusStyle@1010 : Text[30];
      ViewContactDetailsLbl@1019 : TextConst 'ENU=Open contact details;ENG=Open contact details';

    LOCAL PROCEDURE CalcInvoiceDiscount@3();
    VAR
      SalesInvoiceLine@1000 : Record 113;
    BEGIN
      SalesInvoiceLine.SETRANGE("Document No.","No.");
      SalesInvoiceLine.CALCSUMS("Inv. Discount Amount","Line Amount");
      InvoiceDiscountAmount := SalesInvoiceLine."Inv. Discount Amount";
      SubTotalAmount := SalesInvoiceLine."Line Amount";
    END;

    LOCAL PROCEDURE UpdateNoOfAttachmentsLabel@7(NoOfAttachments@1000 : Integer);
    BEGIN
      IF NoOfAttachments = 0 THEN
        NoOfAttachmentsValueTxt := AddAttachmentTxt
      ELSE
        NoOfAttachmentsValueTxt := STRSUBSTNO(NoOfAttachmentsTxt,NoOfAttachments);
    END;

    LOCAL PROCEDURE UpdateCurrencyFormat@9();
    VAR
      GLSetup@1002 : Record 98;
      Currency@1001 : Record 4;
      CurrencySymbol@1000 : Text[10];
    BEGIN
      IF "Currency Code" = '' THEN BEGIN
        GLSetup.GET;
        CurrencySymbol := GLSetup.GetCurrencySymbol;
      END ELSE BEGIN
        IF Currency.GET("Currency Code") THEN;
        CurrencySymbol := Currency.GetCurrencySymbol;
      END;
      CurrencyFormat := STRSUBSTNO('%1<precision, 2:2><standard format, 0>',CurrencySymbol);
    END;

    LOCAL PROCEDURE GetDocumentStatus@2();
    VAR
      O365SalesDocument@1000 : Record 2103;
    BEGIN
      OutStandingStatusStyle := '';
      O365SalesDocument.SETRANGE("No.","No.");
      O365SalesDocument.SETRANGE(Posted,TRUE);
      IF O365SalesDocument.OnFind('+') THEN BEGIN
        IF O365SalesDocument."Document Status" = O365SalesDocument."Document Status"::"Canceled Invoice" THEN
          Status := O365SalesDocument."Total Invoiced Amount"
        ELSE
          Status := O365SalesDocument."Outstanding Status";

        IF O365SalesDocument.IsOverduePostedInvoice THEN
          OutStandingStatusStyle := 'Unfavorable'
        ELSE
          IF O365SalesDocument.Posted AND NOT O365SalesDocument.Canceled AND (O365SalesDocument."Outstanding Amount" <= 0) THEN
            OutStandingStatusStyle := 'Favorable'
          ELSE
            IF O365SalesDocument."Last Email Sent Status" = O365SalesDocument."Last Email Sent Status"::Error THEN BEGIN
              Status := O365SalesDocument."Total Invoiced Amount";
              OutStandingStatusStyle := 'Unfavorable';
            END;
      END;
    END;

    LOCAL PROCEDURE GetInvDiscountCaption@8() : Text;
    VAR
      O365SalesInvoiceMgmt@1000 : Codeunit 2310;
      DiscountPercentage@1001 : Decimal;
    BEGIN
      CalcInvoiceDiscount;

      IF SubTotalAmount = 0 THEN
        DiscountPercentage := 0
      ELSE
        DiscountPercentage := (100 * InvoiceDiscountAmount) / SubTotalAmount;

      EXIT(O365SalesInvoiceMgmt.GetInvoiceDiscountCaption(DiscountPercentage));
    END;

    BEGIN
    END.
  }
}

