OBJECT Page 2316 BC O365 Customer List
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Customers;
               ENG=Customers];
    SourceTable=Table18;
    SourceTableView=SORTING(Name)
                    WHERE(Blocked=CONST(" "));
    PageType=List;
    CardPageID=BC O365 Sales Customer Card;
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 SETRANGE("Date Filter",0D,WORKDATE);
               END;

    OnAfterGetRecord=BEGIN
                       OverdueAmount := CalcOverdueBalance;
                     END;

    ActionList=ACTIONS
    {
      { 35      ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 5       ;1   ;Action    ;
                      Name=Edit;
                      ShortCutKey=Return;
                      CaptionML=[ENU=Edit;
                                 ENG=Edit];
                      ToolTipML=[ENU=Opens the Customer Card;
                                 ENG=Opens the Customer Card];
                      ApplicationArea=#Basic,#Suite,#Invoicing;
                      OnAction=VAR
                                 BCO365SalesCustomerCard@1000 : Page 2318;
                               BEGIN
                                 BCO365SalesCustomerCard.SetNewDocumentActionsVisible;
                                 BCO365SalesCustomerCard.SETRECORD(Rec);
                                 BCO365SalesCustomerCard.RUN;
                               END;
                                }
      { 7       ;1   ;Action    ;
                      Name=New;
                      CaptionML=[ENU=New customer;
                                 ENG=New customer];
                      ToolTipML=[ENU=Create a new customer;
                                 ENG=Create a new customer];
                      ApplicationArea=#Basic,#Suite,#Invoicing;
                      RunObject=Page 2318;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Invoicing-MDL-New;
                      RunPageMode=Create }
    }
  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                ContainerType=ContentArea }

    { 3   ;1   ;Group     ;
                CaptionML=[ENU="";
                           ENG=""];
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                Width=12;
                ToolTipML=[ENU=Specifies the customer's name. This name will appear on all sales documents for the customer. You can enter a maximum of 50 characters, both numbers and letters.;
                           ENG=Specifies the customer's name. This name will appear on all sales documents for the customer. You can enter a maximum of 50 characters, both numbers and letters.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=Name;
                OnDrillDown=BEGIN
                              PAGE.RUNMODAL(PAGE::"BC O365 Sales Customer Card",Rec);
                            END;
                             }

    { 2   ;2   ;Field     ;
                Width=12;
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="E-Mail" }

    { 9   ;2   ;Field     ;
                Lookup=No;
                DrillDown=No;
                CaptionML=[ENU=Outstanding;
                           ENG=Outstanding];
                ToolTipML=[ENU=Specifies the customer's balance.;
                           ENG=Specifies the customer's balance.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr="Balance (LCY)";
                AutoFormatType=10;
                AutoFormatExpr='1' }

    { 8   ;2   ;Field     ;
                Lookup=No;
                DrillDown=No;
                CaptionML=[ENU=Overdue;
                           ENG=Overdue];
                ToolTipML=[ENU=Specifies payments from the customer that are overdue per today's date.;
                           ENG=Specifies payments from the customer that are overdue per today's date.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                BlankZero=Yes;
                SourceExpr=OverdueAmount;
                AutoFormatType=10;
                AutoFormatExpr='1';
                Editable=FALSE;
                Style=Unfavorable;
                StyleExpr=OverdueAmount > 0 }

  }
  CODE
  {
    VAR
      OverdueAmount@1000 : Decimal;

    BEGIN
    END.
  }
}

