OBJECT Page 2328 BC O365 Email Settings Part
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Email Settings;
               ENG=Email Settings];
    SourceTable=Table2118;
    DelayedInsert=Yes;
    PageType=ListPart;
    OnAfterGetRecord=BEGIN
                       RecipientTypeValue := RecipientType;
                     END;

    OnNewRecord=BEGIN
                  RecipientTypeValue := RecipientTypeValue::CC;
                END;

    ActionList=ACTIONS
    {
      { 5       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 6       ;1   ;Action    ;
                      Name=RemoveAddress;
                      CaptionML=[ENU=Remove Address;
                                 ENG=Remove Address];
                      ToolTipML=[ENU=Removes the current address.;
                                 ENG=Removes the current address.];
                      ApplicationArea=#Basic,#Suite,#Invoicing;
                      Promoted=Yes;
                      Image=Delete;
                      Scope=Repeater;
                      OnAction=BEGIN
                                 DELETE(TRUE);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 7   ;1   ;Group     ;
                GroupType=Group;
                InstructionalTextML=[ENU=You can add email addresses to include your accountant or yourself for all sent invoices and estimates.;
                                     ENG=You can add email addresses to include your accountant or yourself for all sent invoices and estimates.] }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=Email;
                OnValidate=BEGIN
                             IF (Email = '') AND (xRec.Email <> '') THEN
                               CurrPage.UPDATE(FALSE);
                           END;
                            }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=CC/BCC;
                           ENG=CC/BCC];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=RecipientTypeValue;
                OnValidate=BEGIN
                             IF GET(xRec.Code,xRec.RecipientType) THEN BEGIN
                               RENAME(Email,RecipientTypeValue);
                               VALIDATE(Email,Email);
                               MODIFY(TRUE);
                             END ELSE
                               VALIDATE(RecipientType,RecipientTypeValue);
                           END;
                            }

  }
  CODE
  {
    VAR
      RecipientTypeValue@1000 : 'CC,BCC';

    BEGIN
    END.
  }
}

