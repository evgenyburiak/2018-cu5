OBJECT Page 2346 BC O365 VAT Posting Setup List
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=VAT Rates;
               ENG=VAT Rates];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table324;
    PageType=ListPart;
    RefreshOnActivate=Yes;
    ShowFilter=No;
    OnOpenPage=BEGIN
                 DefaultVATProductPostingGroupCode := O365TemplateManagement.GetDefaultVATProdPostingGroup;
               END;

    OnAfterGetRecord=BEGIN
                       IF Code = DefaultVATProductPostingGroupCode THEN BEGIN
                         Description := STRSUBSTNO(DefaultVATRateTxt,Description);
                         StyleExpr := 'Strong'
                       END ELSE
                         StyleExpr := 'Standard';
                     END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 5       ;1   ;Action    ;
                      Name=Open;
                      ShortCutKey=Return;
                      CaptionML=[ENU=Open;
                                 ENG=Open];
                      ToolTipML=[ENU=Open the card for the selected record.;
                                 ENG=Open the card for the selected record.];
                      ApplicationArea=#Basic,#Suite,#Invoicing;
                      Visible=FALSE;
                      Image=DocumentEdit;
                      Scope=Repeater;
                      OnAction=BEGIN
                                 PAGE.RUNMODAL(PAGE::"O365 VAT Posting Setup Card",Rec);
                                 DefaultVATProductPostingGroupCode := O365TemplateManagement.GetDefaultVATProdPostingGroup;
                                 CurrPage.UPDATE;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the VAT rate used to calculate VAT on what you buy or sell.;
                           ENG=Specifies the VAT rate used to calculate VAT on what you buy or sell.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=Description;
                Editable=FALSE;
                StyleExpr=StyleExpr }

  }
  CODE
  {
    VAR
      O365TemplateManagement@1000 : Codeunit 2142;
      StyleExpr@1001 : Text;
      DefaultVATProductPostingGroupCode@1002 : Code[20];
      DefaultVATRateTxt@1003 : TextConst '@@@="%1 = a VAT rate name, such as ""Reduced VAT""";ENU=%1 (Default);ENG=%1 (Default)';

    BEGIN
    END.
  }
}

