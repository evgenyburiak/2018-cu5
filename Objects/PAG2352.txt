OBJECT Page 2352 BC O365 Country/Region List
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Countries/Regions;
               ENG=Countries/Regions];
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table2152;
    PageType=List;
    SourceTableTemporary=Yes;
    RefreshOnActivate=Yes;
    OnOpenPage=VAR
                 CountryRegion@1000 : Record 9;
               BEGIN
                 DELETEALL;
                 IF CountryRegion.FINDSET THEN
                   REPEAT
                     Code := CountryRegion.Code;
                     Name := CountryRegion.GetNameInCurrentLanguage;
                     "VAT Scheme" := CountryRegion."VAT Scheme";
                     IF INSERT THEN;
                   UNTIL CountryRegion.NEXT = 0;
               END;

    OnInsertRecord=BEGIN
                     EXIT(O365SalesManagement.InsertNewCountryCode(Rec));
                   END;

    OnModifyRecord=BEGIN
                     EXIT(O365SalesManagement.ModifyCountryCode(xRec,Rec));
                   END;

  }
  CONTROLS
  {
    { 1000;0   ;Container ;
                ContainerType=ContentArea }

    { 1001;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 1002;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ISO code of the country or region.;
                           ENG=Specifies the ISO code of the country or region.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=Code;
                OnValidate=BEGIN
                             IF (xRec.Code <> '') AND (Code <> xRec.Code) THEN
                               ERROR(RenameCountryErr);
                           END;
                            }

    { 1003;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the country or region.;
                           ENG=Specifies the name of the country or region.];
                ApplicationArea=#Basic,#Suite,#Invoicing;
                SourceExpr=Name }

  }
  CODE
  {
    VAR
      O365SalesManagement@1000 : Codeunit 2107;
      RenameCountryErr@1001 : TextConst 'ENU=You cannot change the country code.;ENG=You cannot change the country code.';

    BEGIN
    END.
  }
}

