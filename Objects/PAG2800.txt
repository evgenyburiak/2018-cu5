OBJECT Page 2800 Native - KPIs Entity
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[@@@={Locked};
               ENU=nativeInvoicingRoleCenterKpi;
               ENG=nativeInvoicingRoleCenterKpi];
    Description=ENU=Activites;
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table9069;
    DelayedInsert=Yes;
    PageType=List;
    OnOpenPage=BEGIN
                 OnOpenActivitiesPage(CurrencyFormatTxt);
               END;

    OnAfterGetCurrRecord=BEGIN
                           CALCFIELDS("Invoiced CM","Invoiced YTD","Sales Invoices Outstanding","Sales Invoices Overdue");
                           invoicedCM := "Invoiced CM";
                           invoicedYTD := "Invoiced YTD";
                           salesInvoicesOutstanding := "Sales Invoices Outstanding";
                           salesInvoicesOverdue := "Sales Invoices Overdue";
                         END;

  }
  CONTROLS
  {
    { 8   ;    ;Container ;
                ContainerType=ContentArea }

    { 7   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                Name=primaryKey;
                CaptionML=[@@@={Locked};
                           ENU=primaryKey;
                           ENG=primaryKey];
                ApplicationArea=#All;
                SourceExpr="Primary Key" }

    { 5   ;2   ;Field     ;
                Name=invoicedYearToDate;
                CaptionML=[@@@={Locked};
                           ENU=invoicedYearToDate;
                           ENG=invoicedYearToDate];
                ToolTipML=[ENU=Specifies the total invoiced amount for this year.;
                           ENG=Specifies the total invoiced amount for this year.];
                ApplicationArea=#All;
                SourceExpr=invoicedYTD;
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormatTxt }

    { 10  ;2   ;Field     ;
                Name=numberOfInvoicesYearToDate;
                CaptionML=[@@@={Locked};
                           ENU=numberOfInvoicesYearToDate;
                           ENG=numberOfInvoicesYearToDate];
                ToolTipML=[ENU=Specifies the total number of invoices for this year.;
                           ENG=Specifies the total number of invoices for this year.];
                ApplicationArea=#All;
                SourceExpr="No. of Invoices YTD" }

    { 6   ;2   ;Field     ;
                Name=invoicedCurrentMonth;
                CaptionML=[@@@={Locked};
                           ENU=invoicedCurrentMonth;
                           ENG=invoicedCurrentMonth];
                ToolTipML=[ENU=Specifies the total amount invoiced for the current month.;
                           ENG=Specifies the total amount invoiced for the current month.];
                ApplicationArea=#All;
                SourceExpr=invoicedCM;
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormatTxt }

    { 9   ;2   ;Field     ;
                Name=salesInvoicesOutsdanding;
                CaptionML=[@@@={Locked};
                           ENU=salesInvoicesOutstanding;
                           ENG=salesInvoicesOutstanding];
                ToolTipML=[ENU=Specifies the total amount that has not yet been paid.;
                           ENG=Specifies the total amount that has not yet been paid.];
                ApplicationArea=#All;
                SourceExpr=salesInvoicesOutstanding;
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormatTxt }

    { 3   ;2   ;Field     ;
                Name=salesInvoicesOverdue;
                CaptionML=[@@@={Locked};
                           ENU=salesInvoicesOverdue;
                           ENG=salesInvoicesOverdue];
                ToolTipML=[ENU=Specifies the total amount that has not been paid and is after the due date.;
                           ENG=Specifies the total amount that has not been paid and is after the due date.];
                ApplicationArea=#All;
                SourceExpr=salesInvoicesOverdue;
                AutoFormatType=10;
                AutoFormatExpr=CurrencyFormatTxt }

    { 2   ;2   ;Field     ;
                Name=numberOfQuotes;
                CaptionML=[@@@={Locked};
                           ENU=numberOfQuotes;
                           ENG=numberOfQuotes];
                ToolTipML=[ENU=Specifies the number of estimates.;
                           ENG=Specifies the number of estimates.];
                ApplicationArea=#All;
                SourceExpr="No. of Quotes" }

    { 1   ;2   ;Field     ;
                Name=numberOfDraftInvoices;
                CaptionML=[@@@={Locked};
                           ENU=numberOfDraftInvoices;
                           ENG=numberOfDraftInvoices];
                ToolTipML=[ENU=Specifies the number of draft invoices.;
                           ENG=Specifies the number of draft invoices.];
                ApplicationArea=#All;
                SourceExpr="No. of Draft Invoices" }

  }
  CODE
  {
    VAR
      CurrencyFormatTxt@1000 : Text;
      invoicedYTD@1001 : Decimal;
      invoicedCM@1002 : Decimal;
      salesInvoicesOutstanding@1003 : Decimal;
      salesInvoicesOverdue@1004 : Decimal;

    LOCAL PROCEDURE OnOpenActivitiesPage@1(VAR CurrencyFormatTxt@1004 : Text);
    VAR
      AccountingPeriod@1003 : Record 50;
      GLSetup@1002 : Record 98;
      O365SalesStatistics@1001 : Codeunit 2100;
    BEGIN
      RESET;
      IF NOT GET THEN BEGIN
        INIT;
        INSERT;
      END;

      O365SalesStatistics.GetCurrentAccountingPeriod(AccountingPeriod);

      SETFILTER("Due Date Filter",'..%1',WORKDATE);
      SETFILTER("Overdue Date Filter",'<%1',WORKDATE);
      SETFILTER("YTD Date Filter",'%1..%2',AccountingPeriod."Starting Date",WORKDATE);
      SETFILTER("CM Date Filter",'%1..%2',CALCDATE('<CM+1D-1M>',WORKDATE),WORKDATE);

      GLSetup.GET;
      CurrencyFormatTxt := STRSUBSTNO('%1<precision, 0:0><standard format, 0>',GLSetup.GetCurrencySymbol);
    END;

    BEGIN
    END.
  }
}

