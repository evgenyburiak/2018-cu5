OBJECT Page 2840 Native - General Setting
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[@@@={Locked};
               ENU=nativeInvoicingGeneralSettings;
               ENG=nativeInvoicingGeneralSettings];
    SaveValues=Yes;
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table2840;
    DelayedInsert=Yes;
    PageType=List;
    SourceTableTemporary=Yes;
    OnOpenPage=BEGIN
                 BINDSUBSCRIPTION(NativeAPILanguageHandler);
                 LoadRecord;
                 SetCalculatedFields;
               END;

    OnModifyRecord=BEGIN
                     SaveRecord;
                   END;

  }
  CONTROLS
  {
    { 2   ;0   ;Container ;
                ContainerType=ContentArea }

    { 3   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 7   ;2   ;Field     ;
                Name=primaryKey;
                CaptionML=[@@@={Locked};
                           ENU=primaryKey;
                           ENG=primaryKey];
                ApplicationArea=#All;
                SourceExpr="Primary Key";
                Editable=false }

    { 5   ;2   ;Field     ;
                Name=currencySymbol;
                CaptionML=[@@@={Locked};
                           ENU=currencySymbol;
                           ENG=currencySymbol];
                ToolTipML=[ENU=Specifies the currency symbol.;
                           ENG=Specifies the currency symbol.];
                ApplicationArea=#All;
                SourceExpr="Currency Symbol" }

    { 6   ;2   ;Field     ;
                Name=paypalEmailAddress;
                CaptionML=[@@@={Locked};
                           ENU=paypalEmailAddress;
                           ENG=paypalEmailAddress];
                ToolTipML=[ENU=Specifies the PayPal email address.;
                           ENG=Specifies the PayPal email address.];
                ApplicationArea=#All;
                SourceExpr="Paypal Email Address";
                OnValidate=VAR
                             dnRegex@1001 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Text.RegularExpressions.Regex";
                             dnMatch@1000 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Text.RegularExpressions.Match";
                           BEGIN
                             dnMatch := dnRegex.Match("Paypal Email Address",EmailValidatorRegexTxt);
                             IF ("Paypal Email Address" <> '') AND (NOT dnMatch.Success) THEN
                               ERROR(EmailInvalidErr);
                           END;
                            }

    { 1   ;2   ;Field     ;
                Name=countryRegionCode;
                CaptionML=[@@@={Locked};
                           ENU=countryRegionCode;
                           ENG=countryRegionCode];
                ApplicationArea=#All;
                SourceExpr="Country/Region Code" }

    { 4   ;2   ;Field     ;
                Name=languageId;
                CaptionML=[@@@={Locked};
                           ENU=languageId;
                           ENG=languageId];
                ApplicationArea=#All;
                SourceExpr="Language Locale ID" }

    { 14  ;2   ;Field     ;
                Name=languageCode;
                CaptionML=[ENU=languageCode;
                           ENG=languageCode];
                ApplicationArea=#All;
                SourceExpr="Language Code";
                Editable=FALSE }

    { 15  ;2   ;Field     ;
                Name=languageDisplayName;
                CaptionML=[ENU=languageDisplayName;
                           ENG=languageDisplayName];
                ApplicationArea=#All;
                SourceExpr="Language Display Name";
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                Name=defaultTaxId;
                CaptionML=[@@@={Locked};
                           ENU=defaultTaxId;
                           ENG=defaultTaxId];
                ApplicationArea=#All;
                SourceExpr="Default Tax ID" }

    { 9   ;2   ;Field     ;
                Name=defaultTaxDisplayName;
                CaptionML=[@@@={Locked};
                           ENU=defaultTaxDisplayName;
                           ENG=defaultTaxDisplayName];
                ApplicationArea=#All;
                SourceExpr="Defauilt Tax Description" }

    { 10  ;2   ;Field     ;
                Name=defaultPaymentTermsId;
                CaptionML=[@@@={Locked};
                           ENU=defaultPaymentTermsId;
                           ENG=defaultPaymentTermsId];
                ApplicationArea=#All;
                SourceExpr="Default Payment Terms ID" }

    { 11  ;2   ;Field     ;
                Name=defaultPaymentTermsDisplayName;
                CaptionML=[@@@={Locked};
                           ENU=defaultPaymentTermsDisplayName;
                           ENG=defaultPaymentTermsDisplayName];
                ApplicationArea=#All;
                SourceExpr="Def. Pmt. Term Description" }

    { 12  ;2   ;Field     ;
                Name=defaultPaymentMethodId;
                CaptionML=[@@@={Locked};
                           ENU=defaultPaymentMethodId;
                           ENG=defaultPaymentMethodId];
                ApplicationArea=#All;
                SourceExpr="Default Payment Method ID" }

    { 13  ;2   ;Field     ;
                Name=defaultPaymentMethodDisplayName;
                CaptionML=[@@@={Locked};
                           ENU=defaultPaymentMethodDisplayName;
                           ENG=defaultPaymentMethodDisplayName];
                ApplicationArea=#All;
                SourceExpr="Def. Pmt. Method Description" }

    { 16  ;2   ;Field     ;
                Name=amountRoundingPrecision;
                CaptionML=[@@@={Locked};
                           ENU=amountRoundingPrecision;
                           ENG=amountRoundingPrecision];
                ApplicationArea=#All;
                SourceExpr="Amount Rounding Precision" }

    { 17  ;2   ;Field     ;
                Name=draftInvoiceFileName;
                CaptionML=[@@@={Locked};
                           ENU=draftInvoiceFileName;
                           ENG=draftInvoiceFileName];
                ToolTipML=[ENU=Specifies template of PDF file name for draft sales invoices.;
                           ENG=Specifies template of PDF file name for draft sales invoices.];
                ApplicationArea=#All;
                SourceExpr=DraftInvoiceFileName;
                Editable=FALSE }

    { 19  ;2   ;Field     ;
                Name=postedInvoiceFileName;
                CaptionML=[@@@={Locked};
                           ENU=postedInvoiceFileName;
                           ENG=postedInvoiceFileName];
                ToolTipML=[ENU=Specifies template of PDF file name for posted sales invoices.;
                           ENG=Specifies template of PDF file name for posted sales invoices.];
                ApplicationArea=#All;
                SourceExpr=PostedInvoiceFileName;
                Editable=FALSE }

    { 18  ;2   ;Field     ;
                Name=quoteFileName;
                CaptionML=[@@@={Locked};
                           ENU=quoteFileName;
                           ENG=quoteFileName];
                ToolTipML=[ENU=Specifies template of PDF file name for sales quotes.;
                           ENG=Specifies template of PDF file name for sales quotes.];
                ApplicationArea=#All;
                SourceExpr=QuoteFileName;
                Editable=FALSE }

    { 20  ;2   ;Field     ;
                Name=taxableGroupId;
                CaptionML=[@@@={Locked};
                           ENU=taxableGroupId;
                           ENG=taxableGroupId];
                ToolTipML=[ENU=Specifies the taxable group ID.;
                           ENG=Specifies the taxable group ID.];
                ApplicationArea=#All;
                SourceExpr=TaxableGroupId;
                Editable=FALSE }

    { 21  ;2   ;Field     ;
                Name=nonTaxableGroupId;
                CaptionML=[@@@={Locked};
                           ENU=nonTaxableGroupId;
                           ENG=nonTaxableGroupId];
                ToolTipML=[ENU=Specifies the non-taxable group ID.;
                           ENG=Specifies the non-taxable group ID.];
                ApplicationArea=#All;
                SourceExpr=NonTaxableGroupId;
                Editable=FALSE }

    { 24  ;2   ;Field     ;
                Name=enableSynchronization;
                CaptionML=[@@@={Locked};
                           ENU=enableSynchronization;
                           ENG=enableSynchronization];
                ToolTipML=[ENU=Specifies whether Microsoft synchronization is enabled.;
                           ENG=Specifies whether Microsoft synchronisation is enabled.];
                ApplicationArea=#All;
                SourceExpr=EnableSync }

    { 22  ;2   ;Field     ;
                Name=enableSyncCoupons;
                CaptionML=[@@@={Locked};
                           ENU=enableSyncCoupons;
                           ENG=enableSyncCoupons];
                ApplicationArea=#All;
                SourceExpr=EnableSyncCoupons }

  }
  CODE
  {
    VAR
      EmailValidatorRegexTxt@1001 : TextConst '@@@={Locked};ENU=^[A-Z0-9a-z._%+-]+@(?:[A-Za-z0-9.-]+\.)+[A-Za-z]{2,64}$;ENG=^[A-Z0-9a-z._%+-]+@(?:[A-Za-z0-9.-]+\.)+[A-Za-z]{2,64}$';
      EmailInvalidErr@1000 : TextConst 'ENU=Invalid Email Address.;ENG=Invalid Email Address.';
      NativeAPILanguageHandler@1008 : Codeunit 2850;
      PostedInvoiceFileName@1004 : Text[250];
      DraftInvoiceFileName@1002 : Text[250];
      QuoteFileName@1003 : Text[250];
      DocNoPlaceholderTxt@1005 : TextConst '@@@={Locked};ENU={0};ENG={0}';
      TaxableGroupId@1006 : GUID;
      NonTaxableGroupId@1007 : GUID;

    LOCAL PROCEDURE SetCalculatedFields@3();
    VAR
      TempSalesInvoiceHeader@1004 : TEMPORARY Record 112;
      TempSalesHeader@1003 : TEMPORARY Record 36;
      TaxableTaxGroup@1005 : Record 321;
      NonTaxableTaxGroup@1006 : Record 321;
      NativeReports@1001 : Codeunit 2822;
      DocumentMailing@1000 : Codeunit 260;
      NativeEDMTypes@1002 : Codeunit 2801;
    BEGIN
      DocumentMailing.GetAttachmentFileName(
        PostedInvoiceFileName,DocNoPlaceholderTxt,
        TempSalesInvoiceHeader.GetDocTypeTxt,NativeReports.PostedSalesInvoiceReportId);

      TempSalesHeader."Document Type" := TempSalesHeader."Document Type"::Invoice;
      DocumentMailing.GetAttachmentFileName(
        DraftInvoiceFileName,DocNoPlaceholderTxt,
        TempSalesHeader.GetDocTypeTxt,NativeReports.DraftSalesInvoiceReportId);

      TempSalesHeader."Document Type" := TempSalesHeader."Document Type"::Quote;
      DocumentMailing.GetAttachmentFileName(
        QuoteFileName,DocNoPlaceholderTxt,
        TempSalesHeader.GetDocTypeTxt,NativeReports.SalesQuoteReportId);

      IF NativeEDMTypes.GetTaxGroupFromTaxable(TRUE,TaxableTaxGroup) THEN
        TaxableGroupId := TaxableTaxGroup.Id
      ELSE
        CLEAR(TaxableGroupId);

      IF NativeEDMTypes.GetTaxGroupFromTaxable(FALSE,NonTaxableTaxGroup) THEN
        NonTaxableGroupId := NonTaxableTaxGroup.Id
      ELSE
        CLEAR(NonTaxableGroupId);
    END;

    BEGIN
    END.
  }
}

