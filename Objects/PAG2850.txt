OBJECT Page 2850 Native - Tax Area
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[@@@={Locked};
               ENU=taxAreas;
               ENG=taxAreas];
    SourceTable=Table5504;
    DelayedInsert=Yes;
    PageType=List;
    SourceTableTemporary=Yes;
    OnOpenPage=BEGIN
                 BINDSUBSCRIPTION(NativeAPILanguageHandler);
                 LoadRecords;
               END;

    OnInsertRecord=BEGIN
                     PropagateInsert;
                   END;

    OnModifyRecord=BEGIN
                     PropagateModify;
                   END;

    OnDeleteRecord=BEGIN
                     PropagateDelete;
                   END;

  }
  CONTROLS
  {
    { 6   ;0   ;Container ;
                ContainerType=ContentArea }

    { 5   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                Name=id;
                CaptionML=[@@@={Locked};
                           ENU=id;
                           ENG=id];
                ApplicationArea=#All;
                SourceExpr=Id }

    { 3   ;2   ;Field     ;
                Name=code;
                CaptionML=[@@@={Locked};
                           ENU=code;
                           ENG=code];
                ApplicationArea=#All;
                SourceExpr=Code }

    { 2   ;2   ;Field     ;
                Name=displayName;
                CaptionML=[@@@={Locked};
                           ENU=displayName;
                           ENG=displayName];
                ApplicationArea=#All;
                SourceExpr=Description }

    { 7   ;2   ;Field     ;
                Name=taxType;
                CaptionML=[ENU=taxType;
                           ENG=taxType];
                ApplicationArea=#All;
                SourceExpr=Type;
                Editable=FALSE }

    { 1   ;2   ;Field     ;
                Name=lastModifiedDateTime;
                CaptionML=[@@@={Locked};
                           ENU=lastModifiedDateTime;
                           ENG=lastModifiedDateTime];
                ApplicationArea=#All;
                SourceExpr="Last Modified Date Time";
                Editable=FALSE }

  }
  CODE
  {
    VAR
      NativeAPILanguageHandler@1000 : Codeunit 2850;

    BEGIN
    END.
  }
}

