OBJECT Page 2876 Native - Tax Rates
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Native - Tax Rates;
               ENG=Native - Tax Rates];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table5502;
    DelayedInsert=No;
    PageType=List;
    SourceTableTemporary=Yes;
    OnOpenPage=BEGIN
                 LoadRecords;
               END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                Name=taxAreaId;
                CaptionML=[@@@={Locked};
                           ENU=taxAreaId;
                           ENG=taxAreaId];
                ApplicationArea=#All;
                SourceExpr="Tax Area ID" }

    { 4   ;2   ;Field     ;
                Name=taxGroupId;
                CaptionML=[@@@={Locked};
                           ENU=taxGroupId;
                           ENG=taxGroupId];
                ApplicationArea=#All;
                SourceExpr="Tax Group ID" }

    { 5   ;2   ;Field     ;
                Name=taxRate;
                CaptionML=[@@@={Locked};
                           ENU=taxRate;
                           ENG=taxRate];
                ApplicationArea=#All;
                SourceExpr="Tax Rate" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

