OBJECT Page 4 Payment Terms
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Payment Terms;
               ENG=Payment Terms];
    SourceTable=Table3;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 19      ;1   ;Action    ;
                      CaptionML=[ENU=T&ranslation;
                                 ENG=T&ranslation];
                      ToolTipML=[ENU=View or edit descriptions for each payment method in different languages.;
                                 ENG=View or edit descriptions for each payment method in different languages.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 756;
                      RunPageLink=Payment Term=FIELD(Code);
                      Promoted=Yes;
                      Image=Translation;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code to identify this set of payment terms.;
                           ENG=Specifies a code to identify this set of payment terms.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a formula that determines how to calculate the due date, for example, when you create an invoice.;
                           ENG=Specifies a formula that determines how to calculate the due date, for example, when you create an invoice.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Due Date Calculation" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date formula if the payment terms include a possible payment discount.;
                           ENG=Specifies the date formula if the payment terms include a possible payment discount.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Discount Date Calculation" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the percentage of the invoice amount (amount including VAT is the default setting) that will constitute a possible payment discount.;
                           ENG=Specifies the percentage of the invoice amount (amount including VAT is the default setting) that will constitute a possible payment discount.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Discount %" }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that a payment discount, cash discount, cash discount date, and due date are calculated on credit memos with these payment terms.;
                           ENG=Specifies that a payment discount, cash discount, cash discount date, and due date are calculated on credit memos with these payment terms.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Calc. Pmt. Disc. on Cr. Memos" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an explanation of the payment terms.;
                           ENG=Specifies an explanation of the payment terms.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

