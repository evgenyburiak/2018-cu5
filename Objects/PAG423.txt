OBJECT Page 423 Customer Bank Account Card
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846,NAVGB11.00.00.19846;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Customer Bank Account Card;
               ENG=Customer Bank Account Card];
    SourceTable=Table287;
    PageType=Card;
    OnAfterGetCurrRecord=BEGIN
                           HandleAddressLookupVisibility;
                         END;

    ActionList=ACTIONS
    {
      { 3       ;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 5       ;1   ;Action    ;
                      CaptionML=[ENU=Direct Debit Mandates;
                                 ENG=Direct Debit Mandates];
                      ToolTipML=[ENU=View or edit direct-debit mandates that you set up to reflect agreements with customers to collect invoice payments from their bank account.;
                                 ENG=View or edit direct-debit mandates that you set up to reflect agreements with customers to collect invoice payments from their bank account.];
                      ApplicationArea=#Suite;
                      RunObject=Page 1230;
                      RunPageLink=Customer No.=FIELD(Customer No.),
                                  Customer Bank Account Code=FIELD(Code);
                      Promoted=Yes;
                      Image=MakeAgreement;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           ENG=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code to identify this customer bank account.;
                           ENG=Specifies a code to identify this customer bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the bank where the customer has the bank account.;
                           ENG=Specifies the name of the bank where the customer has the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 1040004;2;Group     ;
                Visible=IsAddressLookupTextEnabled;
                GroupType=Group }

    { 1040003;3;Field     ;
                Name=LookupAddress;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=LookupAddressLbl;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              ShowPostcodeLookup(TRUE);
                            END;

                ShowCaption=No }

    { 1040001;2;Field     ;
                Name=Address;
                ToolTipML=[ENU=Specifies the customer's address. This address will appear on all sales documents for the customer.;
                           ENG=Specifies the customer's address. This address will appear on all sales documents for the customer.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Address;
                OnValidate=VAR
                             PostcodeBusinessLogic@1040000 : Codeunit 10500;
                           BEGIN
                             PostcodeBusinessLogic.ShowDiscoverabilityNotificationIfNeccessary;
                           END;
                            }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies additional address information.;
                           ENG=Specifies additional address information.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Address 2" }

    { 1040000;2;Field     ;
                ToolTipML=[ENU=Specifies the city of the bank where the customer has the bank account.;
                           ENG=Specifies the city of the bank where the customer has the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=City }

    { 1040002;2;Field     ;
                ToolTipML=[ENU="Specifies the county of the bank where the customer has the bank account. ";
                           ENG="Specifies the county of the bank where the customer has the bank account. "];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=County }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the country/region of the address.;
                           ENG=Specifies the country/region of the address.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Country/Region Code";
                OnValidate=BEGIN
                             HandleAddressLookupVisibility;
                           END;
                            }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the telephone number of the bank where the customer has the bank account.;
                           ENG=Specifies the telephone number of the bank where the customer has the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Phone No." }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the bank employee regularly contacted in connection with this bank account.;
                           ENG=Specifies the name of the bank employee regularly contacted in connection with this bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Contact }

    { 1040005;2;Field     ;
                ToolTipML=[ENU=Specifies the postal code.;
                           ENG=Specifies the postcode.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Post Code";
                Importance=Promoted;
                OnValidate=VAR
                             PostcodeBusinessLogic@1040000 : Codeunit 10500;
                           BEGIN
                             PostcodeBusinessLogic.ShowDiscoverabilityNotificationIfNeccessary;
                             ShowPostcodeLookup(FALSE);
                           END;
                            }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the relevant currency code for the bank account.;
                           ENG=Specifies the relevant currency code for the bank account.];
                ApplicationArea=#Suite;
                SourceExpr="Currency Code" }

    { 64  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the bank branch.;
                           ENG=Specifies the number of the bank branch.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Branch No." }

    { 36  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number used by the bank for the bank account.;
                           ENG=Specifies the number used by the bank for the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Account No." }

    { 50  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a bank identification number of your own choice.;
                           ENG=Specifies a bank identification number of your own choice.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Transit No." }

    { 1902768601;1;Group  ;
                CaptionML=[ENU=Communication;
                           ENG=Communication] }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the fax number of the bank where the customer has the bank account.;
                           ENG=Specifies the fax number of the bank where the customer has the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Fax No." }

    { 26  ;2   ;Field     ;
                ExtendedDatatype=E-Mail;
                ToolTipML=[ENU=Specifies the email address associated with the bank account.;
                           ENG=Specifies the email address associated with the bank account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="E-Mail" }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the bank web site.;
                           ENG=Specifies the bank web site.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Home Page" }

    { 1905090301;1;Group  ;
                CaptionML=[ENU=Transfer;
                           ENG=Transfer] }

    { 42  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the SWIFT code (international bank identifier code) of the bank where the customer has the account.;
                           ENG=Specifies the SWIFT code (international bank identifier code) of the bank where the customer has the account.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="SWIFT Code" }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the bank account's international bank account number.;
                           ENG=Specifies the bank account's international bank account number.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=IBAN }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the format standard to be used in bank transfers if you use the Bank Clearing Code field to identify you as the sender.;
                           ENG=Specifies the format standard to be used in bank transfers if you use the Bank Clearing Code field to identify you as the sender.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Clearing Standard" }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for bank clearing that is required according to the format standard you selected in the Bank Clearing Standard field.;
                           ENG=Specifies the code for bank clearing that is required according to the format standard you selected in the Bank Clearing Standard field.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Clearing Code" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      LookupAddressLbl@1040000 : TextConst 'ENU=Lookup address from postcode;ENG=Lookup address from postcode';
      IsAddressLookupTextEnabled@1040001 : Boolean;

    LOCAL PROCEDURE ShowPostcodeLookup@1040000(ShowInputFields@1040002 : Boolean);
    VAR
      TempEnteredAutocompleteAddress@1040000 : TEMPORARY Record 9090;
      TempAutocompleteAddress@1040003 : TEMPORARY Record 9090;
      PostcodeBusinessLogic@1040001 : Codeunit 10500;
    BEGIN
      IF ("Country/Region Code" <> 'GB') AND ("Country/Region Code" <> '') THEN
        EXIT;

      IF NOT PostcodeBusinessLogic.IsConfigured OR (("Post Code" = '') AND NOT ShowInputFields) THEN
        EXIT;

      TempEnteredAutocompleteAddress.Address := Address;
      TempEnteredAutocompleteAddress.Postcode := "Post Code";

      IF NOT PostcodeBusinessLogic.ShowLookupWindow(TempEnteredAutocompleteAddress,ShowInputFields,TempAutocompleteAddress) THEN
        EXIT;

      CopyAutocompleteFields(TempAutocompleteAddress);
      HandleAddressLookupVisibility;
    END;

    LOCAL PROCEDURE CopyAutocompleteFields@1040001(VAR TempAutocompleteAddress@1040000 : TEMPORARY Record 9090);
    BEGIN
      Address := TempAutocompleteAddress.Address;
      "Address 2" := TempAutocompleteAddress."Address 2";
      "Post Code" := TempAutocompleteAddress.Postcode;
      City := TempAutocompleteAddress.City;
      County := TempAutocompleteAddress.County;
      "Country/Region Code" := TempAutocompleteAddress."Country / Region";
    END;

    LOCAL PROCEDURE HandleAddressLookupVisibility@1040002();
    VAR
      PostcodeBusinessLogic@1040000 : Codeunit 10500;
    BEGIN
      IF NOT CurrPage.EDITABLE OR NOT PostcodeBusinessLogic.IsConfigured THEN
        IsAddressLookupTextEnabled := FALSE
      ELSE
        IsAddressLookupTextEnabled := ("Country/Region Code" = 'GB') OR ("Country/Region Code" = '');
    END;

    BEGIN
    END.
  }
}

