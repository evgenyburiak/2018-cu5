OBJECT Page 458 No. Series Relationships
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=No. Series Relationships;
               ENG=No. Series Relationships];
    SourceTable=Table310;
    DataCaptionFields=Code;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code that represents the related number series.;
                           ENG=Specifies the number series code that represents the related number series.];
                ApplicationArea=#Advanced;
                SourceExpr=Code;
                Visible=FALSE }

    { 11  ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[ENU=Specifies the description of the number series represented by the code in the Code field.;
                           ENG=Specifies the description of the number series represented by the code in the Code field.];
                ApplicationArea=#Advanced;
                SourceExpr=Description;
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for a number series that you want to include in the group of related number series.;
                           ENG=Specifies the code for a number series that you want to include in the group of related number series.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Series Code" }

    { 6   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[ENU=Specifies the description of the number series represented by the code in the Series Code field.;
                           ENG=Specifies the description of the number series represented by the code in the Series Code field.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Series Description" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

