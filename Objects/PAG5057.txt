OBJECT Page 5057 Contact Alt. Address List
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Contact Alt. Address List;
               ENG=Contact Alt. Address List];
    SourceTable=Table5051;
    DataCaptionFields=Contact No.,Code;
    PageType=List;
    CardPageID=Contact Alt. Address Card;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 29      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Alt. Contact Address;
                                 ENG=&Alt. Contact Address] }
      { 31      ;2   ;Action    ;
                      CaptionML=[ENU=Date Ranges;
                                 ENG=Date Ranges];
                      ToolTipML=[ENU=Specify date ranges that apply to the contact's alternate address.;
                                 ENG=Specify date ranges that apply to the contact's alternate address.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 5058;
                      RunPageLink=Contact No.=FIELD(Contact No.),
                                  Contact Alt. Address Code=FIELD(Code);
                      Image=DateRange }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the alternate address.;
                           ENG=Specifies the code for the alternate address.];
                ApplicationArea=#All;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the company for the alternate address.;
                           ENG=Specifies the name of the company for the alternate address.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Company Name" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the additional part of the company name for the alternate address.;
                           ENG=Specifies the additional part of the company name for the alternate address.];
                ApplicationArea=#Advanced;
                SourceExpr="Company Name 2";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the alternate address of the contact.;
                           ENG=Specifies the alternate address of the contact.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Address }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies additional address information.;
                           ENG=Specifies additional address information.];
                ApplicationArea=#Advanced;
                SourceExpr="Address 2";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the city of the contact's alternate address.;
                           ENG=Specifies the city of the contact's alternate address.];
                ApplicationArea=#Advanced;
                SourceExpr=City;
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code.;
                           ENG=Specifies the postcode.];
                ApplicationArea=#Advanced;
                SourceExpr="Post Code";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the county for the contact's alternate address.;
                           ENG=Specifies the county for the contact's alternate address.];
                ApplicationArea=#Advanced;
                SourceExpr=County;
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the country/region of the address.;
                           ENG=Specifies the country/region of the address.];
                ApplicationArea=#Advanced;
                SourceExpr="Country/Region Code";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the telephone number for the alternate address.;
                           ENG=Specifies the telephone number for the alternate address.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Phone No." }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the fax number for the alternate address.;
                           ENG=Specifies the fax number for the alternate address.];
                ApplicationArea=#Advanced;
                SourceExpr="Fax No.";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ExtendedDatatype=E-Mail;
                ToolTipML=[ENU=Specifies the e-mail address for the contact at the alternate address.;
                           ENG=Specifies the e-mail address for the contact at the alternate address.];
                ApplicationArea=#Advanced;
                SourceExpr="E-Mail";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

