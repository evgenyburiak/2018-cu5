OBJECT Page 5063 Mailing Groups
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Mailing Groups;
               ENG=Mailing Groups];
    SourceTable=Table5055;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 11      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Mailing Group;
                                 ENG=&Mailing Group];
                      Image=Group }
      { 12      ;2   ;Action    ;
                      CaptionML=[ENU=C&ontacts;
                                 ENG=C&ontacts];
                      ToolTipML=[ENU=View a list of the contact companies you have assigned the mailing group to.;
                                 ENG=View a list of the contact companies you have assigned the mailing group to.];
                      ApplicationArea=#RelationshipMgmt;
                      RunObject=Page 5065;
                      RunPageLink=Mailing Group Code=FIELD(Code);
                      Image=CustomerContact }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the mailing group.;
                           ENG=Specifies the code for the mailing group.];
                ApplicationArea=#All;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the description of the mailing group.;
                           ENG=Specifies the description of the mailing group.];
                ApplicationArea=#All;
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of contacts that have been assigned the mailing group. This field is not editable.;
                           ENG=Specifies the number of contacts that have been assigned the mailing group. This field is not editable.];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="No. of Contacts";
                DrillDownPageID=Mailing Group Contacts }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

