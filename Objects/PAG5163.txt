OBJECT Page 5163 Sales Quote Archive Subform
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Lines;
               ENG=Lines];
    LinksAllowed=No;
    SourceTable=Table5108;
    SourceTableView=WHERE(Document Type=CONST(Quote));
    PageType=ListPart;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 ENG=&Line];
                      Image=Line }
      { 1904320404;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 ENG=Dimensions];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 ENG=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyse transaction history.];
                      ApplicationArea=#Dimensions;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1900639404;2 ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 ENG=Co&mments];
                      ToolTipML=[ENU=View or add comments for the record.;
                                 ENG=View or add comments for the record.];
                      ApplicationArea=#Advanced;
                      Image=ViewComments;
                      OnAction=BEGIN
                                 ShowLineComments;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of entity that will be posted for this sales line, such as Item, Resource, or G/L Account.;
                           ENG=Specifies the type of entity that will be posted for this sales line, such as Item, Resource, or G/L Account.];
                ApplicationArea=#Advanced;
                SourceExpr=Type }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the involved entry or record, according to the specified number series.;
                           ENG=Specifies the number of the involved entry or record, according to the specified number series.];
                ApplicationArea=#Advanced;
                SourceExpr="No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the cross-referenced item number. If you enter a cross reference between yours and your vendor's or customer's item number, then this number will override the standard item number when you enter the cross-reference number on a sales or purchase document.;
                           ENG=Specifies the cross-referenced item number. If you enter a cross reference between yours and your vendor's or customer's item number, then this number will override the standard item number when you enter the cross-reference number on a sales or purchase document.];
                ApplicationArea=#Advanced;
                SourceExpr="Cross-Reference No.";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the variant of the item on the line.;
                           ENG=Specifies the variant of the item on the line.];
                ApplicationArea=#Advanced;
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that a substitute is available for the item on the sales line.;
                           ENG=Specifies that a substitute is available for the item on the sales line.];
                ApplicationArea=#Advanced;
                SourceExpr="Substitution Available";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that this item is a nonstock item.;
                           ENG=Specifies that this item is a nonstock item.];
                ApplicationArea=#Advanced;
                SourceExpr=Nonstock;
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the VAT specification of the involved item or resource to link transactions made for this record with the appropriate general ledger account according to the VAT posting setup.;
                           ENG=Specifies the VAT specification of the involved item or resource to link transactions made for this record with the appropriate general ledger account according to the VAT posting setup.];
                ApplicationArea=#Advanced;
                SourceExpr="VAT Prod. Posting Group";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the sales quote archive.;
                           ENG=Specifies a description of the sales quote archive.];
                ApplicationArea=#Advanced;
                SourceExpr=Description }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the location from where inventory items to the customer on the sales document are to be shipped by default.;
                           ENG=Specifies the location from where inventory items to the customer on the sales document are to be shipped by default.];
                ApplicationArea=#Advanced;
                SourceExpr="Location Code" }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many units are being sold.;
                           ENG=Specifies how many units are being sold.];
                ApplicationArea=#Advanced;
                BlankZero=Yes;
                SourceExpr=Quantity }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how each unit of the item or resource is measured, such as in pieces or hours. By default, the value in the Base Unit of Measure field on the item or resource card is inserted.;
                           ENG=Specifies how each unit of the item or resource is measured, such as in pieces or hours. By default, the value in the Base Unit of Measure field on the item or resource card is inserted.];
                ApplicationArea=#Advanced;
                SourceExpr="Unit of Measure Code" }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the item or resource's unit of measure, such as piece or hour.;
                           ENG=Specifies the name of the item or resource's unit of measure, such as piece or hour.];
                ApplicationArea=#Advanced;
                SourceExpr="Unit of Measure";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the cost, in LCY, of one unit of the item or resource on the line.;
                           ENG=Specifies the cost, in LCY, of one unit of the item or resource on the line.];
                ApplicationArea=#Advanced;
                SourceExpr="Unit Cost (LCY)";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the price of one unit of the item or resource. You can enter a price manually or have it entered according to the Price/Profit Calculation field on the related card.;
                           ENG=Specifies the price of one unit of the item or resource. You can enter a price manually or have it entered according to the Price/Profit Calculation field on the related card.];
                ApplicationArea=#Advanced;
                BlankZero=Yes;
                SourceExpr="Unit Price" }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the net amount, excluding any invoice discount amount, that must be paid for products on the line.;
                           ENG=Specifies the net amount, excluding any invoice discount amount, that must be paid for products on the line.];
                ApplicationArea=#Advanced;
                BlankZero=Yes;
                SourceExpr="Line Amount" }

    { 32  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the discount percentage that is granted for the item on the line.;
                           ENG=Specifies the discount percentage that is granted for the item on the line.];
                ApplicationArea=#Advanced;
                BlankZero=Yes;
                SourceExpr="Line Discount %" }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the discount amount that is granted for the item on the line.;
                           ENG=Specifies the discount amount that is granted for the item on the line.];
                ApplicationArea=#Advanced;
                SourceExpr="Line Discount Amount";
                Visible=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the invoice line is included when the invoice discount is calculated.;
                           ENG=Specifies if the invoice line is included when the invoice discount is calculated.];
                ApplicationArea=#Advanced;
                SourceExpr="Allow Invoice Disc." }

    { 38  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that you can assign item charges to this line.;
                           ENG=Specifies that you can assign item charges to this line.];
                ApplicationArea=#Advanced;
                SourceExpr="Allow Item Charge Assignment";
                Visible=FALSE }

    { 44  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the job number that the archived document was linked to.;
                           ENG=Specifies the job number that the archived document was linked to.];
                ApplicationArea=#Advanced;
                SourceExpr="Job No.";
                Visible=FALSE }

    { 50  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the blanket order that the record originates from.;
                           ENG=Specifies the number of the blanket order that the record originates from.];
                ApplicationArea=#Advanced;
                SourceExpr="Blanket Order No.";
                Visible=FALSE }

    { 52  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the blanket order line that the record originates from.;
                           ENG=Specifies the number of the blanket order line that the record originates from.];
                ApplicationArea=#Advanced;
                SourceExpr="Blanket Order Line No.";
                Visible=FALSE }

    { 54  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item ledger entry that the document or journal line is applied to.;
                           ENG=Specifies the number of the item ledger entry that the document or journal line is applied to.];
                ApplicationArea=#Advanced;
                SourceExpr="Appl.-to Item Entry";
                Visible=FALSE }

    { 56  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 1, which is one of two global dimension codes that you set up in the General Ledger Setup window.;
                           ENG=Specifies the code for Shortcut Dimension 1, which is one of two global dimension codes that you set up in the General Ledger Setup window.];
                ApplicationArea=#Dimensions;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 58  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 2, which is one of two global dimension codes that you set up in the General Ledger Setup window.;
                           ENG=Specifies the code for Shortcut Dimension 2, which is one of two global dimension codes that you set up in the General Ledger Setup window.];
                ApplicationArea=#Dimensions;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

