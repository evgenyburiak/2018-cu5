OBJECT Page 5167 Purchase Order Archive
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846,NAVGB11.00.00.19846;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Purchase Order Archive;
               ENG=Purchase Order Archive];
    DeleteAllowed=No;
    SourceTable=Table5109;
    SourceTableView=WHERE(Document Type=CONST(Order));
    PageType=Document;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 116     ;1   ;ActionGroup;
                      CaptionML=[ENU=Ver&sion;
                                 ENG=Ver&sion];
                      Image=Versions }
      { 119     ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Card;
                                 ENG=Card];
                      ToolTipML=[ENU=View or change detailed information about the record on the document or journal line.;
                                 ENG=View or change detailed information about the record on the document or journal line.];
                      ApplicationArea=#Advanced;
                      RunObject=Page 26;
                      RunPageLink=No.=FIELD(Buy-from Vendor No.);
                      Image=EditLines }
      { 120     ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 ENG=Dimensions];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 ENG=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyse transaction history.];
                      ApplicationArea=#Dimensions;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 133     ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 ENG=Co&mments];
                      ToolTipML=[ENU=View or add comments for the record.;
                                 ENG=View or add comments for the record.];
                      ApplicationArea=#Advanced;
                      RunObject=Page 5179;
                      RunPageLink=Document Type=FIELD(Document Type),
                                  No.=FIELD(No.),
                                  Document Line No.=CONST(0),
                                  Doc. No. Occurrence=FIELD(Doc. No. Occurrence),
                                  Version No.=FIELD(Version No.);
                      Image=ViewComments }
      { 130     ;2   ;Action    ;
                      CaptionML=[ENU=Print;
                                 ENG=Print];
                      ToolTipML=[ENU=Print the information in the window. A print request window opens where you can specify what to include on the print-out.;
                                 ENG=Print the information in the window. A print request window opens where you can specify what to include on the print-out.];
                      ApplicationArea=#Advanced;
                      Image=Print;
                      OnAction=BEGIN
                                 DocPrint.PrintPurchHeaderArch(Rec);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           ENG=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the involved entry or record, according to the specified number series.;
                           ENG=Specifies the number of the involved entry or record, according to the specified number series.];
                ApplicationArea=#Advanced;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the vendor who delivered the items.;
                           ENG=Specifies the name of the vendor who delivered the items.];
                ApplicationArea=#Advanced;
                SourceExpr="Buy-from Vendor No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the vendor who delivered the items.;
                           ENG=Specifies the name of the vendor who delivered the items.];
                ApplicationArea=#Advanced;
                SourceExpr="Buy-from Vendor Name" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the address of the vendor who delivered the items.;
                           ENG=Specifies the address of the vendor who delivered the items.];
                ApplicationArea=#Advanced;
                SourceExpr="Buy-from Address" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional part of the address of the vendor who delivered the items.;
                           ENG=Specifies an additional part of the address of the vendor who delivered the items.];
                ApplicationArea=#Advanced;
                SourceExpr="Buy-from Address 2" }

    { 1040000;2;Field     ;
                ToolTipML=[ENU=Specifies the city of the vendor who shipped the items. The program copies the city from the Buy-from City field on the purchase header.;
                           ENG=Specifies the city of the vendor who shipped the items. The program copies the city from the Buy-from City field on the purchase header.];
                SourceExpr="Buy-from City" }

    { 1040002;2;Field     ;
                ToolTipML=[ENU=Specifies the county of the vendor's address that the items are purchased from. The program retrieves the county name automatically from the Vendor table when the Buy-from Vendor No. field is filled in;
                           ENG=Specifies the county of the vendor's address that the items are purchased from. The program retrieves the county name automatically from the Vendor table when the Buy-from Vendor No. field is filled in];
                SourceExpr="Buy-from County" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the post code of the vendor who delivered the items.;
                           ENG=Specifies the postcode of the vendor who delivered the items.];
                ApplicationArea=#Advanced;
                SourceExpr="Buy-from Post Code" }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the contact person at the vendor who delivered the items.;
                           ENG=Specifies the name of the contact person at the vendor who delivered the items.];
                ApplicationArea=#Advanced;
                SourceExpr="Buy-from Contact" }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the entry's posting date.;
                           ENG=Specifies the entry's posting date.];
                ApplicationArea=#Advanced;
                SourceExpr="Posting Date" }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the order was created.;
                           ENG=Specifies the date when the order was created.];
                ApplicationArea=#Advanced;
                SourceExpr="Order Date" }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the related document was created.;
                           ENG=Specifies the date when the related document was created.];
                ApplicationArea=#Advanced;
                SourceExpr="Document Date" }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the vendor's order number.;
                           ENG=Specifies the vendor's order number.];
                ApplicationArea=#Advanced;
                SourceExpr="Vendor Order No." }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the vendor's shipment number. It is inserted in the corresponding field on the source document during posting.;
                           ENG=Specifies the vendor's shipment number. It is inserted in the corresponding field on the source document during posting.];
                ApplicationArea=#Advanced;
                SourceExpr="Vendor Shipment No." }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document number of the original document you received from the vendor. You can require the document number for posting, or let it be optional. By default, it's required, so that this document references the original. Making document numbers optional removes a step from the posting process. For example, if you attach the original invoice as a PDF, you might not need to enter the document number. To specify whether document numbers are required, in the Purchases & Payables Setup window, select or clear the Ext. Doc. No. Mandatory field.;
                           ENG=Specifies the document number of the original document you received from the vendor. You can require the document number for posting, or let it be optional. By default, it's required, so that this document references the original. Making document numbers optional removes a step from the posting process. For example, if you attach the original invoice as a PDF, you might not need to enter the document number. To specify whether document numbers are required, in the Purchases & Payables Setup window, select or clear the Ext. Doc. No. Mandatory field.];
                ApplicationArea=#Advanced;
                SourceExpr="Vendor Invoice No." }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the order address of the related vendor.;
                           ENG=Specifies the order address of the related vendor.];
                ApplicationArea=#Advanced;
                SourceExpr="Order Address Code" }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies which purchaser is assigned to the vendor.;
                           ENG=Specifies which purchaser is assigned to the vendor.];
                ApplicationArea=#Advanced;
                SourceExpr="Purchaser Code" }

    { 32  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the responsibility center, such as a distribution hub, that is associated with the involved user, company, customer, or vendor.;
                           ENG=Specifies the code of the responsibility centre, such as a distribution hub, that is associated with the involved user, company, customer, or vendor.];
                ApplicationArea=#Advanced;
                SourceExpr="Responsibility Center" }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the record is open, waiting to be approved, invoiced for prepayment, or released to the next stage of processing.;
                           ENG=Specifies whether the record is open, waiting to be approved, invoiced for prepayment, or released to the next stage of processing.];
                ApplicationArea=#Advanced;
                SourceExpr=Status }

    { 115 ;1   ;Part      ;
                Name=PurchLinesArchive;
                ApplicationArea=#Advanced;
                SubPageLink=Document No.=FIELD(No.),
                            Doc. No. Occurrence=FIELD(Doc. No. Occurrence),
                            Version No.=FIELD(Version No.);
                PagePartID=Page5168;
                PartType=Page }

    { 1905885101;1;Group  ;
                CaptionML=[ENU=Invoicing;
                           ENG=Invoicing] }

    { 36  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the vendor that you received the invoice from.;
                           ENG=Specifies the number of the vendor that you received the invoice from.];
                ApplicationArea=#Advanced;
                SourceExpr="Pay-to Vendor No." }

    { 38  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the vendor who you received the invoice from.;
                           ENG=Specifies the name of the vendor who you received the invoice from.];
                ApplicationArea=#Advanced;
                SourceExpr="Pay-to Name" }

    { 40  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the address of the vendor that you received the invoice from.;
                           ENG=Specifies the address of the vendor that you received the invoice from.];
                ApplicationArea=#Advanced;
                SourceExpr="Pay-to Address" }

    { 42  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional part of the address of the vendor that the invoice was received from.;
                           ENG=Specifies an additional part of the address of the vendor that the invoice was received from.];
                ApplicationArea=#Advanced;
                SourceExpr="Pay-to Address 2" }

    { 1040004;2;Field     ;
                ToolTipML=[ENU=Specifies the city of the vendor that you received the invoice from. The program copies the city from the Pay-to City field on the purchase header.;
                           ENG=Specifies the city of the vendor that you received the invoice from. The program copies the city from the Pay-to City field on the purchase header.];
                SourceExpr="Pay-to City" }

    { 1040006;2;Field     ;
                ToolTipML=[ENU=Specifies the county of the vendor you received the invoice from. The program copies the city from the Pay-to City field on the purchase header.;
                           ENG=Specifies the county of the vendor you received the invoice from. The program copies the city from the Pay-to City field on the purchase header.];
                SourceExpr="Pay-to County" }

    { 44  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the post code of the vendor that you received the invoice from.;
                           ENG=Specifies the postcode of the vendor that you received the invoice from.];
                ApplicationArea=#Advanced;
                SourceExpr="Pay-to Post Code" }

    { 46  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the person to contact about an invoice from this vendor.;
                           ENG=Specifies the name of the person to contact about an invoice from this vendor.];
                ApplicationArea=#Advanced;
                SourceExpr="Pay-to Contact" }

    { 48  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 1, which is one of two global dimension codes that you set up in the General Ledger Setup window.;
                           ENG=Specifies the code for Shortcut Dimension 1, which is one of two global dimension codes that you set up in the General Ledger Setup window.];
                ApplicationArea=#Dimensions;
                SourceExpr="Shortcut Dimension 1 Code" }

    { 50  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 2, which is one of two global dimension codes that you set up in the General Ledger Setup window.;
                           ENG=Specifies the code for Shortcut Dimension 2, which is one of two global dimension codes that you set up in the General Ledger Setup window.];
                ApplicationArea=#Dimensions;
                SourceExpr="Shortcut Dimension 2 Code" }

    { 52  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a formula that calculates the payment due date, payment discount date, and payment discount amount.;
                           ENG=Specifies a formula that calculates the payment due date, payment discount date, and payment discount amount.];
                ApplicationArea=#Advanced;
                SourceExpr="Payment Terms Code" }

    { 54  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies when the related purchase invoice must be paid.;
                           ENG=Specifies when the related purchase invoice must be paid.];
                ApplicationArea=#Advanced;
                SourceExpr="Due Date" }

    { 56  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the payment discount percent granted if payment is made on or before the date in the Pmt. Discount Date field.;
                           ENG=Specifies the payment discount percent granted if payment is made on or before the date in the Pmt. Discount Date field.];
                ApplicationArea=#Advanced;
                SourceExpr="Payment Discount %" }

    { 58  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how to make payment, such as with bank transfer, cash,  or check.;
                           ENG=Specifies how to make payment, such as with bank transfer, cash, or cheque.];
                ApplicationArea=#Advanced;
                SourceExpr="Payment Method Code" }

    { 60  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that the related entry represents an unpaid invoice for which either a payment suggestion, a reminder, or a finance charge memo exists.;
                           ENG=Specifies that the related entry represents an unpaid invoice for which either a payment suggestion, a reminder, or a finance charge memo exists.];
                ApplicationArea=#Advanced;
                SourceExpr="On Hold" }

    { 62  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the Unit Price and Line Amount fields on document lines should be shown with or without VAT.;
                           ENG=Specifies if the Unit Price and Line Amount fields on document lines should be shown with or without VAT.];
                ApplicationArea=#Advanced;
                SourceExpr="Prices Including VAT" }

    { 1906801201;1;Group  ;
                CaptionML=[ENU=Shipping;
                           ENG=Shipping] }

    { 64  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the customer at the address that the items are shipped to.;
                           ENG=Specifies the name of the customer at the address that the items are shipped to.];
                ApplicationArea=#Advanced;
                SourceExpr="Ship-to Name" }

    { 66  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the address that the items are shipped to.;
                           ENG=Specifies the address that the items are shipped to.];
                ApplicationArea=#Advanced;
                SourceExpr="Ship-to Address" }

    { 68  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional part of the ship-to address, in case it is a long address.;
                           ENG=Specifies an additional part of the ship-to address, in case it is a long address.];
                ApplicationArea=#Advanced;
                SourceExpr="Ship-to Address 2" }

    { 1040008;2;Field     ;
                ToolTipML=[ENU=Specifies the city name of the address that the items in the purchase order will be shipped to.;
                           ENG=Specifies the city name of the address that the items in the purchase order will be shipped to.];
                SourceExpr="Ship-to City" }

    { 1040010;2;Field     ;
                ToolTipML=[ENU=Specifies the county name of the address that the items in the purchase order will be shipped to.;
                           ENG=Specifies the county name of the address that the items in the purchase order will be shipped to.];
                SourceExpr="Ship-to County" }

    { 70  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the address that the items are shipped to.;
                           ENG=Specifies the postcode of the address that the items are shipped to.];
                ApplicationArea=#Advanced;
                SourceExpr="Ship-to Post Code" }

    { 72  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the contact person at the address that the items are shipped to.;
                           ENG=Specifies the name of the contact person at the address that the items are shipped to.];
                ApplicationArea=#Advanced;
                SourceExpr="Ship-to Contact" }

    { 74  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for the location where you want the items to be placed when they are received.;
                           ENG=Specifies a code for the location where you want the items to be placed when they are received.];
                ApplicationArea=#Advanced;
                SourceExpr="Location Code" }

    { 76  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the time it takes to make items part of available inventory, after the items have been posted as received.;
                           ENG=Specifies the time it takes to make items part of available inventory, after the items have been posted as received.];
                ApplicationArea=#Warehouse;
                SourceExpr="Inbound Whse. Handling Time" }

    { 78  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the delivery conditions of the related shipment, such as free on board (FOB).;
                           ENG=Specifies the delivery conditions of the related shipment, such as free on board (FOB).];
                ApplicationArea=#Advanced;
                SourceExpr="Shipment Method Code" }

    { 80  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a date formula for the amount of time it takes to replenish the item.;
                           ENG=Specifies a date formula for the amount of time it takes to replenish the item.];
                ApplicationArea=#Advanced;
                SourceExpr="Lead Time Calculation" }

    { 82  ;2   ;Field     ;
                ToolTipML=[ENU="Specifies the date you want the vendor to deliver your order. ";
                           ENG="Specifies the date you want the vendor to deliver your order. "];
                ApplicationArea=#Advanced;
                SourceExpr="Requested Receipt Date" }

    { 84  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date that the vendor has promised to deliver the order.;
                           ENG=Specifies the date that the vendor has promised to deliver the order.];
                ApplicationArea=#Advanced;
                SourceExpr="Promised Receipt Date" }

    { 86  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date on which the invoiced items were expected.;
                           ENG=Specifies the date on which the invoiced items were expected.];
                ApplicationArea=#Advanced;
                SourceExpr="Expected Receipt Date" }

    { 88  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the customer.;
                           ENG=Specifies the number of the customer.];
                ApplicationArea=#Advanced;
                SourceExpr="Sell-to Customer No." }

    { 90  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for an alternate shipment address if you want to ship to another address than the one that has been entered automatically. This field is also used in case of drop shipment.;
                           ENG=Specifies a code for an alternate shipment address if you want to ship to another address than the one that has been entered automatically. This field is also used in case of drop shipment.];
                ApplicationArea=#Advanced;
                SourceExpr="Ship-to Code" }

    { 1907468901;1;Group  ;
                CaptionML=[ENU=Foreign Trade;
                           ENG=Foreign Trade] }

    { 92  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the currency that is used on the entry.;
                           ENG=Specifies the currency that is used on the entry.];
                ApplicationArea=#Advanced;
                SourceExpr="Currency Code" }

    { 94  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of transaction that the document represents, for the purpose of reporting to INTRASTAT.;
                           ENG=Specifies the type of transaction that the document represents, for the purpose of reporting to INTRASTAT.];
                ApplicationArea=#Advanced;
                SourceExpr="Transaction Type" }

    { 96  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a specification of the document's transaction, for the purpose of reporting to INTRASTAT.;
                           ENG=Specifies a specification of the document's transaction, for the purpose of reporting to INTRASTAT.];
                ApplicationArea=#Advanced;
                SourceExpr="Transaction Specification" }

    { 98  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the transport method, for the purpose of reporting to INTRASTAT.;
                           ENG=Specifies the transport method, for the purpose of reporting to INTRASTAT.];
                ApplicationArea=#Advanced;
                SourceExpr="Transport Method" }

    { 100 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the port of entry where the items pass into your country/region, for reporting to Intrastat.;
                           ENG=Specifies the code of the port of entry where the items pass into your country/region, for reporting to Intrastat.];
                ApplicationArea=#Advanced;
                SourceExpr="Entry Point" }

    { 102 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the area of the customer or vendor, for the purpose of reporting to INTRASTAT.;
                           ENG=Specifies the area of the customer or vendor, for the purpose of reporting to INTRASTAT.];
                ApplicationArea=#Advanced;
                SourceExpr=Area }

    { 1904291901;1;Group  ;
                CaptionML=[ENU=Version;
                           ENG=Version] }

    { 104 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the version number of the archived document.;
                           ENG=Specifies the version number of the archived document.];
                ApplicationArea=#Advanced;
                SourceExpr="Version No." }

    { 106 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the user ID of the person who archived this document.;
                           ENG=Specifies the user ID of the person who archived this document.];
                ApplicationArea=#Advanced;
                SourceExpr="Archived By" }

    { 108 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the document was archived.;
                           ENG=Specifies the date when the document was archived.];
                ApplicationArea=#Advanced;
                SourceExpr="Date Archived" }

    { 110 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies what time the document was archived.;
                           ENG=Specifies what time the document was archived.];
                ApplicationArea=#Advanced;
                SourceExpr="Time Archived" }

    { 112 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that the archived document is linked to an interaction log entry.;
                           ENG=Specifies that the archived document is linked to an interaction log entry.];
                ApplicationArea=#Advanced;
                SourceExpr="Interaction Exist" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      DocPrint@1000 : Codeunit 229;

    BEGIN
    END.
  }
}

