OBJECT Page 5180 Sales Archive Comment Sheet
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Comment Sheet;
               ENG=Comment Sheet];
    SourceTable=Table5126;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the version number of the archived document.;
                           ENG=Specifies the version number of the archived document.];
                ApplicationArea=#Advanced;
                SourceExpr=Date }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document line number of the quote or order to which the comment applies.;
                           ENG=Specifies the document line number of the quote or order to which the comment applies.];
                ApplicationArea=#Advanced;
                SourceExpr=Code;
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the line number for the comment.;
                           ENG=Specifies the line number for the comment.];
                ApplicationArea=#Advanced;
                SourceExpr=Comment }

  }
  CODE
  {

    BEGIN
    END.
  }
}

