OBJECT Page 5216 Employee Statistics Groups
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Employee Statistics Groups;
               ENG=Employee Statistics Groups];
    LinksAllowed=No;
    SourceTable=Table5212;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for the employee statistics group.;
                           ENG=Specifies a code for the employee statistics group.];
                ApplicationArea=#Advanced;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description for the employee statistics group.;
                           ENG=Specifies a description for the employee statistics group.];
                ApplicationArea=#Advanced;
                SourceExpr=Description }

  }
  CODE
  {

    BEGIN
    END.
  }
}

