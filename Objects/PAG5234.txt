OBJECT Page 5234 HR Confidential Comment Sheet
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Confidential Comment Sheet;
               ENG=Confidential Comment Sheet];
    MultipleNewLines=Yes;
    LinksAllowed=No;
    SourceTable=Table5219;
    DataCaptionExpr=Caption(Rec);
    DelayedInsert=Yes;
    PageType=List;
    AutoSplitKey=Yes;
    OnNewRecord=BEGIN
                  SetUpNewLine;
                END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the comment was created.;
                           ENG=Specifies the date when the comment was created.];
                ApplicationArea=#Advanced;
                SourceExpr=Date }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the comment itself.;
                           ENG=Specifies the comment itself.];
                ApplicationArea=#Advanced;
                SourceExpr=Comment }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for the comment.;
                           ENG=Specifies a code for the comment.];
                ApplicationArea=#Advanced;
                SourceExpr=Code;
                Visible=FALSE }

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=untitled;ENG=untitled';
      Employee@1001 : Record 5200;
      ConfidentialInfo@1002 : Record 5216;

    LOCAL PROCEDURE Caption@1(HRCommentLine@1000 : Record 5219) : Text[110];
    BEGIN
      IF ConfidentialInfo.GET(HRCommentLine."No.",HRCommentLine.Code,HRCommentLine."Table Line No.") AND
         Employee.GET(HRCommentLine."No.")
      THEN
        EXIT(HRCommentLine."No." + ' ' + Employee.FullName + ' ' +
          ConfidentialInfo."Confidential Code");
      EXIT(Text000);
    END;

    BEGIN
    END.
  }
}

