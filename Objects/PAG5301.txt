OBJECT Page 5301 Outlook Synch. Entity Subform
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Lines;
               ENG=Lines];
    LinksAllowed=No;
    SourceTable=Table5301;
    DelayedInsert=Yes;
    PageType=ListPart;
    AutoSplitKey=Yes;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 ENG=&Line];
                      Image=Line }
      { 1900206204;2 ;Action    ;
                      CaptionML=[ENU=Fields;
                                 ENG=Fields];
                      ToolTipML=[ENU=View the fields to be synchronized.;
                                 ENG=View the fields to be synchronised.];
                      ApplicationArea=#Advanced;
                      Image=OutlookSyncFields;
                      OnAction=BEGIN
                                 ShowElementFields;
                               END;
                                }
      { 1902759804;2 ;Action    ;
                      CaptionML=[ENU=Dependencies;
                                 ENG=Dependencies];
                      ToolTipML=[ENU=View records that must be synchronized before dependent records, such as a customer record that must be synchronized before a contact record.;
                                 ENG=View records that must be synchronised before dependent records, such as a customer record that must be synchronised before a contact record.];
                      ApplicationArea=#Advanced;
                      OnAction=BEGIN
                                 ShowDependencies;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the Dynamics NAV table which corresponds to the Outlook item a collection of which is specified in the Outlook Collection field.;
                           ENG=Specifies the number of the Dynamics NAV table which corresponds to the Outlook item a collection of which is specified in the Outlook Collection field.];
                ApplicationArea=#Advanced;
                SourceExpr="Table No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the Dynamics NAV table to synchronize. The program fills in this field when you specify a table number in the Table No. field.;
                           ENG=Specifies the name of the Dynamics NAV table to synchronise. The program fills in this field when you specify a table number in the Table No. field.];
                ApplicationArea=#Advanced;
                SourceExpr="Table Caption" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a filter expression that defines which Dynamics NAV entries will be selected for synchronization. It is used to define relations between tables specified in the Table No. fields.;
                           ENG=Specifies a filter expression that defines which Dynamics NAV entries will be selected for synchronisation. It is used to define relations between tables specified in the Table No. fields.];
                ApplicationArea=#Advanced;
                SourceExpr="Table Relation";
                OnAssistEdit=BEGIN
                               CALCFIELDS("Master Table No.");
                               IF "Table No." <> 0 THEN BEGIN
                                 IF ISNULLGUID("Record GUID") THEN
                                   "Record GUID" := CREATEGUID;
                                 VALIDATE("Table Relation",OSynchSetupMgt.ShowOSynchFiltersForm("Record GUID","Table No.","Master Table No."));
                               END;
                             END;
                              }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the Outlook collection that corresponds to the set of Dynamics NAV records selected for synchronization in the Table No. field.;
                           ENG=Specifies the name of the Outlook collection that corresponds to the set of Dynamics NAV records selected for synchronisation in the Table No. field.];
                ApplicationArea=#Advanced;
                SourceExpr="Outlook Collection" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of dependent entities which must be synchronized. If these entities are synchronized, the synchronization process is considered to be completed successfully for the current entity. You assign these dependent entities on the Outlook Synch. Dependency table.;
                           ENG=Specifies the number of dependent entities which must be synchronised. If these entities are synchronised, the synchronisation process is considered to be completed successfully for the current entity. You assign these dependent entities on the Outlook Synch. Dependency table.];
                ApplicationArea=#Advanced;
                SourceExpr="No. of Dependencies" }

  }
  CODE
  {
    VAR
      OSynchSetupMgt@1000 : Codeunit 5300;

    BEGIN
    END.
  }
}

