OBJECT Page 5320 Exchange Folders
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Exchange Folders;
               ENG=Exchange Folders];
    SourceTable=Table5320;
    SourceTableView=SORTING(FullPath)
                    ORDER(Ascending);
    PageType=List;
    SourceTableTemporary=Yes;
    RefreshOnActivate=No;
    ShowFilter=No;
    OnOpenPage=BEGIN
                 IF NOT ExchangeWebServicesClient.ReadBuffer(Rec) THEN
                   ExchangeWebServicesClient.GetPublicFolders(Rec);
                 IF FINDFIRST THEN;
                 CurrPage.UPDATE(FALSE);
               END;

    OnClosePage=BEGIN
                  // This has to be called before GETRECORD that copies the content
                  CALCFIELDS("Unique ID");
                END;

    ActionList=ACTIONS
    {
      { 7       ;    ;ActionContainer;
                      Name=FolderActions;
                      CaptionML=[ENU=Folder actions;
                                 ENG=Folder actions];
                      ActionContainerType=ActionItems }
      { 2       ;1   ;Action    ;
                      Name=GetChildren;
                      CaptionML=[ENU=Get subfolders;
                                 ENG=Get subfolders];
                      ToolTipML=[ENU=Access the subfolder. Repeat as many times as you need to access the path that you want.;
                                 ENG=Access the subfolder. Repeat as many times as you need to access the path that you want.];
                      ApplicationArea=#Advanced;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Find;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 SelectedExchangeFolder@1001 : Record 5320;
                                 HasChildren@1002 : Boolean;
                               BEGIN
                                 IF NOT Cached THEN BEGIN
                                   SelectedExchangeFolder := Rec;
                                   HasChildren := ExchangeWebServicesClient.GetPublicFolders(Rec);
                                   CurrPage.SETRECORD(SelectedExchangeFolder);
                                   IF HasChildren THEN
                                     NEXT;
                                 END;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                Editable=FALSE;
                IndentationColumnName=Depth;
                IndentationControls=Name;
                ShowAsTree=Yes;
                GroupType=Repeater;
                Layout=Columns }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Folder Name;
                           ENG=Folder Name];
                ToolTipML=[ENU=Specifies the name of the public folder that is specified for use with email logging.;
                           ENG=Specifies the name of the public folder that is specified for use with email logging.];
                ApplicationArea=#Advanced;
                SourceExpr=Name }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=Folder Path;
                           ENG=Folder Path];
                ToolTipML=[ENU=Specifies the complete path to the public folder that is specified for use with email logging.;
                           ENG=Specifies the complete path to the public folder that is specified for use with email logging.];
                ApplicationArea=#Advanced;
                SourceExpr=FullPath }

  }
  CODE
  {
    VAR
      ExchangeWebServicesClient@1004 : Codeunit 5320;

    [External]
    PROCEDURE Initialize@1(ExchWebServicesClient@1000 : Codeunit 5320;Caption@1001 : Text);
    BEGIN
      ExchangeWebServicesClient := ExchWebServicesClient;
      CurrPage.CAPTION := Caption;
    END;

    BEGIN
    END.
  }
}

