OBJECT Page 5488 Dimension Values Entity
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[@@@={Locked};
               ENU=dimensionValues;
               ENG=dimensionValues];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table349;
    DelayedInsert=Yes;
    PageType=ListPart;
    ODataKeyFields=Id;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                Name=id;
                CaptionML=[@@@={Locked};
                           ENU=Id;
                           ENG=Id];
                ApplicationArea=#All;
                SourceExpr=Id }

    { 5   ;2   ;Field     ;
                Name=code;
                CaptionML=[@@@={Locked};
                           ENU=Code;
                           ENG=Code];
                ApplicationArea=#All;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                Name=displayName;
                CaptionML=[@@@={Locked};
                           ENU=DisplayName;
                           ENG=DisplayName];
                ApplicationArea=#All;
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                Name=lastModifiedDateTime;
                CaptionML=[@@@={Locked};
                           ENU=LastModifiedDateTime;
                           ENG=LastModifiedDateTime];
                ApplicationArea=#All;
                SourceExpr="Last Modified Date Time" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

