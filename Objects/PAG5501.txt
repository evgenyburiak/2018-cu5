OBJECT Page 5501 Balance Sheet Entity
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[@@@={Locked};
               ENU=balanceSheet;
               ENG=balanceSheet];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table5487;
    DelayedInsert=Yes;
    PageType=API;
    SourceTableTemporary=Yes;
    EntitySetName=balanceSheet;
    EntityName=balanceSheet;
    OnOpenPage=VAR
                 GraphMgtReports@1000 : Codeunit 5488;
                 RecVariant@1002 : Variant;
               BEGIN
                 RecVariant := Rec;
                 GraphMgtReports.SetUpBalanceSheetAPIData(RecVariant);
               END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 7   ;2   ;Field     ;
                Name=lineNumber;
                CaptionML=[@@@={Locked};
                           ENU=Line No.;
                           ENG=Line No.];
                ApplicationArea=#All;
                SourceExpr="Line No." }

    { 3   ;2   ;Field     ;
                Name=display;
                CaptionML=[@@@={Locked};
                           ENU=Description;
                           ENG=Description];
                ApplicationArea=#All;
                SourceExpr=Description }

    { 4   ;2   ;Field     ;
                Name=balance;
                CaptionML=[@@@={Locked};
                           ENU=Balance;
                           ENG=Balance];
                ApplicationArea=#All;
                BlankZero=Yes;
                SourceExpr=Balance;
                AutoFormatType=10 }

    { 5   ;2   ;Field     ;
                Name=lineType;
                CaptionML=[@@@={Locked};
                           ENU=Line Type;
                           ENG=Line Type];
                ApplicationArea=#All;
                SourceExpr="Line Type" }

    { 6   ;2   ;Field     ;
                Name=indentation;
                CaptionML=[@@@={Locked};
                           ENU=Indentation;
                           ENG=Indentation];
                ApplicationArea=#All;
                SourceExpr=Indentation }

    { 8   ;2   ;Field     ;
                Name=dateFilter;
                CaptionML=[@@@={Locked};
                           ENU=Date Filter;
                           ENG=Date Filter];
                ApplicationArea=#All;
                SourceExpr="Date Filter" }

  }
  CODE
  {
    VAR
      Balance@1002 : Decimal;

    BEGIN
    END.
  }
}

