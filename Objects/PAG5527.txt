OBJECT Page 5527 Purchase Invoice Entity
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[@@@={Locked};
               ENU=purchaseInvoices;
               ENG=purchaseInvoices];
    SourceTable=Table5477;
    DelayedInsert=Yes;
    PageType=API;
    EntitySetName=purchaseInvoices;
    EntityName=purchaseInvoice;
    OnAfterGetRecord=VAR
                       PurchInvAggregator@1000 : Codeunit 5529;
                     BEGIN
                       SetCalculatedFields;
                       PurchInvAggregator.RedistributeInvoiceDiscounts(Rec);
                     END;

    OnNewRecord=BEGIN
                  ClearCalculatedFields;
                END;

    OnInsertRecord=VAR
                     PurchInvAggregator@1000 : Codeunit 5529;
                   BEGIN
                     CheckVendor;
                     ProcessBillingPostalAddress;

                     PurchInvAggregator.PropagateOnInsert(Rec,TempFieldBuffer);
                     SetCalculatedFields;

                     PurchInvAggregator.RedistributeInvoiceDiscounts(Rec);

                     EXIT(FALSE);
                   END;

    OnModifyRecord=VAR
                     PurchInvAggregator@1000 : Codeunit 5529;
                   BEGIN
                     IF xRec.Id <> Id THEN
                       ERROR(CannotChangeIDErr);

                     ProcessBillingPostalAddress;

                     PurchInvAggregator.PropagateOnModify(Rec,TempFieldBuffer);

                     SetCalculatedFields;

                     PurchInvAggregator.RedistributeInvoiceDiscounts(Rec);

                     EXIT(FALSE);
                   END;

    OnDeleteRecord=VAR
                     PurchInvAggregator@1000 : Codeunit 5529;
                   BEGIN
                     PurchInvAggregator.PropagateOnDelete(Rec);

                     EXIT(FALSE);
                   END;

    ODataKeyFields=Id;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                Name=id;
                CaptionML=[@@@={Locked};
                           ENU=id;
                           ENG=id];
                ApplicationArea=#All;
                SourceExpr=Id;
                OnValidate=BEGIN
                             RegisterFieldSet(FIELDNO(Id));
                           END;
                            }

    { 4   ;2   ;Field     ;
                Name=number;
                CaptionML=[@@@={Locked};
                           ENU=Number;
                           ENG=Number];
                ApplicationArea=#All;
                SourceExpr="No.";
                Editable=FALSE;
                OnValidate=BEGIN
                             RegisterFieldSet(FIELDNO("No."));
                           END;
                            }

    { 5   ;2   ;Field     ;
                Name=invoiceDate;
                CaptionML=[@@@={Locked};
                           ENU=invoiceDate;
                           ENG=invoiceDate];
                ApplicationArea=#All;
                SourceExpr="Document Date";
                OnValidate=BEGIN
                             RegisterFieldSet(FIELDNO("Document Date"));
                             WORKDATE("Document Date"); // TODO: replicate page logic and set other dates appropriately
                           END;
                            }

    { 6   ;2   ;Field     ;
                Name=dueDate;
                CaptionML=[@@@={Locked};
                           ENU=dueDate;
                           ENG=dueDate];
                ApplicationArea=#All;
                SourceExpr="Due Date";
                OnValidate=BEGIN
                             RegisterFieldSet(FIELDNO("Due Date"));
                           END;
                            }

    { 18  ;2   ;Field     ;
                Name=vendorInvoiceNumber;
                CaptionML=[@@@={Locked};
                           ENU=vendorInvoiceNumber;
                           ENG=vendorInvoiceNumber];
                ApplicationArea=#All;
                SourceExpr="Vendor Invoice No.";
                OnValidate=BEGIN
                             RegisterFieldSet(FIELDNO("Vendor Invoice No."));
                           END;
                            }

    { 7   ;2   ;Field     ;
                Name=vendorId;
                CaptionML=[@@@={Locked};
                           ENU=vendorId;
                           ENG=vendorId];
                ApplicationArea=#All;
                SourceExpr="Vendor Id";
                OnValidate=BEGIN
                             Vendor.SETRANGE(Id,"Vendor Id");
                             IF NOT Vendor.FINDFIRST THEN
                               ERROR(CouldNotFindVendorErr);

                             "Buy-from Vendor No." := Vendor."No.";
                             RegisterFieldSet(FIELDNO("Vendor Id"));
                             RegisterFieldSet(FIELDNO("Buy-from Vendor No."));
                           END;
                            }

    { 19  ;2   ;Field     ;
                Name=vendorNumber;
                CaptionML=[@@@={Locked};
                           ENU=vendorNumber;
                           ENG=vendorNumber];
                ApplicationArea=#All;
                SourceExpr="Buy-from Vendor No.";
                OnValidate=BEGIN
                             IF Vendor."No." <> '' THEN
                               EXIT;

                             IF NOT Vendor.GET("Buy-from Vendor No.") THEN
                               ERROR(CouldNotFindVendorErr);

                             "Vendor Id" := Vendor.Id;
                             RegisterFieldSet(FIELDNO("Vendor Id"));
                             RegisterFieldSet(FIELDNO("Buy-from Vendor No."));
                           END;
                            }

    { 20  ;2   ;Field     ;
                Name=vendorName;
                CaptionML=[@@@={Locked};
                           ENU=vendorName;
                           ENG=vendorName];
                ApplicationArea=#All;
                SourceExpr="Buy-from Vendor Name" }

    { 21  ;2   ;Field     ;
                Name=buyFromAddress;
                CaptionML=[@@@={Locked};
                           ENU=buyFromAddress;
                           ENG=buyFromAddress];
                ToolTipML=[ENU=Specifies the billing address of the Purchase Invoice.;
                           ENG=Specifies the billing address of the Purchase Invoice.];
                ApplicationArea=#All;
                SourceExpr=BillingPostalAddressJSONText;
                OnValidate=BEGIN
                             BillingPostalAddressSet := TRUE;
                           END;

                ODataEDMType=POSTALADDRESS }

    { 8   ;2   ;Field     ;
                Name=currencyCode;
                CaptionML=[@@@={Locked};
                           ENU=currencyCode;
                           ENG=currencyCode];
                ApplicationArea=#All;
                SourceExpr=CurrencyCodeTxt;
                OnValidate=BEGIN
                             "Currency Code" :=
                               GraphMgtGeneralTools.TranslateCurrencyCodeToNAVCurrencyCode(
                                 LCYCurrencyCode,COPYSTR(CurrencyCodeTxt,1,MAXSTRLEN(LCYCurrencyCode)));
                             RegisterFieldSet(FIELDNO("Currency Code"));
                           END;
                            }

    { 9   ;2   ;Field     ;
                Name=paymentTerms;
                CaptionML=[@@@={Locked};
                           ENU=paymentTerms;
                           ENG=paymentTerms];
                ApplicationArea=#All;
                SourceExpr="Payment Terms Code";
                OnValidate=BEGIN
                             RegisterFieldSet(FIELDNO("Payment Terms Code"));
                           END;
                            }

    { 10  ;2   ;Field     ;
                Name=shipmentMethod;
                CaptionML=[@@@={Locked};
                           ENU=shipmentMethod;
                           ENG=shipmentMethod];
                ApplicationArea=#All;
                SourceExpr="Shipment Method Code";
                OnValidate=BEGIN
                             RegisterFieldSet(FIELDNO("Shipment Method Code"));
                           END;
                            }

    { 22  ;2   ;Field     ;
                Name=pricesIncludeTax;
                CaptionML=[@@@={Locked};
                           ENU=pricesIncludeTax;
                           ENG=pricesIncludeTax];
                ApplicationArea=#All;
                SourceExpr="Prices Including VAT";
                OnValidate=BEGIN
                             RegisterFieldSet(FIELDNO("Prices Including VAT"));
                           END;
                            }

    { 23  ;2   ;Part      ;
                Name=purchaseInvoiceLines;
                CaptionML=[@@@={Locked};
                           ENU=Lines;
                           ENG=Lines];
                ApplicationArea=#All;
                SubPageLink=Document Id=FIELD(Id);
                PagePartID=Page5528;
                EntitySetName=purchaseInvoiceLines;
                EntityName=purchaseInvoiceLine;
                PartType=Page }

    { 12  ;2   ;Field     ;
                Name=discountAmount;
                CaptionML=[@@@={Locked};
                           ENU=discountAmount;
                           ENG=discountAmount];
                ApplicationArea=#All;
                SourceExpr="Invoice Discount Amount";
                OnValidate=BEGIN
                             RegisterFieldSet(FIELDNO("Invoice Discount Amount"));
                           END;
                            }

    { 13  ;2   ;Field     ;
                Name=discountAppliedBeforeTax;
                CaptionML=[@@@={Locked};
                           ENU=discountAppliedBeforeTax;
                           ENG=discountAppliedBeforeTax];
                ApplicationArea=#All;
                SourceExpr="Discount Applied Before Tax" }

    { 11  ;2   ;Field     ;
                Name=totalAmountExcludingTax;
                CaptionML=[@@@={Locked};
                           ENU=totalAmountExcludingTax;
                           ENG=totalAmountExcludingTax];
                ApplicationArea=#All;
                SourceExpr=Amount;
                Editable=FALSE }

    { 14  ;2   ;Field     ;
                Name=totalTaxAmount;
                CaptionML=[@@@={Locked};
                           ENU=totalTaxAmount;
                           ENG=totalTaxAmount];
                ApplicationArea=#All;
                SourceExpr="Total Tax Amount";
                Editable=FALSE;
                OnValidate=BEGIN
                             RegisterFieldSet(FIELDNO("Total Tax Amount"));
                           END;
                            }

    { 17  ;2   ;Field     ;
                Name=totalAmountIncludingTax;
                CaptionML=[@@@={Locked};
                           ENU=totalAmountIncludingTax;
                           ENG=totalAmountIncludingTax];
                ApplicationArea=#All;
                SourceExpr="Amount Including VAT";
                OnValidate=BEGIN
                             RegisterFieldSet(FIELDNO("Amount Including VAT"));
                           END;
                            }

    { 15  ;2   ;Field     ;
                Name=status;
                CaptionML=[@@@={Locked};
                           ENU=status;
                           ENG=status];
                ApplicationArea=#All;
                SourceExpr=Status;
                Editable=FALSE }

    { 16  ;2   ;Field     ;
                Name=lastModifiedDateTime;
                CaptionML=[@@@={Locked};
                           ENU=lastModifiedDateTime;
                           ENG=lastModifiedDateTime];
                ApplicationArea=#All;
                SourceExpr="Last Modified Date Time" }

  }
  CODE
  {
    VAR
      TempFieldBuffer@1006 : TEMPORARY Record 8450;
      Vendor@1005 : Record 23;
      GraphMgtGeneralTools@1004 : Codeunit 5465;
      LCYCurrencyCode@1003 : Code[10];
      CurrencyCodeTxt@1002 : Text;
      BillingPostalAddressJSONText@1001 : Text;
      BillingPostalAddressSet@1000 : Boolean;
      CannotChangeIDErr@1010 : TextConst '@@@={Locked};ENU=The id cannot be changed.;ENG=The id cannot be changed.';
      VendorNotProvidedErr@1009 : TextConst '@@@={Locked};ENU=A vendorNumber or a vendorID must be provided.;ENG=A vendorNumber or a vendorID must be provided.';
      CouldNotFindVendorErr@1008 : TextConst '@@@={Locked};ENU=The vendor cannot be found.;ENG=The vendor cannot be found.';

    LOCAL PROCEDURE SetCalculatedFields@6();
    VAR
      GraphMgtPurchaseInvoice@1000 : Codeunit 5527;
    BEGIN
      BillingPostalAddressJSONText := GraphMgtPurchaseInvoice.PayToVendorAddressToJSON(Rec);
      CurrencyCodeTxt := GraphMgtGeneralTools.TranslateNAVCurrencyCodeToCurrencyCode(LCYCurrencyCode,"Currency Code");
    END;

    LOCAL PROCEDURE ClearCalculatedFields@10();
    BEGIN
      CLEAR(BillingPostalAddressJSONText);
      TempFieldBuffer.DELETEALL;
    END;

    LOCAL PROCEDURE RegisterFieldSet@11(FieldNo@1000 : Integer);
    VAR
      LastOrderNo@1001 : Integer;
    BEGIN
      LastOrderNo := 1;
      IF TempFieldBuffer.FINDLAST THEN
        LastOrderNo := TempFieldBuffer.Order + 1;

      CLEAR(TempFieldBuffer);
      TempFieldBuffer.Order := LastOrderNo;
      TempFieldBuffer."Table ID" := DATABASE::"Purch. Inv. Entity Aggregate";
      TempFieldBuffer."Field ID" := FieldNo;
      TempFieldBuffer.INSERT;
    END;

    LOCAL PROCEDURE CheckVendor@1();
    VAR
      BlankGUID@1000 : GUID;
    BEGIN
      IF ("Buy-from Vendor No." = '') AND
         ("Vendor Id" = BlankGUID)
      THEN
        ERROR(VendorNotProvidedErr);
    END;

    LOCAL PROCEDURE ProcessBillingPostalAddress@5();
    VAR
      GraphMgtPurchaseInvoice@1000 : Codeunit 5527;
    BEGIN
      IF NOT BillingPostalAddressSet THEN
        EXIT;

      GraphMgtPurchaseInvoice.ProcessComplexTypes(Rec,BillingPostalAddressJSONText);

      IF xRec."Buy-from Address" <> "Buy-from Address" THEN
        RegisterFieldSet(FIELDNO("Buy-from Address"));

      IF xRec."Buy-from Address 2" <> "Buy-from Address 2" THEN
        RegisterFieldSet(FIELDNO("Buy-from Address 2"));

      IF xRec."Buy-from City" <> "Buy-from City" THEN
        RegisterFieldSet(FIELDNO("Buy-from City"));

      IF xRec."Buy-from Country/Region Code" <> "Buy-from Country/Region Code" THEN
        RegisterFieldSet(FIELDNO("Buy-from Country/Region Code"));

      IF xRec."Buy-from Post Code" <> "Buy-from Post Code" THEN
        RegisterFieldSet(FIELDNO("Buy-from Post Code"));

      IF xRec."Buy-from County" <> "Buy-from County" THEN
        RegisterFieldSet(FIELDNO("Buy-from County"));
    END;

    BEGIN
    END.
  }
}

