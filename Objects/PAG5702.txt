OBJECT Page 5702 Stockkeeping Unit Comment List
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Stockkeeping Unit Comment List;
               ENG=Stockkeeping Unit Comment List];
    MultipleNewLines=Yes;
    LinksAllowed=No;
    SourceTable=Table5701;
    DataCaptionFields=Location Code,Item No.,Variant Code;
    PageType=List;
    AutoSplitKey=Yes;
    OnNewRecord=BEGIN
                  SetUpNewLine;
                END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the item number to which the SKU refers to.;
                           ENG=Specifies the item number to which the SKU refers to.];
                ApplicationArea=#Advanced;
                SourceExpr="Item No." }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the variant of the item on the line.;
                           ENG=Specifies the variant of the item on the line.];
                ApplicationArea=#Advanced;
                SourceExpr="Variant Code" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the location code (for example, warehouse of distribution center) to which the SKU applies.;
                           ENG=Specifies the location code (for example, warehouse of distribution centre) to which the SKU applies.];
                ApplicationArea=#Advanced;
                SourceExpr="Location Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date the comment was created.;
                           ENG=Specifies the date the comment was created.];
                ApplicationArea=#Advanced;
                SourceExpr=Date }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the comment itself.;
                           ENG=Specifies the comment itself.];
                ApplicationArea=#Advanced;
                SourceExpr=Comment }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for the comment.;
                           ENG=Specifies a code for the comment.];
                ApplicationArea=#Advanced;
                SourceExpr=Code;
                Visible=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

