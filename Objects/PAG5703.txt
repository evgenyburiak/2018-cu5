OBJECT Page 5703 Location Card
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846,NAVGB11.00.00.19846;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Location Card;
               ENG=Location Card];
    SourceTable=Table14;
    PageType=Card;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Location;
                                ENG=New,Process,Report,Location];
    OnInit=BEGIN
             UseCrossDockingEnable := TRUE;
             UsePutAwayWorksheetEnable := TRUE;
             BinMandatoryEnable := TRUE;
             RequireShipmentEnable := TRUE;
             RequireReceiveEnable := TRUE;
             RequirePutAwayEnable := TRUE;
             RequirePickEnable := TRUE;
             DefaultBinSelectionEnable := TRUE;
             UseADCSEnable := TRUE;
             DirectedPutawayandPickEnable := TRUE;
             CrossDockBinCodeEnable := TRUE;
             PickAccordingToFEFOEnable := TRUE;
             AdjustmentBinCodeEnable := TRUE;
             ShipmentBinCodeEnable := TRUE;
             ReceiptBinCodeEnable := TRUE;
             FromProductionBinCodeEnable := TRUE;
             ToProductionBinCodeEnable := TRUE;
             OpenShopFloorBinCodeEnable := TRUE;
             ToAssemblyBinCodeEnable := TRUE;
             FromAssemblyBinCodeEnable := TRUE;
             AssemblyShipmentBinCodeEnable := TRUE;
             CrossDockDueDateCalcEnable := TRUE;
             AlwaysCreatePutawayLineEnable := TRUE;
             AlwaysCreatePickLineEnable := TRUE;
             PutAwayTemplateCodeEnable := TRUE;
             AllowBreakbulkEnable := TRUE;
             SpecialEquipmentEnable := TRUE;
             BinCapacityPolicyEnable := TRUE;
             BaseCalendarCodeEnable := TRUE;
             InboundWhseHandlingTimeEnable := TRUE;
             OutboundWhseHandlingTimeEnable := TRUE;
             EditInTransit := TRUE;
           END;

    OnAfterGetRecord=BEGIN
                       UpdateEnabled;
                       TransitValidation;
                     END;

    OnAfterGetCurrRecord=BEGIN
                           HandleAddressLookupVisibility;
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 36      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Location;
                                 ENG=&Location];
                      Image=Warehouse }
      { 32      ;2   ;Action    ;
                      CaptionML=[ENU=&Resource Locations;
                                 ENG=&Resource Locations];
                      ToolTipML=[ENU=View or edit information about where resources are located. In this window, you can assign resources to locations.;
                                 ENG=View or edit information about where resources are located. In this window, you can assign resources to locations.];
                      ApplicationArea=#Advanced;
                      RunObject=Page 6015;
                      RunPageLink=Location Code=FIELD(Code);
                      Image=Resource }
      { 11      ;2   ;Action    ;
                      CaptionML=[ENU=Inventory Posting Setup;
                                 ENG=Inventory Posting Setup];
                      ToolTipML=[ENU=Set up links between inventory posting groups, inventory locations, and general ledger accounts to define where transactions for inventory items are recorded in the general ledger.;
                                 ENG=Set up links between inventory posting groups, inventory locations, and general ledger accounts to define where transactions for inventory items are recorded in the general ledger.];
                      ApplicationArea=#Location;
                      RunObject=Page 5826;
                      RunPageLink=Location Code=FIELD(Code);
                      Promoted=Yes;
                      Image=PostedInventoryPick;
                      PromotedCategory=Category4;
                      PromotedOnly=Yes }
      { 7300    ;2   ;Action    ;
                      CaptionML=[ENU=&Zones;
                                 ENG=&Zones];
                      ToolTipML=[ENU=View or edit information about zones that you use in your warehouse to structure your bins under zones.;
                                 ENG=View or edit information about zones that you use in your warehouse to structure your bins under zones.];
                      ApplicationArea=#Warehouse;
                      RunObject=Page 7300;
                      RunPageLink=Location Code=FIELD(Code);
                      Promoted=Yes;
                      Image=Zones;
                      PromotedCategory=Process }
      { 7302    ;2   ;Action    ;
                      CaptionML=[ENU=&Bins;
                                 ENG=&Bins];
                      ToolTipML=[ENU=View or edit information about zones that you use in your warehouse to hold items.;
                                 ENG=View or edit information about zones that you use in your warehouse to hold items.];
                      ApplicationArea=#Warehouse;
                      RunObject=Page 7302;
                      RunPageLink=Location Code=FIELD(Code);
                      Promoted=Yes;
                      Image=Bins;
                      PromotedCategory=Process }
      { 101     ;2   ;Action    ;
                      CaptionML=[ENU=Online Map;
                                 ENG=Online Map];
                      ToolTipML=[ENU=View the address on an online map.;
                                 ENG=View the address on an online map.];
                      ApplicationArea=#Location;
                      Promoted=Yes;
                      Image=Map;
                      PromotedCategory=Category4;
                      PromotedOnly=Yes;
                      OnAction=BEGIN
                                 DisplayMap;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           ENG=General] }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a location code for the warehouse or distribution center where your items are handled and stored before being sold.;
                           ENG=Specifies a location code for the warehouse or distribution centre where your items are handled and stored before being sold.];
                ApplicationArea=#Location;
                SourceExpr=Code;
                Importance=Promoted }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name or address of the location.;
                           ENG=Specifies the name or address of the location.];
                ApplicationArea=#Location;
                SourceExpr=Name }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that this location is an in-transit location.;
                           ENG=Specifies that this location is an in-transit location.];
                ApplicationArea=#Location;
                SourceExpr="Use As In-Transit";
                Editable=EditInTransit;
                OnValidate=BEGIN
                             UpdateEnabled;
                           END;
                            }

    { 1902768601;1;Group  ;
                CaptionML=[ENU=Address & Contact;
                           ENG=Address & Contact];
                GroupType=Group }

    { 13  ;2   ;Group     ;
                Name=AddressDetails;
                CaptionML=[ENU=Address;
                           ENG=Address];
                GroupType=Group }

    { 1040003;3;Group     ;
                Visible=IsAddressLookupTextEnabled;
                GroupType=Group }

    { 1040001;4;Field     ;
                Name=LookupAddress;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=LookupAddressLbl;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              ShowPostcodeLookup(TRUE);
                            END;

                ShowCaption=No }

    { 1040008;3;Field     ;
                Name=Address;
                ToolTipML=[ENU=Specifies the customer's address. This address will appear on all sales documents for the customer.;
                           ENG=Specifies the customer's address. This address will appear on all sales documents for the customer.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Address;
                OnValidate=VAR
                             PostcodeBusinessLogic@1040000 : Codeunit 10500;
                           BEGIN
                             PostcodeBusinessLogic.ShowDiscoverabilityNotificationIfNeccessary;
                           END;
                            }

    { 8   ;3   ;Field     ;
                ToolTipML=[ENU=Specifies additional address information.;
                           ENG=Specifies additional address information.];
                ApplicationArea=#Location;
                SourceExpr="Address 2" }

    { 1040000;3;Field     ;
                ToolTipML=[ENU=Specifies the city of the location.;
                           ENG=Specifies the city of the location.];
                ApplicationArea=#Location;
                SourceExpr=City }

    { 1040002;3;Field     ;
                ToolTipML=[ENU=Specifies the county of the location.;
                           ENG=Specifies the county of the location.];
                ApplicationArea=#Location;
                SourceExpr=County }

    { 1040004;3;Field     ;
                ToolTipML=[ENU=Specifies the postal code.;
                           ENG=Specifies the postcode.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Post Code";
                Importance=Promoted;
                OnValidate=VAR
                             PostcodeBusinessLogic@1040000 : Codeunit 10500;
                           BEGIN
                             PostcodeBusinessLogic.ShowDiscoverabilityNotificationIfNeccessary;
                             ShowPostcodeLookup(FALSE);
                           END;
                            }

    { 14  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the country/region of the address.;
                           ENG=Specifies the country/region of the address.];
                ApplicationArea=#Location;
                SourceExpr="Country/Region Code";
                OnValidate=BEGIN
                             HandleAddressLookupVisibility;
                           END;
                            }

    { 16  ;3   ;Field     ;
                Name=ShowMap;
                ToolTipML=[ENU=Specifies the address of the location on your preferred map website.;
                           ENG=Specifies the address of the location on your preferred map website.];
                ApplicationArea=#Location;
                SourceExpr=ShowMapLbl;
                Editable=FALSE;
                Style=StrongAccent;
                StyleExpr=TRUE;
                OnDrillDown=BEGIN
                              CurrPage.UPDATE;
                              DisplayMap;
                            END;

                ShowCaption=No }

    { 15  ;2   ;Group     ;
                Name=ContactDetails;
                CaptionML=[ENU=Contact;
                           ENG=Contact];
                GroupType=Group }

    { 12  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the contact person at the location;
                           ENG=Specifies the name of the contact person at the location];
                ApplicationArea=#Location;
                SourceExpr=Contact }

    { 18  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the telephone number of the location.;
                           ENG=Specifies the telephone number of the location.];
                ApplicationArea=#Location;
                SourceExpr="Phone No.";
                Importance=Promoted }

    { 28  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the fax number of the location.;
                           ENG=Specifies the fax number of the location.];
                ApplicationArea=#Advanced;
                SourceExpr="Fax No." }

    { 30  ;3   ;Field     ;
                ExtendedDatatype=E-Mail;
                ToolTipML=[ENU=Specifies the email address of the location.;
                           ENG=Specifies the email address of the location.];
                ApplicationArea=#Location;
                SourceExpr="E-Mail" }

    { 26  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the location's web site.;
                           ENG=Specifies the location's web site.];
                ApplicationArea=#Location;
                SourceExpr="Home Page" }

    { 1907509201;1;Group  ;
                CaptionML=[ENU=Warehouse;
                           ENG=Warehouse] }

    { 42  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the location requires a dedicated warehouse activity when receiving items.;
                           ENG=Specifies if the location requires a dedicated warehouse activity when receiving items.];
                ApplicationArea=#Warehouse;
                SourceExpr="Require Receive";
                Enabled=RequireReceiveEnable;
                OnValidate=BEGIN
                             UpdateEnabled;
                           END;
                            }

    { 60  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the location requires a dedicated warehouse activity when shipping items.;
                           ENG=Specifies if the location requires a dedicated warehouse activity when shipping items.];
                ApplicationArea=#Warehouse;
                SourceExpr="Require Shipment";
                Enabled=RequireShipmentEnable;
                OnValidate=BEGIN
                             UpdateEnabled;
                           END;
                            }

    { 50  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the location requires a dedicated warehouse activity when putting items away.;
                           ENG=Specifies if the location requires a dedicated warehouse activity when putting items away.];
                ApplicationArea=#Warehouse;
                SourceExpr="Require Put-away";
                Importance=Promoted;
                Enabled=RequirePutAwayEnable;
                OnValidate=BEGIN
                             UpdateEnabled;
                           END;
                            }

    { 87  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if put-aways for posted warehouse receipts must be created with the put-away worksheet. If the check box is not selected, put-aways are created directly when you post a warehouse receipt.;
                           ENG=Specifies if put-aways for posted warehouse receipts must be created with the put-away worksheet. If the check box is not selected, put-aways are created directly when you post a warehouse receipt.];
                ApplicationArea=#Warehouse;
                SourceExpr="Use Put-away Worksheet";
                Enabled=UsePutAwayWorksheetEnable }

    { 52  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the location requires a dedicated warehouse activity when picking items.;
                           ENG=Specifies if the location requires a dedicated warehouse activity when picking items.];
                ApplicationArea=#Warehouse;
                SourceExpr="Require Pick";
                Importance=Promoted;
                Enabled=RequirePickEnable;
                OnValidate=BEGIN
                             UpdateEnabled;
                           END;
                            }

    { 78  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the location requires that a bin code is specified on all item transactions.;
                           ENG=Specifies if the location requires that a bin code is specified on all item transactions.];
                ApplicationArea=#Warehouse;
                SourceExpr="Bin Mandatory";
                Importance=Promoted;
                Enabled=BinMandatoryEnable;
                OnValidate=BEGIN
                             UpdateEnabled;
                           END;
                            }

    { 90  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the location requires advanced warehouse functionality, such as calculated bin suggestion.;
                           ENG=Specifies if the location requires advanced warehouse functionality, such as calculated bin suggestion.];
                ApplicationArea=#Warehouse;
                SourceExpr="Directed Put-away and Pick";
                Enabled=DirectedPutawayandPickEnable;
                OnValidate=BEGIN
                             UpdateEnabled;
                           END;
                            }

    { 45  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the automatic data capture system that warehouse employees must use to keep track of items within the warehouse.;
                           ENG=Specifies the automatic data capture system that warehouse employees must use to keep track of items within the warehouse.];
                ApplicationArea=#Warehouse;
                SourceExpr="Use ADCS";
                Enabled=UseADCSEnable }

    { 82  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the method used to select the default bin.;
                           ENG=Specifies the method used to select the default bin.];
                ApplicationArea=#Warehouse;
                SourceExpr="Default Bin Selection";
                Enabled=DefaultBinSelectionEnable }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU="Specifies a date formula for the time it takes to get items ready to ship from this location. The time element is used in the calculation of the delivery date as follows: Shipment Date + Outbound Warehouse Handling Time = Planned Shipment Date + Shipping Time = Planned Delivery Date.";
                           ENG="Specifies a date formula for the time it takes to get items ready to ship from this location. The time element is used in the calculation of the delivery date as follows: Shipment Date + Outbound Warehouse Handling Time = Planned Shipment Date + Shipping Time = Planned Delivery Date."];
                ApplicationArea=#Warehouse;
                SourceExpr="Outbound Whse. Handling Time";
                Enabled=OutboundWhseHandlingTimeEnable }

    { 33  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the time it takes to make items part of available inventory, after the items have been posted as received.;
                           ENG=Specifies the time it takes to make items part of available inventory, after the items have been posted as received.];
                ApplicationArea=#Warehouse;
                SourceExpr="Inbound Whse. Handling Time";
                Enabled=InboundWhseHandlingTimeEnable }

    { 40  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a customizable calendar for planning that holds the location's working days and holidays.;
                           ENG=Specifies a customisable calendar for planning that holds the location's working days and holidays.];
                ApplicationArea=#Warehouse;
                SourceExpr="Base Calendar Code";
                Enabled=BaseCalendarCodeEnable }

    { 47  ;2   ;Field     ;
                Name=Customized Calendar;
                CaptionML=[ENU=Customized Calendar;
                           ENG=Customised Calendar];
                ToolTipML=[ENU=Specifies if the location has a customized calendar with working days that are different from those in the company's base calendar.;
                           ENG=Specifies if the location has a customised calendar with working days that are different from those in the company's base calendar.];
                ApplicationArea=#Warehouse;
                SourceExpr=CalendarMgmt.CustomizedCalendarExistText(CustomizedCalendar."Source Type"::Location,Code,'',"Base Calendar Code");
                Editable=FALSE;
                OnDrillDown=BEGIN
                              CurrPage.SAVERECORD;
                              TESTFIELD("Base Calendar Code");
                              CalendarMgmt.ShowCustomizedCalendar(
                                CustomizedCalEntry."Source Type"::Location,Code,'',"Base Calendar Code");
                            END;
                             }

    { 49  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the location supports movement of items directly from the receiving dock to the shipping dock.;
                           ENG=Specifies if the location supports movement of items directly from the receiving dock to the shipping dock.];
                ApplicationArea=#Warehouse;
                SourceExpr="Use Cross-Docking";
                Enabled=UseCrossDockingEnable;
                OnValidate=BEGIN
                             UpdateEnabled;
                           END;
                            }

    { 55  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the cross-dock due date calculation.;
                           ENG=Specifies the cross-dock due date calculation.];
                ApplicationArea=#Warehouse;
                SourceExpr="Cross-Dock Due Date Calc.";
                Enabled=CrossDockDueDateCalcEnable }

    { 1907883401;1;Group  ;
                CaptionML=[ENU=Bins;
                           ENG=Bins] }

    { 100 ;2   ;Group     ;
                CaptionML=[ENU=Receipt;
                           ENG=Receipt] }

    { 98  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the default receipt bin code.;
                           ENG=Specifies the default receipt bin code.];
                ApplicationArea=#Warehouse;
                SourceExpr="Receipt Bin Code";
                Importance=Promoted;
                Enabled=ReceiptBinCodeEnable }

    { 105 ;2   ;Group     ;
                CaptionML=[ENU=Shipment;
                           ENG=Shipment] }

    { 103 ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the default shipment bin code.;
                           ENG=Specifies the default shipment bin code.];
                ApplicationArea=#Warehouse;
                SourceExpr="Shipment Bin Code";
                Importance=Promoted;
                Enabled=ShipmentBinCodeEnable }

    { 63  ;2   ;Group     ;
                CaptionML=[ENU=Production;
                           ENG=Production] }

    { 66  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the bin that functions as the default open shop floor bin.;
                           ENG=Specifies the bin that functions as the default open shop floor bin.];
                ApplicationArea=#Warehouse;
                SourceExpr="Open Shop Floor Bin Code";
                Enabled=OpenShopFloorBinCodeEnable }

    { 68  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the bin in the production area where components picked for production are placed by default, before they can be consumed.;
                           ENG=Specifies the bin in the production area where components picked for production are placed by default, before they can be consumed.];
                ApplicationArea=#Warehouse;
                SourceExpr="To-Production Bin Code";
                Enabled=ToProductionBinCodeEnable }

    { 72  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the bin in the production area, where finished end items are taken from by default, when the process involves warehouse activity.;
                           ENG=Specifies the bin in the production area, where finished end items are taken from by default, when the process involves warehouse activity.];
                ApplicationArea=#Warehouse;
                SourceExpr="From-Production Bin Code";
                Enabled=FromProductionBinCodeEnable }

    { 58  ;2   ;Group     ;
                CaptionML=[ENU=Adjustment;
                           ENG=Adjustment] }

    { 61  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the bin in which you record observed differences in inventory quantities.;
                           ENG=Specifies the code of the bin in which you record observed differences in inventory quantities.];
                ApplicationArea=#Warehouse;
                SourceExpr="Adjustment Bin Code";
                Enabled=AdjustmentBinCodeEnable }

    { 71  ;2   ;Group     ;
                CaptionML=[ENU=Cross-Dock;
                           ENG=Cross-Dock] }

    { 65  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the bin code that is used by default for the receipt of items to be cross-docked.;
                           ENG=Specifies the bin code that is used by default for the receipt of items to be cross-docked.];
                ApplicationArea=#Warehouse;
                SourceExpr="Cross-Dock Bin Code";
                Enabled=CrossDockBinCodeEnable }

    { 3   ;2   ;Group     ;
                Name=Assembly;
                CaptionML=[ENU=Assembly;
                           ENG=Assembly];
                GroupType=Group }

    { 7   ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the bin in the assembly area where components are placed by default before they can be consumed in assembly.;
                           ENG=Specifies the bin in the assembly area where components are placed by default before they can be consumed in assembly.];
                ApplicationArea=#Warehouse;
                SourceExpr="To-Assembly Bin Code";
                Enabled=ToAssemblyBinCodeEnable }

    { 5   ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the bin in the assembly area where finished assembly items are posted to when they are assembled to stock.;
                           ENG=Specifies the bin in the assembly area where finished assembly items are posted to when they are assembled to stock.];
                ApplicationArea=#Warehouse;
                SourceExpr="From-Assembly Bin Code";
                Enabled=FromAssemblyBinCodeEnable }

    { 9   ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the bin where finished assembly items are posted to when they are assembled to a linked sales order.;
                           ENG=Specifies the bin where finished assembly items are posted to when they are assembled to a linked sales order.];
                ApplicationArea=#Warehouse;
                SourceExpr="Asm.-to-Order Shpt. Bin Code";
                Enabled=AssemblyShipmentBinCodeEnable }

    { 1905577301;1;Group  ;
                CaptionML=[ENU=Bin Policies;
                           ENG=Bin Policies] }

    { 56  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies where the program will first looks for special equipment designated for warehouse activities.;
                           ENG=Specifies where the program will first looks for special equipment designated for warehouse activities.];
                ApplicationArea=#Warehouse;
                SourceExpr="Special Equipment";
                Enabled=SpecialEquipmentEnable }

    { 76  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how bins are automatically filled, according to their capacity.;
                           ENG=Specifies how bins are automatically filled, according to their capacity.];
                ApplicationArea=#Warehouse;
                SourceExpr="Bin Capacity Policy";
                Importance=Promoted;
                Enabled=BinCapacityPolicyEnable }

    { 74  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that an order can be fulfilled with items stored in alternate units of measure, if an item stored in the requested unit of measure is not found.;
                           ENG=Specifies that an order can be fulfilled with items stored in alternate units of measure, if an item stored in the requested unit of measure is not found.];
                ApplicationArea=#Warehouse;
                SourceExpr="Allow Breakbulk";
                Enabled=AllowBreakbulkEnable }

    { 84  ;2   ;Group     ;
                CaptionML=[ENU=Put-away;
                           ENG=Put-away] }

    { 85  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the put-away template to be used at this location.;
                           ENG=Specifies the put-away template to be used at this location.];
                ApplicationArea=#Warehouse;
                SourceExpr="Put-away Template Code";
                Enabled=PutAwayTemplateCodeEnable }

    { 20  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies that a put-away line is created, even if an appropriate zone and bin in which to place the items cannot be found.;
                           ENG=Specifies that a put-away line is created, even if an appropriate zone and bin in which to place the items cannot be found.];
                ApplicationArea=#Warehouse;
                SourceExpr="Always Create Put-away Line";
                Enabled=AlwaysCreatePutawayLineEnable }

    { 89  ;2   ;Group     ;
                CaptionML=[ENU=Pick;
                           ENG=Pick] }

    { 92  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies that a pick line is created, even if an appropriate zone and bin from which to pick the item cannot be found.;
                           ENG=Specifies that a pick line is created, even if an appropriate zone and bin from which to pick the item cannot be found.];
                ApplicationArea=#Warehouse;
                SourceExpr="Always Create Pick Line";
                Enabled=AlwaysCreatePickLineEnable }

    { 95  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies whether to use the First-Expired-First-Out (FEFO) method to determine which items to pick, according to expiration dates.;
                           ENG=Specifies whether to use the First-Expired-First-Out (FEFO) method to determine which items to pick, according to expiration dates.];
                ApplicationArea=#Warehouse;
                SourceExpr="Pick According to FEFO";
                Importance=Promoted;
                Enabled=PickAccordingToFEFOEnable }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      CustomizedCalEntry@1002 : Record 7603;
      CustomizedCalendar@1004 : Record 7602;
      CalendarMgmt@1001 : Codeunit 7600;
      OutboundWhseHandlingTimeEnable@19013978 : Boolean INDATASET;
      InboundWhseHandlingTimeEnable@19043763 : Boolean INDATASET;
      BaseCalendarCodeEnable@19028310 : Boolean INDATASET;
      BinCapacityPolicyEnable@19043253 : Boolean INDATASET;
      SpecialEquipmentEnable@19068232 : Boolean INDATASET;
      AllowBreakbulkEnable@19072730 : Boolean INDATASET;
      PutAwayTemplateCodeEnable@19015566 : Boolean INDATASET;
      AlwaysCreatePickLineEnable@19008722 : Boolean INDATASET;
      AlwaysCreatePutawayLineEnable@19036225 : Boolean INDATASET;
      CrossDockDueDateCalcEnable@19016670 : Boolean INDATASET;
      OpenShopFloorBinCodeEnable@19054478 : Boolean INDATASET;
      ToProductionBinCodeEnable@19078604 : Boolean INDATASET;
      FromProductionBinCodeEnable@19048183 : Boolean INDATASET;
      ReceiptBinCodeEnable@19004447 : Boolean INDATASET;
      ShipmentBinCodeEnable@19025907 : Boolean INDATASET;
      AdjustmentBinCodeEnable@19064629 : Boolean INDATASET;
      ToAssemblyBinCodeEnable@1003 : Boolean INDATASET;
      FromAssemblyBinCodeEnable@1000 : Boolean INDATASET;
      AssemblyShipmentBinCodeEnable@1005 : Boolean;
      PickAccordingToFEFOEnable@19053256 : Boolean INDATASET;
      CrossDockBinCodeEnable@19033158 : Boolean INDATASET;
      DirectedPutawayandPickEnable@19066672 : Boolean INDATASET;
      UseADCSEnable@19044183 : Boolean INDATASET;
      DefaultBinSelectionEnable@19026851 : Boolean INDATASET;
      RequirePickEnable@19030451 : Boolean INDATASET;
      RequirePutAwayEnable@19005969 : Boolean INDATASET;
      RequireReceiveEnable@19046465 : Boolean INDATASET;
      RequireShipmentEnable@19000887 : Boolean INDATASET;
      BinMandatoryEnable@19041387 : Boolean INDATASET;
      UsePutAwayWorksheetEnable@19034135 : Boolean INDATASET;
      UseCrossDockingEnable@19061575 : Boolean INDATASET;
      EditInTransit@1101 : Boolean INDATASET;
      ShowMapLbl@1006 : TextConst 'ENU=Show on Map;ENG=Show on Map';
      LookupAddressLbl@1040000 : TextConst 'ENU=Lookup address from postcode;ENG=Lookup address from postcode';
      IsAddressLookupTextEnabled@1040003 : Boolean;

    LOCAL PROCEDURE UpdateEnabled@1();
    BEGIN
      RequirePickEnable := NOT "Use As In-Transit" AND NOT "Directed Put-away and Pick";
      RequirePutAwayEnable := NOT "Use As In-Transit" AND NOT "Directed Put-away and Pick";
      RequireReceiveEnable := NOT "Use As In-Transit" AND NOT "Directed Put-away and Pick";
      RequireShipmentEnable := NOT "Use As In-Transit" AND NOT "Directed Put-away and Pick";
      OutboundWhseHandlingTimeEnable := NOT "Use As In-Transit";
      InboundWhseHandlingTimeEnable := NOT "Use As In-Transit";
      BinMandatoryEnable := NOT "Use As In-Transit" AND NOT "Directed Put-away and Pick";
      DirectedPutawayandPickEnable := NOT "Use As In-Transit" AND "Bin Mandatory";
      BaseCalendarCodeEnable := NOT "Use As In-Transit";

      BinCapacityPolicyEnable := "Directed Put-away and Pick";
      SpecialEquipmentEnable := "Directed Put-away and Pick";
      AllowBreakbulkEnable := "Directed Put-away and Pick";
      PutAwayTemplateCodeEnable := "Directed Put-away and Pick";
      UsePutAwayWorksheetEnable :=
        "Directed Put-away and Pick" OR ("Require Put-away" AND "Require Receive" AND NOT "Use As In-Transit");
      AlwaysCreatePickLineEnable := "Directed Put-away and Pick";
      AlwaysCreatePutawayLineEnable := "Directed Put-away and Pick";

      UseCrossDockingEnable := NOT "Use As In-Transit" AND "Require Receive" AND "Require Shipment" AND "Require Put-away" AND
        "Require Pick";
      CrossDockDueDateCalcEnable := "Use Cross-Docking";

      OpenShopFloorBinCodeEnable := "Bin Mandatory";
      ToProductionBinCodeEnable := "Bin Mandatory";
      FromProductionBinCodeEnable := "Bin Mandatory";
      ReceiptBinCodeEnable := "Bin Mandatory" AND "Require Receive";
      ShipmentBinCodeEnable := "Bin Mandatory" AND "Require Shipment";
      AdjustmentBinCodeEnable := "Directed Put-away and Pick";
      CrossDockBinCodeEnable := "Bin Mandatory" AND "Use Cross-Docking";
      ToAssemblyBinCodeEnable := "Bin Mandatory";
      FromAssemblyBinCodeEnable := "Bin Mandatory";
      AssemblyShipmentBinCodeEnable := "Bin Mandatory" AND NOT ShipmentBinCodeEnable;
      DefaultBinSelectionEnable := "Bin Mandatory" AND NOT "Directed Put-away and Pick";
      UseADCSEnable := NOT "Use As In-Transit" AND "Directed Put-away and Pick";
      PickAccordingToFEFOEnable := "Require Pick" AND "Bin Mandatory";
    END;

    LOCAL PROCEDURE TransitValidation@1101();
    VAR
      TransferHeader@1000 : Record 5740;
    BEGIN
      TransferHeader.SETFILTER("In-Transit Code",Code);
      EditInTransit := TransferHeader.ISEMPTY;
    END;

    LOCAL PROCEDURE ShowPostcodeLookup@1040000(ShowInputFields@1040002 : Boolean);
    VAR
      TempEnteredAutocompleteAddress@1040000 : TEMPORARY Record 9090;
      TempAutocompleteAddress@1040003 : TEMPORARY Record 9090;
      PostcodeBusinessLogic@1040001 : Codeunit 10500;
    BEGIN
      IF ("Country/Region Code" <> 'GB') AND ("Country/Region Code" <> '') THEN
        EXIT;

      IF NOT PostcodeBusinessLogic.IsConfigured OR (("Post Code" = '') AND NOT ShowInputFields) THEN
        EXIT;

      TempEnteredAutocompleteAddress.Address := Address;
      TempEnteredAutocompleteAddress.Postcode := "Post Code";

      IF NOT PostcodeBusinessLogic.ShowLookupWindow(TempEnteredAutocompleteAddress,ShowInputFields,TempAutocompleteAddress) THEN
        EXIT;

      CopyAutocompleteFields(TempAutocompleteAddress);
      HandleAddressLookupVisibility;
    END;

    LOCAL PROCEDURE CopyAutocompleteFields@1040001(VAR TempAutocompleteAddress@1040000 : TEMPORARY Record 9090);
    BEGIN
      Address := TempAutocompleteAddress.Address;
      "Address 2" := TempAutocompleteAddress."Address 2";
      "Post Code" := TempAutocompleteAddress.Postcode;
      City := TempAutocompleteAddress.City;
      County := TempAutocompleteAddress.County;
      "Country/Region Code" := TempAutocompleteAddress."Country / Region";
    END;

    LOCAL PROCEDURE HandleAddressLookupVisibility@1040002();
    VAR
      PostcodeBusinessLogic@1040000 : Codeunit 10500;
    BEGIN
      IF NOT CurrPage.EDITABLE OR NOT PostcodeBusinessLogic.IsConfigured THEN
        IsAddressLookupTextEnabled := FALSE
      ELSE
        IsAddressLookupTextEnabled := ("Country/Region Code" = 'GB') OR ("Country/Region Code" = '');
    END;

    BEGIN
    END.
  }
}

