OBJECT Page 5718 Item Substitution Entries
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Item Substitution Entries;
               ENG=Item Substitution Entries];
    SourceTable=Table5715;
    DelayedInsert=Yes;
    DataCaptionFields=No.,Description;
    PageType=Worksheet;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 15      ;1   ;Action    ;
                      CaptionML=[ENU=&Condition;
                                 ENG=&Condition];
                      ToolTipML=[ENU=Specify a condition for the item substitution, which is for information only and does not affect the item substitution.;
                                 ENG=Specify a condition for the item substitution, which is for information only and does not affect the item substitution.];
                      ApplicationArea=#Advanced;
                      RunObject=Page 5719;
                      RunPageLink=Type=FIELD(Type),
                                  No.=FIELD(No.),
                                  Variant Code=FIELD(Variant Code),
                                  Substitute Type=FIELD(Substitute Type),
                                  Substitute No.=FIELD(Substitute No.),
                                  Substitute Variant Code=FIELD(Substitute Variant Code);
                      Promoted=Yes;
                      Image=ViewComments;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 10  ;1   ;Group     ;
                CaptionML=[ENU=General;
                           ENG=General] }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the variant of the item on the line.;
                           ENG=Specifies the variant of the item on the line.];
                ApplicationArea=#Advanced;
                SourceExpr="Variant Code" }

    { 23  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies when items on the document are shipped or were shipped. A shipment date is usually calculated from a requested delivery date plus lead time.;
                           ENG=Specifies when items on the document are shipped or were shipped. A shipment date is usually calculated from a requested delivery date plus lead time.];
                ApplicationArea=#Suite;
                SourceExpr="Shipment Date" }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item that can be used as a substitute in case the original item is unavailable.;
                           ENG=Specifies the number of the item that can be used as a substitute in case the original item is unavailable.];
                ApplicationArea=#Suite;
                SourceExpr="Substitute No." }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the variant that can be used as a substitute.;
                           ENG=Specifies the code of the variant that can be used as a substitute.];
                ApplicationArea=#Advanced;
                SourceExpr="Substitute Variant Code";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the description of the substitute item.;
                           ENG=Specifies the description of the substitute item.];
                ApplicationArea=#Suite;
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many units (such as pieces, boxes, or cans) of the item are available.;
                           ENG=Specifies how many units (such as pieces, boxes, or cans) of the item are available.];
                ApplicationArea=#Suite;
                DecimalPlaces=0:5;
                SourceExpr=Inventory }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the substitute item quantity available on the shipment date.;
                           ENG=Specifies the substitute item quantity available on the shipment date.];
                ApplicationArea=#Suite;
                SourceExpr="Quantity Avail. on Shpt. Date" }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that a condition exists for this substitution.;
                           ENG=Specifies that a condition exists for this substitution.];
                ApplicationArea=#Advanced;
                SourceExpr=Condition }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

