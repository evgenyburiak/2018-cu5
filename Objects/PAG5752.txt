OBJECT Page 5752 Posted Transfer Shipments
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Posted Transfer Shipments;
               ENG=Posted Transfer Shipments];
    SourceTable=Table5744;
    PageType=List;
    CardPageID=Posted Transfer Shipment;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Shipment;
                                ENG=New,Process,Report,Shipment];
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 17      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Shipment;
                                 ENG=&Shipment];
                      Image=Shipment }
      { 19      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 ENG=Statistics];
                      ToolTipML=[ENU=View statistical information about the transfer order, such as the quantity and total weight transferred.;
                                 ENG=View statistical information about the transfer order, such as the quantity and total weight transferred.];
                      ApplicationArea=#Location;
                      RunObject=Page 5756;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Category4;
                      PromotedOnly=Yes }
      { 20      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 ENG=Co&mments];
                      ToolTipML=[ENU=View or add comments for the record.;
                                 ENG=View or add comments for the record.];
                      ApplicationArea=#Advanced;
                      RunObject=Page 5750;
                      RunPageLink=Document Type=CONST(Posted Transfer Shipment),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 1102601000;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 ENG=Dimensions];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 ENG=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyse transaction history.];
                      ApplicationArea=#Location;
                      Promoted=Yes;
                      Image=Dimensions;
                      PromotedCategory=Category4;
                      PromotedOnly=Yes;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 22      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 ENG=&Print];
                      ToolTipML=[ENU=Prepare to print the document. A report request window for the document opens where you can specify what to include on the print-out.;
                                 ENG=Prepare to print the document. A report request window for the document opens where you can specify what to include on the print-out.];
                      ApplicationArea=#Location;
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      PromotedOnly=Yes;
                      OnAction=BEGIN
                                 CurrPage.SETSELECTIONFILTER(TransShptHeader);
                                 TransShptHeader.PrintRecords(TRUE);
                               END;
                                }
      { 21      ;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 ENG=&Navigate];
                      ToolTipML=[ENU=Find all entries and documents that exist for the document number and posting date on the selected entry or document.;
                                 ENG=Find all entries and documents that exist for the document number and posting date on the selected entry or document.];
                      ApplicationArea=#Advanced;
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the involved entry or record, according to the specified number series.;
                           ENG=Specifies the number of the involved entry or record, according to the specified number series.];
                ApplicationArea=#Location;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the location that items are transferred from.;
                           ENG=Specifies the code of the location that items are transferred from.];
                ApplicationArea=#Location;
                SourceExpr="Transfer-from Code" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the location that the items are transferred to.;
                           ENG=Specifies the code of the location that the items are transferred to.];
                ApplicationArea=#Location;
                SourceExpr="Transfer-to Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the posting date of this document.;
                           ENG=Specifies the posting date of this document.];
                ApplicationArea=#Location;
                SourceExpr="Posting Date" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 1, which is one of two global dimension codes that you set up in the General Ledger Setup window.;
                           ENG=Specifies the code for Shortcut Dimension 1, which is one of two global dimension codes that you set up in the General Ledger Setup window.];
                ApplicationArea=#Location;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 2, which is one of two global dimension codes that you set up in the General Ledger Setup window.;
                           ENG=Specifies the code for Shortcut Dimension 2, which is one of two global dimension codes that you set up in the General Ledger Setup window.];
                ApplicationArea=#Location;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 1102601001;2;Field  ;
                ToolTipML=[ENU=Specifies when items on the document are shipped or were shipped. A shipment date is usually calculated from a requested delivery date plus lead time.;
                           ENG=Specifies when items on the document are shipped or were shipped. A shipment date is usually calculated from a requested delivery date plus lead time.];
                ApplicationArea=#Location;
                SourceExpr="Shipment Date";
                Visible=FALSE }

    { 1102601003;2;Field  ;
                ToolTipML=[ENU=Specifies the delivery conditions of the related shipment, such as free on board (FOB).;
                           ENG=Specifies the delivery conditions of the related shipment, such as free on board (FOB).];
                ApplicationArea=#Location;
                SourceExpr="Shipment Method Code";
                Visible=FALSE }

    { 1102601005;2;Field  ;
                ToolTipML=[ENU=Specifies the code for the shipping agent who is transporting the items.;
                           ENG=Specifies the code for the shipping agent who is transporting the items.];
                ApplicationArea=#Location;
                SourceExpr="Shipping Agent Code";
                Visible=FALSE }

    { 1102601007;2;Field  ;
                ToolTipML=[ENU=Specifies the receipt date of the transfer order.;
                           ENG=Specifies the receipt date of the transfer order.];
                ApplicationArea=#Location;
                SourceExpr="Receipt Date";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      TransShptHeader@1000 : Record 5744;

    BEGIN
    END.
  }
}

