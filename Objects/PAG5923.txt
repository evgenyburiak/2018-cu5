OBJECT Page 5923 Loaner List
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Loaner List;
               ENG=Loaner List];
    SourceTable=Table5913;
    PageType=List;
    CardPageID=Loaner Card;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 20      ;1   ;ActionGroup;
                      CaptionML=[ENU=L&oaner;
                                 ENG=L&oaner];
                      Image=Loaners }
      { 23      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 ENG=Co&mments];
                      ToolTipML=[ENU=View or add comments for the record.;
                                 ENG=View or add comments for the record.];
                      ApplicationArea=#Service;
                      RunObject=Page 5911;
                      RunPageLink=Table Name=CONST(Loaner),
                                  Table Subtype=CONST(0),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 24      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Loaner E&ntries;
                                 ENG=Loaner E&ntries];
                      ToolTipML=[ENU=View the history of the loaner.;
                                 ENG=View the history of the loaner.];
                      ApplicationArea=#Service;
                      RunObject=Page 5924;
                      RunPageView=SORTING(Loaner No.)
                                  ORDER(Ascending);
                      RunPageLink=Loaner No.=FIELD(No.);
                      Image=Entries }
      { 1900000005;0 ;ActionContainer;
                      ActionContainerType=NewDocumentItems }
      { 1906302005;1 ;Action    ;
                      CaptionML=[ENU=New Service Order;
                                 ENG=New Service Order];
                      ToolTipML=[ENU="Create an order for specific service work to be performed on a customer's item. ";
                                 ENG="Create an order for specific service work to be performed on a customer's item. "];
                      ApplicationArea=#Service;
                      RunObject=Page 5900;
                      Promoted=Yes;
                      Image=Document;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 21      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 ENG=F&unctions];
                      Image=Action }
      { 25      ;2   ;Action    ;
                      CaptionML=[ENU=Receive;
                                 ENG=Receive];
                      ToolTipML=[ENU=Register that a loaner has been received back from the service customer.;
                                 ENG=Register that a loaner has been received back from the service customer.];
                      ApplicationArea=#Service;
                      Image=ReceiveLoaner;
                      OnAction=VAR
                                 ServLoanerMgt@1003 : Codeunit 5901;
                               BEGIN
                                 ServLoanerMgt.Receive(Rec);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the involved entry or record, according to the specified number series.;
                           ENG=Specifies the number of the involved entry or record, according to the specified number series.];
                ApplicationArea=#Service;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the loaner.;
                           ENG=Specifies a description of the loaner.];
                ApplicationArea=#Service;
                SourceExpr=Description }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional description of the loaner.;
                           ENG=Specifies an additional description of the loaner.];
                ApplicationArea=#Service;
                SourceExpr="Description 2" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that the loaner has been lent to a customer.;
                           ENG=Specifies that the loaner has been lent to a customer.];
                ApplicationArea=#Service;
                SourceExpr=Lent }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document type of the loaner entry.;
                           ENG=Specifies the document type of the loaner entry.];
                ApplicationArea=#Service;
                SourceExpr="Document Type" }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the service document for the service item that was lent.;
                           ENG=Specifies the number of the service document for the service item that was lent.];
                ApplicationArea=#Service;
                SourceExpr="Document No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

