OBJECT Page 5927 Fault Codes
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Fault Codes;
               ENG=Fault Codes];
    SourceTable=Table5918;
    DataCaptionFields=Fault Area Code,Symptom Code;
    PageType=List;
    OnInit=BEGIN
             SymptomCodeVisible := TRUE;
             FaultAreaCodeVisible := TRUE;
           END;

    OnOpenPage=BEGIN
                 FaultAreaCodeVisible := NOT CurrPage.LOOKUPMODE;
                 SymptomCodeVisible := NOT CurrPage.LOOKUPMODE;
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the fault area associated with the fault code.;
                           ENG=Specifies the code of the fault area associated with the fault code.];
                ApplicationArea=#Service;
                SourceExpr="Fault Area Code";
                Visible=FaultAreaCodeVisible }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the symptom linked to the fault code.;
                           ENG=Specifies the code of the symptom linked to the fault code.];
                ApplicationArea=#Service;
                SourceExpr="Symptom Code";
                Visible=SymptomCodeVisible }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for the fault.;
                           ENG=Specifies a code for the fault.];
                ApplicationArea=#Service;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the fault code.;
                           ENG=Specifies a description of the fault code.];
                ApplicationArea=#Service;
                SourceExpr=Description }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      FaultAreaCodeVisible@19067961 : Boolean INDATASET;
      SymptomCodeVisible@19078417 : Boolean INDATASET;

    BEGIN
    END.
  }
}

