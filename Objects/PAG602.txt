OBJECT Page 602 IC Dimension List
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Intercompany Dimension List;
               ENG=Intercompany Dimension List];
    SourceTable=Table411;
    PageType=List;
    OnInit=BEGIN
             CurrPage.LOOKUPMODE := TRUE;
           END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the intercompany dimension code.;
                           ENG=Specifies the intercompany dimension code.];
                ApplicationArea=#Intercompany;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the intercompany dimension name.;
                           ENG=Specifies the intercompany dimension name.];
                ApplicationArea=#Intercompany;
                SourceExpr=Name }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

