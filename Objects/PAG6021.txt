OBJECT Page 6021 Resource Service Zones
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Resource Service Zones;
               ENG=Resource Service Zones];
    SourceTable=Table5958;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the title of the resource located in the service zone.;
                           ENG=Specifies the title of the resource located in the service zone.];
                ApplicationArea=#Service;
                SourceExpr="Resource No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the service zone where the resource will be located. A resource can be located in more than one zone at a time.;
                           ENG=Specifies the code of the service zone where the resource will be located. A resource can be located in more than one zone at a time.];
                ApplicationArea=#Service;
                SourceExpr="Service Zone Code" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the starting date when the resource is located in the service zone.;
                           ENG=Specifies the starting date when the resource is located in the service zone.];
                ApplicationArea=#Service;
                SourceExpr="Starting Date" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the resource's assignment to the service zone.;
                           ENG=Specifies a description of the resource's assignment to the service zone.];
                ApplicationArea=#Service;
                SourceExpr=Description }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

