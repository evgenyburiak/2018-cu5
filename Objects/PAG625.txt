OBJECT Page 625 Unapply Employee Entries
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Unapply Employee Entries;
               ENG=Unapply Employee Entries];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table5223;
    DataCaptionExpr=Caption;
    PageType=Worksheet;
    SourceTableTemporary=Yes;
    OnOpenPage=BEGIN
                 InsertEntries;
               END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 42      ;1   ;Action    ;
                      Name=Unapply;
                      CaptionML=[ENU=&Unapply;
                                 ENG=&Unapply];
                      ToolTipML=[ENU=Unselect one or more ledger entries that you want to unapply this record.;
                                 ENG=Unselect one or more ledger entries that you want to unapply this record.];
                      ApplicationArea=#BasicHR;
                      Promoted=Yes;
                      Image=UnApply;
                      PromotedCategory=Process;
                      PromotedOnly=Yes;
                      OnAction=VAR
                                 EmplEntryApplyPostedEntries@1000 : Codeunit 224;
                               BEGIN
                                 IF ISEMPTY THEN
                                   ERROR(NothingToApplyErr);
                                 IF NOT CONFIRM(UnapplyEntriesQst,FALSE) THEN
                                   EXIT;

                                 EmplEntryApplyPostedEntries.PostUnApplyEmployee(DtldEmplLedgEntry2,DocNo,PostingDate);
                                 PostingDate := 0D;
                                 DocNo := '';
                                 DELETEALL;
                                 MESSAGE(EntriesUnappliedMsg);

                                 CurrPage.CLOSE;
                               END;
                                }
      { 3       ;1   ;Action    ;
                      Name=Preview;
                      CaptionML=[ENU=Preview Unapply;
                                 ENG=Preview Unapply];
                      ToolTipML=[ENU=Preview how unapplying one or more ledger entries will look like.;
                                 ENG=Preview how unapplying one or more ledger entries will look like.];
                      ApplicationArea=#BasicHR;
                      Promoted=Yes;
                      Image=ViewPostedOrder;
                      PromotedCategory=Process;
                      PromotedOnly=Yes;
                      OnAction=VAR
                                 EmplEntryApplyPostedEntries@1000 : Codeunit 224;
                               BEGIN
                                 IF ISEMPTY THEN
                                   ERROR(NothingToApplyErr);

                                 EmplEntryApplyPostedEntries.PreviewUnapply(DtldEmplLedgEntry2,DocNo,PostingDate);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 33  ;1   ;Group     ;
                CaptionML=[ENU=General;
                           ENG=General] }

    { 34  ;2   ;Field     ;
                Name=DocuNo;
                CaptionML=[ENU=Document No.;
                           ENG=Document No.];
                ToolTipML=[ENU=Specifies the document number that will be assigned to the entries that will be created when you click Unapply.;
                           ENG=Specifies the document number that will be assigned to the entries that will be created when you click Unapply.];
                ApplicationArea=#BasicHR;
                SourceExpr=DocNo }

    { 35  ;2   ;Field     ;
                Name=PostDate;
                CaptionML=[ENU=Posting Date;
                           ENG=Posting Date];
                ToolTipML=[ENU=Specifies the posting date that will be assigned to the general ledger entries that will be created when you click Unapply.;
                           ENG=Specifies the posting date that will be assigned to the general ledger entries that will be created when you click Unapply.];
                ApplicationArea=#BasicHR;
                SourceExpr=PostingDate }

    { 1   ;1   ;Group     ;
                Editable=FALSE;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the posting date of the detailed vendor ledger entry.;
                           ENG=Specifies the posting date of the detailed vendor ledger entry.];
                ApplicationArea=#BasicHR;
                SourceExpr="Posting Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the entry type of the detailed vendor ledger entry.;
                           ENG=Specifies the entry type of the detailed vendor ledger entry.];
                ApplicationArea=#BasicHR;
                SourceExpr="Entry Type" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document type of the detailed vendor ledger entry.;
                           ENG=Specifies the document type of the detailed vendor ledger entry.];
                ApplicationArea=#BasicHR;
                SourceExpr="Document Type" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document number of the transaction that created the entry.;
                           ENG=Specifies the document number of the transaction that created the entry.];
                ApplicationArea=#BasicHR;
                SourceExpr="Document No." }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the vendor account to which the entry is posted.;
                           ENG=Specifies the number of the vendor account to which the entry is posted.];
                ApplicationArea=#BasicHR;
                SourceExpr="Employee No." }

    { 43  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document type that the initial vendor ledger entry was created with.;
                           ENG=Specifies the document type that the initial vendor ledger entry was created with.];
                ApplicationArea=#BasicHR;
                SourceExpr="Initial Document Type" }

    { 40  ;2   ;Field     ;
                CaptionML=[ENU=Initial Document No.;
                           ENG=Initial Document No.];
                ToolTipML=[ENU=Specifies the number of the document for which the entry is unapplied.;
                           ENG=Specifies the number of the document for which the entry is unapplied.];
                ApplicationArea=#BasicHR;
                SourceExpr=GetDocumentNo }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the Global Dimension 1 code of the initial vendor ledger entry.;
                           ENG=Specifies the Global Dimension 1 code of the initial vendor ledger entry.];
                ApplicationArea=#BasicHR;
                SourceExpr="Initial Entry Global Dim. 1";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the Global Dimension 2 code of the initial vendor ledger entry.;
                           ENG=Specifies the Global Dimension 2 code of the initial vendor ledger entry.];
                ApplicationArea=#BasicHR;
                SourceExpr="Initial Entry Global Dim. 2";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the currency if the amount is in a foreign currency.;
                           ENG=Specifies the code for the currency if the amount is in a foreign currency.];
                ApplicationArea=#BasicHR;
                SourceExpr="Currency Code" }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the amount of the detailed vendor ledger entry.;
                           ENG=Specifies the amount of the detailed vendor ledger entry.];
                ApplicationArea=#BasicHR;
                SourceExpr=Amount }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the amount of the entry in LCY.;
                           ENG=Specifies the amount of the entry in LCY.];
                ApplicationArea=#BasicHR;
                SourceExpr="Amount (LCY)" }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the total of the ledger entries that represent debits.;
                           ENG=Specifies the total of the ledger entries that represent debits.];
                ApplicationArea=#BasicHR;
                SourceExpr="Debit Amount";
                Visible=FALSE }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the total of the ledger entries that represent debits, expressed in LCY.;
                           ENG=Specifies the total of the ledger entries that represent debits, expressed in LCY.];
                ApplicationArea=#BasicHR;
                SourceExpr="Debit Amount (LCY)";
                Visible=FALSE }

    { 7   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the total of the ledger entries that represent credits.;
                           ENG=Specifies the total of the ledger entries that represent credits.];
                ApplicationArea=#BasicHR;
                SourceExpr="Credit Amount";
                Visible=FALSE }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the total of the ledger entries that represent credits, expressed in the local currency.;
                           ENG=Specifies the total of the ledger entries that represent credits, expressed in the local currency.];
                ApplicationArea=#BasicHR;
                SourceExpr="Credit Amount (LCY)";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the user who posted the entry, to be used, for example, in the change log.;
                           ENG=Specifies the ID of the user who posted the entry, to be used, for example, in the change log.];
                ApplicationArea=#BasicHR;
                SourceExpr="User ID";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the source code that specifies where the entry was created.;
                           ENG=Specifies the source code that specifies where the entry was created.];
                ApplicationArea=#BasicHR;
                SourceExpr="Source Code";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the reason code, a supplementary source code that enables you to trace the entry.;
                           ENG=Specifies the reason code, a supplementary source code that enables you to trace the entry.];
                ApplicationArea=#BasicHR;
                SourceExpr="Reason Code";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the entry number of the vendor ledger entry that the detailed vendor ledger entry line was created for.;
                           ENG=Specifies the entry number of the vendor ledger entry that the detailed vendor ledger entry line was created for.];
                ApplicationArea=#BasicHR;
                SourceExpr="Employee Ledger Entry No.";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the entry number of the detailed vendor ledger entry.;
                           ENG=Specifies the entry number of the detailed vendor ledger entry.];
                ApplicationArea=#BasicHR;
                SourceExpr="Entry No." }

  }
  CODE
  {
    VAR
      DtldEmplLedgEntry2@1004 : Record 5223;
      Employee@1005 : Record 5200;
      DocNo@1000 : Code[20];
      PostingDate@1001 : Date;
      EmplLedgEntryNo@1002 : Integer;
      EntriesUnappliedMsg@1008 : TextConst 'ENU=The entries were successfully unapplied.;ENG=The entries were successfully unapplied.';
      NothingToApplyErr@1003 : TextConst 'ENU=There is nothing to unapply.;ENG=There is nothing to unapply.';
      UnapplyEntriesQst@1006 : TextConst 'ENU=To unapply these entries, correcting entries will be posted.\Do you want to unapply the entries?;ENG=To unapply these entries, correcting entries will be posted.\Do you want to unapply the entries?';

    [External]
    PROCEDURE SetDtldEmplLedgEntry@4(EntryNo@1001 : Integer);
    BEGIN
      DtldEmplLedgEntry2.GET(EntryNo);
      EmplLedgEntryNo := DtldEmplLedgEntry2."Employee Ledger Entry No.";
      PostingDate := DtldEmplLedgEntry2."Posting Date";
      DocNo := DtldEmplLedgEntry2."Document No.";
      Employee.GET(DtldEmplLedgEntry2."Employee No.");
    END;

    LOCAL PROCEDURE InsertEntries@1();
    VAR
      DtldEmplLedgEntry@1000 : Record 5223;
    BEGIN
      IF DtldEmplLedgEntry2."Transaction No." = 0 THEN BEGIN
        DtldEmplLedgEntry.SETCURRENTKEY("Application No.","Employee No.","Entry Type");
        DtldEmplLedgEntry.SETRANGE("Application No.",DtldEmplLedgEntry2."Application No.");
      END ELSE BEGIN
        DtldEmplLedgEntry.SETCURRENTKEY("Transaction No.","Employee No.","Entry Type");
        DtldEmplLedgEntry.SETRANGE("Transaction No.",DtldEmplLedgEntry2."Transaction No.");
      END;
      DtldEmplLedgEntry.SETRANGE("Employee No.",DtldEmplLedgEntry2."Employee No.");
      DELETEALL;
      IF DtldEmplLedgEntry.FIND('-') THEN
        REPEAT
          IF (DtldEmplLedgEntry."Entry Type" <> DtldEmplLedgEntry."Entry Type"::"Initial Entry") AND
             NOT DtldEmplLedgEntry.Unapplied
          THEN BEGIN
            Rec := DtldEmplLedgEntry;
            INSERT;
          END;
        UNTIL DtldEmplLedgEntry.NEXT = 0;
    END;

    LOCAL PROCEDURE GetDocumentNo@7() : Code[20];
    VAR
      EmployeeLedgerEntry@1000 : Record 5222;
    BEGIN
      IF EmployeeLedgerEntry.GET("Employee Ledger Entry No.") THEN;
      EXIT(EmployeeLedgerEntry."Document No.");
    END;

    LOCAL PROCEDURE Caption@5() : Text[100];
    VAR
      EmployeeLedgerEntry@1000 : Record 5222;
    BEGIN
      EXIT(STRSUBSTNO(
          '%1 %2 %3 %4',
          Employee."No.",
          Employee.FullName,
          EmployeeLedgerEntry.FIELDCAPTION("Entry No."),
          EmplLedgEntryNo));
    END;

    BEGIN
    END.
  }
}

