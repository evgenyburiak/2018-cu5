OBJECT Page 6628 Sales Return Order Arc Subform
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Lines;
               ENG=Lines];
    SourceTable=Table5108;
    SourceTableView=WHERE(Document Type=CONST(Return Order));
    PageType=ListPart;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 ENG=&Line];
                      Image=Line }
      { 1907838004;2 ;Action    ;
                      Name=Dimensions;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 ENG=Dimensions];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 ENG=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyse transaction history.];
                      ApplicationArea=#Dimensions;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1903079504;2 ;Action    ;
                      Name=Comments;
                      CaptionML=[ENU=Co&mments;
                                 ENG=Co&mments];
                      ToolTipML=[ENU=View or add comments for the record.;
                                 ENG=View or add comments for the record.];
                      ApplicationArea=#Advanced;
                      Image=ViewComments;
                      OnAction=BEGIN
                                 ShowLineComments;
                               END;
                                }
      { 7       ;2   ;Action    ;
                      Name=DeferralSchedule;
                      CaptionML=[ENU=Deferral Schedule;
                                 ENG=Deferral Schedule];
                      ToolTipML=[ENU=View or edit the deferral schedule that governs how revenue made with this sales document is deferred to different accounting periods when the document is posted.;
                                 ENG=View or edit the deferral schedule that governs how revenue made with this sales document is deferred to different accounting periods when the document is posted.];
                      ApplicationArea=#Advanced;
                      Image=PaymentPeriod;
                      OnAction=BEGIN
                                 ShowDeferrals;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the line type.;
                           ENG=Specifies the line type.];
                ApplicationArea=#Advanced;
                SourceExpr=Type }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the involved entry or record, according to the specified number series.;
                           ENG=Specifies the number of the involved entry or record, according to the specified number series.];
                ApplicationArea=#Advanced;
                SourceExpr="No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the cross-referenced item number. If you enter a cross reference between yours and your vendor's or customer's item number, then this number will override the standard item number when you enter the cross-reference number on a sales or purchase document.;
                           ENG=Specifies the cross-referenced item number. If you enter a cross reference between yours and your vendor's or customer's item number, then this number will override the standard item number when you enter the cross-reference number on a sales or purchase document.];
                ApplicationArea=#Advanced;
                SourceExpr="Cross-Reference No.";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the variant of the item on the line.;
                           ENG=Specifies the variant of the item on the line.];
                ApplicationArea=#Advanced;
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether a substitute is available for the item.;
                           ENG=Specifies whether a substitute is available for the item.];
                ApplicationArea=#Advanced;
                SourceExpr="Substitution Available";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies which purchaser is assigned to the vendor.;
                           ENG=Specifies which purchaser is assigned to the vendor.];
                ApplicationArea=#Advanced;
                SourceExpr="Purchasing Code";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that this item is a nonstock item.;
                           ENG=Specifies that this item is a nonstock item.];
                ApplicationArea=#Advanced;
                SourceExpr=Nonstock;
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the VAT specification of the involved item or resource to link transactions made for this record with the appropriate general ledger account according to the VAT posting setup.;
                           ENG=Specifies the VAT specification of the involved item or resource to link transactions made for this record with the appropriate general ledger account according to the VAT posting setup.];
                ApplicationArea=#Advanced;
                SourceExpr="VAT Prod. Posting Group";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the sales return order arc.;
                           ENG=Specifies a description of the sales return order arc.];
                ApplicationArea=#Advanced;
                SourceExpr=Description }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if your vendor ships the items directly to your customer.;
                           ENG=Specifies if your vendor ships the items directly to your customer.];
                ApplicationArea=#Advanced;
                SourceExpr="Drop Shipment";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that the item on the sales return order is a special-order item.;
                           ENG=Specifies that the item on the sales return order is a special-order item.];
                ApplicationArea=#Advanced;
                SourceExpr="Special Order";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code explaining why the item was returned.;
                           ENG=Specifies the code explaining why the item was returned.];
                ApplicationArea=#Advanced;
                SourceExpr="Return Reason Code";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the location from where inventory items to the customer on the sales document are to be shipped by default.;
                           ENG=Specifies the location from where inventory items to the customer on the sales document are to be shipped by default.];
                ApplicationArea=#Advanced;
                SourceExpr="Location Code" }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether items will never, automatically (Always), or optionally be reserved for this customer. Optional means that you must manually reserve items for this customer.;
                           ENG=Specifies whether items will never, automatically (Always), or optionally be reserved for this customer. Optional means that you must manually reserve items for this customer.];
                ApplicationArea=#Advanced;
                SourceExpr=Reserve;
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many units are being returned.;
                           ENG=Specifies how many units are being returned.];
                ApplicationArea=#Advanced;
                SourceExpr=Quantity }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how each unit of the item or resource is measured, such as in pieces or hours. By default, the value in the Base Unit of Measure field on the item or resource card is inserted.;
                           ENG=Specifies how each unit of the item or resource is measured, such as in pieces or hours. By default, the value in the Base Unit of Measure field on the item or resource card is inserted.];
                ApplicationArea=#Advanced;
                SourceExpr="Unit of Measure Code" }

    { 36  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the item or resource's unit of measure, such as piece or hour.;
                           ENG=Specifies the name of the item or resource's unit of measure, such as piece or hour.];
                ApplicationArea=#Advanced;
                SourceExpr="Unit of Measure";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the cost, in LCY, of one unit of the item or resource on the line.;
                           ENG=Specifies the cost, in LCY, of one unit of the item or resource on the line.];
                ApplicationArea=#Advanced;
                SourceExpr="Unit Cost (LCY)";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the price of one unit of the item or resource. You can enter a price manually or have it entered according to the Price/Profit Calculation field on the related card.;
                           ENG=Specifies the price of one unit of the item or resource. You can enter a price manually or have it entered according to the Price/Profit Calculation field on the related card.];
                ApplicationArea=#Advanced;
                SourceExpr="Unit Price";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the net amount, excluding any invoice discount amount, that must be paid for products on the line.;
                           ENG=Specifies the net amount, excluding any invoice discount amount, that must be paid for products on the line.];
                ApplicationArea=#Advanced;
                SourceExpr="Line Amount" }

    { 44  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the discount percentage that is granted for the item on the line.;
                           ENG=Specifies the discount percentage that is granted for the item on the line.];
                ApplicationArea=#Advanced;
                SourceExpr="Line Discount %" }

    { 46  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the discount amount that is granted for the item on the line.;
                           ENG=Specifies the discount amount that is granted for the item on the line.];
                ApplicationArea=#Advanced;
                SourceExpr="Line Discount Amount";
                Visible=FALSE }

    { 48  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the invoice line is included when the invoice discount is calculated.;
                           ENG=Specifies if the invoice line is included when the invoice discount is calculated.];
                ApplicationArea=#Advanced;
                SourceExpr="Allow Invoice Disc.";
                Visible=FALSE }

    { 50  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the total calculated invoice discount amount for the line.;
                           ENG=Specifies the total calculated invoice discount amount for the line.];
                ApplicationArea=#Advanced;
                SourceExpr="Inv. Discount Amount";
                Visible=FALSE }

    { 52  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity of items that remain to be shipped.;
                           ENG=Specifies the quantity of items that remain to be shipped.];
                ApplicationArea=#Advanced;
                SourceExpr="Qty. to Ship" }

    { 54  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many units of the item on the line have been posted as shipped.;
                           ENG=Specifies how many units of the item on the line have been posted as shipped.];
                ApplicationArea=#Advanced;
                SourceExpr="Quantity Shipped" }

    { 56  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity that remains to be invoiced. It is calculated as Quantity - Qty. Invoiced.;
                           ENG=Specifies the quantity that remains to be invoiced. It is calculated as Quantity - Qty. Invoiced.];
                ApplicationArea=#Advanced;
                SourceExpr="Qty. to Invoice" }

    { 58  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many units of the item on the line have been posted as invoiced.;
                           ENG=Specifies how many units of the item on the line have been posted as invoiced.];
                ApplicationArea=#Advanced;
                SourceExpr="Quantity Invoiced" }

    { 60  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that you can assign item charges to this line.;
                           ENG=Specifies that you can assign item charges to this line.];
                ApplicationArea=#Advanced;
                SourceExpr="Allow Item Charge Assignment";
                Visible=FALSE }

    { 66  ;2   ;Field     ;
                ToolTipML=[ENU="Specifies the requested delivery date for the sales return order. ";
                           ENG="Specifies the requested delivery date for the sales return order. "];
                ApplicationArea=#Advanced;
                SourceExpr="Requested Delivery Date";
                Visible=FALSE }

    { 68  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date that you have promised to deliver the order, as a result of the Order Promising function.;
                           ENG=Specifies the date that you have promised to deliver the order, as a result of the Order Promising function.];
                ApplicationArea=#Advanced;
                SourceExpr="Promised Delivery Date";
                Visible=FALSE }

    { 70  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the planned date that the shipment will be delivered at the customer's address. If the customer requests a delivery date, the program calculates whether the items will be available for delivery on this date. If the items are available, the planned delivery date will be the same as the requested delivery date. If not, the program calculates the date that the items are available for delivery and enters this date in the Planned Delivery Date field.;
                           ENG=Specifies the planned date that the shipment will be delivered at the customer's address. If the customer requests a delivery date, the program calculates whether the items will be available for delivery on this date. If the items are available, the planned delivery date will be the same as the requested delivery date. If not, the program calculates the date that the items are available for delivery and enters this date in the Planned Delivery Date field.];
                ApplicationArea=#Advanced;
                SourceExpr="Planned Delivery Date" }

    { 72  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date that the shipment should ship from the warehouse. If the customer requests a delivery date, the program calculates the planned shipment date by subtracting the shipping time from the requested delivery date. If the customer does not request a delivery date or the requested delivery date cannot be met, the program calculates the content of this field by adding the shipment time to the shipping date.;
                           ENG=Specifies the date that the shipment should ship from the warehouse. If the customer requests a delivery date, the program calculates the planned shipment date by subtracting the shipping time from the requested delivery date. If the customer does not request a delivery date or the requested delivery date cannot be met, the program calculates the content of this field by adding the shipment time to the shipping date.];
                ApplicationArea=#Advanced;
                SourceExpr="Planned Shipment Date" }

    { 74  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies when items on the document are shipped or were shipped. A shipment date is usually calculated from a requested delivery date plus lead time.;
                           ENG=Specifies when items on the document are shipped or were shipped. A shipment date is usually calculated from a requested delivery date plus lead time.];
                ApplicationArea=#Advanced;
                SourceExpr="Shipment Date" }

    { 76  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the shipping agent who is transporting the items.;
                           ENG=Specifies the code for the shipping agent who is transporting the items.];
                ApplicationArea=#Advanced;
                SourceExpr="Shipping Agent Code";
                Visible=FALSE }

    { 78  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the service, such as a one-day delivery, that is offered by the shipping agent.;
                           ENG=Specifies the code for the service, such as a one-day delivery, that is offered by the shipping agent.];
                ApplicationArea=#Advanced;
                SourceExpr="Shipping Agent Service Code";
                Visible=FALSE }

    { 80  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how long it takes from when the items are shipped from the warehouse to when they are delivered.;
                           ENG=Specifies how long it takes from when the items are shipped from the warehouse to when they are delivered.];
                ApplicationArea=#Advanced;
                SourceExpr="Shipping Time";
                Visible=FALSE }

    { 82  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the related job.;
                           ENG=Specifies the number of the related job.];
                ApplicationArea=#Advanced;
                SourceExpr="Job No.";
                Visible=FALSE }

    { 90  ;2   ;Field     ;
                ToolTipML=[ENU="Specifies a date formula for the time it takes to get items ready to ship from this location. The time element is used in the calculation of the delivery date as follows: Shipment Date + Outbound Warehouse Handling Time = Planned Shipment Date + Shipping Time = Planned Delivery Date.";
                           ENG="Specifies a date formula for the time it takes to get items ready to ship from this location. The time element is used in the calculation of the delivery date as follows: Shipment Date + Outbound Warehouse Handling Time = Planned Shipment Date + Shipping Time = Planned Delivery Date."];
                ApplicationArea=#Warehouse;
                SourceExpr="Outbound Whse. Handling Time";
                Visible=FALSE }

    { 92  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the blanket order that the record originates from.;
                           ENG=Specifies the number of the blanket order that the record originates from.];
                ApplicationArea=#Advanced;
                SourceExpr="Blanket Order No.";
                Visible=FALSE }

    { 94  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the blanket order line that the record originates from.;
                           ENG=Specifies the number of the blanket order line that the record originates from.];
                ApplicationArea=#Advanced;
                SourceExpr="Blanket Order Line No.";
                Visible=FALSE }

    { 96  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the posting date of the related fixed asset transaction, such as a depreciation.;
                           ENG=Specifies the posting date of the related fixed asset transaction, such as a depreciation.];
                ApplicationArea=#Advanced;
                SourceExpr="FA Posting Date";
                Visible=FALSE }

    { 98  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if depreciation was calculated until the FA posting date of the line.;
                           ENG=Specifies if depreciation was calculated until the FA posting date of the line.];
                ApplicationArea=#Advanced;
                SourceExpr="Depr. until FA Posting Date";
                Visible=FALSE }

    { 100 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the depreciation book to which the line will be posted if you have selected Fixed Asset in the Type field for this line.;
                           ENG=Specifies the code for the depreciation book to which the line will be posted if you have selected Fixed Asset in the Type field for this line.];
                ApplicationArea=#Advanced;
                SourceExpr="Depreciation Book Code";
                Visible=FALSE }

    { 102 ;2   ;Field     ;
                ToolTipML=[ENU="Specifies, if the type is Fixed Asset, that information on the line is to be posted to all relevant lines. ";
                           ENG="Specifies, if the type is Fixed Asset, that information on the line is to be posted to all relevant lines. "];
                ApplicationArea=#Advanced;
                SourceExpr="Use Duplication List";
                Visible=FALSE }

    { 104 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a depreciation book code if you want the journal line to be posted to that depreciation book, as well as to the depreciation book in the Depreciation Book Code field.;
                           ENG=Specifies a depreciation book code if you want the journal line to be posted to that depreciation book, as well as to the depreciation book in the Depreciation Book Code field.];
                ApplicationArea=#Advanced;
                SourceExpr="Duplicate in Depreciation Book";
                Visible=FALSE }

    { 106 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item ledger entry that the document or journal line is applied from.;
                           ENG=Specifies the number of the item ledger entry that the document or journal line is applied from.];
                ApplicationArea=#Advanced;
                SourceExpr="Appl.-from Item Entry";
                Visible=FALSE }

    { 108 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item ledger entry that the document or journal line is applied to.;
                           ENG=Specifies the number of the item ledger entry that the document or journal line is applied to.];
                ApplicationArea=#Advanced;
                SourceExpr="Appl.-to Item Entry";
                Visible=FALSE }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the deferral template that governs how expenses paid with this purchase document are deferred to the different accounting periods when the expenses were incurred.;
                           ENG=Specifies the deferral template that governs how expenses paid with this purchase document are deferred to the different accounting periods when the expenses were incurred.];
                ApplicationArea=#Advanced;
                SourceExpr="Deferral Code" }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the starting date of the returns deferral period.;
                           ENG=Specifies the starting date of the returns deferral period.];
                ApplicationArea=#Advanced;
                SourceExpr="Returns Deferral Start Date" }

    { 110 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 1, which is one of two global dimension codes that you set up in the General Ledger Setup window.;
                           ENG=Specifies the code for Shortcut Dimension 1, which is one of two global dimension codes that you set up in the General Ledger Setup window.];
                ApplicationArea=#Dimensions;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 112 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 2, which is one of two global dimension codes that you set up in the General Ledger Setup window.;
                           ENG=Specifies the code for Shortcut Dimension 2, which is one of two global dimension codes that you set up in the General Ledger Setup window.];
                ApplicationArea=#Dimensions;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

