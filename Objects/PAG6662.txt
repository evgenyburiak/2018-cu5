OBJECT Page 6662 Posted Return Receipts
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Posted Return Receipts;
               ENG=Posted Return Receipts];
    SourceTable=Table6660;
    SourceTableView=SORTING(Posting Date)
                    ORDER(Descending);
    PageType=List;
    CardPageID=Posted Return Receipt;
    OnOpenPage=VAR
                 HasFilters@1000 : Boolean;
               BEGIN
                 HasFilters := GETFILTERS <> '';
                 SetSecurityFilterOnRespCenter;
                 IF HasFilters THEN
                   IF FINDFIRST THEN;
               END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 15      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Return Rcpt.;
                                 ENG=&Return Rcpt.];
                      Image=Receipt }
      { 28      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 ENG=Statistics];
                      ToolTipML=[ENU=View statistical information, such as the value of posted entries, for the record.;
                                 ENG=View statistical information, such as the value of posted entries, for the record.];
                      ApplicationArea=#SalesReturnOrder;
                      RunObject=Page 6665;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 33      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 ENG=Co&mments];
                      ToolTipML=[ENU=View or add comments for the record.;
                                 ENG=View or add comments for the record.];
                      ApplicationArea=#SalesReturnOrder;
                      RunObject=Page 67;
                      RunPageLink=Document Type=CONST(Posted Return Receipt),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 1102601000;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 ENG=Dimensions];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 ENG=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyse transaction history.];
                      ApplicationArea=#Dimensions;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 21      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 ENG=&Print];
                      ToolTipML=[ENU=Prepare to print the document. A report request window for the document opens where you can specify what to include on the print-out.;
                                 ENG=Prepare to print the document. A report request window for the document opens where you can specify what to include on the print-out.];
                      ApplicationArea=#SalesReturnOrder;
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CurrPage.SETSELECTIONFILTER(ReturnRcptHeader);
                                 ReturnRcptHeader.PrintRecords(TRUE);
                               END;
                                }
      { 22      ;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 ENG=&Navigate];
                      ToolTipML=[ENU=Find all entries and documents that exist for the document number and posting date on the selected entry or document.;
                                 ENG=Find all entries and documents that exist for the document number and posting date on the selected entry or document.];
                      ApplicationArea=#SalesReturnOrder;
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the involved entry or record, according to the specified number series.;
                           ENG=Specifies the number of the involved entry or record, according to the specified number series.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the customer.;
                           ENG=Specifies the number of the customer.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Sell-to Customer No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the customer.;
                           ENG=Specifies the name of the customer.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Sell-to Customer Name" }

    { 23  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the customer's main address.;
                           ENG=Specifies the postcode of the customer's main address.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Sell-to Post Code";
                Visible=FALSE }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the country/region code of the customer's main address.;
                           ENG=Specifies the country/region code of the customer's main address.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Sell-to Country/Region Code";
                Visible=FALSE }

    { 31  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the contact person at the customer's main address.;
                           ENG=Specifies the name of the contact person at the customer's main address.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Sell-to Contact";
                Visible=FALSE }

    { 99  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the customer that you send or sent the invoice or credit memo to.;
                           ENG=Specifies the number of the customer that you send or sent the invoice or credit memo to.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Bill-to Customer No.";
                Visible=FALSE }

    { 97  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the customer that you send or sent the invoice or credit memo to.;
                           ENG=Specifies the name of the customer that you send or sent the invoice or credit memo to.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Bill-to Name";
                Visible=FALSE }

    { 29  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the customer's billing address.;
                           ENG=Specifies the postcode of the customer's billing address.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Bill-to Post Code";
                Visible=FALSE }

    { 25  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the country/region code of the customer's billing address.;
                           ENG=Specifies the country/region code of the customer's billing address.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Bill-to Country/Region Code";
                Visible=FALSE }

    { 87  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the contact person at the customer's billing address.;
                           ENG=Specifies the name of the contact person at the customer's billing address.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Bill-to Contact";
                Visible=FALSE }

    { 83  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for an alternate shipment address if you want to ship to another address than the one that has been entered automatically. This field is also used in case of drop shipment.;
                           ENG=Specifies a code for an alternate shipment address if you want to ship to another address than the one that has been entered automatically. This field is also used in case of drop shipment.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Ship-to Code";
                Visible=FALSE }

    { 81  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the customer at the address that the items are shipped to.;
                           ENG=Specifies the name of the customer at the address that the items are shipped to.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Ship-to Name";
                Visible=FALSE }

    { 17  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the address that the items are shipped to.;
                           ENG=Specifies the postcode of the address that the items are shipped to.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Ship-to Post Code";
                Visible=FALSE }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the country/region code of the address that the items are shipped to.;
                           ENG=Specifies the country/region code of the address that the items are shipped to.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Ship-to Country/Region Code";
                Visible=FALSE }

    { 71  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the contact person at the address that the items are shipped to.;
                           ENG=Specifies the name of the contact person at the address that the items are shipped to.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Ship-to Contact";
                Visible=FALSE }

    { 67  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the entry's posting date.;
                           ENG=Specifies the entry's posting date.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Posting Date";
                Visible=FALSE }

    { 53  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies which salesperson is associated with the posted return receipts.;
                           ENG=Specifies which salesperson is associated with the posted return receipts.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Salesperson Code";
                Visible=FALSE }

    { 59  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 1, which is one of two global dimension codes that you set up in the General Ledger Setup window.;
                           ENG=Specifies the code for Shortcut Dimension 1, which is one of two global dimension codes that you set up in the General Ledger Setup window.];
                ApplicationArea=#Dimensions;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 57  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 2, which is one of two global dimension codes that you set up in the General Ledger Setup window.;
                           ENG=Specifies the code for Shortcut Dimension 2, which is one of two global dimension codes that you set up in the General Ledger Setup window.];
                ApplicationArea=#Dimensions;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the currency that is used on the entry.;
                           ENG=Specifies the currency that is used on the entry.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Currency Code" }

    { 61  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the location from where inventory items to the customer on the sales document are to be shipped by default.;
                           ENG=Specifies the location from where inventory items to the customer on the sales document are to be shipped by default.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Location Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many times the document has been printed.;
                           ENG=Specifies how many times the document has been printed.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="No. Printed" }

    { 1102601001;2;Field  ;
                ToolTipML=[ENU=Specifies the date when the related document was created.;
                           ENG=Specifies the date when the related document was created.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Document Date";
                Visible=FALSE }

    { 1102601003;2;Field  ;
                ToolTipML=[ENU=Specifies the delivery conditions of the related shipment, such as free on board (FOB).;
                           ENG=Specifies the delivery conditions of the related shipment, such as free on board (FOB).];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Shipment Method Code";
                Visible=FALSE }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the shipping agent who is transporting the items.;
                           ENG=Specifies the code for the shipping agent who is transporting the items.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Shipping Agent Code";
                Visible=FALSE }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the shipping agent's package number.;
                           ENG=Specifies the shipping agent's package number.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Package Tracking No.";
                Visible=FALSE }

    { 1102601005;2;Field  ;
                ToolTipML=[ENU=Specifies when items on the document are shipped or were shipped. A shipment date is usually calculated from a requested delivery date plus lead time.;
                           ENG=Specifies when items on the document are shipped or were shipped. A shipment date is usually calculated from a requested delivery date plus lead time.];
                ApplicationArea=#SalesReturnOrder;
                SourceExpr="Shipment Date";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ReturnRcptHeader@1000 : Record 6660;

    BEGIN
    END.
  }
}

