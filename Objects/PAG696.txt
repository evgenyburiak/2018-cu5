OBJECT Page 696 All Objects
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=All Objects;
               ENG=All Objects];
    SourceTable=Table2000000038;
    DataCaptionFields=Object Type;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=Object Type;
                           ENG=Object Type];
                ToolTipML=[ENU=Specifies the type of the object.;
                           ENG=Specifies the type of the object.];
                ApplicationArea=#Advanced;
                SourceExpr="Object Type" }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=Object ID;
                           ENG=Object ID];
                ToolTipML=[ENU=Specifies the ID of the object.;
                           ENG=Specifies the ID of the object.];
                ApplicationArea=#Advanced;
                SourceExpr="Object ID" }

    { 6   ;2   ;Field     ;
                CaptionML=[ENU=Object Name;
                           ENG=Object Name];
                ToolTipML=[ENU=Specifies the name of the object.;
                           ENG=Specifies the name of the object.];
                ApplicationArea=#Advanced;
                SourceExpr="Object Name" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

