OBJECT Page 7116 Analysis Report Names
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Analysis Report Names;
               ENG=Analysis Report Names];
    SourceTable=Table7111;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the analysis report name.;
                           ENG=Specifies the analysis report name.];
                ApplicationArea=#Advanced;
                SourceExpr=Name }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the analysis report description.;
                           ENG=Specifies the analysis report description.];
                ApplicationArea=#Advanced;
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the analysis line template name for this analysis report.;
                           ENG=Specifies the analysis line template name for this analysis report.];
                ApplicationArea=#Advanced;
                SourceExpr="Analysis Line Template Name" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the column template name for this analysis report.;
                           ENG=Specifies the column template name for this analysis report.];
                ApplicationArea=#Advanced;
                SourceExpr="Analysis Column Template Name" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

