OBJECT Page 7395 Posted Invt. Pick List
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Posted Invt. Pick List;
               ENG=Posted Invt. Pick List];
    SourceTable=Table7342;
    PageType=List;
    CardPageID=Posted Invt. Pick;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1102601000;1 ;ActionGroup;
                      CaptionML=[ENU=P&ick;
                                 ENG=P&ick];
                      Image=CreateInventoryPickup }
      { 1102601002;2 ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 ENG=Co&mments];
                      ToolTipML=[ENU=View or add comments for the record.;
                                 ENG=View or add comments for the record.];
                      ApplicationArea=#Warehouse;
                      RunObject=Page 5776;
                      RunPageLink=Table Name=CONST(Posted Invt. Pick),
                                  Type=CONST(" "),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1102601003;1 ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 ENG=&Navigate];
                      ToolTipML=[ENU=Find all entries and documents that exist for the document number and posting date on the selected entry or document.;
                                 ENG=Find all entries and documents that exist for the document number and posting date on the selected entry or document.];
                      ApplicationArea=#Warehouse;
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the involved entry or record, according to the specified number series.;
                           ENG=Specifies the number of the involved entry or record, according to the specified number series.];
                ApplicationArea=#Warehouse;
                SourceExpr="No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the posting date from the inventory pick.;
                           ENG=Specifies the posting date from the inventory pick.];
                ApplicationArea=#Warehouse;
                SourceExpr="Posting Date" }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the source document that the entry originates from.;
                           ENG=Specifies the number of the source document that the entry originates from.];
                ApplicationArea=#Warehouse;
                SourceExpr="Source No." }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the inventory pick number from which the pick was posted.;
                           ENG=Specifies the inventory pick number from which the pick was posted.];
                ApplicationArea=#Warehouse;
                SourceExpr="Invt Pick No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the location code for where the posted inventory pick occurred.;
                           ENG=Specifies the location code for where the posted inventory pick occurred.];
                ApplicationArea=#Warehouse;
                SourceExpr="Location Code" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series from which entry or record numbers are assigned to new entries or records.;
                           ENG=Specifies the number series from which entry or record numbers are assigned to new entries or records.];
                ApplicationArea=#Warehouse;
                SourceExpr="No. Series" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

