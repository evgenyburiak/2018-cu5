OBJECT Page 8633 Config. Questions FactBox
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Config. Questions FactBox;
               ENG=Config. Questions FactBox];
    SourceTable=Table8612;
    PageType=ListPart;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the questionnaire.;
                           ENG=Specifies the code for the questionnaire.];
                ApplicationArea=#Advanced;
                SourceExpr="Questionnaire Code";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the question area.;
                           ENG=Specifies the code for the question area.];
                ApplicationArea=#Advanced;
                SourceExpr="Question Area Code";
                Visible=FALSE }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a question that is to be answered on the setup questionnaire. On the Actions tab, in the Question group, choose Update Questions to auto populate the question list based on the fields in the table on which the question area is based. You can modify the text to be more meaningful to the person responsible for filling out the questionnaire. For example, you could rewrite the Name? question as What is the name of your company?;
                           ENG=Specifies a question that is to be answered on the setup questionnaire. On the Actions tab, in the Question group, choose Update Questions to auto populate the question list based on the fields in the table on which the question area is based. You can modify the text to be more meaningful to the person responsible for filling out the questionnaire. For example, you could rewrite the Name? question as What is the name of your company?];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Question }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the answer to the question. The answer to the question should match the format of the answer option and must be a value that the database supports. If it does not, then there will be an error when you apply the answer.;
                           ENG=Specifies the answer to the question. The answer to the question should match the format of the answer option and must be a value that the database supports. If it does not, then there will be an error when you apply the answer.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Answer }

  }
  CODE
  {

    BEGIN
    END.
  }
}

