OBJECT Page 8636 Config. Field Mapping
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Config. Field Mapping;
               ENG=Config. Field Mapping];
    SourceTable=Table8628;
    PageType=List;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the old value in the data that you want to map to new value. Usually, the value is one that is based on an option list.;
                           ENG=Specifies the old value in the data that you want to map to new value. Usually, the value is one that is based on an option list.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Old Value" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the value in the data in Dynamics NAV to which you want to map the old value. Usually, the value is one that is in an existing option list.;
                           ENG=Specifies the value in the data in Dynamics NAV to which you want to map the old value. Usually, the value is one that is in an existing option list.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="New Value" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

