OBJECT Page 9025 Small Business Report Catalog
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Report Catalog;
               ENG=Report Catalogue];
    PageType=CardPart;
  }
  CONTROLS
  {
    { 10  ;    ;Container ;
                Name=Reports Contet Area;
                ContainerType=ContentArea }

    { 11  ;1   ;Group     ;
                Name=AgedAccountsReports;
                CaptionML=[ENU=Aged Accounts Reports;
                           ENG=Aged Accounts Reports];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 3       ;    ;Action    ;
                                  Name=AgedAccountsReceivable;
                                  CaptionML=[ENU=Aged Accounts Receivable;
                                             ENG=Aged Accounts Receivable];
                                  ToolTipML=[ENU=Specifies amounts owed by customers and the length of time outstanding.;
                                             ENG=Specifies amounts owed by customers and the length of time outstanding.];
                                  ApplicationArea=#Basic,#Suite;
                                  Image=TileReport;
                                  RunPageMode=Create;
                                  OnAction=BEGIN
                                             SmallBusinessReportCatalogCU.RunAgedAccountsReceivableReport(FALSE);
                                           END;
                                            }
                  { 2       ;    ;Action    ;
                                  Name=AgedAccountsPayable;
                                  CaptionML=[ENU=Aged Accounts Payable;
                                             ENG=Aged Accounts Payable];
                                  ToolTipML=[ENU=Specifies amounts owed to creditors and the length of time outstanding.;
                                             ENG=Specifies amounts owed to creditors and the length of time outstanding.];
                                  ApplicationArea=#Basic,#Suite;
                                  Image=TileReport;
                                  RunPageMode=Create;
                                  OnAction=BEGIN
                                             SmallBusinessReportCatalogCU.RunAgedAccountsPayableReport(FALSE);
                                           END;
                                            }
                }
                 }

    { 12  ;1   ;Group     ;
                Name=CustomersAndVendors;
                CaptionML=[ENU=Customers and Vendors;
                           ENG=Customers and Vendors];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 6       ;    ;Action    ;
                                  Name=CustomerTop10List;
                                  CaptionML=[ENU=Customer Top 10 List;
                                             ENG=Customer Top 10 List];
                                  ToolTipML=[ENU=Specifies information about customers' purchases and balances.;
                                             ENG=Specifies information about customers' purchases and balances.];
                                  ApplicationArea=#Basic,#Suite;
                                  OnAction=BEGIN
                                             SmallBusinessReportCatalogCU.RunCustomerTop10ListReport(FALSE);
                                           END;
                                            }
                  { 7       ;    ;Action    ;
                                  Name=VendorTop10List;
                                  CaptionML=[ENU=Vendor Top 10 List;
                                             ENG=Vendor Top 10 List];
                                  ToolTipML=[ENU=View a list of the vendors from whom you purchase the most or to whom you owe the most.;
                                             ENG=View a list of the vendors from whom you purchase the most or to whom you owe the most.];
                                  ApplicationArea=#Basic,#Suite;
                                  OnAction=BEGIN
                                             SmallBusinessReportCatalogCU.RunVendorTop10ListReport(FALSE);
                                           END;
                                            }
                  { 8       ;    ;Action    ;
                                  Name=CustomerStatement;
                                  CaptionML=[ENU=Customer Statement;
                                             ENG=Customer Statement];
                                  ToolTipML=[ENU=Specifies a list of customer statements.;
                                             ENG=Specifies a list of customer statements.];
                                  ApplicationArea=#Basic,#Suite;
                                  OnAction=BEGIN
                                             SmallBusinessReportCatalogCU.RunCustomerStatementReport(FALSE);
                                           END;
                                            }
                }
                 }

    { 13  ;1   ;Group     ;
                Name=OtherReports;
                CaptionML=[ENU=Other Reports;
                           ENG=Other Reports];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 9       ;    ;Action    ;
                                  Name=Trial Balance;
                                  CaptionML=[ENU=Trial Balance;
                                             ENG=Trial Balance];
                                  ToolTipML=[ENU=Specifies the chart of accounts with balances and net changes. You can use the report at the close of an accounting period or fiscal year.;
                                             ENG=Specifies the chart of accounts with balances and net changes. You can use the report at the close of an accounting period or fiscal year.];
                                  ApplicationArea=#Basic,#Suite;
                                  Image=TileCurrency;
                                  OnAction=BEGIN
                                             SmallBusinessReportCatalogCU.RunTrialBalanceReport(FALSE);
                                           END;
                                            }
                  { 1       ;    ;Action    ;
                                  Name=Detail Trial Balance;
                                  CaptionML=[ENU=Detail Trial Balance;
                                             ENG=Detail Trial Balance];
                                  ToolTipML=[ENU=Specifies general ledger account balances and activities.;
                                             ENG=Specifies general ledger account balances and activities.];
                                  ApplicationArea=#Basic,#Suite;
                                  Image=TileCurrency;
                                  OnAction=BEGIN
                                             SmallBusinessReportCatalogCU.RunDetailTrialBalanceReport(FALSE);
                                           END;
                                            }
                }
                 }

  }
  CODE
  {
    VAR
      SmallBusinessReportCatalogCU@1000 : Codeunit 9025;

    BEGIN
    END.
  }
}

