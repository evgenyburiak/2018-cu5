OBJECT Page 9038 Production Planner Activities
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Activities;
               ENG=Activities];
    SourceTable=Table9056;
    PageType=CardPart;
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;

                 SETRANGE("User ID Filter",USERID);
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                CaptionML=[ENU=Production Orders;
                           ENG=Production Orders];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 21      ;0   ;Action    ;
                                  CaptionML=[ENU=Change Production Order Status;
                                             ENG=Change Production Order Status];
                                  ToolTipML=[ENU=Change the production order to another status, such as Released.;
                                             ENG=Change the production order to another status, such as Released.];
                                  ApplicationArea=#Manufacturing;
                                  RunObject=Page 99000914;
                                  Image=ChangeStatus }
                  { 12      ;0   ;Action    ;
                                  CaptionML=[ENU=New Production Order;
                                             ENG=New Production Order];
                                  ToolTipML=[ENU="Prepare to produce an end item. ";
                                             ENG="Prepare to produce an end item. "];
                                  ApplicationArea=#Manufacturing;
                                  RunObject=Page 99000813;
                                  RunPageMode=Create }
                  { 28      ;0   ;Action    ;
                                  CaptionML=[ENU=Navigate;
                                             ENG=Navigate];
                                  ToolTipML=[ENU=Find all entries and documents that exist for the document number and posting date on the selected entry or document.;
                                             ENG=Find all entries and documents that exist for the document number and posting date on the selected entry or document.];
                                  ApplicationArea=#Advanced;
                                  RunObject=Page 344;
                                  Image=Navigate }
                }
                 }

    { 1   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of planned production orders that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of planned production orders that are displayed in the Manufacturing Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Manufacturing;
                SourceExpr="Planned Prod. Orders - All";
                DrillDownPageID=Planned Production Orders }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of firm planned production orders that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of firm planned production orders that are displayed in the Manufacturing Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Manufacturing;
                SourceExpr="Firm Plan. Prod. Orders - All";
                DrillDownPageID=Firm Planned Prod. Orders }

    { 7   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of released production orders that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of released production orders that are displayed in the Manufacturing Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Manufacturing;
                SourceExpr="Released Prod. Orders - All";
                DrillDownPageID=Released Production Orders }

    { 3   ;1   ;Group     ;
                CaptionML=[ENU=Planning - Operations;
                           ENG=Planning - Operations];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 15      ;0   ;Action    ;
                                  CaptionML=[ENU=New Purchase Order;
                                             ENG=New Purchase Order];
                                  ToolTipML=[ENU=Purchase goods or services from a vendor.;
                                             ENG=Purchase goods or services from a vendor.];
                                  ApplicationArea=#Advanced;
                                  RunObject=Page 50;
                                  RunPageMode=Create }
                  { 16      ;0   ;Action    ;
                                  CaptionML=[ENU=Edit Planning Worksheet;
                                             ENG=Edit Planning Worksheet];
                                  ToolTipML=[ENU=Plan supply orders automatically to fulfill new demand.;
                                             ENG=Plan supply orders automatically to fulfil new demand.];
                                  ApplicationArea=#Advanced;
                                  RunObject=Page 99000852 }
                  { 17      ;0   ;Action    ;
                                  CaptionML=[ENU=Edit Subcontracting Worksheet;
                                             ENG=Edit Subcontracting Worksheet];
                                  ToolTipML=[ENU=Plan outsourcing of operation on released production orders.;
                                             ENG=Plan outsourcing of operation on released production orders.];
                                  ApplicationArea=#Manufacturing;
                                  RunObject=Page 99000886 }
                }
                 }

    { 11  ;2   ;Field     ;
                DrillDown=Yes;
                CaptionML=[ENU=My Purchase Orders;
                           ENG=My Purchase Orders];
                ToolTipML=[ENU=Specifies the number of purchase orders that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of purchase orders that are displayed in the Manufacturing Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Advanced;
                SourceExpr="Purchase Orders";
                DrillDownPageID=Purchase Order List }

    { 10  ;1   ;Group     ;
                CaptionML=[ENU=Design;
                           ENG=Design];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 24      ;0   ;Action    ;
                                  CaptionML=[ENU=New Item;
                                             ENG=New Item];
                                  ToolTipML=[ENU=Create an item card based on the stockkeeping unit.;
                                             ENG=Create an item card based on the stockkeeping unit.];
                                  ApplicationArea=#Advanced;
                                  RunObject=Page 30;
                                  Image=NewItem;
                                  RunPageMode=Create }
                  { 25      ;0   ;Action    ;
                                  CaptionML=[ENU=New Production BOM;
                                             ENG=New Production BOM];
                                  ToolTipML=[ENU=Create a bill of material that defines the components in a produced item.;
                                             ENG=Create a bill of material that defines the components in a produced item.];
                                  ApplicationArea=#Manufacturing;
                                  RunObject=Page 99000786;
                                  RunPageMode=Create }
                  { 26      ;0   ;Action    ;
                                  CaptionML=[ENU=New Routing;
                                             ENG=New Routing];
                                  ToolTipML=[ENU=Create a routing that defines the operations required to produce an end item.;
                                             ENG=Create a routing that defines the operations required to produce an end item.];
                                  ApplicationArea=#Manufacturing;
                                  RunObject=Page 99000766;
                                  RunPageMode=Create }
                }
                 }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of production BOMs that are under development that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of production BOMs that are under development that are displayed in the Manufacturing Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Manufacturing;
                SourceExpr="Prod. BOMs under Development";
                DrillDownPageID=Production BOM List }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the routings under development that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the routings under development that are displayed in the Manufacturing Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Manufacturing;
                SourceExpr="Routings under Development";
                DrillDownPageID=Routing List }

    { 6   ;1   ;Group     ;
                CaptionML=[ENU=My User Tasks;
                           ENG=My User Tasks];
                GroupType=CueGroup }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=Pending User Tasks;
                           ENG=Pending User Tasks];
                ToolTipML=[ENU=Specifies the number of pending tasks that are assigned to you.;
                           ENG=Specifies the number of pending tasks that are assigned to you.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Pending Tasks";
                DrillDownPageID=User Task List;
                Image=Checklist }

  }
  CODE
  {

    BEGIN
    END.
  }
}

