OBJECT Page 905 Assembly Setup
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Assembly Setup;
               ENG=Assembly Setup];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table905;
    PageType=Card;
    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;
               END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=General;
                CaptionML=[ENU=General;
                           ENG=General];
                GroupType=Group }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the assembly availability warning appears during sales order entry.;
                           ENG=Specifies whether the assembly availability warning appears during sales order entry.];
                ApplicationArea=#Assembly;
                SourceExpr="Stockout Warning" }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how dimension codes are distributed to assembly components when they are consumed in assembly order posting.;
                           ENG=Specifies how dimension codes are distributed to assembly components when they are consumed in assembly order posting.];
                ApplicationArea=#Suite;
                SourceExpr="Copy Component Dimensions from" }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies at which location assembly orders are created by default.;
                           ENG=Specifies at which location assembly orders are created by default.];
                ApplicationArea=#Location;
                SourceExpr="Default Location for Orders" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that comments on assembly order lines are copied to the resulting posted documents.;
                           ENG=Specifies that comments on assembly order lines are copied to the resulting posted documents.];
                ApplicationArea=#Assembly;
                SourceExpr="Copy Comments when Posting" }

    { 3   ;1   ;Group     ;
                Name=Numbering;
                CaptionML=[ENU=Numbering;
                           ENG=Numbering];
                GroupType=Group }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code used to assign numbers to assembly orders when they are created.;
                           ENG=Specifies the number series code used to assign numbers to assembly orders when they are created.];
                ApplicationArea=#Assembly;
                SourceExpr="Assembly Order Nos." }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code used to assign numbers to assembly quotes when they are created.;
                           ENG=Specifies the number series code used to assign numbers to assembly quotes when they are created.];
                ApplicationArea=#Assembly;
                SourceExpr="Assembly Quote Nos." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code used to assign numbers to assembly blanket orders when they are created.;
                           ENG=Specifies the number series code used to assign numbers to assembly blanket orders when they are created.];
                ApplicationArea=#Assembly;
                SourceExpr="Blanket Assembly Order Nos." }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code used to assign numbers to assembly orders when they are posted.;
                           ENG=Specifies the number series code used to assign numbers to assembly orders when they are posted.];
                ApplicationArea=#Assembly;
                SourceExpr="Posted Assembly Order Nos." }

    { 14  ;1   ;Group     ;
                Name=Warehouse;
                CaptionML=[ENU=Warehouse;
                           ENG=Warehouse];
                GroupType=Group }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that an inventory movement for the required components is created automatically when you create an inventory pick.;
                           ENG=Specifies that an inventory movement for the required components is created automatically when you create an inventory pick.];
                ApplicationArea=#Warehouse;
                SourceExpr="Create Movements Automatically" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

