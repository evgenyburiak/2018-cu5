OBJECT Page 9053 WMS Ship & Receive Activities
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Activities;
               ENG=Activities];
    SourceTable=Table9051;
    PageType=CardPart;
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;

                 SETRANGE("Date Filter",0D,WORKDATE);
                 SETRANGE("Date Filter2",WORKDATE,WORKDATE);
                 SETFILTER("User ID Filter",USERID);

                 LocationCode := GetEmployeeLocation(USERID);
                 SETFILTER("Location Filter",LocationCode);
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 3   ;1   ;Group     ;
                CaptionML=[ENU=Outbound - Today;
                           ENG=Outbound - Today];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 7       ;0   ;Action    ;
                                  CaptionML=[ENU=New Whse. Shipment;
                                             ENG=New Whse. Shipment];
                                  ToolTipML=[ENU=Ship items according to an advanced warehouse configuration.;
                                             ENG=Ship items according to an advanced warehouse configuration.];
                                  ApplicationArea=#Warehouse;
                                  RunObject=Page 7335;
                                  RunPageMode=Create }
                  { 1       ;0   ;Action    ;
                                  CaptionML=[ENU=New Transfer Order;
                                             ENG=New Transfer Order];
                                  ToolTipML=[ENU=Move items from one warehouse location to another.;
                                             ENG=Move items from one warehouse location to another.];
                                  ApplicationArea=#Advanced;
                                  RunObject=Page 5740;
                                  RunPageMode=Create }
                }
                 }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of released sales orders that are displayed in the Warehouse WMS Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of released sales orders that are displayed in the Warehouse WMS Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Warehouse;
                SourceExpr="Released Sales Orders - Today";
                DrillDownPageID=Sales Order List }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of shipments that are displayed in the Warehouse WMS Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of shipments that are displayed in the Warehouse WMS Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Warehouse;
                SourceExpr="Shipments - Today";
                DrillDownPageID=Warehouse Shipment List }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of picked shipments that are displayed in the Warehouse WMS Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of picked shipments that are displayed in the Warehouse WMS Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Warehouse;
                SourceExpr="Picked Shipments - Today";
                DrillDownPageID=Warehouse Shipment List }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of posted shipments that are displayed in the Warehouse WMS Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of posted shipments that are displayed in the Warehouse WMS Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Warehouse;
                SourceExpr="Posted Shipments - Today";
                DrillDownPageID=Posted Whse. Shipment List }

    { 20  ;1   ;Group     ;
                CaptionML=[ENU=Inbound - Today;
                           ENG=Inbound - Today];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 18      ;0   ;Action    ;
                                  CaptionML=[ENU=New Purchase Order;
                                             ENG=New Purchase Order];
                                  ToolTipML=[ENU=Purchase goods or services from a vendor.;
                                             ENG=Purchase goods or services from a vendor.];
                                  ApplicationArea=#Advanced;
                                  RunObject=Page 50;
                                  RunPageMode=Create }
                  { 19      ;0   ;Action    ;
                                  CaptionML=[ENU=New Whse. Receipt;
                                             ENG=New Whse. Receipt];
                                  ToolTipML=[ENU="Receive items according to an advanced warehouse configuration. ";
                                             ENG="Receive items according to an advanced warehouse configuration. "];
                                  ApplicationArea=#Warehouse;
                                  RunObject=Page 5768;
                                  RunPageMode=Create }
                }
                 }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of expected purchase orders that are displayed in the Warehouse WMS Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of expected purchase orders that are displayed in the Warehouse WMS Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Warehouse;
                SourceExpr="Expected Purchase Orders";
                DrillDownPageID=Purchase Order List }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of arrivals that are displayed in the Warehouse WMS Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of arrivals that are displayed in the Warehouse WMS Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Warehouse;
                SourceExpr=Arrivals;
                DrillDownPageID=Warehouse Receipts }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of posted receipts that are displayed in the Warehouse WMS Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of posted receipts that are displayed in the Warehouse WMS Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Warehouse;
                SourceExpr="Posted Receipts - Today";
                DrillDownPageID=Posted Whse. Receipt List }

    { 21  ;1   ;Group     ;
                CaptionML=[ENU=Internal;
                           ENG=Internal];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 31      ;0   ;Action    ;
                                  CaptionML=[ENU=Edit Put-away Worksheet;
                                             ENG=Edit Put-away Worksheet];
                                  ToolTipML=[ENU=Plan and organize different kinds of put-aways, including put-aways with lines from several orders. You can also assign the planned put-aways to particular warehouse employees.;
                                             ENG=Plan and organise different kinds of put-aways, including put-aways with lines from several orders. You can also assign the planned put-aways to particular warehouse employees.];
                                  ApplicationArea=#Warehouse;
                                  RunObject=Page 7352 }
                  { 32      ;0   ;Action    ;
                                  CaptionML=[ENU=Edit Pick Worksheet;
                                             ENG=Edit Pick Worksheet];
                                  ToolTipML=[ENU=Plan and organize different kinds of picks, including picks with lines from several orders or assignment of picks to particular employees.;
                                             ENG=Plan and organise different kinds of picks, including picks with lines from several orders or assignment of picks to particular employees.];
                                  ApplicationArea=#Warehouse;
                                  RunObject=Page 7345 }
                  { 33      ;0   ;Action    ;
                                  CaptionML=[ENU=Edit Movement Worksheet;
                                             ENG=Edit Movement Worksheet];
                                  ToolTipML=[ENU=Prepare to move items between bins within the warehouse.;
                                             ENG=Prepare to move items between bins within the warehouse.];
                                  ApplicationArea=#Warehouse;
                                  RunObject=Page 7351 }
                }
                 }

    { 29  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of picks that are displayed in the Warehouse WMS Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of picks that are displayed in the Warehouse WMS Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Warehouse;
                SourceExpr="Picks - All";
                DrillDownPageID=Warehouse Picks }

    { 27  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of put-always that are displayed in the Warehouse WMS Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of put-always that are displayed in the Warehouse WMS Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Warehouse;
                SourceExpr="Put-aways - All";
                DrillDownPageID=Warehouse Put-aways }

    { 25  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of movements that are displayed in the Warehouse WMS Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of movements that are displayed in the Warehouse WMS Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Warehouse;
                SourceExpr="Movements - All";
                DrillDownPageID=Warehouse Movements }

    { 23  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of registered picks that are displayed in the Warehouse WMS Cue on the Role Center. The documents are filtered by today's date.;
                           ENG=Specifies the number of registered picks that are displayed in the Warehouse WMS Cue on the Role Centre. The documents are filtered by today's date.];
                ApplicationArea=#Warehouse;
                SourceExpr="Registered Picks - Today";
                DrillDownPageID=Registered Whse. Picks }

    { 4   ;1   ;Group     ;
                CaptionML=[ENU=My User Tasks;
                           ENG=My User Tasks];
                GroupType=CueGroup }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=Pending User Tasks;
                           ENG=Pending User Tasks];
                ToolTipML=[ENU=Specifies the number of pending tasks that are assigned to you.;
                           ENG=Specifies the number of pending tasks that are assigned to you.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Pending Tasks";
                DrillDownPageID=User Task List;
                Image=Checklist }

  }
  CODE
  {
    VAR
      LocationCode@1000 : Text[1024];

    BEGIN
    END.
  }
}

