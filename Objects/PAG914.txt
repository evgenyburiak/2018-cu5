OBJECT Page 914 Assemble-to-Order Lines
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Assemble-to-Order Lines;
               ENG=Assemble-to-Order Lines];
    SourceTable=Table901;
    DataCaptionExpr=GetCaption;
    DelayedInsert=Yes;
    PopulateAllFields=Yes;
    PageType=Worksheet;
    AutoSplitKey=Yes;
    OnAfterGetRecord=BEGIN
                       UpdateAvailWarning;
                     END;

    OnDeleteRecord=VAR
                     AssemblyLineReserve@1000 : Codeunit 926;
                   BEGIN
                     IF (Quantity <> 0) AND ItemExists("No.") THEN BEGIN
                       COMMIT;
                       IF NOT AssemblyLineReserve.DeleteLineConfirm(Rec) THEN
                         EXIT(FALSE);
                       AssemblyLineReserve.DeleteLine(Rec);
                     END;
                   END;

    ActionList=ACTIONS
    {
      { 27      ;    ;ActionContainer;
                      CaptionML=[ENU=Line;
                                 ENG=Line];
                      ActionContainerType=ActionItems }
      { 37      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Reserve;
                                 ENG=&Reserve];
                      ToolTipML=[ENU=Reserve the quantity that is required on the document line that you opened this window for.;
                                 ENG=Reserve the quantity that is required on the document line that you opened this window for.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=LineReserve;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ShowReservation;
                               END;
                                }
      { 26      ;1   ;Action    ;
                      CaptionML=[ENU=Select Item Substitution;
                                 ENG=Select Item Substitution];
                      ToolTipML=[ENU=Select another item that has been set up to be traded instead of the original item if it is unavailable.;
                                 ENG=Select another item that has been set up to be traded instead of the original item if it is unavailable.];
                      ApplicationArea=#Suite;
                      Image=SelectItemSubstitution;
                      OnAction=BEGIN
                                 ShowItemSub;
                                 CurrPage.UPDATE;
                               END;
                                }
      { 14      ;1   ;Action    ;
                      CaptionML=[ENU=Explode BOM;
                                 ENG=Explode BOM];
                      ToolTipML=[ENU=Insert new lines for the components on the bill of materials, for example to sell the parent item as a kit. CAUTION: The line for the parent item will be deleted and represented by a description only. To undo, you must delete the component lines and add a line the parent item again.;
                                 ENG=Insert new lines for the components on the bill of materials, for example to sell the parent item as a kit. CAUTION: The line for the parent item will be deleted and represented by a description only. To undo, you must delete the component lines and add a line the parent item again.];
                      ApplicationArea=#Assembly;
                      Image=ExplodeBOM;
                      OnAction=BEGIN
                                 ExplodeAssemblyList;
                                 CurrPage.UPDATE;
                               END;
                                }
      { 16      ;1   ;Action    ;
                      CaptionML=[ENU=Assembly BOM;
                                 ENG=Assembly BOM];
                      ToolTipML=[ENU=View or edit the bill of material that specifies which items and resources are required to assemble the assembly item.;
                                 ENG=View or edit the bill of material that specifies which items and resources are required to assemble the assembly item.];
                      ApplicationArea=#Assembly;
                      Image=BulletList;
                      OnAction=BEGIN
                                 ShowAssemblyList;
                               END;
                                }
      { 44      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Create Inventor&y Movement;
                                 ENG=Create Inventor&y Movement];
                      ToolTipML=[ENU=Create an inventory movement to handle items on the document according to a basic warehouse configuration.;
                                 ENG=Create an inventory movement to handle items on the document according to a basic warehouse configuration.];
                      ApplicationArea=#Warehouse;
                      Image=CreatePutAway;
                      OnAction=VAR
                                 AssemblyHeader@1000 : Record 900;
                                 ATOMovementsCreated@1001 : Integer;
                                 TotalATOMovementsToBeCreated@1002 : Integer;
                               BEGIN
                                 AssemblyHeader.GET("Document Type","Document No.");
                                 AssemblyHeader.CreateInvtMovement(FALSE,FALSE,FALSE,ATOMovementsCreated,TotalATOMovementsToBeCreated);
                               END;
                                }
      { 45      ;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 36      ;1   ;Action    ;
                      CaptionML=[ENU=Show Document;
                                 ENG=Show Document];
                      ToolTipML=[ENU=Open the document that the selected line exists on.;
                                 ENG=Open the document that the selected line exists on.];
                      ApplicationArea=#Assembly;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=View;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ATOLink@1002 : Record 904;
                                 SalesLine@1000 : Record 37;
                               BEGIN
                                 ATOLink.GET("Document Type","Document No.");
                                 SalesLine.GET(ATOLink."Document Type",ATOLink."Document No.",ATOLink."Document Line No.");
                                 ATOLink.ShowAsm(SalesLine);
                               END;
                                }
      { 23      ;1   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 ENG=Dimensions];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 ENG=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyse transaction history.];
                      ApplicationArea=#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 40      ;1   ;Action    ;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[ENU=Item &Tracking Lines;
                                 ENG=Item &Tracking Lines];
                      ToolTipML=[ENU=View or edit serial numbers and lot numbers that are assigned to the item on the document or journal line.;
                                 ENG=View or edit serial numbers and lot numbers that are assigned to the item on the document or journal line.];
                      ApplicationArea=#ItemTracking;
                      Promoted=Yes;
                      Image=ItemTrackingLines;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 OpenItemTrackingLines;
                               END;
                                }
      { 25      ;1   ;ActionGroup;
                      CaptionML=[ENU=Item Availability by;
                                 ENG=Item Availability by];
                      ActionContainerType=NewDocumentItems;
                      Image=ItemAvailability }
      { 39      ;2   ;Action    ;
                      CaptionML=[ENU=Event;
                                 ENG=Event];
                      ToolTipML=[ENU=View how the actual and the projected available balance of an item will develop over time according to supply and demand events.;
                                 ENG=View how the actual and the projected available balance of an item will develop over time according to supply and demand events.];
                      ApplicationArea=#Assembly;
                      Image=Event;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromAsmLine(Rec,ItemAvailFormsMgt.ByEvent);
                               END;
                                }
      { 28      ;2   ;Action    ;
                      CaptionML=[ENU=Period;
                                 ENG=Period];
                      ToolTipML=[ENU=View the projected quantity of the item over time according to time periods, such as day, week, or month.;
                                 ENG=View the projected quantity of the item over time according to time periods, such as day, week, or month.];
                      ApplicationArea=#Assembly;
                      Image=Period;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromAsmLine(Rec,ItemAvailFormsMgt.ByPeriod);
                               END;
                                }
      { 32      ;2   ;Action    ;
                      CaptionML=[ENU=Variant;
                                 ENG=Variant];
                      ToolTipML=[ENU=View or edit the item's variants. Instead of setting up each color of an item as a separate item, you can set up the various colors as variants of the item.;
                                 ENG=View or edit the item's variants. Instead of setting up each colour of an item as a separate item, you can set up the various colours as variants of the item.];
                      ApplicationArea=#Assembly;
                      Image=ItemVariant;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromAsmLine(Rec,ItemAvailFormsMgt.ByVariant);
                               END;
                                }
      { 29      ;2   ;Action    ;
                      AccessByPermission=TableData 14=R;
                      CaptionML=[ENU=Location;
                                 ENG=Location];
                      ToolTipML=[ENU=View the actual and projected quantity of the item per location.;
                                 ENG=View the actual and projected quantity of the item per location.];
                      ApplicationArea=#Location;
                      Image=Warehouse;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromAsmLine(Rec,ItemAvailFormsMgt.ByLocation);
                               END;
                                }
      { 43      ;2   ;Action    ;
                      CaptionML=[ENU=BOM Level;
                                 ENG=BOM Level];
                      ToolTipML=[ENU=View availability figures for items on bills of materials that show how many units of a parent item you can make based on the availability of child items.;
                                 ENG=View availability figures for items on bills of materials that show how many units of a parent item you can make based on the availability of child items.];
                      ApplicationArea=#Assembly;
                      Image=BOMLevel;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromAsmLine(Rec,ItemAvailFormsMgt.ByBOM);
                               END;
                                }
      { 21      ;1   ;Action    ;
                      CaptionML=[ENU=Comments;
                                 ENG=Comments];
                      ToolTipML=[ENU=View or add comments for the record.;
                                 ENG=View or add comments for the record.];
                      ApplicationArea=#Advanced;
                      RunObject=Page 907;
                      RunPageLink=Document Type=FIELD(Document Type),
                                  Document No.=FIELD(Document No.),
                                  Document Line No.=FIELD(Line No.);
                      Image=ViewComments }
      { 34      ;1   ;Action    ;
                      Name=ShowWarning;
                      CaptionML=[ENU=Show Warning;
                                 ENG=Show Warning];
                      ToolTipML=[ENU=View details about availability issues.;
                                 ENG=View details about availability issues.];
                      ApplicationArea=#Assembly;
                      Image=ShowWarning;
                      OnAction=BEGIN
                                 ShowAvailabilityWarning;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 33  ;2   ;Field     ;
                DrillDown=Yes;
                ToolTipML=[ENU=Specifies Yes if the assembly component is not available in the quantity and on the due date of the assembly order line.;
                           ENG=Specifies Yes if the assembly component is not available in the quantity and on the due date of the assembly order line.];
                ApplicationArea=#Assembly;
                BlankZero=Yes;
                SourceExpr="Avail. Warning";
                OnDrillDown=BEGIN
                              ShowAvailabilityWarning;
                            END;
                             }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the assembly order line is of type Item or Resource.;
                           ENG=Specifies if the assembly order line is of type Item or Resource.];
                ApplicationArea=#Assembly;
                SourceExpr=Type }

    { 35  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the involved entry or record, according to the specified number series.;
                           ENG=Specifies the number of the involved entry or record, according to the specified number series.];
                ApplicationArea=#Assembly;
                SourceExpr="No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the description of the assembly component.;
                           ENG=Specifies the description of the assembly component.];
                ApplicationArea=#Assembly;
                SourceExpr=Description }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the second description of the assembly component.;
                           ENG=Specifies the second description of the assembly component.];
                ApplicationArea=#Assembly;
                SourceExpr="Description 2";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the variant of the item on the line.;
                           ENG=Specifies the variant of the item on the line.];
                ApplicationArea=#Advanced;
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the location from which you want to post consumption of the assembly component.;
                           ENG=Specifies the location from which you want to post consumption of the assembly component.];
                ApplicationArea=#Location;
                SourceExpr="Location Code";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how each unit of the item or resource is measured, such as in pieces or hours. By default, the value in the Base Unit of Measure field on the item or resource card is inserted.;
                           ENG=Specifies how each unit of the item or resource is measured, such as in pieces or hours. By default, the value in the Base Unit of Measure field on the item or resource card is inserted.];
                ApplicationArea=#Assembly;
                SourceExpr="Unit of Measure Code" }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many units of the assembly component are required to assemble one assembly item.;
                           ENG=Specifies how many units of the assembly component are required to assemble one assembly item.];
                ApplicationArea=#Assembly;
                SourceExpr="Quantity per" }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many units of the assembly component are expected to be consumed.;
                           ENG=Specifies how many units of the assembly component are expected to be consumed.];
                ApplicationArea=#Assembly;
                SourceExpr=Quantity }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many units of the assembly component have been reserved for this assembly order line.;
                           ENG=Specifies how many units of the assembly component have been reserved for this assembly order line.];
                ApplicationArea=#Assembly;
                SourceExpr="Reserved Quantity" }

    { 38  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many units of the assembly component have been posted as consumed during the assembly.;
                           ENG=Specifies how many units of the assembly component have been posted as consumed during the assembly.];
                ApplicationArea=#Assembly;
                SourceExpr="Consumed Quantity";
                Visible=FALSE }

    { 41  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many units of the assembly component have been moved or picked for the assembly order line.;
                           ENG=Specifies how many units of the assembly component have been moved or picked for the assembly order line.];
                ApplicationArea=#Warehouse;
                SourceExpr="Qty. Picked";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many units of the assembly component are currently on warehouse pick lines.;
                           ENG=Specifies how many units of the assembly component are currently on warehouse pick lines.];
                ApplicationArea=#Warehouse;
                SourceExpr="Pick Qty.";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the assembly component must be available for consumption by the assembly order.;
                           ENG=Specifies the date when the assembly component must be available for consumption by the assembly order.];
                ApplicationArea=#Assembly;
                SourceExpr="Due Date";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the lead-time offset that is defined for the assembly component on the assembly BOM.;
                           ENG=Specifies the lead-time offset that is defined for the assembly component on the assembly BOM.];
                ApplicationArea=#Assembly;
                SourceExpr="Lead-Time Offset";
                Visible=FALSE }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 1, which is one of two global dimension codes that you set up in the General Ledger Setup window.;
                           ENG=Specifies the code for Shortcut Dimension 1, which is one of two global dimension codes that you set up in the General Ledger Setup window.];
                ApplicationArea=#Suite;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 2, which is one of two global dimension codes that you set up in the General Ledger Setup window.;
                           ENG=Specifies the code for Shortcut Dimension 2, which is one of two global dimension codes that you set up in the General Ledger Setup window.];
                ApplicationArea=#Suite;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the bin where assembly components must be placed prior to assembly and from where they are posted as consumed.;
                           ENG=Specifies the code of the bin where assembly components must be placed prior to assembly and from where they are posted as consumed.];
                ApplicationArea=#Warehouse;
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 17  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies links between business transactions made for the item and an inventory account in the general ledger, to group amounts for that item type.;
                           ENG=Specifies links between business transactions made for the item and an inventory account in the general ledger, to group amounts for that item type.];
                ApplicationArea=#Assembly;
                SourceExpr="Inventory Posting Group";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the cost of one unit of the item or resource on the line.;
                           ENG=Specifies the cost of one unit of the item or resource on the line.];
                ApplicationArea=#Assembly;
                SourceExpr="Unit Cost";
                Visible=FALSE }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the cost of the assembly order line.;
                           ENG=Specifies the cost of the assembly order line.];
                ApplicationArea=#Assembly;
                SourceExpr="Cost Amount" }

    { 46  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity per unit of measure of the component item on the assembly order line.;
                           ENG=Specifies the quantity per unit of measure of the component item on the assembly order line.];
                ApplicationArea=#Assembly;
                SourceExpr="Qty. per Unit of Measure";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how the cost of the resource on the assembly order line is allocated to the assembly item.;
                           ENG=Specifies how the cost of the resource on the assembly order line is allocated to the assembly item.];
                ApplicationArea=#Assembly;
                SourceExpr="Resource Usage Type" }

    { 7   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item ledger entry that the document or journal line is applied to.;
                           ENG=Specifies the number of the item ledger entry that the document or journal line is applied to.];
                ApplicationArea=#Assembly;
                SourceExpr="Appl.-to Item Entry";
                Visible=FALSE }

    { 31  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item ledger entry that the document or journal line is applied from.;
                           ENG=Specifies the number of the item ledger entry that the document or journal line is applied from.];
                ApplicationArea=#Assembly;
                SourceExpr="Appl.-from Item Entry";
                Visible=FALSE }

  }
  CODE
  {
    VAR
      ItemAvailFormsMgt@1000 : Codeunit 353;

    LOCAL PROCEDURE GetCaption@1() : Text[250];
    VAR
      ObjTransln@1009 : Record 377;
      AsmHeader@1003 : Record 900;
      SourceTableName@1002 : Text[250];
      SourceFilter@1001 : Text[200];
      Description@1000 : Text[100];
    BEGIN
      Description := '';

      IF AsmHeader.GET("Document Type","Document No.") THEN BEGIN
        SourceTableName := ObjTransln.TranslateObject(ObjTransln."Object Type"::Table,27);
        SourceFilter := AsmHeader."Item No.";
        Description := AsmHeader.Description;
      END;
      EXIT(STRSUBSTNO('%1 %2 %3',SourceTableName,SourceFilter,Description));
    END;

    BEGIN
    END.
  }
}

