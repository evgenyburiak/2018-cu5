OBJECT Page 9320 Service Credit Memos
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Service Credit Memos;
               ENG=Service Credit Memos];
    SourceTable=Table5900;
    SourceTableView=WHERE(Document Type=CONST(Credit Memo));
    DataCaptionFields=Customer No.;
    PageType=List;
    CardPageID=Service Credit Memo;
    OnOpenPage=BEGIN
                 SetSecurityFilterOnRespCenter;

                 CopyCustomerFilter;
               END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1102601005;1 ;ActionGroup;
                      CaptionML=[ENU=&Cr. Memo;
                                 ENG=&Cr. Memo];
                      Image=CreditMemo }
      { 1102601007;2 ;Action    ;
                      Name=Statistics;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 ENG=Statistics];
                      ToolTipML=[ENU=View statistical information, such as the value of posted entries, for the record.;
                                 ENG=View statistical information, such as the value of posted entries, for the record.];
                      ApplicationArea=#Service;
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CalcInvDiscForHeader;
                                 COMMIT;
                                 PAGE.RUNMODAL(PAGE::"Service Statistics",Rec);
                               END;
                                }
      { 1102601009;2 ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 ENG=Co&mments];
                      ToolTipML=[ENU=View or add comments for the record.;
                                 ENG=View or add comments for the record.];
                      ApplicationArea=#Service;
                      RunObject=Page 5911;
                      RunPageLink=Table Name=CONST(Service Header),
                                  Table Subtype=FIELD(Document Type),
                                  No.=FIELD(No.),
                                  Type=CONST(General);
                      Image=ViewComments }
      { 1102601010;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 ENG=Dimensions];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 ENG=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyse transaction history.];
                      ApplicationArea=#Dimensions;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDocDim;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 1102601012;2 ;Action    ;
                      CaptionML=[ENU=Service Document Lo&g;
                                 ENG=Service Document Lo&g];
                      ToolTipML=[ENU=View a list of the service document changes that have been logged. The program creates entries in the window when, for example, the response time or service order status changed, a resource was allocated, a service order was shipped or invoiced, and so on. Each line in this window identifies the event that occurred to the service document. The line contains the information about the field that was changed, its old and new value, the date and time when the change took place, and the ID of the user who actually made the changes.;
                                 ENG=View a list of the service document changes that have been logged. The program creates entries in the window when, for example, the response time or service order status changed, a resource was allocated, a service order was shipped or invoiced, and so on. Each line in this window identifies the event that occurred to the service document. The line contains the information about the field that was changed, its old and new value, the date and time when the change took place, and the ID of the user who actually made the changes.];
                      ApplicationArea=#Service;
                      Image=Log;
                      OnAction=VAR
                                 TempServDocLog@1000 : TEMPORARY Record 5912;
                               BEGIN
                                 TempServDocLog.RESET;
                                 TempServDocLog.DELETEALL;
                                 TempServDocLog.CopyServLog(TempServDocLog."Document Type"::"Credit Memo","No.");

                                 TempServDocLog.RESET;
                                 TempServDocLog.SETCURRENTKEY("Change Date","Change Time");
                                 TempServDocLog.ASCENDING(FALSE);

                                 PAGE.RUN(0,TempServDocLog);
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1102601000;1 ;ActionGroup;
                      CaptionML=[ENU=P&osting;
                                 ENG=P&osting];
                      Image=Post }
      { 1102601002;2 ;Action    ;
                      ShortCutKey=F9;
                      CaptionML=[ENU=P&ost;
                                 ENG=P&ost];
                      ToolTipML=[ENU=Finalize the document or journal by posting the amounts and quantities to the related accounts in your company books.;
                                 ENG=Finalise the document or journal by posting the amounts and quantities to the related accounts in your company books.];
                      ApplicationArea=#Service;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ServPostYesNo@1000 : Codeunit 5981;
                               BEGIN
                                 ServPostYesNo.PostDocument(Rec);
                               END;
                                }
      { 5       ;2   ;Action    ;
                      Name=Preview;
                      CaptionML=[ENU=Preview Posting;
                                 ENG=Preview Posting];
                      ToolTipML=[ENU=Review the different types of entries that will be created when you post the document or journal.;
                                 ENG=Review the different types of entries that will be created when you post the document or journal.];
                      ApplicationArea=#Service;
                      Image=ViewPostedOrder;
                      OnAction=VAR
                                 ServPostYesNo@1000 : Codeunit 5981;
                               BEGIN
                                 ServPostYesNo.PreviewDocument(Rec);
                               END;
                                }
      { 3       ;2   ;Action    ;
                      Name=PostAndSend;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Post and &Send;
                                 ENG=Post and &Send];
                      ToolTipML=[ENU=Finalize and prepare to send the document according to the customer's sending profile, such as attached to an email. The Send document to window opens first so you can confirm or select a sending profile.;
                                 ENG=Finalise and prepare to send the document according to the customer's sending profile, such as attached to an email. The Send document to window opens first so you can confirm or select a sending profile.];
                      ApplicationArea=#Service;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostSendTo;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"Service-Post and Send",Rec);
                               END;
                                }
      { 1102601003;2 ;Action    ;
                      ShortCutKey=Shift+F9;
                      CaptionML=[ENU=Post and &Print;
                                 ENG=Post and &Print];
                      ToolTipML=[ENU=Finalize and prepare to print the document or journal. The values and quantities are posted to the related accounts. A report request window where you can specify what to include on the print-out.;
                                 ENG=Finalise and prepare to print the document or journal. The values and quantities are posted to the related accounts. A report request window where you can specify what to include on the print-out.];
                      ApplicationArea=#Service;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ServPostPrint@1000 : Codeunit 5982;
                               BEGIN
                                 ServPostPrint.PostDocument(Rec);
                               END;
                                }
      { 1102601004;2 ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Post &Batch;
                                 ENG=Post &Batch];
                      ToolTipML=[ENU=Post several documents at once. A report request window opens where you can specify which documents to post.;
                                 ENG=Post several documents at once. A report request window opens where you can specify which documents to post.];
                      ApplicationArea=#Service;
                      Image=PostBatch;
                      OnAction=BEGIN
                                 REPORT.RUNMODAL(REPORT::"Batch Post Service Cr. Memos",TRUE,TRUE,Rec);
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the involved entry or record, according to the specified number series.;
                           ENG=Specifies the number of the involved entry or record, according to the specified number series.];
                ApplicationArea=#Service;
                SourceExpr="No." }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the service order status, which reflects the repair or maintenance status of all service items on the service order.;
                           ENG=Specifies the service order status, which reflects the repair or maintenance status of all service items on the service order.];
                ApplicationArea=#Service;
                SourceExpr=Status }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the order was created.;
                           ENG=Specifies the date when the order was created.];
                ApplicationArea=#Service;
                SourceExpr="Order Date" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the time when the service order was created.;
                           ENG=Specifies the time when the service order was created.];
                ApplicationArea=#Service;
                SourceExpr="Order Time" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the customer who owns the items in the service document.;
                           ENG=Specifies the number of the customer who owns the items in the service document.];
                ApplicationArea=#Service;
                SourceExpr="Customer No." }

    { 25  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for an alternate shipment address if you want to ship to another address than the one that has been entered automatically. This field is also used in case of drop shipment.;
                           ENG=Specifies a code for an alternate shipment address if you want to ship to another address than the one that has been entered automatically. This field is also used in case of drop shipment.];
                ApplicationArea=#Service;
                SourceExpr="Ship-to Code" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the customer to whom the items on the document will be shipped.;
                           ENG=Specifies the name of the customer to whom the items on the document will be shipped.];
                ApplicationArea=#Service;
                SourceExpr=Name }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the location (for example, warehouse or distribution center) of the items specified on the service item lines.;
                           ENG=Specifies the code of the location (for example, warehouse or distribution centre) of the items specified on the service item lines.];
                ApplicationArea=#Location;
                SourceExpr="Location Code" }

    { 23  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the priority of the service order.;
                           ENG=Specifies the priority of the service order.];
                ApplicationArea=#Service;
                SourceExpr=Priority }

    { 121 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 1, which is one of two global dimension codes that you set up in the General Ledger Setup window.;
                           ENG=Specifies the code for Shortcut Dimension 1, which is one of two global dimension codes that you set up in the General Ledger Setup window.];
                ApplicationArea=#Dimensions;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 119 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 2, which is one of two global dimension codes that you set up in the General Ledger Setup window.;
                           ENG=Specifies the code for Shortcut Dimension 2, which is one of two global dimension codes that you set up in the General Ledger Setup window.];
                ApplicationArea=#Dimensions;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 31  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the user who is responsible for the document.;
                           ENG=Specifies the ID of the user who is responsible for the document.];
                ApplicationArea=#Service;
                SourceExpr="Assigned User ID" }

    { 1102601001;2;Field  ;
                ToolTipML=[ENU=Specifies the date when the related document was created.;
                           ENG=Specifies the date when the related document was created.];
                ApplicationArea=#Service;
                SourceExpr="Document Date";
                Visible=FALSE }

    { 1102601008;2;Field  ;
                ToolTipML=[ENU=Specifies a formula that calculates the payment due date, payment discount date, and payment discount amount.;
                           ENG=Specifies a formula that calculates the payment due date, payment discount date, and payment discount amount.];
                ApplicationArea=#Service;
                SourceExpr="Payment Terms Code";
                Visible=FALSE }

    { 1102601014;2;Field  ;
                ToolTipML=[ENU=Specifies when the invoice is due.;
                           ENG=Specifies when the invoice is due.];
                ApplicationArea=#Service;
                SourceExpr="Due Date";
                Visible=FALSE }

    { 1102601016;2;Field  ;
                ToolTipML=[ENU=Specifies the type of the posted document that this document or journal line will be applied to when you post, for example to register payment.;
                           ENG=Specifies the type of the posted document that this document or journal line will be applied to when you post, for example to register payment.];
                ApplicationArea=#Service;
                SourceExpr="Applies-to Doc. Type";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1902018507;1;Part   ;
                ApplicationArea=#Service;
                SubPageLink=No.=FIELD(Bill-to Customer No.),
                            Date Filter=FIELD(Date Filter);
                PagePartID=Page9082;
                Visible=TRUE;
                PartType=Page }

    { 1900316107;1;Part   ;
                ApplicationArea=#Service;
                SubPageLink=No.=FIELD(Customer No.),
                            Date Filter=FIELD(Date Filter);
                PagePartID=Page9084;
                Visible=TRUE;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

