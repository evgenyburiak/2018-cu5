OBJECT Page 9503 Debugger Watch Value FactBox
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Debugger Watch Value FactBox;
               ENG=Debugger Watch Value FactBox];
    LinksAllowed=No;
    SourceTable=Table2000000103;
    PageType=ListPart;
    ActionList=ACTIONS
    {
      { 6       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 9       ;1   ;Separator  }
      { 7       ;1   ;Action    ;
                      Name=Delete Watch;
                      ShortCutKey=Ctrl+Delete;
                      CaptionML=[ENU=Delete Watch;
                                 ENG=Delete Watch];
                      ToolTipML=[ENU=Delete the selected variables from the watch list.;
                                 ENG=Delete the selected variables from the watch list.];
                      ApplicationArea=#All;
                      Image=Delete;
                      OnAction=VAR
                                 DebuggerWatch@1000 : Record 2000000104;
                                 DebuggerWatchValue@1002 : Record 2000000103;
                                 DebuggerWatchValueTemp@1003 : TEMPORARY Record 2000000103;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(DebuggerWatchValue);

                                 // Copy the selected records to take a snapshot of the watches,
                                 // otherwise the DELETEALL would dynamically change the Debugger Watch Value primary keys
                                 // causing incorrect records to be deleted.

                                 IF DebuggerWatchValue.FIND('-') THEN
                                   REPEAT
                                     DebuggerWatchValueTemp := DebuggerWatchValue;
                                     DebuggerWatchValueTemp.INSERT;
                                   UNTIL DebuggerWatchValue.NEXT = 0;

                                 IF DebuggerWatchValueTemp.FIND('-') THEN BEGIN
                                   REPEAT
                                     DebuggerWatch.SETRANGE(Path,DebuggerWatchValueTemp.Name);
                                     DebuggerWatch.DELETEALL(TRUE);
                                   UNTIL DebuggerWatchValueTemp.NEXT = 0;
                                 END;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Name;
                           ENG=Name];
                ToolTipML=[ENU=Specifies the name of the variable that has been added to the Debugger Watch Value FactBox.;
                           ENG=Specifies the name of the variable that has been added to the Debugger Watch Value FactBox.];
                ApplicationArea=#All;
                SourceExpr=Name }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=Value;
                           ENG=Value];
                ToolTipML=[ENU=Specifies the value of variables for which you have specified to add a watch.;
                           ENG=Specifies the value of variables for which you have specified to add a watch.];
                ApplicationArea=#All;
                SourceExpr=Value }

    { 5   ;2   ;Field     ;
                CaptionML=[ENU=Type;
                           ENG=Type];
                ToolTipML=[ENU=Specifies the type of variables for which you have specified to add a watch.;
                           ENG=Specifies the type of variables for which you have specified to add a watch.];
                ApplicationArea=#All;
                SourceExpr=Type }

  }
  CODE
  {

    BEGIN
    END.
  }
}

