OBJECT Page 9622 Table Field Types ListPart
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Table Field Types ListPart;
               ENG=Table Field Types ListPart];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table2000000172;
    DelayedInsert=No;
    PageType=ListPart;
    OnAfterGetRecord=BEGIN
                       FieldType := FORMAT(FieldTypeGroup);
                     END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies Name displayed to users.;
                           ENG=Specifies Name displayed to users.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Display Name" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies description.;
                           ENG=Specifies description.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

  }
  CODE
  {
    VAR
      FieldType@1000 : Text;

    [Internal]
    PROCEDURE GetSelectedRecord@1(VAR TableFieldTypes@1000 : Record 2000000172);
    BEGIN
      CurrPage.SETSELECTIONFILTER(TableFieldTypes);
    END;

    [External]
    PROCEDURE GetSelectedRecType@12() : Text;
    BEGIN
      EXIT(FieldType);
    END;

    BEGIN
    END.
  }
}

