OBJECT Page 9629 Available Field Selection Page
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Select Field;
               ENG=Select Field];
    SourceTable=Table2000000041;
    PageType=List;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Name;
                           ENG=Name];
                ToolTipML=[ENU=Specifies the names of the available Windows languages.;
                           ENG=Specifies the names of the available Windows languages.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Field Caption" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

