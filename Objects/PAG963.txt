OBJECT Page 963 Time Sheet Arc. Comment Sheet
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Comment Sheet;
               ENG=Comment Sheet];
    MultipleNewLines=Yes;
    LinksAllowed=No;
    SourceTable=Table957;
    DelayedInsert=Yes;
    DataCaptionFields=No.;
    PageType=List;
    AutoSplitKey=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                Editable=FALSE;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when a comment was entered for an archived time sheet.;
                           ENG=Specifies the date when a comment was entered for an archived time sheet.];
                ApplicationArea=#Advanced;
                SourceExpr=Date }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the comment relating to an archived time sheet or time sheet line.;
                           ENG=Specifies the comment relating to an archived time sheet or time sheet line.];
                ApplicationArea=#Advanced;
                SourceExpr=Comment }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for a comment for an archived time sheet.;
                           ENG=Specifies a code for a comment for an archived time sheet.];
                ApplicationArea=#Advanced;
                SourceExpr=Code;
                Visible=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

