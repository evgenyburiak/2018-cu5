OBJECT Page 9814 Device Card
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Device Card;
               ENG=Device Card];
    SourceTable=Table2000000130;
    DelayedInsert=Yes;
    PageType=Card;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           ENG=General];
                GroupType=Group }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the MAC Address for the device. MAC is an acronym for Media Access Control. A MAC Address is a unique identifier that is assigned to network interfaces for communications.;
                           ENG=Specifies the MAC Address for the device. MAC is an acronym for Media Access Control. A MAC Address is a unique identifier that is assigned to network interfaces for communications.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="MAC Address" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a name for the device.;
                           ENG=Specifies a name for the device.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the device type.;
                           ENG=Specifies the device type.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Device Type" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the device is enabled.;
                           ENG=Specifies whether the device is enabled.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Enabled }

    { 7   ;0   ;Container ;
                ContainerType=FactBoxArea }

    { 8   ;1   ;Part      ;
                PartType=System;
                SystemPartID=Notes }

    { 9   ;1   ;Part      ;
                PartType=System;
                SystemPartID=RecordLinks }

  }
  CODE
  {

    BEGIN
    END.
  }
}

