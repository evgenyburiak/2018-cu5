OBJECT Page 9829 User Groups FactBox
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=User Groups;
               ENG=User Groups];
    SourceTable=Table9000;
    PageType=ListPart;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=specifies a code for the user group.;
                           ENG=specifies a code for the user group.];
                ApplicationArea=#All;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the user group.;
                           ENG=Specifies the name of the user group.];
                ApplicationArea=#All;
                SourceExpr=Name }

    { 5   ;2   ;Field     ;
                CaptionML=[ENU=Default Profile;
                           ENG=Default Profile];
                ToolTipML=[ENU=Specifies the profile that is assigned to the user group by default.;
                           ENG=Specifies the profile that is assigned to the user group by default.];
                ApplicationArea=#All;
                SourceExpr="Default Profile ID" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

