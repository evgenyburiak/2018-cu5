OBJECT Page 99000764 Routing List
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Routing List;
               ENG=Routing List];
    SourceTable=Table99000763;
    DataCaptionFields=No.,Description;
    PageType=List;
    CardPageID=Routing;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 9       ;1   ;ActionGroup;
                      CaptionML=[ENU=&Routing;
                                 ENG=&Routing];
                      Image=Route }
      { 11      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 ENG=Co&mments];
                      ToolTipML=[ENU=View or add comments for the record.;
                                 ENG=View or add comments for the record.];
                      ApplicationArea=#Manufacturing;
                      RunObject=Page 99000784;
                      RunPageLink=Table Name=CONST(Routing Header),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 12      ;2   ;Action    ;
                      CaptionML=[ENU=&Versions;
                                 ENG=&Versions];
                      ToolTipML=[ENU="View or edit other versions of the routing, typically with other operations data. ";
                                 ENG="View or edit other versions of the routing, typically with other operations data. "];
                      ApplicationArea=#Manufacturing;
                      RunObject=Page 99000808;
                      RunPageLink=Routing No.=FIELD(No.);
                      Promoted=No;
                      Image=RoutingVersions;
                      PromotedCategory=Process }
      { 18      ;2   ;Action    ;
                      CaptionML=[ENU=Where-used;
                                 ENG=Where-used];
                      ToolTipML=[ENU=View a list of BOMs in which the item is used.;
                                 ENG=View a list of BOMs in which the item is used.];
                      ApplicationArea=#Manufacturing;
                      RunObject=Page 99000782;
                      RunPageView=SORTING(Routing No.);
                      RunPageLink=Routing No.=FIELD(No.);
                      Image=Where-Used }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1906688806;1 ;Action    ;
                      CaptionML=[ENU=Routing Sheet;
                                 ENG=Routing Sheet];
                      ToolTipML=[ENU=View basic information for routings, such as send-ahead quantity, setup time, run time and time unit. This report shows you the operations to be performed in this routing, the work or machine centers to be used, the personnel, the tools, and the description of each operation.;
                                 ENG=View basic information for routings, such as send-ahead quantity, setup time, run time and time unit. This report shows you the operations to be performed in this routing, the work or machine centres to be used, the personnel, the tools, and the description of each operation.];
                      ApplicationArea=#Manufacturing;
                      RunObject=Report 99000787;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the involved entry or record, according to the specified number series.;
                           ENG=Specifies the number of the involved entry or record, according to the specified number series.];
                ApplicationArea=#Manufacturing;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description for the routing header.;
                           ENG=Specifies a description for the routing header.];
                ApplicationArea=#Manufacturing;
                SourceExpr=Description }

    { 1102601000;2;Field  ;
                ToolTipML=[ENU=Specifies in which order operations in the routing are performed.;
                           ENG=Specifies in which order operations in the routing are performed.];
                ApplicationArea=#Manufacturing;
                SourceExpr=Type;
                Visible=FALSE }

    { 1102601002;2;Field  ;
                ToolTipML=[ENU=Specifies the status of this routing.;
                           ENG=Specifies the status of this routing.];
                ApplicationArea=#Manufacturing;
                SourceExpr=Status;
                Visible=FALSE }

    { 1102601004;2;Field  ;
                ToolTipML=[ENU=Specifies the number series you want to use to create a new version of this routing.;
                           ENG=Specifies the number series you want to use to create a new version of this routing.];
                ApplicationArea=#Manufacturing;
                SourceExpr="Version Nos.";
                Visible=FALSE }

    { 1102601006;2;Field  ;
                ToolTipML=[ENU=Specifies when the routing card was last modified.;
                           ENG=Specifies when the routing card was last modified.];
                ApplicationArea=#Manufacturing;
                SourceExpr="Last Date Modified";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

