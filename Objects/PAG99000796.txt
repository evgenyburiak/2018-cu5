OBJECT Page 99000796 Routing Comment List
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Comment List;
               ENG=Comment List];
    LinksAllowed=No;
    SourceTable=Table99000775;
    DataCaptionExpr=Caption;
    PageType=List;
    AutoSplitKey=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the comment was created.;
                           ENG=Specifies the date when the comment was created.];
                ApplicationArea=#Manufacturing;
                SourceExpr=Date }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the actual comment.;
                           ENG=Specifies the actual comment.];
                ApplicationArea=#Manufacturing;
                SourceExpr=Comment }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for the comments.;
                           ENG=Specifies a code for the comments.];
                ApplicationArea=#Manufacturing;
                SourceExpr=Code;
                Visible=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

