OBJECT Page 99000820 Prod. Order Capacity Need
{
  OBJECT-PROPERTIES
  {
    Date=21/12/17;
    Time=12:00:00;
    Version List=NAVW111.00.00.19846;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Prod. Order Capacity Need;
               ENG=Prod. Order Capacity Need];
    SourceTable=Table5410;
    DataCaptionFields=Status,Prod. Order No.;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the related production order.;
                           ENG=Specifies the number of the related production order.];
                ApplicationArea=#Manufacturing;
                SourceExpr="Prod. Order No.";
                Visible=FALSE }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of capacity need.;
                           ENG=Specifies the type of capacity need.];
                ApplicationArea=#Manufacturing;
                SourceExpr=Type }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the involved entry or record, according to the specified number series.;
                           ENG=Specifies the number of the involved entry or record, according to the specified number series.];
                ApplicationArea=#Manufacturing;
                SourceExpr="No." }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the starting time of the capacity need.;
                           ENG=Specifies the starting time of the capacity need.];
                ApplicationArea=#Manufacturing;
                SourceExpr="Starting Time" }

    { 25  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date and the starting time, which are combined in a format called "starting date-time".;
                           ENG=Specifies the date and the starting time, which are combined in a format called "starting date-time".];
                ApplicationArea=#Manufacturing;
                SourceExpr="Starting Date-Time";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ending time of the capacity need.;
                           ENG=Specifies the ending time of the capacity need.];
                ApplicationArea=#Manufacturing;
                SourceExpr="Ending Time" }

    { 27  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date and the ending time, which are combined in a format called "ending date-time".;
                           ENG=Specifies the date and the ending time, which are combined in a format called "ending date-time".];
                ApplicationArea=#Manufacturing;
                SourceExpr="Ending Date-Time";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when this capacity need occurred.;
                           ENG=Specifies the date when this capacity need occurred.];
                ApplicationArea=#Manufacturing;
                SourceExpr=Date }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the send-ahead quantity is of type Input, Output, or Both.;
                           ENG=Specifies if the send-ahead quantity is of type Input, Output, or Both.];
                ApplicationArea=#Manufacturing;
                SourceExpr="Send-Ahead Type" }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the time type of the capacity need.;
                           ENG=Specifies the time type of the capacity need.];
                ApplicationArea=#Manufacturing;
                SourceExpr="Time Type" }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the capacity need of planned operations.;
                           ENG=Specifies the capacity need of planned operations.];
                ApplicationArea=#Manufacturing;
                SourceExpr="Allocated Time" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

