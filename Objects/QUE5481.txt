OBJECT Query 5481 Vendor Purchases Entity
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[@@@={Locked};
               ENU=vendorPurchases;
               ENG=vendorPurchases];
    EntitySetName=vendorPurchases;
    EntityName=vendorPurchase;
    QueryType=API;
  }
  ELEMENTS
  {
    { 1   ;    ;DataItem;                    ;
               DataItemTable=Table23 }

    { 6   ;1   ;Column  ;vendorId            ;
               CaptionML=[@@@={Locked};
                          ENU=Id;
                          ENG=Id];
               DataSource=Id }

    { 2   ;1   ;Column  ;vendorNumber        ;
               CaptionML=[@@@={Locked};
                          ENU=No;
                          ENG=No];
               DataSource=No. }

    { 8   ;1   ;Column  ;name                ;
               CaptionML=[@@@={Locked};
                          ENU=Name;
                          ENG=Name];
               DataSource=Name }

    { 3   ;1   ;DataItem;                    ;
               DataItemTable=Table25;
               DataItemLink=Vendor No.=Vendor."No.";
               DataItemLinkType=SQL Advanced Options }

    { 4   ;2   ;Column  ;totalPurchaseAmount ;
               CaptionML=[@@@={Locked};
                          ENU=TotalPurchaseAmount;
                          ENG=TotalPurchaseAmount];
               DataSource=Purchase (LCY);
               ReverseSign=Yes;
               MethodType=Totals;
               Method=Sum }

    { 5   ;2   ;Filter  ;dateFilter          ;
               CaptionML=[@@@={Locked};
                          ENU=DateFilter;
                          ENG=DateFilter];
               DataSource=Posting Date }

  }
  CODE
  {

    BEGIN
    END.
  }
}

