OBJECT Report 497 Batch Post Purchase Invoices
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Batch Post Purchase Invoices;
               ENG=Batch Post Purchase Invoices];
    ProcessingOnly=Yes;
  }
  DATASET
  {
    { 4458;    ;DataItem;                    ;
               DataItemTable=Table38;
               DataItemTableView=SORTING(Document Type,No.)
                                 WHERE(Document Type=CONST(Invoice));
               ReqFilterHeadingML=[ENU=Purchase Invoice;
                                   ENG=Purchase Invoice];
               OnPreDataItem=VAR
                               PurchaseBatchPostMgt@1001 : Codeunit 1372;
                             BEGIN
                               PurchaseBatchPostMgt.RunBatch("Purchase Header",ReplacePostingDate,PostingDateReq,ReplaceDocumentDate,CalcInvDisc,FALSE,TRUE);

                               CurrReport.BREAK;
                             END;

               ReqFilterFields=No.,Status }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
      SaveValues=Yes;
      OnOpenPage=VAR
                   PurchasesPayablesSetup@1000 : Record 312;
                 BEGIN
                   PurchasesPayablesSetup.GET;
                   CalcInvDisc := PurchasesPayablesSetup."Calc. Inv. Discount";
                 END;

    }
    CONTROLS
    {
      { 1900000001;0;Container;
                  ContainerType=ContentArea }

      { 1900000002;1;Group  ;
                  CaptionML=[ENU=Options;
                             ENG=Options] }

      { 1   ;2   ;Field     ;
                  Name=PostingDate;
                  CaptionML=[ENU=Posting Date;
                             ENG=Posting Date];
                  ToolTipML=[ENU=Specifies the date that the program will use as the document and/or posting date when you post if you place a check mark in one or both of the following boxes.;
                             ENG=Specifies the date that the program will use as the document and/or posting date when you post if you place a check mark in one or both of the following boxes.];
                  ApplicationArea=#Advanced;
                  SourceExpr=PostingDateReq }

      { 2   ;2   ;Field     ;
                  Name=ReplacePostingDate;
                  CaptionML=[ENU=Replace Posting Date;
                             ENG=Replace Posting Date];
                  ToolTipML=[ENU=Specifies if you want to replace the purchase invoices' posting date with the date entered in the field above.;
                             ENG=Specifies if you want to replace the purchase invoices' posting date with the date entered in the field above.];
                  ApplicationArea=#Advanced;
                  SourceExpr=ReplacePostingDate;
                  OnValidate=BEGIN
                               IF ReplacePostingDate THEN
                                 MESSAGE(Text003);
                             END;
                              }

      { 3   ;2   ;Field     ;
                  Name=ReplaceDocumentDate;
                  CaptionML=[ENU=Replace Document Date;
                             ENG=Replace Document Date];
                  ToolTipML=[ENU=Specifies if the new document date will be applied.;
                             ENG=Specifies if the new document date will be applied.];
                  ApplicationArea=#Advanced;
                  SourceExpr=ReplaceDocumentDate }

      { 5   ;2   ;Field     ;
                  CaptionML=[ENU=Calc. Inv. Discount;
                             ENG=Calc. Inv. Discount];
                  ToolTipML=[ENU=Specifies if you want the invoice discount amount to be automatically calculated on the invoices before posting.;
                             ENG=Specifies if you want the invoice discount amount to be automatically calculated on the invoices before posting.];
                  ApplicationArea=#Advanced;
                  SourceExpr=CalcInvDisc;
                  OnValidate=VAR
                               PurchasesPayablesSetup@1000 : Record 312;
                             BEGIN
                               PurchasesPayablesSetup.GET;
                               PurchasesPayablesSetup.TESTFIELD("Calc. Inv. Discount",FALSE);
                             END;
                              }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      Text003@1003 : TextConst 'ENU=The exchange rate associated with the new posting date on the purchase header will not apply to the purchase lines.;ENG=The exchange rate associated with the new posting date on the purchase header will not apply to the purchase lines.';
      PostingDateReq@1008 : Date;
      ReplacePostingDate@1012 : Boolean;
      ReplaceDocumentDate@1013 : Boolean;
      CalcInvDisc@1014 : Boolean;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

