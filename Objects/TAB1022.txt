OBJECT Table 1022 Job Planning Line Invoice
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Job Planning Line Invoice;
               ENG=Job Planning Line Invoice];
    LookupPageID=Page1029;
    DrillDownPageID=Page1029;
  }
  FIELDS
  {
    { 1   ;   ;Job No.             ;Code20        ;TableRelation=Job;
                                                   CaptionML=[ENU=Job No.;
                                                              ENG=Job No.];
                                                   Editable=No }
    { 2   ;   ;Job Task No.        ;Code20        ;TableRelation="Job Task"."Job Task No." WHERE (Job No.=FIELD(Job No.));
                                                   CaptionML=[ENU=Job Task No.;
                                                              ENG=Job Task No.];
                                                   Editable=No }
    { 3   ;   ;Job Planning Line No.;Integer      ;TableRelation="Job Planning Line"."Line No." WHERE (Job No.=FIELD(Job No.),
                                                                                                       Job Task No.=FIELD(Job Task No.));
                                                   CaptionML=[ENU=Job Planning Line No.;
                                                              ENG=Job Planning Line No.];
                                                   Editable=No }
    { 4   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              ENG=Document Type];
                                                   OptionCaptionML=[ENU=" ,Invoice,Credit Memo,Posted Invoice,Posted Credit Memo";
                                                                    ENG=" ,Invoice,Credit Memo,Posted Invoice,Posted Credit Memo"];
                                                   OptionString=[ ,Invoice,Credit Memo,Posted Invoice,Posted Credit Memo];
                                                   Editable=No }
    { 5   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              ENG=Document No.];
                                                   Editable=No }
    { 6   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              ENG=Line No.] }
    { 7   ;   ;Quantity Transferred;Decimal       ;CaptionML=[ENU=Quantity Transferred;
                                                              ENG=Quantity Transferred];
                                                   Editable=No }
    { 8   ;   ;Transferred Date    ;Date          ;CaptionML=[ENU=Transferred Date;
                                                              ENG=Transferred Date];
                                                   Editable=No }
    { 9   ;   ;Invoiced Date       ;Date          ;CaptionML=[ENU=Invoiced Date;
                                                              ENG=Invoiced Date];
                                                   Editable=No }
    { 10  ;   ;Invoiced Amount (LCY);Decimal      ;CaptionML=[ENU=Invoiced Amount (LCY);
                                                              ENG=Invoiced Amount (LCY)];
                                                   Editable=No }
    { 11  ;   ;Invoiced Cost Amount (LCY);Decimal ;CaptionML=[ENU=Invoiced Cost Amount (LCY);
                                                              ENG=Invoiced Cost Amount (LCY)];
                                                   Editable=No }
    { 12  ;   ;Job Ledger Entry No.;Integer       ;TableRelation="Job Ledger Entry";
                                                   CaptionML=[ENU=Job Ledger Entry No.;
                                                              ENG=Job Ledger Entry No.];
                                                   BlankZero=Yes;
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Job No.,Job Task No.,Job Planning Line No.,Document Type,Document No.,Line No.;
                                                   SumIndexFields=Quantity Transferred,Invoiced Amount (LCY),Invoiced Cost Amount (LCY);
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE InitFromJobPlanningLine@1(JobPlanningLine@1000 : Record 1003);
    BEGIN
      "Job No." := JobPlanningLine."Job No.";
      "Job Task No." := JobPlanningLine."Job Task No.";
      "Job Planning Line No." := JobPlanningLine."Line No.";
      "Quantity Transferred" := JobPlanningLine."Qty. to Transfer to Invoice";
    END;

    PROCEDURE InitFromSales@3(SalesHeader@1000 : Record 36;PostingDate@1001 : Date;LineNo@1002 : Integer);
    BEGIN
      IF SalesHeader."Document Type" = SalesHeader."Document Type"::Invoice THEN
        "Document Type" := "Document Type"::Invoice;
      IF SalesHeader."Document Type" = SalesHeader."Document Type"::"Credit Memo" THEN
        "Document Type" := "Document Type"::"Credit Memo";
      "Document No." := SalesHeader."No.";
      "Line No." := LineNo;
      "Transferred Date" := PostingDate
    END;

    BEGIN
    END.
  }
}

