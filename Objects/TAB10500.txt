OBJECT Table 10500 Type of Supply
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVGB11.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Type of Supply;
               ENG=Type of Supply];
    LookupPageID=Page10500;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;ObsoleteState=Pending;
                                                   ObsoleteReason=Removed based on feedback.;
                                                   CaptionML=[ENU=Code;
                                                              ENG=Code];
                                                   NotBlank=Yes }
    { 10  ;   ;Description         ;Text30        ;ObsoleteState=Pending;
                                                   ObsoleteReason=Removed based on feedback.;
                                                   CaptionML=[ENU=Description;
                                                              ENG=Description] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Code,Description                         }
  }
  CODE
  {

    BEGIN
    END.
  }
}

