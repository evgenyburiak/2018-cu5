OBJECT Table 1222 Data Exch. Def
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               IF Name = '' THEN
                 Name := Code;

               CheckPositivePayExportFileType;
             END;

    OnModify=BEGIN
               CheckPositivePayExportFileType;
             END;

    OnDelete=VAR
               DataExchLineDef@1000 : Record 1227;
             BEGIN
               DataExchLineDef.SETRANGE("Data Exch. Def Code",Code);
               DataExchLineDef.DELETEALL(TRUE);
             END;

    CaptionML=[ENU=Data Exch. Def;
               ENG=Data Exch. Def];
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              ENG=Code];
                                                   NotBlank=Yes }
    { 2   ;   ;Name                ;Text100       ;CaptionML=[ENU=Name;
                                                              ENG=Name] }
    { 3   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              ENG=Type];
                                                   OptionCaptionML=[ENU=Bank Statement Import,Payment Export,Payroll Import,Generic Import,Positive Pay Export,Generic Export;
                                                                    ENG=Bank Statement Import,Payment Export,Payroll Import,Generic Import,Positive Pay Export,Generic Export];
                                                   OptionString=Bank Statement Import,Payment Export,Payroll Import,Generic Import,Positive Pay Export,Generic Export }
    { 4   ;   ;Reading/Writing XMLport;Integer    ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(XMLport));
                                                   CaptionML=[ENU=Reading/Writing XMLport;
                                                              ENG=Reading/Writing XMLport] }
    { 5   ;   ;Header Lines        ;Integer       ;CaptionML=[ENU=Header Lines;
                                                              ENG=Header Lines] }
    { 8   ;   ;Header Tag          ;Text250       ;CaptionML=[ENU=Header Tag;
                                                              ENG=Header Tag] }
    { 9   ;   ;Footer Tag          ;Text250       ;CaptionML=[ENU=Footer Tag;
                                                              ENG=Footer Tag] }
    { 10  ;   ;Column Separator    ;Option        ;InitValue=Comma;
                                                   CaptionML=[ENU=Column Separator;
                                                              ENG=Column Separator];
                                                   OptionCaptionML=[ENU=,Tab,Semicolon,Comma,Space,Custom;
                                                                    ENG=,Tab,Semicolon,Comma,Space,Custom];
                                                   OptionString=,Tab,Semicolon,Comma,Space,Custom }
    { 11  ;   ;File Encoding       ;Option        ;InitValue=WINDOWS;
                                                   CaptionML=[ENU=File Encoding;
                                                              ENG=File Encoding];
                                                   OptionCaptionML=[ENU=MS-DOS,UTF-8,UTF-16,WINDOWS;
                                                                    ENG=MS-DOS,UTF-8,UTF-16,WINDOWS];
                                                   OptionString=MS-DOS,UTF-8,UTF-16,WINDOWS }
    { 13  ;   ;File Type           ;Option        ;CaptionML=[ENU=File Type;
                                                              ENG=File Type];
                                                   OptionCaptionML=[ENU=Xml,Variable Text,Fixed Text,Json;
                                                                    ENG=Xml,Variable Text,Fixed Text,Json];
                                                   OptionString=Xml,Variable Text,Fixed Text,Json }
    { 14  ;   ;Ext. Data Handling Codeunit;Integer;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Codeunit));
                                                   CaptionML=[ENU=Ext. Data Handling Codeunit;
                                                              ENG=Ext. Data Handling Codeunit] }
    { 15  ;   ;Reading/Writing Codeunit;Integer   ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Codeunit));
                                                   CaptionML=[ENU=Reading/Writing Codeunit;
                                                              ENG=Reading/Writing Codeunit] }
    { 16  ;   ;Validation Codeunit ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Codeunit));
                                                   CaptionML=[ENU=Validation Codeunit;
                                                              ENG=Validation Codeunit] }
    { 17  ;   ;Data Handling Codeunit;Integer     ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Codeunit));
                                                   CaptionML=[ENU=Data Handling Codeunit;
                                                              ENG=Data Handling Codeunit] }
    { 18  ;   ;User Feedback Codeunit;Integer     ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Codeunit));
                                                   CaptionML=[ENU=User Feedback Codeunit;
                                                              ENG=User Feedback Codeunit] }
    { 19  ;   ;Custom Column Separator;Text1      ;CaptionML=[ENU=Custom Column Separator;
                                                              ENG=Custom Column Separator] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
    {    ;Type                                     }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      ColumnSeparatorMissingErr@1001 : TextConst 'ENU=Column separator is missing in the definition.;ENG=Column separator is missing in the definition.';
      PositivePayFileTypeErr@1000 : TextConst 'ENU=The positive pay file that you are exporting must be of type Fixed Text or Variable Text.;ENG=The positive pay file that you are exporting must be of type Fixed Text or Variable Text.';

    [External]
    PROCEDURE InsertRec@1(NewCode@1000 : Code[20];NewName@1001 : Text[100];NewType@1002 : Option;ProcessingXMLport@1003 : Integer;HeaderCount@1004 : Integer;HeaderTag@1005 : Text[250];FooterTag@1006 : Text[250]);
    BEGIN
      INIT;
      VALIDATE(Code,NewCode);
      VALIDATE(Name,NewName);
      VALIDATE(Type,NewType);
      VALIDATE("Reading/Writing XMLport",ProcessingXMLport);
      VALIDATE("Header Lines",HeaderCount);
      VALIDATE("Header Tag",HeaderTag);
      VALIDATE("Footer Tag",FooterTag);
      INSERT;
    END;

    [External]
    PROCEDURE InsertRecForExport@4(NewCode@1000 : Code[20];NewName@1001 : Text[100];NewType@1002 : Option;ProcessingXMLport@1003 : Integer;FileType@1004 : Option);
    BEGIN
      INIT;
      VALIDATE(Code,NewCode);
      VALIDATE(Name,NewName);
      "File Type" := FileType;
      Type := NewType;
      VALIDATE("File Type",FileType);
      VALIDATE(Type,NewType);

      VALIDATE("Reading/Writing XMLport",ProcessingXMLport);
      INSERT;
    END;

    [External]
    PROCEDURE ColumnSeparatorChar@3() : Text;
    VAR
      SeparatorChar@1000 : Text;
    BEGIN
      CASE "Column Separator" OF
        "Column Separator"::Tab:
          BEGIN
            SeparatorChar[1] := 9;
            EXIT(SeparatorChar);
          END;
        "Column Separator"::Semicolon:
          EXIT(';');
        "Column Separator"::Comma:
          EXIT(',');
        "Column Separator"::Space:
          EXIT(' ');
        "Column Separator"::Custom:
          BEGIN
            IF "Custom Column Separator" <> '' THEN
              EXIT("Custom Column Separator");
            ERROR(ColumnSeparatorMissingErr);
          END;
        ELSE
          ERROR(ColumnSeparatorMissingErr)
      END
    END;

    [External]
    PROCEDURE CheckEnableDisableIsNonXMLFileType@9() : Boolean;
    BEGIN
      EXIT(NOT ("File Type" IN ["File Type"::Xml,"File Type"::Json]))
    END;

    [External]
    PROCEDURE CheckEnableDisableIsImportType@18() : Boolean;
    BEGIN
      IF Type IN [Type::"Payment Export",Type::"Positive Pay Export"] THEN
        EXIT(FALSE);
      EXIT(NOT ("File Type" IN ["File Type"::Xml,"File Type"::Json]))
    END;

    [External]
    PROCEDURE CheckEnableDisableIsBankStatementImportType@15() : Boolean;
    BEGIN
      EXIT(Type = Type::"Bank Statement Import");
    END;

    [External]
    PROCEDURE CheckEnableDisableDelimitedFileType@8() : Boolean;
    BEGIN
      EXIT("File Type" = "File Type"::"Variable Text");
    END;

    [External]
    PROCEDURE PositivePayUpdateCodeunits@2() : Boolean;
    BEGIN
      IF Type = Type::"Positive Pay Export" THEN BEGIN
        "Validation Codeunit" := CODEUNIT::"Exp. Validation Pos. Pay";
        "Reading/Writing Codeunit" := CODEUNIT::"Exp. Writing Pos. Pay";
        "Reading/Writing XMLport" := XMLPORT::"Export Generic Fixed Width";
        "Ext. Data Handling Codeunit" := CODEUNIT::"Exp. External Data Pos. Pay";
        "User Feedback Codeunit" := CODEUNIT::"Exp. User Feedback Pos. Pay";
      END ELSE BEGIN
        "Validation Codeunit" := 0;
        "Reading/Writing Codeunit" := 0;
        "Reading/Writing XMLport" := 0;
        "Ext. Data Handling Codeunit" := 0;
        "User Feedback Codeunit" := 0;
      END;
      EXIT(Type = Type::"Positive Pay Export");
    END;

    [External]
    PROCEDURE ProcessDataExchange@6(VAR DataExch@1000 : Record 1220);
    VAR
      DataExchLineDef@1003 : Record 1227;
      DataExchMapping@1002 : Record 1224;
    BEGIN
      IF "Data Handling Codeunit" <> 0 THEN
        CODEUNIT.RUN("Data Handling Codeunit",DataExch);

      DataExchLineDef.SETRANGE("Data Exch. Def Code",Code);
      DataExchLineDef.SETRANGE("Parent Code",'');
      DataExchLineDef.FINDSET;

      REPEAT
        DataExchMapping.SETRANGE("Data Exch. Def Code",DataExch."Data Exch. Def Code");
        DataExchMapping.SETRANGE("Data Exch. Line Def Code",DataExchLineDef.Code);
        DataExchMapping.FINDSET;

        REPEAT
          IF DataExchMapping."Pre-Mapping Codeunit" <> 0 THEN
            CODEUNIT.RUN(DataExchMapping."Pre-Mapping Codeunit",DataExch);

          IF DataExchMapping."Mapping Codeunit" <> 0 THEN
            CODEUNIT.RUN(DataExchMapping."Mapping Codeunit",DataExch);

          IF DataExchMapping."Post-Mapping Codeunit" <> 0 THEN
            CODEUNIT.RUN(DataExchMapping."Post-Mapping Codeunit",DataExch);
        UNTIL DataExchMapping.NEXT = 0;
      UNTIL DataExchLineDef.NEXT = 0;
    END;

    LOCAL PROCEDURE IsFixedOrVariableText@20() : Boolean;
    BEGIN
      EXIT("File Type" IN ["File Type"::"Fixed Text","File Type"::"Variable Text"]);
    END;

    LOCAL PROCEDURE CheckPositivePayExportFileType@22();
    BEGIN
      IF Type <> Type::"Positive Pay Export" THEN
        EXIT;

      IF NOT IsFixedOrVariableText THEN
        ERROR(PositivePayFileTypeErr);
    END;

    BEGIN
    END.
  }
}

