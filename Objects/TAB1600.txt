OBJECT Table 1600 Office Add-in Context
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Office Add-in Context;
               ENG=Office Add-in Context];
  }
  FIELDS
  {
    { 1   ;   ;Email               ;Text80        ;CaptionML=[ENU=Email;
                                                              ENG=Email];
                                                   Description=Email address of the Outlook contact. }
    { 2   ;   ;Name                ;Text250       ;CaptionML=[ENU=Name;
                                                              ENG=Name];
                                                   Description=Display name of the Outlook contact. }
    { 3   ;   ;Document Type       ;Text20        ;CaptionML=[ENU=Document Type;
                                                              ENG=Document Type];
                                                   Description=Type of the referenced document. }
    { 4   ;   ;Document No.        ;Code250       ;CaptionML=[ENU=Document No.;
                                                              ENG=Document No.];
                                                   Description=No. of the referenced document. }
    { 5   ;   ;Regular Expression Match;Text250   ;CaptionML=[ENU=Regular Expression Match;
                                                              ENG=Regular Expression Match];
                                                   Description=Raw regular expression match. }
    { 6   ;   ;Duration            ;Text20        ;CaptionML=[ENU=Duration;
                                                              ENG=Duration];
                                                   Description=Duration of the meeting. }
    { 8   ;   ;Command             ;Text30        ;CaptionML=[ENU=Command;
                                                              ENG=Command];
                                                   Description=Outlook add-in command. }
    { 12  ;   ;Item ID             ;Text250       ;CaptionML=[ENU=Item ID;
                                                              ENG=Item ID];
                                                   Description=Exchange item ID. }
    { 13  ;   ;Version             ;Text20        ;CaptionML=[ENU=Version;
                                                              ENG=Version];
                                                   Description=Add-in version }
    { 14  ;   ;Contact No.         ;Code20        ;CaptionML=[ENU=Contact No.;
                                                              ENG=Contact No.] }
    { 15  ;   ;Subject             ;Text250       ;CaptionML=[ENU=Subject;
                                                              ENG=Subject];
                                                   Description=Subject of the appointment or message. }
    { 16  ;   ;Item Type           ;Option        ;CaptionML=[ENU=Item Type;
                                                              ENG=Item Type];
                                                   OptionCaptionML=[ENU=Message,Appointment;
                                                                    ENG=Message,Appointment];
                                                   OptionString=Message,Appointment }
  }
  KEYS
  {
    {    ;Email                                   ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    [External]
    PROCEDURE CommandType@1() Type : Integer;
    VAR
      DummyOfficeContactAssociations@1000 : Record 1625;
      OutlookCommand@1001 : DotNet "'Microsoft.Dynamics.Nav.ClientExtensions, Version=11.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35'.Microsoft.Dynamics.Nav.Client.Hosts.OutlookCommand";
    BEGIN
      CASE Command OF
        OutlookCommand.NewPurchaseCreditMemo,OutlookCommand.NewPurchaseInvoice,OutlookCommand.NewPurchaseOrder:
          Type := DummyOfficeContactAssociations."Associated Table"::Vendor;
        OutlookCommand.NewSalesCreditMemo,OutlookCommand.NewSalesInvoice,
        OutlookCommand.NewSalesOrder,OutlookCommand.NewSalesQuote:
          Type := DummyOfficeContactAssociations."Associated Table"::Customer;
        ELSE
          Type := DummyOfficeContactAssociations."Associated Table"::" ";
      END;
    END;

    [External]
    PROCEDURE IsAppointment@2() : Boolean;
    BEGIN
      EXIT("Item Type" = "Item Type"::Appointment);
    END;

    BEGIN
    END.
  }
}

