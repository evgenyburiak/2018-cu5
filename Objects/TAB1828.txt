OBJECT Table 1828 Business Unit Information
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Business Unit Information;
               ENG=Business Unit Information];
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              ENG=Code] }
    { 2   ;   ;Name                ;Text30        ;CaptionML=[ENU=Name;
                                                              ENG=Name] }
    { 3   ;   ;Company Name        ;Text30        ;CaptionML=[ENU=Company Name;
                                                              ENG=Company Name] }
    { 4   ;   ;Currency Code       ;Code10        ;CaptionML=[ENU=Currency Code;
                                                              ENG=Currency Code] }
    { 5   ;   ;Currency Exchange Rate Table;Option;CaptionML=[ENU=Currency Exchange Rate Table;
                                                              ENG=Currency Exchange Rate Table];
                                                   OptionCaptionML=[ENU=Local,Business Unit;
                                                                    ENG=Local,Business Unit];
                                                   OptionString=Local,Business Unit }
    { 6   ;   ;Starting Date       ;Date          ;CaptionML=[ENU=Starting Date;
                                                              ENG=Starting Date] }
    { 7   ;   ;Ending Date         ;Date          ;CaptionML=[ENU=Ending Date;
                                                              ENG=Ending Date] }
    { 8   ;   ;Exch. Rate Gains Acc.;Code20       ;CaptionML=[ENU=Exch. Rate Gains Acc.;
                                                              ENG=Exch. Rate Gains Acc.] }
    { 9   ;   ;Exch. Rate Losses Acc.;Code20      ;CaptionML=[ENU=Exch. Rate Losses Acc.;
                                                              ENG=Exch. Rate Losses Acc.] }
    { 10  ;   ;Residual Account    ;Code20        ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Residual Account");
                                                              END;

                                                   CaptionML=[ENU=Residual Account;
                                                              ENG=Residual Account] }
    { 11  ;   ;Comp. Exch. Rate Gains Acc.;Code20 ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Comp. Exch. Rate Gains Acc.");
                                                              END;

                                                   CaptionML=[ENU=Comp. Exch. Rate Gains Acc.;
                                                              ENG=Comp. Exch. Rate Gains Acc.] }
    { 12  ;   ;Comp. Exch. Rate Losses Acc.;Code20;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Comp. Exch. Rate Losses Acc.");
                                                              END;

                                                   CaptionML=[ENU=Comp. Exch. Rate Losses Acc.;
                                                              ENG=Comp. Exch. Rate Losses Acc.] }
    { 13  ;   ;Equity Exch. Rate Gains Acc.;Code20;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Equity Exch. Rate Gains Acc.");
                                                              END;

                                                   CaptionML=[ENU=Equity Exch. Rate Gains Acc.;
                                                              ENG=Equity Exch. Rate Gains Acc.] }
    { 14  ;   ;Equity Exch. Rate Losses Acc.;Code20;
                                                   TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Equity Exch. Rate Losses Acc.");
                                                              END;

                                                   CaptionML=[ENU=Equity Exch. Rate Losses Acc.;
                                                              ENG=Equity Exch. Rate Losses Acc.] }
    { 15  ;   ;Minority Exch. Rate Gains Acc.;Code20;
                                                   TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Minority Exch. Rate Gains Acc.");
                                                              END;

                                                   CaptionML=[ENU=Minority Exch. Rate Gains Acc.;
                                                              ENG=Minority Exch. Rate Gains Acc.] }
    { 16  ;   ;Minority Exch. Rate Losses Acc;Code20;
                                                   TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Minority Exch. Rate Losses Acc");
                                                              END;

                                                   CaptionML=[ENU=Minority Exch. Rate Losses Acc;
                                                              ENG=Minority Exch. Rate Losses Acc] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    LOCAL PROCEDURE CheckGLAcc@2(AccNo@1000 : Code[20]);
    VAR
      GLAcc@1001 : Record 15;
    BEGIN
      IF AccNo <> '' THEN BEGIN
        GLAcc.GET(AccNo);
        GLAcc.CheckGLAcc;
      END;
    END;

    BEGIN
    END.
  }
}

