OBJECT Table 1829 Consolidation Account
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00,NAVGB11.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Consolidation Account;
               ENG=Consolidation Account];
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              ENG=No.];
                                                   NotBlank=Yes }
    { 2   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              ENG=Name] }
    { 3   ;   ;Income/Balance      ;Option        ;CaptionML=[ENU=Income/Balance;
                                                              ENG=Income/Balance];
                                                   OptionCaptionML=[ENU=Income Statement,Balance Sheet;
                                                                    ENG=Income Statement,Balance Sheet];
                                                   OptionString=Income Statement,Balance Sheet }
    { 4   ;   ;Blocked             ;Boolean       ;CaptionML=[ENU=Blocked;
                                                              ENG=Blocked] }
    { 5   ;   ;Direct Posting      ;Boolean       ;InitValue=Yes;
                                                   CaptionML=[ENU=Direct Posting;
                                                              ENG=Direct Posting] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      XPurchaseVATPERCENTEUTok@1040005 : TextConst '@@@="%1=Goods or Services text.";ENU=Purchase VAT %1 EU;ENG=Purchase VAT %1 EU';
      XPurchaseVATPERCENTTok@1040004 : TextConst '@@@="%1=Goods or Services text.";ENU=Purchase VAT %1;ENG=Purchase VAT %1';
      XPurchaseFullVATTok@1040003 : TextConst '@@@="%1=Goods or Services text.";ENU=Purchase Full VAT %1;ENG=Purchase Full VAT %1';
      XEmployeesPayableTok@1040002 : TextConst 'ENU=Employees Payable;ENG=Employees Payable';
      XSalesVATPERCENTTok@1040001 : TextConst '@@@="%1=Goods or Services text.";ENU=Sales VAT %1;ENG=Sales VAT %1';
      XSalesFullVATTok@1040000 : TextConst '@@@="%1=Goods or Services text.";ENU=Sales Full VAT %1;ENG=Sales Full VAT %1';

    PROCEDURE PopulateAccountsForGB@1040000();
    VAR
      ServicesVATText@1040001 : Text[30];
      GoodsVATText@1040000 : Text[30];
    BEGIN
      GoodsVATText := 'Goods';
      ServicesVATText := 'Services';

      InsertData('10100','Income, Services',0,TRUE);
      InsertData('10200','Income, Product Sales',0,FALSE);
      InsertData('10300','Sales Discounts',0,FALSE);
      InsertData('10400','Sales Returns & Allowances',0,FALSE);
      InsertData('10500','Interest Income',0,TRUE);
      InsertData('20100','Cost of Materials',0,FALSE);
      InsertData('20200','Cost of Labour',0,FALSE);
      InsertData('30100','Rent Expense',0,TRUE);
      InsertData('30200','Advertising Expense',0,TRUE);
      InsertData('30300','Interest Expense',0,TRUE);
      InsertData('30400','Bank Charges and Fees',0,TRUE);
      InsertData('30500','Processing Fees',0,TRUE);
      InsertData('30600','Bad Debt Expense',0,TRUE);
      InsertData('30700','Salaries Expense',0,TRUE);
      InsertData('30800','Payroll Tax Expense',0,TRUE);
      InsertData('30900','Workers Compensation ',0,TRUE);
      InsertData('31000','Health & Dental Insurance Expense',0,TRUE);
      InsertData('31100','Life Insurance Expense',0,TRUE);
      InsertData('31200','Repairs and Maintenance Expense',0,TRUE);
      InsertData('31300','Utilities Expense',0,TRUE);
      InsertData('31400','Office Supplies Expense',0,TRUE);
      InsertData('31500','Miscellaneous Expense',0,TRUE);
      InsertData('31600','Depreciation, Equipment',0,FALSE);
      InsertData('31900','Rounding',0,TRUE);

      InsertData('40100','Checking account',1,TRUE);
      InsertData('40200','Savings account',1,TRUE);
      InsertData('40300','Petty Cash',1,TRUE);
      InsertData('40400','Accounts Receivable',1,TRUE);
      InsertData('40500','Prepaid Rent',1,TRUE);
      InsertData('40600','Prepaid Insurance',1,TRUE);
      InsertData('40700','Inventory',1,TRUE);
      InsertData('40800','Equipment',1,TRUE);
      InsertData('40900','Accumulated Depreciation',1,TRUE);
      InsertData('41000','Vendor Prepayments',1,TRUE);
      InsertData('46200',STRSUBSTNO(XPurchaseVATPERCENTEUTok,GoodsVATText),1,FALSE);
      InsertData('46210',STRSUBSTNO(XPurchaseVATPERCENTEUTok,ServicesVATText),1,FALSE);
      InsertData('46300',STRSUBSTNO(XPurchaseVATPERCENTTok,GoodsVATText),1,FALSE);
      InsertData('46310',STRSUBSTNO(XPurchaseVATPERCENTTok,ServicesVATText),1,FALSE);
      InsertData('46320',STRSUBSTNO(XPurchaseFullVATTok,ServicesVATText),1,TRUE);
      InsertData('46330',STRSUBSTNO(XPurchaseFullVATTok,GoodsVATText),1,TRUE);
      InsertData('50100','Accounts Payable',1,TRUE);
      InsertData('50200','Purchase Discounts',1,FALSE);
      InsertData('50300','Purchase Returns & Allowances',1,FALSE);
      InsertData('50400','Deferred Revenue',1,FALSE);
      InsertData('50500','Credit Cards',1,FALSE);
      InsertData('50700','Accrued Salaries & Wages',1,TRUE);
      InsertData('51400','Employee Benefits Payable',1,TRUE);
      InsertData('51500','Holiday Compensation Payable',1,TRUE);
      InsertData('51600',XEmployeesPayableTok,1,TRUE);
      InsertData('51900','Notes Payable',1,TRUE);
      InsertData('52000','Customer Prepayments',1,TRUE);
      InsertData('56100',STRSUBSTNO(XSalesVATPERCENTTok,GoodsVATText),1,FALSE);
      InsertData('56110',STRSUBSTNO(XSalesVATPERCENTTok,ServicesVATText),1,FALSE);
      InsertData('56120',STRSUBSTNO(XSalesFullVATTok,ServicesVATText),1,TRUE);
      InsertData('56130',STRSUBSTNO(XSalesFullVATTok,GoodsVATText),1,TRUE);
      InsertData('60100','Share Capital',1,TRUE);
      InsertData('60200','Retained Earnings',1,TRUE);
      InsertData('60300','Dividends',1,TRUE);
    END;

    PROCEDURE PopulateAccounts@1();
    BEGIN
      InsertData('10100','Checking account',1,TRUE);
    END;

    LOCAL PROCEDURE InsertData@2(AccountNo@1000 : Code[20];AccountName@1001 : Text[50];IncomeBalance@1003 : Option;DirectPosting@1009 : Boolean);
    VAR
      ConsolidationAccount@1010 : Record 1829;
    BEGIN
      ConsolidationAccount.INIT;
      ConsolidationAccount.VALIDATE("No.",AccountNo);
      ConsolidationAccount.VALIDATE(Name,AccountName);
      ConsolidationAccount.VALIDATE("Direct Posting",DirectPosting);
      ConsolidationAccount.VALIDATE("Income/Balance",IncomeBalance);
      ConsolidationAccount.INSERT;
    END;

    PROCEDURE PopulateConsolidationAccountsForExistingCompany@4(ConsolidatedCompany@1000 : Text[50]);
    VAR
      GLAccount@1001 : Record 15;
    BEGIN
      GLAccount.CHANGECOMPANY(ConsolidatedCompany);
      GLAccount.RESET;
      GLAccount.SETFILTER("Account Type",FORMAT(GLAccount."Account Type"::Posting));
      IF GLAccount.FIND('-') THEN
        REPEAT
          InsertData(GLAccount."No.",GLAccount.Name,GLAccount."Income/Balance",GLAccount."Direct Posting");
        UNTIL GLAccount.NEXT = 0;
    END;

    PROCEDURE ValidateCountry@3(CountryCode@1000 : Code[2]) : Boolean;
    VAR
      ApplicationManagement@1001 : Codeunit 1;
    BEGIN
      IF STRPOS(ApplicationManagement.ApplicationVersion,CountryCode) = 1 THEN
        EXIT(TRUE);

      EXIT(FALSE);
    END;

    BEGIN
    END.
  }
}

