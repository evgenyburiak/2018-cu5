OBJECT Table 2000000162 NAV App Capabilities
{
  OBJECT-PROPERTIES
  {
    Date=27/04/18;
    Time=12:00:00;
    Version List=;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=NAV App Capabilities;
               ENG=NAV App Capabilities];
  }
  FIELDS
  {
    { 1   ;   ;Package ID          ;GUID          ;CaptionML=[ENU=Package ID;
                                                              ENG=Package ID] }
    { 2   ;   ;Capability ID       ;Integer       ;CaptionML=[ENU=Capability ID;
                                                              ENG=Capability ID] }
  }
  KEYS
  {
    {    ;Package ID,Capability ID                ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

