OBJECT Table 2000000177 Tenant Profile
{
  OBJECT-PROPERTIES
  {
    Date=27/04/18;
    Time=12:00:00;
    Version List=;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Tenant Profile;
               ENG=Tenant Profile];
  }
  FIELDS
  {
    { 1   ;   ;App ID              ;GUID          ;CaptionML=[ENU=App ID;
                                                              ENG=App ID] }
    { 2   ;   ;Profile ID          ;Code30        ;CaptionML=[ENU=Profile ID;
                                                              ENG=Profile ID] }
    { 3   ;   ;Description         ;Text250       ;CaptionML=[ENU=Description;
                                                              ENG=Description] }
    { 4   ;   ;Role Center ID      ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Page));
                                                   CaptionML=[ENU=Role Center ID;
                                                              ENG=Role Centre ID] }
    { 5   ;   ;Default Role Center ;Boolean       ;CaptionML=[ENU=Default Role Center;
                                                              ENG=Default Role Centre] }
    { 6   ;   ;Disable Personalization;Boolean    ;CaptionML=[ENU=Disable Personalization;
                                                              ENG=Disable Personalisation] }
  }
  KEYS
  {
    {    ;App ID,Profile ID                       ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

