OBJECT Table 2000000190 Entitlement Set
{
  OBJECT-PROPERTIES
  {
    Date=27/04/18;
    Time=12:00:00;
    Version List=;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Entitlement Set;
               ENG=Entitlement Set];
  }
  FIELDS
  {
    { 1   ;   ;ID                  ;Code20        ;CaptionML=[ENU=ID;
                                                              ENG=ID] }
    { 2   ;   ;Name                ;Text250       ;CaptionML=[ENU=Name;
                                                              ENG=Name] }
  }
  KEYS
  {
    {    ;ID                                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

