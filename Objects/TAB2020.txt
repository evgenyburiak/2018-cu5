OBJECT Table 2020 Image Analysis Setup
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    Permissions=TableData 1261=rimd;
    CaptionML=[ENU=Image Analysis Setup;
               ENG=Image Analysis Setup];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[ENU=Primary Key;
                                                              ENG=Primary Key] }
    { 2   ;   ;Period start date   ;DateTime      ;CaptionML=[ENU=Period start date;
                                                              ENG=Period start date] }
    { 3   ;   ;Number of calls     ;Integer       ;CaptionML=[ENU=Number of calls;
                                                              ENG=Number of calls] }
    { 4   ;   ;Api Uri             ;Text250       ;OnValidate=BEGIN
                                                                ValidateApiUri("Api Uri");
                                                              END;

                                                   CaptionML=[ENU=Api Uri;
                                                              ENG=Api Uri] }
    { 5   ;   ;Api Key Key         ;GUID          ;ExtendedDatatype=Masked;
                                                   CaptionML=[ENU=Api Key Key;
                                                              ENG=Api Key Key] }
    { 6   ;   ;Limit value         ;Integer       ;CaptionML=[ENU=Limit value;
                                                              ENG=Limit value] }
    { 7   ;   ;Limit type          ;Option        ;CaptionML=[ENU=Limit type;
                                                              ENG=Limit type];
                                                   OptionCaptionML=[ENU=Year,Month,Day,Hour;
                                                                    ENG=Year,Month,Day,Hour];
                                                   OptionString=Year,Month,Day,Hour }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      ImageAnalysisPeriodTypeErr@1001 : TextConst 'ENU=Sorry, something went wrong on our end. Please try again later.;ENG=Sorry, something went wrong on our end. Please try again later.';
      TestMode@1003 : Boolean;
      TestTime@1004 : Time;
      TestDate@1005 : Date;
      TooManyCallsErr@1007 : TextConst '@@@=%1 is the number of calls per time unit allowed, %2 is the time unit duration (year, month, day, or hour);ENU=Sorry, you''ll have to wait until the start of the next %2. You can analyze %1 images per %2, and you''ve already hit the limit.;ENG=Sorry, you''ll have to wait until the start of the next %2. You can analyse %1 images per %2, and you''ve already hit the limit.';
      InvalidApiUriErr@1000 : TextConst 'ENU=The Api Uri must be a valid Uri for Cognitive Services.;ENG=The Api Uri must be a valid Uri for Cognitive Services.';

    LOCAL PROCEDURE ComputeCurrentPeriodStartDate@11(CurrentPeriodStartDate@1005 : Date;CurrentPeriodStartTime@1003 : Time;Type@1000 : 'Year,Month,Day,Hour') : DateTime;
    BEGIN
      CASE Type OF
        Type::Year:
          EXIT(CREATEDATETIME(CALCDATE('<-CY>',CurrentPeriodStartDate),0T));
        Type::Month:
          EXIT(CREATEDATETIME(CALCDATE('<-CM>',CurrentPeriodStartDate),0T));
        Type::Day:
          EXIT(CREATEDATETIME(CALCDATE('<-CD>',CurrentPeriodStartDate),0T));
        Type::Hour:
          EXIT(ROUNDDATETIME(CREATEDATETIME(CurrentPeriodStartDate,CurrentPeriodStartTime),1000 * 60 * 60,'<'));
      END;

      ERROR(ImageAnalysisPeriodTypeErr);
    END;

    PROCEDURE Increment@2(PeriodType@1000 : 'Year,Month,Day,Hour');
    VAR
      ImageAnalysisSetup@1001 : Record 2020;
    BEGIN
      GetCurrentStatus(ImageAnalysisSetup,PeriodType);

      ImageAnalysisSetup."Number of calls" += 1;
      ImageAnalysisSetup.MODIFY;
    END;

    LOCAL PROCEDURE SetOrUpdateLineIfNeeded@3(VAR ImageAnalysisSetup@1001 : Record 2020;PeriodStartDate@1000 : DateTime);
    BEGIN
      IF NOT ImageAnalysisSetup.GET THEN BEGIN
        ImageAnalysisSetup.INIT;
        ImageAnalysisSetup."Period start date" := PeriodStartDate;
        ImageAnalysisSetup."Number of calls" := 0;
        ImageAnalysisSetup.INSERT;
      END ELSE
        IF ImageAnalysisSetup."Period start date" <> PeriodStartDate THEN BEGIN
          ImageAnalysisSetup."Period start date" := PeriodStartDate;
          ImageAnalysisSetup."Number of calls" := 0;
          ImageAnalysisSetup.MODIFY;
        END;
    END;

    [External]
    PROCEDURE GetCurrentStatus@5(VAR ImageAnalysisSetup@1000 : Record 2020;PeriodType@1001 : 'Year,Month,Day,Hour');
    VAR
      CurrentPeriodStartDate@1002 : DateTime;
    BEGIN
      IF TestMode THEN
        CurrentPeriodStartDate := ComputeCurrentPeriodStartDate(TestDate,TestTime,PeriodType)
      ELSE
        CurrentPeriodStartDate := ComputeCurrentPeriodStartDate(TODAY,TIME,PeriodType);

      SetOrUpdateLineIfNeeded(ImageAnalysisSetup,CurrentPeriodStartDate)
    END;

    PROCEDURE SetTestMode@4(InputTestDate@1000 : Date;InputTestTime@1001 : Time);
    BEGIN
      TestMode := TRUE;
      TestDate := InputTestDate;
      TestTime := InputTestTime;
    END;

    PROCEDURE IsUsageLimitReached@7(VAR UsageLimitError@1003 : Text;MaxCallsPerPeriod@1004 : Integer;PeriodType@1005 : 'Year,Month,Day,Hour') : Boolean;
    VAR
      ImageAnalysisSetup@1000 : Record 2020;
    BEGIN
      GetCurrentStatus(ImageAnalysisSetup,PeriodType);
      IF ImageAnalysisSetup."Number of calls" >= MaxCallsPerPeriod THEN BEGIN
        UsageLimitError := STRSUBSTNO(TooManyCallsErr,FORMAT(MaxCallsPerPeriod),LOWERCASE(FORMAT(PeriodType)));
        EXIT(TRUE);
      END;

      EXIT(FALSE);
    END;

    PROCEDURE ValidateApiUri@1(ApiUri@1000 : Text);
    VAR
      RegEx@1001 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Text.RegularExpressions.Regex";
    BEGIN
      IF ApiUri <> '' THEN
        IF NOT RegEx.IsMatch(ApiUri,'https://([a-z0-9]|\.)*\.api\.cognitive\.microsoft.com/.*') THEN
          ERROR(InvalidApiUriErr);
    END;

    [External]
    PROCEDURE SetApiKey@6(ApiKey@1000 : Text);
    VAR
      ServicePassword@1001 : Record 1261;
    BEGIN
      IF ISNULLGUID("Api Key Key") OR NOT ServicePassword.GET("Api Key Key") THEN BEGIN
        ServicePassword.SavePassword(ApiKey);
        ServicePassword.INSERT(TRUE);
        "Api Key Key" := ServicePassword.Key;
      END ELSE BEGIN
        ServicePassword.SavePassword(ApiKey);
        ServicePassword.MODIFY(TRUE);
      END;
    END;

    [External]
    PROCEDURE GetApiKey@8() : Text;
    VAR
      ServicePassword@1000 : Record 1261;
    BEGIN
      IF NOT ISNULLGUID("Api Key Key") THEN
        IF ServicePassword.GET("Api Key Key") THEN
          EXIT(ServicePassword.GetPassword);
    END;

    [External]
    PROCEDURE GetSingleInstance@9();
    BEGIN
      IF NOT GET THEN BEGIN
        INIT;
        INSERT;
      END;
    END;

    BEGIN
    END.
  }
}

