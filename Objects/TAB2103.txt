OBJECT Table 2103 O365 Sales Document
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    OnDelete=VAR
               SalesDocumentIcon@1000 : Record 2100;
             BEGIN
               SalesDocumentIcon.INIT;
               "Document Icon" := SalesDocumentIcon.Picture; // ensure mediaset is empty
             END;

    CaptionML=[ENU=O365 Sales Document;
               ENG=O365 Sales Document];
  }
  FIELDS
  {
    { 1   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              ENG=Document Type];
                                                   OptionCaptionML=[ENU=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order;
                                                                    ENG=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order];
                                                   OptionString=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order }
    { 2   ;   ;Sell-to Customer No.;Code20        ;TableRelation=Customer;
                                                   CaptionML=[ENU=Sell-to Customer No.;
                                                              ENG=Sell-to Customer No.] }
    { 3   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              ENG=No.] }
    { 24  ;   ;Due Date            ;Date          ;CaptionML=[ENU=Due Date;
                                                              ENG=Due Date] }
    { 32  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Currency Code;
                                                              ENG=Currency Code] }
    { 79  ;   ;Sell-to Customer Name;Text50       ;TableRelation=Customer;
                                                   ValidateTableRelation=No;
                                                   CaptionML=[ENU=Sell-to Customer Name;
                                                              ENG=Sell-to Customer Name] }
    { 84  ;   ;Sell-to Contact     ;Text50        ;CaptionML=[ENU=Sell-to Contact;
                                                              ENG=Sell-to Contact] }
    { 99  ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              ENG=Document Date] }
    { 166 ;   ;Last Email Sent Time;DateTime      ;FieldClass=FlowField;
                                                   CalcFormula=Max("O365 Document Sent History"."Created Date-Time" WHERE (Document Type=FIELD(Document Type),
                                                                                                                           Document No.=FIELD(No.),
                                                                                                                           Posted=FIELD(Posted)));
                                                   CaptionML=[ENU=Last Email Sent Time;
                                                              ENG=Last Email Sent Time] }
    { 167 ;   ;Last Email Sent Status;Option      ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("O365 Document Sent History"."Job Last Status" WHERE (Document Type=FIELD(Document Type),
                                                                                                                            Document No.=FIELD(No.),
                                                                                                                            Posted=FIELD(Posted),
                                                                                                                            Created Date-Time=FIELD(Last Email Sent Time)));
                                                   CaptionML=[ENU=Last Email Sent Status;
                                                              ENG=Last Email Sent Status];
                                                   OptionCaptionML=[ENU=,In Process,Finished,Error;
                                                                    ENG=,In Process,Finished,Error];
                                                   OptionString=,In Process,Finished,Error }
    { 168 ;   ;Sent as Email       ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("O365 Document Sent History" WHERE (Document Type=FIELD(Document Type),
                                                                                                         Document No.=FIELD(No.),
                                                                                                         Posted=FIELD(Posted),
                                                                                                         Job Last Status=CONST(Finished)));
                                                   CaptionML=[ENU=Sent as Email;
                                                              ENG=Sent as Email] }
    { 169 ;   ;Last Email Notif Cleared;Boolean   ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("O365 Document Sent History".NotificationCleared WHERE (Document Type=FIELD(Document Type),
                                                                                                                              Document No.=FIELD(No.),
                                                                                                                              Posted=FIELD(Posted),
                                                                                                                              Created Date-Time=FIELD(Last Email Sent Time)));
                                                   CaptionML=[ENU=Last Email Notif Cleared;
                                                              ENG=Last Email Notif Cleared] }
    { 2100;   ;Posted              ;Boolean       ;CaptionML=[ENU=Posted;
                                                              ENG=Posted] }
    { 2101;   ;Canceled            ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Cancelled Document" WHERE (Source ID=CONST(112),
                                                                                                 Cancelled Doc. No.=FIELD(No.)));
                                                   CaptionML=[ENU=Canceled;
                                                              ENG=Cancelled] }
    { 2102;   ;Currency Symbol     ;Text10        ;CaptionML=[ENU=Currency Symbol;
                                                              ENG=Currency Symbol] }
    { 2103;   ;Document Status     ;Option        ;CaptionML=[ENU=Document Status;
                                                              ENG=Document Status];
                                                   OptionCaptionML=[ENU=Quote,Draft Invoice,Unpaid Invoice,Canceled Invoice,Paid Invoice,Overdue Invoice;
                                                                    ENG=Quote,Draft Invoice,Unpaid Invoice,Cancelled Invoice,Paid Invoice,Overdue Invoice];
                                                   OptionString=Quote,Draft Invoice,Unpaid Invoice,Canceled Invoice,Paid Invoice,Overdue Invoice }
    { 2104;   ;Sales Amount        ;Decimal       ;CaptionML=[ENU=Sales Amount;
                                                              ENG=Sales Amount] }
    { 2105;   ;Outstanding Amount  ;Decimal       ;CaptionML=[ENU=Outstanding Amount;
                                                              ENG=Outstanding Amount] }
    { 2106;   ;Total Invoiced Amount;Text250      ;CaptionML=[ENU=Total Invoiced Amount;
                                                              ENG=Total Invoiced Amount] }
    { 2107;   ;Outstanding Status  ;Text250       ;CaptionML=[ENU=Outstanding Status;
                                                              ENG=Outstanding Status] }
    { 2108;   ;Document Icon       ;MediaSet      ;CaptionML=[ENU=Document Icon;
                                                              ENG=Document Icon] }
    { 2109;   ;Payment Method      ;Code10        ;TableRelation="Payment Method" WHERE (Use for Invoicing=CONST(Yes));
                                                   CaptionML=[ENU=Payment Method;
                                                              ENG=Payment Method] }
    { 2110;   ;Display No.         ;Text20        ;CaptionML=[ENU=Display No.;
                                                              ENG=Display No.] }
    { 2111;   ;Quote Valid Until Date;Date        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Sales Header"."Quote Valid Until Date" WHERE (Document Type=FIELD(Document Type),
                                                                                                                     No.=FIELD(No.)));
                                                   CaptionML=[ENU=Quote Valid Until Date;
                                                              ENG=Quote Valid Until Date] }
    { 2112;   ;Quote Accepted      ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Sales Header"."Quote Accepted" WHERE (Document Type=FIELD(Document Type),
                                                                                                             No.=FIELD(No.)));
                                                   CaptionML=[ENU=Quote Accepted;
                                                              ENG=Quote Accepted] }
    { 2113;   ;Quote Sent to Customer;DateTime    ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Sales Header"."Quote Sent to Customer" WHERE (Document Type=FIELD(Document Type),
                                                                                                                     No.=FIELD(No.)));
                                                   CaptionML=[ENU=Quote Sent to Customer;
                                                              ENG=Quote Sent to Customer] }
    { 2114;   ;Quote Accepted Date ;Date          ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Sales Header"."Quote Accepted Date" WHERE (Document Type=FIELD(Document Type),
                                                                                                                  No.=FIELD(No.)));
                                                   CaptionML=[ENU=Quote Accepted Date;
                                                              ENG=Quote Accepted Date] }
  }
  KEYS
  {
    {    ;Document Type,No.,Posted                ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;Brick               ;Document Date,Sell-to Customer Name,Total Invoiced Amount,Display No.,Outstanding Status,Document Icon }
  }
  CODE
  {
    VAR
      NotSentTxt@1000 : TextConst 'ENU=Not sent;ENG=Not sent';
      OverdueAmountTxt@1001 : TextConst '@@@=%1 currency formatter (e.g.$), %2 amount;ENU=Overdue: %1%2;ENG=Overdue: %1%2';
      OverdueTxt@1006 : TextConst 'ENU=Overdue;ENG=Overdue';
      DueTxt@1002 : TextConst '@@@="%1 = a number of days greater than one";ENU=Due in %1 days;ENG=Due in %1 days';
      DueAmountTxt@1010 : TextConst '@@@="%1 = a number of days greater than one,%2 currency formatter (e.g.$), %3 amount";ENU=%2%3 due in %1 days;ENG=%2%3 due in %1 days';
      DueTodayTxt@1003 : TextConst 'ENU=Due today;ENG=Due today';
      DueTodayAmountTxt@1009 : TextConst '@@@=%1 currency formatter (e.g.$), %2 amount;ENU=%1%2 due today;ENG=%1%2 due today';
      AmountTxt@1004 : TextConst '@@@="%1=Currency symbol %2= amount";ENU=%1%2;ENG=%1%2';
      FormatTxt@1013 : TextConst '@@@={LOCKED} Do not translate;ENU=<Precision,2:2><Standard Format,0>;ENG=<Precision,2:2><Standard Format,0>';
      PaidTxt@1005 : TextConst 'ENU=Paid;ENG=Paid';
      CanceledTxt@1007 : TextConst 'ENU=Canceled;ENG=Cancelled';
      SortByDueDate@1008 : Boolean;
      DueTomorrowTxt@1011 : TextConst 'ENU=Due tomorrow;ENG=Due tomorrow';
      DueTomorrowAmountTxt@1012 : TextConst '@@@=%1 currency formatter (e.g.$), %2 amount;ENU=%1%2 due tomorrow;ENG=%1%2 due tomorrow';
      DraftTxt@1014 : TextConst 'ENU=Draft;ENG=Draft';
      DisplayNoLbl@1015 : TextConst '@@@="%1 = The posted invoice number";ENU=No. %1;ENG=No. %1';
      CreateTxt@1019 : TextConst 'ENU=Create;ENG=Create';
      SelectTypeQst@1020 : TextConst 'ENU=Invoice,Estimate;ENG=Invoice,Estimate';
      SentTxt@1016 : TextConst '@@@="%1 = date";ENU=Sent %1;ENG=Sent %1';
      AcceptedTxt@1017 : TextConst '@@@="%1 = date";ENU=Accepted %1;ENG=Accepted %1';
      ExpiredTxt@1018 : TextConst 'ENU=Expired;ENG=Expired';
      LastEmailFailedTxt@1021 : TextConst 'ENU=Failed to send;ENG=Failed to send';

    [External]
    PROCEDURE UpdateFields@1();
    VAR
      Currency@1000 : Record 4;
    BEGIN
      "Currency Symbol" := Currency.ResolveGLCurrencySymbol("Currency Code");

      IF Posted THEN
        GetAmountsPosted
      ELSE
        GetAmountsUnposted;

      IF "Document Type" = "Document Type"::Quote THEN
        CALCFIELDS("Quote Accepted","Quote Valid Until Date","Quote Sent to Customer","Quote Accepted Date");

      AssignIcon;
      SetIcon;
      SetBrickStatus;
      SetDisplayNo;
    END;

    LOCAL PROCEDURE AssignIcon@4();
    BEGIN
      CASE "Document Type" OF
        "Document Type"::Quote:
          BEGIN
            CASE TRUE OF
              "Quote Accepted":
                "Document Status" := "Document Status"::"Paid Invoice";
              QuoteIsExpired:
                "Document Status" := "Document Status"::"Canceled Invoice";
              "Quote Sent to Customer" <> 0DT:
                "Document Status" := "Document Status"::"Unpaid Invoice";
              ELSE
                "Document Status" := "Document Status"::"Draft Invoice";
            END;
            EXIT;
          END;
        "Document Type"::Invoice:
          CALCFIELDS(Canceled);
        ELSE
          EXIT;
      END;

      IF NOT Posted THEN BEGIN
        "Document Status" := "Document Status"::"Draft Invoice";
        EXIT;
      END;

      IF Canceled THEN BEGIN
        "Document Status" := "Document Status"::"Canceled Invoice";
        EXIT;
      END;

      IF "Outstanding Amount" <= 0 THEN BEGIN
        "Document Status" := "Document Status"::"Paid Invoice";
        EXIT;
      END;

      IF IsOverduePostedInvoice THEN BEGIN
        "Document Status" := "Document Status"::"Overdue Invoice";
        EXIT;
      END;

      "Document Status" := "Document Status"::"Unpaid Invoice";
    END;

    LOCAL PROCEDURE SetIcon@2();
    VAR
      SalesDocumentIcon@1000 : Record 2100;
      MediaResources@1001 : Record 2000000182;
    BEGIN
      IF SalesDocumentIcon.GET("Document Status") THEN
        IF MediaResources.GET(SalesDocumentIcon."Media Resources Ref") THEN;
      "Document Icon" := MediaResources."MediaSet Reference";
    END;

    PROCEDURE IsOverduePostedInvoice@7() : Boolean;
    VAR
      CustLedgerEntry@1000 : Record 21;
    BEGIN
      CustLedgerEntry.SETRANGE("Customer No.","Sell-to Customer No.");
      CustLedgerEntry.SETRANGE("Document Type",CustLedgerEntry."Document Type"::Invoice);
      CustLedgerEntry.SETRANGE("Document No.","No.");
      CustLedgerEntry.SETRANGE(Open,TRUE);
      CustLedgerEntry.SETFILTER("Due Date",'<%1',WORKDATE);
      EXIT(NOT CustLedgerEntry.ISEMPTY);
    END;

    LOCAL PROCEDURE GetRemainingDaysBeforeOverdue@12() : Integer;
    VAR
      CustLedgerEntry@1000 : Record 21;
      RemainingDuration@1001 : Integer;
    BEGIN
      IF IsOverduePostedInvoice THEN
        EXIT(0);

      CustLedgerEntry.SETRANGE("Customer No.","Sell-to Customer No.");
      CustLedgerEntry.SETRANGE("Document Type",CustLedgerEntry."Document Type"::Invoice);
      CustLedgerEntry.SETRANGE("Document No.","No.");
      CustLedgerEntry.SETRANGE(Open,TRUE);
      CustLedgerEntry.SETCURRENTKEY("Due Date");
      CustLedgerEntry.SETASCENDING("Due Date",TRUE);

      IF NOT CustLedgerEntry.FINDFIRST THEN
        EXIT(0);

      RemainingDuration := (CustLedgerEntry."Due Date" - WORKDATE);

      EXIT(RemainingDuration);
    END;

    LOCAL PROCEDURE GetAmountsUnposted@6();
    VAR
      SalesHeader@1000 : Record 36;
    BEGIN
      SalesHeader.GET("Document Type","No.");
      SalesHeader.CALCFIELDS("Amount Including VAT");
      "Sales Amount" := SalesHeader."Amount Including VAT";
      "Payment Method" := SalesHeader."Payment Method Code";
    END;

    LOCAL PROCEDURE GetAmountsPosted@8();
    VAR
      SalesInvoiceHeader@1000 : Record 112;
    BEGIN
      SalesInvoiceHeader.GET("No.");
      SalesInvoiceHeader.CALCFIELDS("Amount Including VAT","Remaining Amount");
      "Sales Amount" := SalesInvoiceHeader."Amount Including VAT";
      "Outstanding Amount" := SalesInvoiceHeader."Remaining Amount";
      "Payment Method" := SalesInvoiceHeader."Payment Method Code";
    END;

    LOCAL PROCEDURE SetBrickStatus@10();
    BEGIN
      CALCFIELDS("Last Email Sent Time","Last Email Sent Status");

      IF "Document Type" = "Document Type"::Quote THEN BEGIN
        SetQuoteBrickStatus;
        EXIT;
      END;

      IF NOT Posted THEN BEGIN
        SetDraftInvoiceBrickStatus;
        EXIT;
      END;

      SetPostedDocumentBrickStatus;
    END;

    LOCAL PROCEDURE SetQuoteBrickStatus@30();
    BEGIN
      CASE TRUE OF
        "Last Email Sent Status" = "Last Email Sent Status"::Error:
          BEGIN
            "Outstanding Status" := STRSUBSTNO(AmountTxt,"Currency Symbol",FORMAT("Sales Amount",0,FormatTxt));
            "Total Invoiced Amount" := LastEmailFailedTxt;// STRSUBSTNO(AcceptedTxt,"Quote Accepted Date");
          END;
        "Quote Accepted":
          BEGIN
            "Outstanding Status" := STRSUBSTNO(AcceptedTxt,"Quote Accepted Date");
            "Total Invoiced Amount" := STRSUBSTNO(AmountTxt,"Currency Symbol",FORMAT("Sales Amount",0,FormatTxt));
          END;
        QuoteIsExpired:
          BEGIN
            "Outstanding Status" := STRSUBSTNO(AmountTxt,"Currency Symbol",FORMAT("Sales Amount",0,FormatTxt));
            "Total Invoiced Amount" := ExpiredTxt;
          END;
        "Quote Sent to Customer" <> 0DT:
          BEGIN
            "Outstanding Status" := STRSUBSTNO(SentTxt,DT2DATE("Quote Sent to Customer"));
            "Total Invoiced Amount" := STRSUBSTNO(AmountTxt,"Currency Symbol",FORMAT("Sales Amount",0,FormatTxt));
          END;
        ELSE BEGIN
          "Outstanding Status" := STRSUBSTNO(AmountTxt,"Currency Symbol",FORMAT("Sales Amount",0,FormatTxt));
          "Total Invoiced Amount" := NotSentTxt;
        END;
      END;
    END;

    LOCAL PROCEDURE SetPostedDocumentBrickStatus@31();
    VAR
      HasPartialPayment@1001 : Boolean;
      DaysToDueDate@1000 : Integer;
    BEGIN
      IF "Outstanding Amount" <= 0 THEN BEGIN
        CALCFIELDS(Canceled);
        IF Canceled THEN BEGIN
          "Total Invoiced Amount" := CanceledTxt;
          "Outstanding Status" := STRSUBSTNO(AmountTxt,"Currency Symbol",FORMAT("Sales Amount",0,FormatTxt));
        END ELSE BEGIN
          "Total Invoiced Amount" := STRSUBSTNO(AmountTxt,"Currency Symbol",FORMAT("Sales Amount",0,FormatTxt));
          "Outstanding Status" := PaidTxt;
        END;
        EXIT;
      END;

      IF "Last Email Sent Status" = "Last Email Sent Status"::Error THEN BEGIN
        "Total Invoiced Amount" := LastEmailFailedTxt;
        "Outstanding Status" := STRSUBSTNO(AmountTxt,"Currency Symbol",FORMAT("Sales Amount",0,FormatTxt));
        EXIT;
      END;

      "Total Invoiced Amount" := STRSUBSTNO(AmountTxt,"Currency Symbol",FORMAT("Sales Amount",0,FormatTxt));
      HasPartialPayment := "Outstanding Amount" <> "Sales Amount";

      IF IsOverduePostedInvoice THEN
        IF HasPartialPayment THEN
          "Outstanding Status" := STRSUBSTNO(OverdueAmountTxt,"Currency Symbol",FORMAT("Outstanding Amount",0,FormatTxt))
        ELSE
          "Outstanding Status" := OverdueTxt
      ELSE BEGIN
        DaysToDueDate := GetRemainingDaysBeforeOverdue;
        IF  DaysToDueDate = 0 THEN
          IF HasPartialPayment THEN
            "Outstanding Status" := STRSUBSTNO(DueTodayAmountTxt,"Currency Symbol",FORMAT("Outstanding Amount",0,FormatTxt))
          ELSE
            "Outstanding Status" := DueTodayTxt
        ELSE
          IF  DaysToDueDate = 1 THEN
            IF HasPartialPayment THEN
              "Outstanding Status" := STRSUBSTNO(DueTomorrowAmountTxt,"Currency Symbol",FORMAT("Outstanding Amount",0,FormatTxt))
            ELSE
              "Outstanding Status" := DueTomorrowTxt
          ELSE
            IF HasPartialPayment THEN
              "Outstanding Status" := STRSUBSTNO(DueAmountTxt,DaysToDueDate,"Currency Symbol",FORMAT("Outstanding Amount",0,FormatTxt))
            ELSE
              "Outstanding Status" := STRSUBSTNO(DueTxt,DaysToDueDate)
      END;
    END;

    LOCAL PROCEDURE SetDraftInvoiceBrickStatus@32();
    BEGIN
      "Total Invoiced Amount" := NotSentTxt;
      "Outstanding Status" := STRSUBSTNO(AmountTxt,"Currency Symbol",FORMAT("Sales Amount",0,FormatTxt));
    END;

    LOCAL PROCEDURE SetDisplayNo@25();
    BEGIN
      IF Posted THEN
        "Display No." := STRSUBSTNO(DisplayNoLbl,"No.")
      ELSE
        CASE "Document Type" OF
          "Document Type"::Invoice:
            "Display No." := DraftTxt;
          "Document Type"::Quote:
            "Display No." := "No.";
          ELSE
            "Display No." := '';
        END;
    END;

    [External]
    PROCEDURE OnFind@5(Which@1000 : Text) : Boolean;
    VAR
      FilterPosted@1001 : Boolean;
    BEGIN
      CASE Which OF
        '+':
          Posted := TRUE; // Get last posted invoice
        '-':
          Posted := FALSE; // Get first sales header
        ELSE
          IF HasPostedFilter(FilterPosted) THEN
            Posted := Posted OR FilterPosted;
      END;

      IF Posted THEN
        EXIT(FindPostedDocument(Which));

      EXIT(FindUnpostedDocument(Which));
    END;

    [External]
    PROCEDURE OnNext@9(Steps@1000 : Integer) : Integer;
    VAR
      SalesInvoiceHeader@1005 : Record 112;
      SalesHeaderResults@1004 : Integer;
      SalesInvoiceHeaderResults@1003 : Integer;
      StepOffset@1002 : Integer;
      FilterPosted@1001 : Boolean;
    BEGIN
      SetSalesInvoiceHeaderFilters(SalesInvoiceHeader);

      IF NOT Posted THEN BEGIN // Look for more Sales Headers
        SalesHeaderResults := GetNextUnpostedDocument(Steps);
        IF SalesHeaderResults <> 0 THEN
          EXIT(SalesHeaderResults);

        IF Steps < 0 THEN // No more sales headers and we are moving "back"
          EXIT(SalesHeaderResults); // therefore, no more recs, so this means we are done

        // No more sales headers, but we are moving forward so move on to sales invoice headers below
        IF NOT SalesInvoiceHeader.FINDSET THEN
          EXIT(0);

        StepOffset += 1; // need to adjust for one step that we did with FINDSET
      END ELSE
        SalesInvoiceHeader.TRANSFERFIELDS(Rec); // Continue from current posted doc

      IF HasPostedFilter(FilterPosted) AND (NOT FilterPosted) THEN
        EXIT(GetPreviousUnpostedDocument(Steps));

      SalesInvoiceHeaderResults := SalesInvoiceHeader.NEXT(Steps - StepOffset);
      IF (SalesInvoiceHeaderResults + StepOffset) <> 0 THEN BEGIN
        SetSalesInvoiceHeaderAsRec(SalesInvoiceHeader);
        EXIT(SalesInvoiceHeaderResults + StepOffset);
      END;

      EXIT(GetPreviousUnpostedDocument(Steps));
    END;

    LOCAL PROCEDURE SetSalesHeaderFilters@22(VAR SalesHeader@1000 : Record 36);
    BEGIN
      SetSalesHeaderKey(SalesHeader);

      CopySalesHeaderFilters(SalesHeader);
      IF SalesHeader.GETFILTER("Document Type") = '' THEN
        SalesHeader.SETRANGE("Document Type","Document Type"::Invoice);

      FILTERGROUP(-1);
      IF GETFILTER("Sell-to Customer Name") <> '' THEN BEGIN
        SalesHeader.FILTERGROUP(-1);
        CopySalesHeaderFilters(SalesHeader);
        SalesHeader.FILTERGROUP(0);
      END;
      FILTERGROUP(0);

      SalesHeader.TRANSFERFIELDS(Rec);
    END;

    LOCAL PROCEDURE SetSalesInvoiceHeaderFilters@19(VAR SalesInvoiceHeader@1000 : Record 112);
    BEGIN
      SetSalesInvoiceHeaderKey(SalesInvoiceHeader);

      CopySalesInvoiceHeaderFilters(SalesInvoiceHeader);

      FILTERGROUP(-1);
      IF GETFILTER("Sell-to Customer Name") <> '' THEN BEGIN
        SalesInvoiceHeader.FILTERGROUP(-1);
        CopySalesInvoiceHeaderFilters(SalesInvoiceHeader);
        SalesInvoiceHeader.FILTERGROUP(0);
      END;
      FILTERGROUP(0);

      SalesInvoiceHeader.TRANSFERFIELDS(Rec);
    END;

    LOCAL PROCEDURE CopySalesHeaderFilters@27(VAR SalesHeader@1000 : Record 36);
    BEGIN
      COPYFILTER("Document Type",SalesHeader."Document Type");
      COPYFILTER("No.",SalesHeader."No.");
      COPYFILTER("Sell-to Customer Name",SalesHeader."Sell-to Customer Name");
      COPYFILTER("Sell-to Customer No.",SalesHeader."Sell-to Customer No.");
      COPYFILTER("Sell-to Contact",SalesHeader."Sell-to Contact");
      COPYFILTER("Document Date",SalesHeader."Document Date");
      COPYFILTER("Last Email Sent Status",SalesHeader."Last Email Sent Status");
      COPYFILTER("Last Email Notif Cleared",SalesHeader."Last Email Notif Cleared");
    END;

    LOCAL PROCEDURE CopySalesInvoiceHeaderFilters@28(VAR SalesInvoiceHeader@1000 : Record 112);
    BEGIN
      COPYFILTER("No.",SalesInvoiceHeader."No.");
      COPYFILTER("Outstanding Amount",SalesInvoiceHeader."Remaining Amount");
      COPYFILTER("Sell-to Customer Name",SalesInvoiceHeader."Sell-to Customer Name");
      COPYFILTER("Sell-to Customer No.",SalesInvoiceHeader."Sell-to Customer No.");
      COPYFILTER("Sell-to Contact",SalesInvoiceHeader."Sell-to Contact");
      COPYFILTER("Document Date",SalesInvoiceHeader."Document Date");
      COPYFILTER("Last Email Sent Status",SalesInvoiceHeader."Last Email Sent Status");
      COPYFILTER("Last Email Notif Cleared",SalesInvoiceHeader."Last Email Notif Cleared");
    END;

    LOCAL PROCEDURE SetSalesHeaderAsRec@17(VAR SalesHeader@1000 : Record 36);
    BEGIN
      TRANSFERFIELDS(SalesHeader);
      Posted := FALSE;
      UpdateFields;
    END;

    LOCAL PROCEDURE SetSalesInvoiceHeaderAsRec@18(VAR SalesInvoiceHeader@1000 : Record 112);
    BEGIN
      TRANSFERFIELDS(SalesInvoiceHeader);
      Posted := TRUE;
      "Document Type" := "Document Type"::Invoice;
      UpdateFields;
    END;

    LOCAL PROCEDURE FindUnpostedDocument@16(Which@1001 : Text) : Boolean;
    VAR
      SalesHeader@1000 : Record 36;
      SalesInvoiceHeader@1002 : Record 112;
      IsPosted@1003 : Boolean;
    BEGIN
      IF HasPostedFilter(IsPosted) AND IsPosted THEN
        EXIT(FALSE);

      SetSalesHeaderFilters(SalesHeader);
      IF SalesHeader.FIND(Which) THEN BEGIN
        SetSalesHeaderAsRec(SalesHeader);
        EXIT(TRUE);
      END;

      IF (STRPOS(Which,'<') > 0) AND (STRPOS(Which,'>') = 0) THEN // We are only interested in unposted docs previous to this one
        EXIT(FALSE); // since there are none, we should exit

      IF HasPostedFilter(IsPosted) AND (NOT IsPosted) THEN
        EXIT(FALSE);  // do not attempt search for posted doc

      // Get the first posted doc since we no longer have any unposted docs
      SetSalesInvoiceHeaderFilters(SalesInvoiceHeader);
      IF SalesInvoiceHeader.FINDFIRST THEN BEGIN
        SetSalesInvoiceHeaderAsRec(SalesInvoiceHeader);
        EXIT(TRUE);
      END;

      EXIT(FALSE);
    END;

    [External]
    PROCEDURE FindPostedDocument@15(Which@1000 : Text) : Boolean;
    VAR
      SalesInvoiceHeader@1001 : Record 112;
      SalesHeader@1002 : Record 36;
      IsPosted@1003 : Boolean;
    BEGIN
      IF (NOT HasPostedFilter(IsPosted)) OR IsPosted THEN BEGIN
        SetSalesInvoiceHeaderFilters(SalesInvoiceHeader);
        IF SalesInvoiceHeader.FIND(Which) THEN BEGIN
          SetSalesInvoiceHeaderAsRec(SalesInvoiceHeader);
          EXIT(TRUE);
        END;
      END;

      IF HasPostedFilter(IsPosted) AND IsPosted THEN
        EXIT(FALSE); // do not attempt search for unposted doc

      // If Which contains '<' or is '+' then we should look for the last Sales Header because there are no posted invoices
      // that match the specified criteria.
      IF (STRPOS(Which,'<') > 0) OR (Which = '+') THEN BEGIN
        SetSalesHeaderFilters(SalesHeader);
        IF SalesHeader.FINDLAST THEN BEGIN
          SetSalesHeaderAsRec(SalesHeader);
          EXIT(TRUE);
        END;
      END;

      // No match
      EXIT(FALSE);
    END;

    LOCAL PROCEDURE GetNextUnpostedDocument@14(Steps@1000 : Integer) : Integer;
    VAR
      SalesHeader@1001 : Record 36;
      IsPosted@1003 : Boolean;
      SalesHeaderResults@1002 : Integer;
    BEGIN
      IF HasPostedFilter(IsPosted) AND IsPosted THEN
        EXIT(0);

      SetSalesHeaderFilters(SalesHeader);
      SalesHeaderResults := SalesHeader.NEXT(Steps);

      IF SalesHeaderResults <> 0 THEN
        SetSalesHeaderAsRec(SalesHeader);

      EXIT(SalesHeaderResults);
    END;

    LOCAL PROCEDURE GetPreviousUnpostedDocument@13(Steps@1000 : Integer) : Integer;
    VAR
      SalesHeader@1001 : Record 36;
      SalesHeaderResults@1002 : Integer;
      IsPosted@1003 : Boolean;
    BEGIN
      IF Steps >= 0 THEN
        EXIT(0); // there must be negative steps

      IF HasPostedFilter(IsPosted) AND IsPosted THEN
        EXIT(0);

      SetSalesHeaderFilters(SalesHeader);

      IF NOT SalesHeader.FIND('+') THEN // step 1 entry back (i.e. get the last sales header)
        EXIT(0); // no previous sales header

      IF Steps < -1 THEN // there are more steps to do
        SalesHeaderResults := SalesHeader.NEXT(Steps + 1) - 1
      ELSE
        SalesHeaderResults := Steps;

      IF SalesHeaderResults <> 0 THEN
        SetSalesHeaderAsRec(SalesHeader);

      EXIT(SalesHeaderResults);
    END;

    LOCAL PROCEDURE HasPostedFilter@11(VAR FilterValue@1001 : Boolean) : Boolean;
    VAR
      PostedFilter@1000 : Boolean;
    BEGIN
      IF GETFILTER(Posted) = '' THEN
        EXIT(FALSE);

      IF NOT EVALUATE(PostedFilter,GETFILTER(Posted)) THEN
        EXIT(FALSE);

      FilterValue := PostedFilter;
      EXIT(TRUE);
    END;

    LOCAL PROCEDURE SetSalesInvoiceHeaderKey@20(VAR SalesInvoiceHeader@1000 : Record 112);
    BEGIN
      IF SortByDueDate THEN BEGIN
        SalesInvoiceHeader.SETCURRENTKEY("Due Date","Document Date","No.");
        SalesInvoiceHeader.SETASCENDING("Due Date",TRUE);
      END ELSE BEGIN
        SalesInvoiceHeader.SETCURRENTKEY("Document Date","Due Date","No.");
        SalesInvoiceHeader.SETASCENDING("Due Date",FALSE);
      END;
      SalesInvoiceHeader.SETASCENDING("Document Date",FALSE);
      SalesInvoiceHeader.SETASCENDING("No.",FALSE);
    END;

    LOCAL PROCEDURE SetSalesHeaderKey@21(VAR SalesHeader@1000 : Record 36);
    BEGIN
      SalesHeader.SETCURRENTKEY("Document Date","No.");
      SalesHeader.SETASCENDING("No.",FALSE);
      SalesHeader.SETASCENDING("Document Date",FALSE);
    END;

    [External]
    PROCEDURE SetSortByDocDate@24();
    BEGIN
      SortByDueDate := FALSE;
    END;

    [External]
    PROCEDURE SetSortByDueDate@23();
    BEGIN
      SortByDueDate := TRUE;
    END;

    LOCAL PROCEDURE QuoteIsExpired@34() : Boolean;
    BEGIN
      EXIT(("Quote Valid Until Date" <> 0D) AND ("Quote Valid Until Date" < WORKDATE));
    END;

    PROCEDURE OpenInvoice@26();
    VAR
      SalesHeader@1001 : Record 36;
      SalesInvoiceHeader@1000 : Record 112;
      O365SalesInvoice@1002 : Page 2110;
      O365SalesQuote@1003 : Page 2141;
    BEGIN
      IF Posted THEN BEGIN
        IF NOT SalesInvoiceHeader.GET("No.") THEN
          EXIT;
        SalesInvoiceHeader.SETRECFILTER;
        PAGE.RUN(PAGE::"O365 Posted Sales Invoice",SalesInvoiceHeader);
      END ELSE BEGIN
        IF NOT SalesHeader.GET("Document Type","No.") THEN
          EXIT;
        SalesHeader.SETRECFILTER;
        CASE "Document Type" OF
          "Document Type"::Invoice:
            BEGIN
              O365SalesInvoice.SETRECORD(SalesHeader);
              O365SalesInvoice.SuppressExitPrompt;
              O365SalesInvoice.RUN;
            END;
          "Document Type"::Quote:
            BEGIN
              O365SalesQuote.SETRECORD(SalesHeader);
              O365SalesQuote.SuppressExitPrompt;
              O365SalesQuote.RUN;
            END;
        END;
      END;
    END;

    PROCEDURE CreateNew@29();
    VAR
      SalesHeader@1000 : Record 36;
    BEGIN
      CASE STRMENU(SelectTypeQst,1,CreateTxt) OF
        1:
          CreateDocument(PAGE::"O365 Sales Invoice",SalesHeader."Document Type"::Invoice);
        2:
          CreateDocument(PAGE::"O365 Sales Quote",SalesHeader."Document Type"::Quote);
      END;
    END;

    PROCEDURE CreateDocument@35(PageID@1000 : Integer;DocType@1001 : Option);
    VAR
      SalesHeader@1002 : Record 36;
    BEGIN
      SalesHeader.INIT;
      SalesHeader."Document Type" := DocType;
      SalesHeader.SetDefaultPaymentServices;
      SalesHeader.INSERT(TRUE);
      PAGE.RUN(PageID,SalesHeader);
    END;

    BEGIN
    END.
  }
}

