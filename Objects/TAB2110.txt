OBJECT Table 2110 O365 Sales Initial Setup
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=O365 Sales Initial Setup;
               ENG=O365 Sales Initial Setup];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[ENU=Primary Key;
                                                              ENG=Primary Key] }
    { 2   ;   ;Payment Reg. Template Name;Code10  ;TableRelation="Gen. Journal Template".Name;
                                                   CaptionML=[ENU=Payment Reg. Template Name;
                                                              ENG=Payment Reg. Template Name] }
    { 3   ;   ;Payment Reg. Batch Name;Code10     ;TableRelation="Gen. Journal Batch".Name WHERE (Journal Template Name=FIELD(Payment Reg. Template Name));
                                                   CaptionML=[ENU=Payment Reg. Batch Name;
                                                              ENG=Payment Reg. Batch Name] }
    { 4   ;   ;Is initialized      ;Boolean       ;CaptionML=[ENU=Is initialized;
                                                              ENG=Is initialised] }
    { 5   ;   ;Default Customer Template;Code10   ;TableRelation="Config. Template Header".Code WHERE (Table ID=CONST(18));
                                                   CaptionML=[ENU=Default Customer Template;
                                                              ENG=Default Customer Template] }
    { 6   ;   ;Default Item Template;Code10       ;TableRelation="Config. Template Header".Code WHERE (Table ID=CONST(27));
                                                   CaptionML=[ENU=Default Item Template;
                                                              ENG=Default Item Template] }
    { 7   ;   ;Default Payment Terms Code;Code10  ;TableRelation="Payment Terms";
                                                   CaptionML=[ENU=Default Payment Terms Code;
                                                              ENG=Default Payment Terms Code] }
    { 8   ;   ;Default Payment Method Code;Code10 ;TableRelation="Payment Method";
                                                   CaptionML=[ENU=Default Payment Method Code;
                                                              ENG=Default Payment Method Code] }
    { 9   ;   ;Sales Invoice No. Series;Code20    ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Sales Invoice No. Series;
                                                              ENG=Sales Invoice No. Series] }
    { 10  ;   ;Posted Sales Inv. No. Series;Code20;TableRelation="No. Series";
                                                   CaptionML=[ENU=Posted Sales Inv. No. Series;
                                                              ENG=Posted Sales Inv. No. Series] }
    { 11  ;   ;Tax Type            ;Option        ;CaptionML=[ENU=Tax Type;
                                                              ENG=VAT Type];
                                                   OptionCaptionML=[ENU=VAT,Sales Tax;
                                                                    ENG=VAT,Sales Tax];
                                                   OptionString=VAT,Sales Tax }
    { 12  ;   ;Default VAT Bus. Posting Group;Code20;
                                                   TableRelation="VAT Business Posting Group";
                                                   CaptionML=[ENU=Default VAT Bus. Posting Group;
                                                              ENG=Default VAT Bus. Posting Group] }
    { 13  ;   ;Normal VAT Prod. Posting Gr.;Code20;TableRelation="VAT Product Posting Group";
                                                   CaptionML=[ENU=Normal VAT Prod. Posting Gr.;
                                                              ENG=Normal VAT Prod. Posting Gr.] }
    { 14  ;   ;Reduced VAT Prod. Posting Gr.;Code20;
                                                   TableRelation="VAT Product Posting Group";
                                                   CaptionML=[ENU=Reduced VAT Prod. Posting Gr.;
                                                              ENG=Reduced VAT Prod. Posting Gr.] }
    { 15  ;   ;Zero VAT Prod. Posting Gr.;Code20  ;TableRelation="VAT Product Posting Group";
                                                   CaptionML=[ENU=Zero VAT Prod. Posting Gr.;
                                                              ENG=Zero VAT Prod. Posting Gr.] }
    { 16  ;   ;C2Graph Endpoint    ;Text250       ;CaptionML=[ENU=C2Graph Endpoint;
                                                              ENG=C2Graph Endpoint] }
    { 17  ;   ;Sales Quote No. Series;Code20      ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Sales Quote No. Series;
                                                              ENG=Sales Quote No. Series] }
    { 18  ;   ;Engage Endpoint     ;Text250       ;CaptionML=[ENU=Engage Endpoint;
                                                              ENG=Engage Endpoint] }
    { 19  ;   ;Coupons Integration Enabled;Boolean;CaptionML=[ENU=Coupons Integration Enabled;
                                                              ENG=Coupons Integration Enabled] }
    { 20  ;   ;Graph Enablement Reminder;Boolean  ;InitValue=Yes;
                                                   CaptionML=[ENU=Graph Enablement Reminder;
                                                              ENG=Graph Enablement Reminder] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE IsUsingSalesTax@1000() : Boolean;
    BEGIN
      IF GET THEN;
      EXIT("Tax Type" = "Tax Type"::"Sales Tax");
    END;

    PROCEDURE IsUsingVAT@1001() : Boolean;
    BEGIN
      IF GET THEN;
      EXIT("Tax Type" = "Tax Type"::VAT);
    END;

    PROCEDURE UpdateDefaultPaymentTerms@3(PaymentTermsCode@1003 : Code[10]);
    VAR
      Customer@1002 : Record 18;
      SalesHeader@1001 : Record 36;
      O365SalesInitialSetup@1004 : Record 2110;
      PaymentTerms@1005 : Record 3;
      ConfigTemplateManagement@1000 : Codeunit 8612;
    BEGIN
      IF NOT O365SalesInitialSetup.GET THEN
        O365SalesInitialSetup.INSERT(TRUE);

      ConfigTemplateManagement.ReplaceDefaultValueForAllTemplates(
        DATABASE::Customer,Customer.FIELDNO("Payment Terms Code"),PaymentTermsCode);
      Customer.SETRANGE("Payment Terms Code",O365SalesInitialSetup."Default Payment Terms Code");
      Customer.MODIFYALL("Payment Terms Code",PaymentTermsCode,TRUE);
      SalesHeader.SETRANGE("Payment Terms Code",O365SalesInitialSetup."Default Payment Terms Code");
      SalesHeader.MODIFYALL("Payment Terms Code",PaymentTermsCode,TRUE);
      O365SalesInitialSetup."Default Payment Terms Code" := PaymentTermsCode;
      O365SalesInitialSetup.MODIFY(TRUE);

      // Update last modified date time, so users of web services can pick up an update
      IF PaymentTerms.GET(PaymentTermsCode) THEN BEGIN
        PaymentTerms."Last Modified Date Time" := CURRENTDATETIME;
        PaymentTerms.MODIFY(TRUE);
      END;
    END;

    PROCEDURE UpdateDefaultPaymentMethod@4(PaymentMethodCode@1003 : Code[10]);
    VAR
      Customer@1002 : Record 18;
      SalesHeader@1001 : Record 36;
      O365SalesInitialSetup@1004 : Record 2110;
      PaymentMethod@1005 : Record 289;
      ConfigTemplateManagement@1000 : Codeunit 8612;
    BEGIN
      IF NOT O365SalesInitialSetup.GET THEN
        O365SalesInitialSetup.INSERT(TRUE);

      ConfigTemplateManagement.ReplaceDefaultValueForAllTemplates(
        DATABASE::Customer,Customer.FIELDNO("Payment Method Code"),PaymentMethodCode);
      Customer.SETRANGE("Payment Method Code",O365SalesInitialSetup."Default Payment Method Code");
      Customer.MODIFYALL("Payment Method Code",PaymentMethodCode,TRUE);
      SalesHeader.SETRANGE("Payment Method Code",O365SalesInitialSetup."Default Payment Method Code");
      SalesHeader.MODIFYALL("Payment Method Code",PaymentMethodCode,TRUE);
      O365SalesInitialSetup."Default Payment Method Code" := PaymentMethodCode;
      O365SalesInitialSetup.MODIFY(TRUE);

      // Update last modified date time, so users of web services can pick up an update
      IF PaymentMethod.GET(PaymentMethodCode) THEN BEGIN
        PaymentMethod."Last Modified Date Time" := CURRENTDATETIME;
        PaymentMethod.MODIFY(TRUE);
      END;
    END;

    PROCEDURE IsDefaultPaymentMethod@2(VAR PaymentMethod@1000 : Record 289) : Boolean;
    BEGIN
      IF NOT GET THEN
        INSERT(TRUE);

      EXIT("Default Payment Method Code" = PaymentMethod.Code);
    END;

    PROCEDURE IsDefaultPaymentTerms@5(VAR PaymentTerms@1000 : Record 3) : Boolean;
    BEGIN
      IF NOT GET THEN
        INSERT(TRUE);

      EXIT("Default Payment Terms Code" = PaymentTerms.Code);
    END;

    BEGIN
    END.
  }
}

