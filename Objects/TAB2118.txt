OBJECT Table 2118 O365 Email Setup
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               IF Email = '' THEN
                 ERROR(EmailAddressEmptyErr);

               Code := Email;
             END;

    OnModify=BEGIN
               IF Email <> '' THEN
                 RENAME(Email,RecipientType)
               ELSE
                 IF DELETE THEN; // If Email validate was called rec may have been deleted already
             END;

    CaptionML=[ENU=O365 Email Setup;
               ENG=O365 Email Setup];
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code80        ;CaptionML=[ENU=Code;
                                                              ENG=Code] }
    { 2   ;   ;Email               ;Text80        ;OnValidate=VAR
                                                                MailManagement@1000 : Codeunit 9520;
                                                              BEGIN
                                                                IF DELCHR(Email,'=',' ') = '' THEN
                                                                  IF DELETE THEN
                                                                    EXIT;

                                                                MailManagement.CheckValidEmailAddress(Email);
                                                              END;

                                                   ExtendedDatatype=E-Mail;
                                                   CaptionML=[ENU=Email;
                                                              ENG=Email] }
    { 3   ;   ;RecipientType       ;Option        ;CaptionML=[ENU=RecipientType;
                                                              ENG=RecipientType];
                                                   OptionCaptionML=[ENU=CC,BCC;
                                                                    ENG=CC,BCC];
                                                   OptionString=CC,BCC }
  }
  KEYS
  {
    {    ;Code,RecipientType                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      EmailAddressEmptyErr@1000 : TextConst 'ENU=An email address must be specified.;ENG=An email address must be specified.';

    [External]
    PROCEDURE GetCCAddressesFromO365EmailSetup@1() Addresses : Text[250];
    BEGIN
      SETRANGE(RecipientType,RecipientType::CC);
      IF FINDSET THEN BEGIN
        REPEAT
          Addresses += Email + ';';
        UNTIL NEXT = 0;
      END;
      Addresses := DELCHR(Addresses,'>',';');
    END;

    [External]
    PROCEDURE GetBCCAddressesFromO365EmailSetup@3() Addresses : Text[250];
    BEGIN
      SETRANGE(RecipientType,RecipientType::BCC);
      IF FINDSET THEN BEGIN
        REPEAT
          Addresses += Email + ';';
        UNTIL NEXT = 0;
      END;
      Addresses := DELCHR(Addresses,'>',';');
    END;

    BEGIN
    END.
  }
}

