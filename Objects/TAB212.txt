OBJECT Table 212 Job Posting Buffer
{
  OBJECT-PROPERTIES
  {
    Date=22/02/18;
    Time=12:00:00;
    Version List=NAVW111.00.00.20783;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Job Posting Buffer;
               ENG=Job Posting Buffer];
  }
  FIELDS
  {
    { 1   ;   ;Job No.             ;Code20        ;TableRelation=Job;
                                                   DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Job No.;
                                                              ENG=Job No.] }
    { 2   ;   ;Entry Type          ;Option        ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Entry Type;
                                                              ENG=Entry Type];
                                                   OptionCaptionML=[ENU=Usage,Sale;
                                                                    ENG=Usage,Sale];
                                                   OptionString=Usage,Sale }
    { 3   ;   ;Posting Group Type  ;Option        ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Posting Group Type;
                                                              ENG=Posting Group Type];
                                                   OptionCaptionML=[ENU=Resource,Item,G/L Account;
                                                                    ENG=Resource,Item,G/L Account];
                                                   OptionString=Resource,Item,G/L Account }
    { 4   ;   ;No.                 ;Code20        ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=No.;
                                                              ENG=No.] }
    { 5   ;   ;Posting Group       ;Code20        ;TableRelation="Job Posting Group";
                                                   DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Posting Group;
                                                              ENG=Posting Group] }
    { 6   ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              ENG=Global Dimension 1 Code];
                                                   CaptionClass='1,1,1' }
    { 7   ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              ENG=Global Dimension 2 Code];
                                                   CaptionClass='1,1,2' }
    { 8   ;   ;Unit of Measure Code;Code10        ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Unit of Measure Code;
                                                              ENG=Unit of Measure Code] }
    { 9   ;   ;Work Type Code      ;Code10        ;TableRelation="Work Type";
                                                   DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Work Type Code;
                                                              ENG=Work Type Code] }
    { 20  ;   ;Amount              ;Decimal       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Amount;
                                                              ENG=Amount];
                                                   AutoFormatType=1 }
    { 21  ;   ;Quantity            ;Decimal       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Quantity;
                                                              ENG=Quantity];
                                                   DecimalPlaces=0:5 }
    { 22  ;   ;Total Cost          ;Decimal       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Total Cost;
                                                              ENG=Total Cost];
                                                   AutoFormatType=1 }
    { 23  ;   ;Total Price         ;Decimal       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Total Price;
                                                              ENG=Total Price];
                                                   AutoFormatType=1 }
    { 24  ;   ;Applies-to ID       ;Code50        ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Applies-to ID;
                                                              ENG=Applies-to ID] }
    { 25  ;   ;Gen. Bus. Posting Group;Code20     ;TableRelation="Gen. Business Posting Group";
                                                   DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              ENG=Gen. Bus. Posting Group] }
    { 26  ;   ;Gen. Prod. Posting Group;Code20    ;TableRelation="Gen. Product Posting Group";
                                                   DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              ENG=Gen. Prod. Posting Group] }
    { 27  ;   ;Additional-Currency Amount;Decimal ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Additional-Currency Amount;
                                                              ENG=Additional-Currency Amount];
                                                   AutoFormatType=1 }
    { 28  ;   ;Dimension Entry No. ;Integer       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Dimension Entry No.;
                                                              ENG=Dimension Entry No.] }
    { 29  ;   ;Variant Code        ;Code10        ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Variant Code;
                                                              ENG=Variant Code] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Dimension Set ID;
                                                              ENG=Dimension Set ID];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Job No.,Entry Type,Posting Group Type,No.,Variant Code,Posting Group,Gen. Bus. Posting Group,Gen. Prod. Posting Group,Unit of Measure Code,Work Type Code,Dimension Entry No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

