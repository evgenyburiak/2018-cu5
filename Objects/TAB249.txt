OBJECT Table 249 VAT Registration Log
{
  OBJECT-PROPERTIES
  {
    Date=22/02/18;
    Time=12:00:00;
    Version List=NAVW111.00.00.20783;
  }
  PROPERTIES
  {
    CaptionML=[ENU=VAT Registration Log;
               ENG=VAT Registration Log];
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;AutoIncrement=Yes;
                                                   CaptionML=[ENU=Entry No.;
                                                              ENG=Entry No.] }
    { 2   ;   ;VAT Registration No.;Text20        ;CaptionML=[ENU=VAT Registration No.;
                                                              ENG=VAT Registration No.];
                                                   NotBlank=Yes }
    { 3   ;   ;Account Type        ;Option        ;CaptionML=[ENU=Account Type;
                                                              ENG=Account Type];
                                                   OptionCaptionML=[ENU=Customer,Vendor,Contact,Company Information;
                                                                    ENG=Customer,Vendor,Contact,Company Information];
                                                   OptionString=Customer,Vendor,Contact,Company Information }
    { 4   ;   ;Account No.         ;Code20        ;TableRelation=IF (Account Type=CONST(Customer)) Customer
                                                                 ELSE IF (Account Type=CONST(Vendor)) Vendor;
                                                   CaptionML=[ENU=Account No.;
                                                              ENG=Account No.] }
    { 5   ;   ;Country/Region Code ;Code10        ;TableRelation=Country/Region.Code;
                                                   CaptionML=[ENU=Country/Region Code;
                                                              ENG=Country/Region Code];
                                                   NotBlank=Yes }
    { 6   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   DataClassification=EndUserIdentifiableInformation;
                                                   CaptionML=[ENU=User ID;
                                                              ENG=User ID] }
    { 10  ;   ;Status              ;Option        ;CaptionML=[ENU=Status;
                                                              ENG=Status];
                                                   OptionCaptionML=[ENU=Not Verified,Valid,Invalid;
                                                                    ENG=Not Verified,Valid,Invalid];
                                                   OptionString=Not Verified,Valid,Invalid }
    { 11  ;   ;Verified Name       ;Text150       ;CaptionML=[ENU=Verified Name;
                                                              ENG=Verified Name] }
    { 12  ;   ;Verified Address    ;Text150       ;CaptionML=[ENU=Verified Address;
                                                              ENG=Verified Address] }
    { 13  ;   ;Verified Date       ;DateTime      ;CaptionML=[ENU=Verified Date;
                                                              ENG=Verified Date] }
    { 14  ;   ;Request Identifier  ;Text200       ;CaptionML=[ENU=Request Identifier;
                                                              ENG=Request Identifier] }
    { 15  ;   ;Verified Street     ;Text50        ;CaptionML=[ENU=Verified Street;
                                                              ENG=Verified Street] }
    { 16  ;   ;Verified Postcode   ;Text20        ;CaptionML=[ENU=Verified Postcode;
                                                              ENG=Verified Postcode] }
    { 17  ;   ;Verified City       ;Text30        ;CaptionML=[ENU=Verified City;
                                                              ENG=Verified City] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;Brick               ;Country/Region Code,VAT Registration No.,Status }
  }
  CODE
  {

    [External]
    PROCEDURE GetCountryCode@2() : Code[10];
    VAR
      CompanyInformation@1000 : Record 79;
      CountryRegion@1001 : Record 9;
    BEGIN
      IF "Country/Region Code" = '' THEN BEGIN
        IF NOT CompanyInformation.GET THEN
          EXIT('');
        EXIT(CompanyInformation."Country/Region Code");
      END;
      CountryRegion.GET("Country/Region Code");
      IF CountryRegion."EU Country/Region Code" = '' THEN
        EXIT("Country/Region Code");
      EXIT(CountryRegion."EU Country/Region Code");
    END;

    [External]
    PROCEDURE GetVATRegNo@12() : Code[20];
    VAR
      VatRegNo@1000 : Code[20];
    BEGIN
      VatRegNo := UPPERCASE("VAT Registration No.");
      VatRegNo := DELCHR(VatRegNo,'=',DELCHR(VatRegNo,'=','ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'));
      IF STRPOS(VatRegNo,UPPERCASE(GetCountryCode)) = 1 THEN
        VatRegNo := DELSTR(VatRegNo,1,STRLEN(GetCountryCode));
      EXIT(VatRegNo);
    END;

    PROCEDURE InitVATRegLog@40(VAR VATRegistrationLog@1000 : Record 249;CountryCode@1001 : Code[10];AcountType@1002 : Option;AccountNo@1003 : Code[20];VATRegNo@1004 : Text[20]);
    BEGIN
      VATRegistrationLog.INIT;
      VATRegistrationLog."Account Type" := AcountType;
      VATRegistrationLog."Account No." := AccountNo;
      VATRegistrationLog."Country/Region Code" := CountryCode;
      VATRegistrationLog."VAT Registration No." := VATRegNo;
    END;

    BEGIN
    END.
  }
}

