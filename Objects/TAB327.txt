OBJECT Table 327 Tax Jurisdiction Translation
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Tax Jurisdiction Translation;
               ENG=VAT Jurisdiction Translation];
  }
  FIELDS
  {
    { 1   ;   ;Tax Jurisdiction Code;Code10       ;TableRelation="Tax Jurisdiction";
                                                   CaptionML=[ENU=Tax Jurisdiction Code;
                                                              ENG=VAT Jurisdiction Code];
                                                   NotBlank=Yes }
    { 2   ;   ;Language Code       ;Code10        ;TableRelation=Language;
                                                   CaptionML=[ENU=Language Code;
                                                              ENG=Language Code] }
    { 3   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              ENG=Description] }
  }
  KEYS
  {
    {    ;Tax Jurisdiction Code,Language Code     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

