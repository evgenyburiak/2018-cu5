OBJECT Table 5224 Payable Employee Ledger Entry
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Payable Employee Ledger Entry;
               ENG=Payable Employee Ledger Entry];
  }
  FIELDS
  {
    { 2   ;   ;Employee No.        ;Code20        ;TableRelation=Employee;
                                                   CaptionML=[ENU=Employee No.;
                                                              ENG=Employee No.] }
    { 3   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              ENG=Entry No.] }
    { 4   ;   ;Employee Ledg. Entry No.;Integer   ;TableRelation="Employee Ledger Entry";
                                                   CaptionML=[ENU=Employee Ledg. Entry No.;
                                                              ENG=Employee Ledg. Entry No.] }
    { 5   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              ENG=Amount];
                                                   AutoFormatType=1 }
    { 7   ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Currency Code;
                                                              ENG=Currency Code] }
    { 8   ;   ;Positive            ;Boolean       ;CaptionML=[ENU=Positive;
                                                              ENG=Positive] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

