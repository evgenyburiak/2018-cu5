OBJECT Table 5487 Balance Sheet Buffer
{
  OBJECT-PROPERTIES
  {
    Date=22/02/18;
    Time=12:00:00;
    Version List=NAVW111.00.00.20783;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Balance Sheet Buffer;
               ENG=Balance Sheet Buffer];
  }
  FIELDS
  {
    { 1   ;   ;Line No.            ;Integer       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Line No.;
                                                              ENG=Line No.] }
    { 2   ;   ;Description         ;Text250       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Description;
                                                              ENG=Description] }
    { 3   ;   ;Balance             ;Decimal       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Balance;
                                                              ENG=Balance] }
    { 4   ;   ;Date Filter         ;Date          ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Date Filter;
                                                              ENG=Date Filter] }
    { 6   ;   ;Line Type           ;Text30        ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Line Type;
                                                              ENG=Line Type] }
    { 7   ;   ;Indentation         ;Integer       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Indentation;
                                                              ENG=Indentation] }
  }
  KEYS
  {
    {    ;Line No.                                ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

