OBJECT Table 5509 Attachment Entity Buffer
{
  OBJECT-PROPERTIES
  {
    Date=22/02/18;
    Time=12:00:00;
    Version List=NAVW111.00.00.20783;
  }
  PROPERTIES
  {
    OnRename=BEGIN
               IF xRec.Id <> Id THEN
                 ERROR(CannotChangeIDErr);
             END;

    CaptionML=[ENU=Attachment Entity Buffer;
               ENG=Attachment Entity Buffer];
  }
  FIELDS
  {
    { 3   ;   ;Created Date-Time   ;DateTime      ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Created Date-Time;
                                                              ENG=Created Date-Time] }
    { 5   ;   ;File Name           ;Text250       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=File Name;
                                                              ENG=File Name] }
    { 6   ;   ;Type                ;Option        ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Type;
                                                              ENG=Type];
                                                   OptionCaptionML=[ENU=" ,Image,PDF,Word,Excel,PowerPoint,Email,XML,Other";
                                                                    ENG=" ,Image,PDF,Word,Excel,PowerPoint,Email,XML,Other"];
                                                   OptionString=[ ,Image,PDF,Word,Excel,PowerPoint,Email,XML,Other] }
    { 8   ;   ;Content             ;BLOB          ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Content;
                                                              ENG=Content];
                                                   SubType=Bitmap }
    { 8000;   ;Id                  ;GUID          ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Id;
                                                              ENG=Id] }
    { 8001;   ;Document Id         ;GUID          ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Document Id;
                                                              ENG=Document Id] }
    { 8002;   ;Byte Size           ;Integer       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Byte Size;
                                                              ENG=Byte Size] }
  }
  KEYS
  {
    {    ;Id                                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      CannotChangeIDErr@1000 : TextConst '@@@={Locked};ENU=The id cannot be changed.;ENG=The id cannot be changed.';

    PROCEDURE SetBinaryContent@17(BinaryContent@1002 : Text);
    VAR
      OutStream@1001 : OutStream;
    BEGIN
      Content.CREATEOUTSTREAM(OutStream);
      OutStream.WRITE(BinaryContent,STRLEN(BinaryContent));
    END;

    PROCEDURE SetTextContent@1(TextContent@1002 : Text);
    VAR
      OutStream@1001 : OutStream;
    BEGIN
      Content.CREATEOUTSTREAM(OutStream,TEXTENCODING::UTF8);
      OutStream.WRITE(TextContent,STRLEN(TextContent));
    END;

    BEGIN
    END.
  }
}

