OBJECT Table 5952 Resource Location
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Resource Location;
               ENG=Resource Location];
    LookupPageID=Page6015;
    DrillDownPageID=Page6015;
  }
  FIELDS
  {
    { 1   ;   ;Location Code       ;Code10        ;TableRelation=Location;
                                                   OnValidate=BEGIN
                                                                CALCFIELDS("Location Name");
                                                              END;

                                                   CaptionML=[ENU=Location Code;
                                                              ENG=Location Code] }
    { 2   ;   ;Location Name       ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Location.Name WHERE (Code=FIELD(Location Code)));
                                                   CaptionML=[ENU=Location Name;
                                                              ENG=Location Name];
                                                   Editable=No }
    { 3   ;   ;Resource No.        ;Code20        ;TableRelation=Resource;
                                                   OnValidate=BEGIN
                                                                CALCFIELDS("Resource Name");
                                                              END;

                                                   CaptionML=[ENU=Resource No.;
                                                              ENG=Resource No.] }
    { 4   ;   ;Resource Name       ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Resource.Name WHERE (No.=FIELD(Resource No.)));
                                                   CaptionML=[ENU=Resource Name;
                                                              ENG=Resource Name];
                                                   Editable=No }
    { 5   ;   ;Starting Date       ;Date          ;CaptionML=[ENU=Starting Date;
                                                              ENG=Starting Date] }
  }
  KEYS
  {
    {    ;Location Code,Resource No.,Starting Date;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

