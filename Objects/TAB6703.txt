OBJECT Table 6703 Booking Service
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               CheckCurrency;
             END;

    OnModify=BEGIN
               CheckCurrency;
             END;

    TableType=Exchange;
    ExternalName=BookingService;
    CaptionML=[ENU=Booking Service;
               ENG=Booking Service];
  }
  FIELDS
  {
    { 1   ;   ;Service ID          ;Text50        ;ExternalName=ServiceId;
                                                   CaptionML=[ENU=Service ID;
                                                              ENG=Service ID] }
    { 2   ;   ;Display Name        ;Text50        ;ExternalName=DisplayName;
                                                   CaptionML=[ENU=Display Name;
                                                              ENG=Display Name] }
    { 3   ;   ;Description         ;Text250       ;CaptionML=[ENU=Description;
                                                              ENG=Description] }
    { 4   ;   ;Price               ;Decimal       ;CaptionML=[ENU=Price;
                                                              ENG=Price] }
    { 6   ;   ;Internal Notes      ;Text250       ;ExternalName=InternalNotes;
                                                   CaptionML=[ENU=Internal Notes;
                                                              ENG=Internal Notes] }
    { 7   ;   ;Default Duration Minutes;Integer   ;InitValue=60;
                                                   ExternalName=DefaultDurationMinutes;
                                                   CaptionML=[ENU=Default Duration Minutes;
                                                              ENG=Default Duration Minutes] }
    { 8   ;   ;Default Email Reminder;Text250     ;ExternalName=DefaultEmailReminder;
                                                   CaptionML=[ENU=Default Email Reminder;
                                                              ENG=Default Email Reminder] }
    { 9   ;   ;Default Email Reminder Set;Boolean ;ExternalName=IsDefaultEmailReminderSet;
                                                   CaptionML=[ENU=Default Email Reminder Set;
                                                              ENG=Default Email Reminder Set] }
    { 10  ;   ;Default Email Reminder Minutes;Integer;
                                                   ExternalName=DefaultEmailReminderMinutes;
                                                   CaptionML=[ENU=Default Email Reminder Minutes;
                                                              ENG=Default Email Reminder Minutes] }
    { 14  ;   ;Pricing Type        ;Integer       ;InitValue=3;
                                                   ExternalName=PricingType;
                                                   CaptionML=[ENU=Pricing Type;
                                                              ENG=Pricing Type] }
    { 15  ;   ;Currency            ;Text10        ;CaptionML=[ENU=Currency;
                                                              ENG=Currency] }
    { 17  ;   ;Exclude From Self Service;Boolean  ;ExternalName=ExcludeFromSelfService;
                                                   CaptionML=[ENU=Exclude From Self Service;
                                                              ENG=Exclude From Self Service] }
    { 28  ;   ;Last Modified Time  ;DateTime      ;ExternalName=LastModifiedTime;
                                                   CaptionML=[ENU=Last Modified Time;
                                                              ENG=Last Modified Time] }
  }
  KEYS
  {
    {    ;Display Name                            ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    LOCAL PROCEDURE CheckCurrency@2();
    VAR
      GeneralLedgerSetup@1000 : Record 98;
    BEGIN
      IF Currency = '' THEN BEGIN
        GeneralLedgerSetup.GET;
        Currency := GeneralLedgerSetup."LCY Code";
      END;
    END;

    BEGIN
    END.
  }
}

