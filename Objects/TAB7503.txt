OBJECT Table 7503 Item Attr. Value Translation
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Item Attr. Value Translation;
               ENG=Item Attr. Value Translation];
  }
  FIELDS
  {
    { 1   ;   ;Attribute ID        ;Integer       ;CaptionML=[ENU=Attribute ID;
                                                              ENG=Attribute ID];
                                                   NotBlank=Yes }
    { 2   ;   ;ID                  ;Integer       ;CaptionML=[ENU=ID;
                                                              ENG=ID];
                                                   NotBlank=Yes }
    { 4   ;   ;Language Code       ;Code10        ;TableRelation=Language;
                                                   CaptionML=[ENU=Language Code;
                                                              ENG=Language Code];
                                                   NotBlank=Yes }
    { 5   ;   ;Name                ;Text250       ;CaptionML=[ENU=Name;
                                                              ENG=Name] }
  }
  KEYS
  {
    {    ;Attribute ID,ID,Language Code           ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

