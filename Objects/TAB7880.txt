OBJECT Table 7880 MS-QBD Setup
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=MS-QBD Setup;
               ENG=MS-QBD Setup];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[ENU=Primary Key;
                                                              ENG=Primary Key] }
    { 2   ;   ;Enabled             ;Boolean       ;CaptionML=[ENU=Enabled;
                                                              ENG=Enabled] }
    { 3   ;   ;Last Sent To        ;Text250       ;ExtendedDatatype=E-Mail;
                                                   CaptionML=[ENU=Last Sent To;
                                                              ENG=Last Sent To] }
    { 4   ;   ;LastEmailBodyPath   ;Text250       ;CaptionML=[ENU=LastEmailBodyPath;
                                                              ENG=LastEmailBodyPath] }
    { 5   ;   ;Last Sent CC        ;Text250       ;CaptionML=[ENU=Last Sent CC;
                                                              ENG=Last Sent CC] }
    { 6   ;   ;Last Sent BCC       ;Text250       ;CaptionML=[ENU=Last Sent BCC;
                                                              ENG=Last Sent BCC] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

