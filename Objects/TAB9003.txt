OBJECT Table 9003 User Group Permission Set
{
  OBJECT-PROPERTIES
  {
    Date=22/11/17;
    Time=12:00:00;
    Version List=NAVW111.00;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    OnInsert=BEGIN
               MarkUserGroupAsCustomized("User Group Code");
             END;

    OnModify=BEGIN
               MarkUserGroupAsCustomized("User Group Code");
             END;

    OnDelete=BEGIN
               MarkUserGroupAsCustomized("User Group Code");
             END;

    OnRename=BEGIN
               MarkUserGroupAsCustomized("User Group Code");
             END;

    CaptionML=[ENU=User Group Permission Set;
               ENG=User Group Permission Set];
  }
  FIELDS
  {
    { 1   ;   ;User Group Code     ;Code20        ;TableRelation="User Group";
                                                   CaptionML=[ENU=User Group Code;
                                                              ENG=User Group Code] }
    { 2   ;   ;Role ID             ;Code20        ;TableRelation="Permission Set";
                                                   CaptionML=[ENU=Role ID;
                                                              ENG=Role ID];
                                                   Editable=No }
    { 3   ;   ;User Group Name     ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("User Group".Name WHERE (Code=FIELD(User Group Code)));
                                                   CaptionML=[ENU=User Group Name;
                                                              ENG=User Group Name];
                                                   Editable=No }
    { 4   ;   ;Role Name           ;Text30        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Permission Set".Name WHERE (Role ID=FIELD(Role ID)));
                                                   CaptionML=[ENU=Role Name;
                                                              ENG=Role Name];
                                                   Editable=No }
    { 5   ;   ;App ID              ;GUID          ;CaptionML=[ENU=App ID;
                                                              ENG=App ID] }
    { 6   ;   ;Scope               ;Option        ;CaptionML=[ENU=Scope;
                                                              ENG=Scope];
                                                   OptionCaptionML=[ENU=System,Tenant;
                                                                    ENG=System,Tenant];
                                                   OptionString=System,Tenant }
    { 7   ;   ;Extension Name      ;Text30        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("NAV App".Name WHERE (ID=FIELD(App ID)));
                                                   CaptionML=[ENU=Extension Name;
                                                              ENG=Extension Name];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;User Group Code,Role ID,Scope,App ID    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    LOCAL PROCEDURE MarkUserGroupAsCustomized@1(UserGroupCode@1000 : Code[20]);
    VAR
      UserGroup@1001 : Record 9000;
    BEGIN
      IF NOT UserGroup.GET(UserGroupCode) THEN
        EXIT;

      UserGroup.Customized := TRUE;
      UserGroup.MODIFY;
    END;

    BEGIN
    END.
  }
}

