OBJECT Table 9154 My Job
{
  OBJECT-PROPERTIES
  {
    Date=22/02/18;
    Time=12:00:00;
    Version List=NAVW111.00.00.20783;
  }
  PROPERTIES
  {
    CaptionML=[ENU=My Job;
               ENG=My Job];
  }
  FIELDS
  {
    { 1   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   DataClassification=EndUserIdentifiableInformation;
                                                   CaptionML=[ENU=User ID;
                                                              ENG=User ID] }
    { 2   ;   ;Job No.             ;Code20        ;TableRelation=Job;
                                                   CaptionML=[ENU=Job No.;
                                                              ENG=Job No.];
                                                   NotBlank=Yes }
    { 3   ;   ;Exclude from Business Chart;Boolean;CaptionML=[ENU=Exclude from Business Chart;
                                                              ENG=Exclude from Business Chart] }
    { 4   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              ENG=Description] }
    { 5   ;   ;Status              ;Option        ;InitValue=Open;
                                                   CaptionML=[ENU=Status;
                                                              ENG=Status];
                                                   OptionCaptionML=[ENU=Planning,Quote,Open,Completed;
                                                                    ENG=Planning,Quote,Open,Completed];
                                                   OptionString=Planning,Quote,Open,Completed }
    { 6   ;   ;Bill-to Name        ;Text50        ;CaptionML=[ENU=Bill-to Name;
                                                              ENG=Bill-to Name] }
    { 7   ;   ;Percent Completed   ;Decimal       ;CaptionML=[ENU=Percent Completed;
                                                              ENG=Percent Completed] }
    { 8   ;   ;Percent Invoiced    ;Decimal       ;CaptionML=[ENU=Percent Invoiced;
                                                              ENG=Percent Invoiced] }
  }
  KEYS
  {
    {    ;User ID,Job No.                         ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

