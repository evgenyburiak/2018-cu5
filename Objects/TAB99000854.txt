OBJECT Table 99000854 Inventory Profile Track Buffer
{
  OBJECT-PROPERTIES
  {
    Date=22/02/18;
    Time=12:00:00;
    Version List=NAVW111.00.00.20783;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Inventory Profile Track Buffer;
               ENG=Inventory Profile Track Buffer];
  }
  FIELDS
  {
    { 1   ;   ;Line No.            ;Integer       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Line No.;
                                                              ENG=Line No.] }
    { 2   ;   ;Priority            ;Integer       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Priority;
                                                              ENG=Priority] }
    { 3   ;   ;Demand Line No.     ;Integer       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Demand Line No.;
                                                              ENG=Demand Line No.] }
    { 4   ;   ;Sequence No.        ;Integer       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Sequence No.;
                                                              ENG=Sequence No.] }
    { 21  ;   ;Source Type         ;Integer       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Source Type;
                                                              ENG=Source Type] }
    { 23  ;   ;Source ID           ;Code20        ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Source ID;
                                                              ENG=Source ID] }
    { 72  ;   ;Quantity Tracked    ;Decimal       ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Quantity Tracked;
                                                              ENG=Quantity Tracked] }
    { 73  ;   ;Surplus Type        ;Option        ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Surplus Type;
                                                              ENG=Surplus Type];
                                                   OptionCaptionML=[ENU=None,Forecast,BlanketOrder,SafetyStock,ReorderPoint,MaxInventory,FixedOrderQty,MaxOrder,MinOrder,OrderMultiple,DampenerQty,PlanningFlexibility,Undefined,EmergencyOrder;
                                                                    ENG=None,Forecast,BlanketOrder,SafetyStock,ReorderPoint,MaxInventory,FixedOrderQty,MaxOrder,MinOrder,OrderMultiple,DampenerQty,PlanningFlexibility,Undefined,EmergencyOrder];
                                                   OptionString=None,Forecast,BlanketOrder,SafetyStock,ReorderPoint,MaxInventory,FixedOrderQty,MaxOrder,MinOrder,OrderMultiple,DampenerQty,PlanningFlexibility,Undefined,EmergencyOrder }
    { 75  ;   ;Warning Level       ;Option        ;DataClassification=SystemMetadata;
                                                   CaptionML=[ENU=Warning Level;
                                                              ENG=Warning Level];
                                                   OptionCaptionML=[ENU=,Emergency,Exception,Attention;
                                                                    ENG=,Emergency,Exception,Attention];
                                                   OptionString=,Emergency,Exception,Attention }
  }
  KEYS
  {
    {    ;Line No.,Priority,Demand Line No.,Sequence No.;
                                                   SumIndexFields=Quantity Tracked;
                                                   MaintainSIFTIndex=No;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

